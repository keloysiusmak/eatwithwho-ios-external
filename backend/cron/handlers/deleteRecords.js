'use strict'

const db = require('../database/connectDatabase')
const Budget = (process.env.IS_TEST) ? require('../../defaults/budget') : require('../models/budget')
const Permission = (process.env.IS_TEST) ? require('../../defaults/permission') : require('../models/permission')
const Record = (process.env.IS_TEST) ? require('../../defaults/record') : require('../models/record')

const FUNC_NAME = '[cron/deleteRecords]'
const outputHelper = require('../helpers/outputHelper')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false

  const now = Math.floor(Date.now() / 1000)

  db.connect().then(() => {
    return Record.find({
      isDeleted: true,
      $or: [
        { deletedAt: {
          $lt: now - 7 * 24 * 60 * 60
        } }, {
          deletedAt: 0
        }, {
          deletedAt: null
        }]
    })
  }).then((result) => {
    const records = result.map(record => {
      return JSON.parse(JSON.stringify(record._id))
    })

    const promises = []
    records.forEach(recordId => {
      const budgetFilter = {
        records: recordId
      }

      const budgetNewFields = {
        $pull: { records: recordId }
      }

      const updateBudget = Budget.findOneAndUpdate(budgetFilter, budgetNewFields)

      const permissionFilter = {
        'permission.record': recordId
      }
      const permissionNewFields = {
        $pull: { records: recordId }
      }
      const updatePermission = Permission.findOneAndUpdate(permissionFilter, permissionNewFields)

      const recordFilter = {
        _id: recordId
      }

      const deleteRecord = Record.deleteOne(recordFilter)
      promises.push(updateBudget, updatePermission, deleteRecord)
      console.log('[cron/deleteRecords] - [recordId ' + recordId + '] - Deleting expired record (' + recordId + ')')
    })

    return Promise.all(promises)
  }).then((result) => {
    const statusMessage = 'Successfully deleted records'
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 200, statusMessage)
  }).catch((error) => {
    const statusMessage = 'Error encountered when deleting records: ' + error
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 500, statusMessage)
  })
}
