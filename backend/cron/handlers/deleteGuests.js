'use strict'

const db = require('../database/connectDatabase')
const Couple = (process.env.IS_TEST) ? require('../../defaults/couple') : require('../models/couple')
const Guest = (process.env.IS_TEST) ? require('../../defaults/guest') : require('../models/guest')
const Permission = (process.env.IS_TEST) ? require('../../defaults/permission') : require('../models/permission')

const FUNC_NAME = '[cron/deleteGuests]'
const outputHelper = require('../helpers/outputHelper')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false

  const now = Math.floor(Date.now() / 1000)

  db.connect().then(() => {
    return Guest.find({
      isDeleted: true,
      $or: [
        { deletedAt: {
          $lt: now - 7 * 24 * 60 * 60
        } }, {
          deletedAt: 0
        }, {
          deletedAt: null
        }]
    })
  }).then((result) => {
    const guests = result.map(guest => {
      return JSON.parse(JSON.stringify(guest._id))
    })

    const promises = []
    guests.forEach(guestId => {
      const coupleFilter = {
        guests: guestId
      }

      const coupleNewFields = {
        $pull: { guests: guestId }
      }

      const updateCouple = Couple.findOneAndUpdate(coupleFilter, coupleNewFields)

      const permissionFilter = {
        'permission.guest': guestId
      }
      const permissionNewFields = {
        $pull: { guest: guestId }
      }
      const updatePermission = Permission.findOneAndUpdate(permissionFilter, permissionNewFields)

      const permissionDelFilter = {
        'guestId': guestId
      }
      const updatePermission2 = Permission.deleteOne(permissionDelFilter)

      const guestFilter = {
        _id: guestId
      }

      const deleteGuest = Guest.deleteOne(guestFilter)
      promises.push(updateCouple, updatePermission, updatePermission2, deleteGuest)
      console.log('[cron/deleteGuests] - [guestId ' + guestId + '] - Deleting expired guest (' + guestId + ')')
    })

    return Promise.all(promises)
  }).then((result) => {
    const statusMessage = 'Successfully deleted guests'
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 200, statusMessage)
  }).catch((error) => {
    const statusMessage = 'Error encountered when deleting guests: ' + error
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 500, statusMessage)
  })
}
