'use strict'

const AWS = require('aws-sdk')
const s3 = new AWS.S3()
const db = require('../database/connectDatabase')
const Activity = (process.env.IS_TEST) ? require('../../defaults/eatwith') : require('../models/eatwith')
const Couple = (process.env.IS_TEST) ? require('../../defaults/couple') : require('../models/couple')
const File = (process.env.IS_TEST) ? require('../../defaults/file') : require('../models/file')
const Permission = (process.env.IS_TEST) ? require('../../defaults/permission') : require('../models/permission')
const Schedule = (process.env.IS_TEST) ? require('../../defaults/schedule') : require('../models/schedule')

const FUNC_NAME = '[cron/deleteActivities]'
const outputHelper = require('../helpers/outputHelper')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false

  const now = Math.floor(Date.now() / 1000)

  db.connect().then(() => {
    const populateFilters = {
      path: 'files',
      model: File
    }
    return Activity.find({
      isDeleted: true,
      $or: [
        { deletedAt: {
          $lt: now - 7 * 24 * 60 * 60
        } }, {
          deletedAt: 0
        }, {
          deletedAt: null
        }]
    }).populate(populateFilters)
  }).then((result) => {
    const activities = result

    const outPromises = []
    activities.forEach(eatwith => {
      outPromises.push(
        Schedule.findOne({ activities: JSON.parse(JSON.stringify(eatwith._id)) }).then((result) => {
          const promises = []
          const schedule = result
          const eatwithId = eatwith._id

          const scheduleFilter = {
            activities: eatwithId
          }

          const scheduleNewFields = {
            $pull: { activities: eatwithId }
          }

          const updateSchedule = Schedule.findOneAndUpdate(scheduleFilter, scheduleNewFields)

          const permissionFilter = {
            'permission.eatwith': eatwithId
          }
          const permissionNewFields = {
            $pull: { activities: eatwithId }
          }
          const updatePermission = Permission.updateMany(permissionFilter, permissionNewFields)

          eatwith.files.forEach(file => {
            const now = Math.floor(Date.now() / 1000)
            const newFields = {
              $inc: {
                filesSize: -file.fileSize
              },
              modifiedAt: now
            }
            const updateCouple = Couple.findOneAndUpdate({ schedule: schedule._id }, newFields)
            promises.push(updateCouple)

            const params = {
              Bucket: 'eatwithwho-uploads-' + process.env.NODE_ENV,
              Key: JSON.parse(JSON.stringify(eatwith._id)) + '/' + file.fileName
            }

            const s3Promise = new Promise((resolve, reject) => {
              s3.deleteObject(params, function (err, data) {
                if (err) {
                  let error = {
                    statusCode: 500
                  }
                  throw error
                }
                resolve()
              })
            })

            const permissionFilter = {
              'permissions.file': JSON.parse(JSON.stringify(file._id))
            }

            const permissionNewFields = {
              $pull: { 'permissions.file': JSON.parse(JSON.stringify(file._id)) }
            }
            promises.push(Permission.updateMany(permissionFilter, permissionNewFields))

            const scheduleFilter = {
              'activities': JSON.parse(JSON.stringify(eatwith._id))
            }

            const scheduleNewFields = {
              $pull: { 'activities': JSON.parse(JSON.stringify(eatwith._id)) }
            }
            promises.push(Schedule.findOneAndUpdate(scheduleFilter, scheduleNewFields))
            promises.push(s3Promise)
          })

          promises.push(updateSchedule)
          promises.push(updatePermission)
          const eatwithFilter = {
            _id: eatwith._id
          }

          const deleteActivity = Activity.deleteOne(eatwithFilter)
          promises.push(deleteActivity)
          console.log('[cron/deleteActivities] - [eatwithId ' + eatwithId + '] - Deleting expired eatwith (' + eatwithId + ')')
          return Promise.all(promises)
        })
      )
    })

    return Promise.all(outPromises)
  }).then((result) => {
    const statusMessage = 'Successfully deleted activities'
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 200, statusMessage)
  }).catch((error) => {
    const statusMessage = 'Error encountered when deleting activities: ' + error
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 500, statusMessage)
  })
}
