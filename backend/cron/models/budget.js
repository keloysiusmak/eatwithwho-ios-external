'use strict'

const defaults = require('../lib/defaults')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const currencies = defaults.currencies

const budgetSchema = new Schema({
  initialBudget: {
    type: Number,
    required: true,
    default: 0
  },
  guestlistBudget: {
    type: Number,
    required: true,
    default: 0
  },
  currency: {
    type: String,
    enum: currencies,
    required: true,
    default: 'SGD'
  },
  payments: {
    type: Array,
    items: {
      type: Schema.Types.ObjectId,
      ref: 'Payment'
    },
    default: []
  },
  records: {
    type: Array,
    items: {
      type: Schema.Types.ObjectId,
      ref: 'record'
    }
  },
  createdAt: {
    type: Number,
    required: true
  },
  modifiedAt: {
    type: Number,
    required: true
  }
})

module.exports.model = mongoose.model('Budget', budgetSchema)
module.exports.permittedFields = ['guestlistBudget', 'currency']
module.exports.selectFilters = 'guestlistBudget modifiedAt _id'
