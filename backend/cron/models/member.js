'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const guestSchema = new Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  reference: {
    type: String,
    required: true,
    unique: true,
    default: function () {
      var text = ''
      var possible = 'abcdefghijklmnopqrstuvwxyz'

      for (var i = 0; i < 6; i++) { text += possible.charAt(Math.floor(Math.random() * possible.length)) }

      return text
    }
  },
  gender: {
    type: String,
    enum: ['male', 'female'],
    required: true
  },
  number: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  imageType: {
    type: String,
    default: 'none',
    required: true,
    enum: ['none', 'jpg', 'png', 'jpeg']
  },
  isDeleted: {
    type: Boolean,
    required: true,
    default: false
  },
  deletedAt: {
    type: Number,
    required: true,
    default: 0
  },
  createdAt: {
    type: Number,
    required: true
  },
  modifiedAt: {
    type: Number,
    required: true
  }
})

module.exports.model = mongoose.model('Guest', guestSchema)
