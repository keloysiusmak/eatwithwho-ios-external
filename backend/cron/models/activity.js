'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const eatwithSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  category: {
    type: String,
    enum: ['photography', 'videography', 'banquet', 'recce', 'makeup', 'travel', 'others'],
    default: 'others',
    required: true
  },
  desc: {
    type: String
  },
  startTime: {
    type: Number,
    required: true
  },
  endTime: {
    type: Number,
    required: true
  },
  isDeleted: {
    type: Boolean,
    required: true,
    default: false
  },
  files: {
    type: Array,
    items: {
      type: Schema.Types.ObjectId,
      ref: 'file'
    },
    default: []
  },
  location: {
    type: String,
    default: ''
  },
  geolocation: {
    type: String,
    default: ''
  },
  createdAt: {
    type: Number,
    required: true
  },
  modifiedAt: {
    type: Number,
    required: true
  }
}, { collection: 'activities' })

module.exports.model = mongoose.model('Activity', eatwithSchema)
