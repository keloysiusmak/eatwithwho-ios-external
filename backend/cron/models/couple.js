'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const coupleSchema = new Schema({
  schedule: {
    type: Schema.Types.ObjectId,
    ref: 'schedule'
  },
  budget: {
    type: Schema.Types.ObjectId,
    ref: 'budget',
    default: null
  },
  guestlist: {
    type: Schema.Types.ObjectId,
    ref: 'guestlist',
    default: null
  },
  checklist: {
    type: Schema.Types.ObjectId,
    ref: 'checklist',
    default: null
  },
  notebook: {
    type: Schema.Types.ObjectId,
    ref: 'notebook',
    default: null
  },
  subscription: {
    type: String,
    required: true,
    enum: ['free'],
    default: 'free'
  },
  filesSize: {
    type: Number,
    required: true,
    default: '0'
  },
  hashtag: {
    type: String,
    default: '',
    maxlength: 60
  },
  eatwithDay: {
    type: Number,
    default: 0
  },
  plan: {
    type: String,
    enum: ['free', 'paid-1'],
    default: 'free'
  },
  eatwithTitle: {
    type: String,
    enum: ['mr', 'mrs', 'ms', 'dr', null],
    default: null
  },
  eatwithFirstName: {
    type: String,
    default: null,
    maxlength: 30,
    trim: true
  },
  eatwithLastName: {
    type: String,
    default: null,
    maxlength: 30,
    trim: true
  },
  venue: {
    type: String,
    default: null,
    maxlength: 120,
    trim: true
  },
  createdAt: {
    type: Number,
    required: true,
    default: Math.floor(Date.now() / 1000)
  },
  modifiedAt: {
    type: Number,
    required: true,
    default: Math.floor(Date.now() / 1000)
  }
})

module.exports.model = mongoose.model('Couple', coupleSchema)
module.exports.permittedFields = ['eatwithDay', 'hashtag', 'eatwithTitle', 'eatwithFirstName', 'eatwithLastName', 'venue']
module.exports.selectFilters = 'checklist guestlist budget notebook subscription eatwithDay hashtag eatwithTitle eatwithFirstName eatwithLastName plan venue _id modifiedAt'
module.exports.checklistSelectFilters = 'checklist'
module.exports.guestlistSelectFilters = 'guestlist'
module.exports.budgetSelectFilters = 'budget'
module.exports.notebookSelectFilters = 'notebook'
