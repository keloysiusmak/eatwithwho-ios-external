'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const scheduleSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  date: {
    type: Number,
    required: true
  },
  activities: {
    type: Array,
    items: {
      type: Schema.Types.ObjectId,
      ref: 'eatwith',
      required: true
    }
  },
  createdAt: {
    type: Number,
    required: true
  },
  modifiedAt: {
    type: Number,
    required: true
  }
})

module.exports.model = mongoose.model('Schedule', scheduleSchema)
