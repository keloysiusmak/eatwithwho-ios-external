'use strict'

module.exports.countryCodes = ['65', '60']
module.exports.currencies = ['SGD', 'USD', 'SEK', 'MYR', 'THB', 'IDR']
module.exports.languages = ['en', 'cn']
module.exports.quotationCategories = ['photography', 'videography', 'makeup']
module.exports.quotationStatuses = ['rejected', 'pending', 'requested-for-quotation', 'accepted', 'meeting-scheduled']
module.exports.titles = ['mr', 'mrs', 'ms', 'dr']
module.exports.toTitleCase = function (str) {
    return (str) ? str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }) : str
}
module.exports.plans = {
    'free': {
        invites: 100
    }
}
