const outputHelper = require('./outputHelper')
const todo = (process.env.IS_TEST) ? require('../../defaults/todo') : require('../models/todo')

function isValidAccount (Account, isCreate, account) {
  let errors = new Account(account).validateSync()
  let error = (errors && !isCreate) ? Object.keys(account).filter(key => {
    return Object.keys(errors.errors).filter(errorKey => errorKey.includes(key)).length > 0
  }).length > 0 : errors
  if (error) {
    return false
  } else if (account.password && (account.password.length < 6 || account.password.length > 60)) {
    return false
  } else {
    return true
  }
}

function isValidUsername(username) {
  return username.length <= 30 && username.length >= 6
}

module.exports = {
  isValidAccount,
  isValidUsername
}
