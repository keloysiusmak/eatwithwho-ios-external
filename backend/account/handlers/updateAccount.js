'use strict'

const db = require('../database/connectDatabase')
const Account = (process.env.IS_TEST) ? require('../../defaults/account') : require('../models/account')

const FUNC_NAME = '[account/updateAccount]'
const accountHelper = require('../helpers/accountHelper')
const outputHelper = require('../helpers/outputHelper')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  const accountId = event.pathParameters.id
  const fieldsToUpdate = event.body

  let parsedFieldsToUpdate

  db.connect().then(() => {
    if (!accountId || !fieldsToUpdate || !outputHelper.isJson(fieldsToUpdate) || !outputHelper.isValidId(accountId)) {
      let error = {
        statusCode: 400
      }
      throw error
    }

    parsedFieldsToUpdate = JSON.parse(fieldsToUpdate)

    const permittedFields = Account.permittedFields

    const newFields = {
      modifiedAt: Math.floor(Date.now() / 1000)
    }

    permittedFields.forEach((field) => {
      if ((field in parsedFieldsToUpdate)) {
        newFields[field] = parsedFieldsToUpdate[field]
      }
    })

    const filter = {
      _id: accountId
    }

    if (!accountHelper.isValidAccount(Account.model, false, newFields)) {
      let error = {
        statusCode: 400,
        subStatusCode: 1
      }
      throw error
    }

    return Account.model.findOneAndUpdate(filter, newFields, { new: true }).select(Account.selectFilters)
  }).then((result) => {
    if (!result) {
      let error = {
        statusCode: 404
      }
      throw error
    }

    const account = result

    const statusMessage = 'Successfully updated account'
    const output = { account }
    return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 200, statusMessage, output)
  }).catch((error) => {
    if (typeof error !== 'string' && error.statusCode && error.statusCode === 400 && error.subStatusCode && error.subStatusCode === 1) {
      const statusMessage = 'Failed to update account as fields were invalid'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 400) {
      const statusMessage = 'Failed to update account as account id or fields was not provided'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 404) {
      const statusMessage = 'Failed to update account as account with given account id does not exist'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 404, statusMessage)
    } else {
      const statusMessage = 'Error encountered when updating account: ' + error
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 500, statusMessage)
    }
  })
}
