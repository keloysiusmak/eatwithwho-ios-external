'use strict'

const bcrypt = require('bcryptjs')

const constants = require('../lib/constants')

const FUNC_NAME = '[account/resetPassword]'
const accountHelper = require('../helpers/accountHelper')
const outputHelper = require('../helpers/outputHelper')

const db = require('../database/connectDatabase')
const Account = (process.env.IS_TEST) ? require('../../defaults/account') : require('../models/account')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  const accountId = event.pathParameters.id
  const passwords = event.body
  let newPassword

  db.connect().then(() => {
    if (!passwords || !outputHelper.isJson(passwords) || !accountId) {
      let error = {
        statusCode: 400
      }
      throw error
    }
    const parsedPasswords = JSON.parse(passwords)
    newPassword = parsedPasswords.password

    if (!newPassword) {
      let error = {
        statusCode: 400
      }
      throw error
    } else if (newPassword.length < 6) {
      let error = {
        statusCode: 400,
        subStatusCode: 1
      }
      throw error
    }

    return bcrypt.hash(newPassword, constants.SALT_ROUNDS)
  }).then((result) => {
    const newPassword = result

    const filter = {
      username: accountId
    }

    const newFields = {
      modifiedAt: Math.floor(Date.now() / 1000),
      password: newPassword
    }

    return Account.model.findOneAndUpdate(filter, newFields, { new: true }).select(Account.selectFilters)
  }).then((result) => {
    if (!result) {
      let error = {
        statusCode: 404
      }
      throw error
    }

    const account = result

    const statusMessage = 'Successfully reset password'

    const output = { account }
    return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 200, statusMessage, output)
  }).catch((error) => {
    if (typeof error !== 'string' && error.statusCode && error.statusCode === 400 && error.subStatusCode && error.subStatusCode === 1) {
      const statusMessage = 'Failed to reset password as fields were invalid'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 400) {
      const statusMessage = 'Failed to reset password as accountId or new password was not provided'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 404) {
      const statusMessage = 'Failed to reset password as account with given account id does not exist'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 404, statusMessage)
    } else {
      const statusMessage = 'Error encountered when resetting password: ' + error
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 500, statusMessage)
    }
  })
}
