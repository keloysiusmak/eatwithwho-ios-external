'use strict'

const bcrypt = require('bcryptjs')

const aws = require('aws-sdk')
const ses = new aws.SES({region: 'ap-southeast-2'})

const constants = require('../lib/constants')

const db = require('../database/connectDatabase')
const Account = (process.env.IS_TEST) ? require('../../defaults/account') : require('../models/account')
const Couple = (process.env.IS_TEST) ? require('../../defaults/couple') : require('../models/couple')
const Permission = (process.env.IS_TEST) ? require('../../defaults/permission') : require('../models/permission')

const FUNC_NAME = '[account/registerAccount]'
const accountHelper = require('../helpers/accountHelper')
const outputHelper = require('../helpers/outputHelper')

const defaults = require('../lib/defaults')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  const type = event.pathParameters.type
  const fieldsToAdd = event.body
  const now = Math.floor(Date.now() / 1000)

  db.connect().then(() => {
    if (!fieldsToAdd || !outputHelper.isJson(fieldsToAdd)) {
      let error = {
        statusCode: 400
      }
      throw error
    }

    let filter = {
      $or: [
        {username: JSON.parse(fieldsToAdd).username},
        {email: JSON.parse(fieldsToAdd).email}
      ]
    }

    return Account.model.findOne(filter)
  }).then(result => {
    if (result) {
      let error = {
        statusCode: 409
      }
      throw error
    }
    let parsedFieldsToAdd = JSON.parse(fieldsToAdd)

    if (!accountHelper.isValidAccount(Account.model, true, parsedFieldsToAdd)) {
      let error = {
        statusCode: 400,
        subStatusCode: 1
      }
      throw error
    }
    const permittedFields = Account.permittedFields
    Object.keys(parsedFieldsToAdd).filter(key => {
      if ((!(permittedFields.includes(key))) || (parsedFieldsToAdd[key] === null)) {
        delete parsedFieldsToAdd[key]
      }
    })
    parsedFieldsToAdd.type = type

    return Promise.all([bcrypt.hash(parsedFieldsToAdd.password, constants.SALT_ROUNDS), parsedFieldsToAdd])
  }).then((result) => {
    let saltedPassword = result[0]
    let parsedFieldsToAdd = result[1]

    parsedFieldsToAdd.password = saltedPassword

    let promises = [parsedFieldsToAdd]

    if (type === "couple") {
      promises.push(Couple.model.create({
        eatwithDay: now
      }))
    }
    return Promise.all(promises)
  }).then((result) => {
    const parsedFieldsToAdd = result[0]
    parsedFieldsToAdd.typeId = result[1]._id

    parsedFieldsToAdd.firstName = defaults.toTitleCase(parsedFieldsToAdd.firstName)
    parsedFieldsToAdd.lastName = defaults.toTitleCase(parsedFieldsToAdd.lastName)

    return Account.model.create(parsedFieldsToAdd)
  }).then((result) => {
    const account = result

    const promises = [account]

    const couplePermissions = (type === "couple") ? [JSON.parse(JSON.stringify(account.typeId))] : []

    promises.push(Permission.model.create({
      accountId: account._id,
      permissions: {
        account: [JSON.parse(JSON.stringify(account._id))],
        couple: couplePermissions,
        budget: [],
        schedule: [],
        guest: [],
        eatwith: []
      },
      modifiedAt: now,
      createdAt: now
    }))

    return Promise.all(promises)
  }).then((result) => {
    const account = result[0];

    let outputAccount = {}

    Account.selectFilters.split(' ').forEach(key => {
      outputAccount[key] = account[key]
    })

    const params = {
      Destination: {
        ToAddresses: ["sius_max@hotmail.com"]
      },
      Message: {
        Body: {
          Html: { 
            Data: "<html>Hi " + account.firstName + ",<br/><br/>Your account has been all set up! Let's start making eatwiths smarter.<br/><br/>With Love,<br/>eatwithwho</html>"
          }
        },
        Subject: { 
          Data: "Welcome to eatwithwho!"
        }
      },
      Source: "no-reply@keloysiusmak.com"
    }

    if (!(process.env.IS_TEST)) {
      ses.sendEmail(params, function (err, data) {
        if (err) {
          let error = {
            statusCode: 502
          }
          throw error
        }
      })
    }

    const statusMessage = 'Successfully registered account'
    const output = { account: outputAccount }
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 200, statusMessage, output)
  }).catch((error) => {
    if (typeof error !== 'string' && error.statusCode && error.statusCode === 409) {
      const statusMessage = 'Failed to register account as there exists an account with conflicting username or email'
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 409, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 400 && error.subStatusCode && error.subStatusCode === 1) {
      const statusMessage = 'Failed to register account as fields were invalid'
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 400) {
      const statusMessage = 'Failed to register account as fields were missing'
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 502) {
      const statusMessage = 'Failed to send add guest as register email failed to send'
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 502, statusMessage)
    } else {
      const statusMessage = 'Error encountered when registering account: ' + error
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 500, statusMessage)
    }
  })
}
