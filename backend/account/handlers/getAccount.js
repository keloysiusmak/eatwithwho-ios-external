'use strict'

const db = require('../database/connectDatabase')
const Account = (process.env.IS_TEST) ? require('../../defaults/account') : require('../models/account')

const FUNC_NAME = '[account/getPassword]'
const accountHelper = require('../helpers/accountHelper')
const outputHelper = require('../helpers/outputHelper')


module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  const accountId = event.pathParameters.id

  db.connect().then(() => {
    if (!accountId || !outputHelper.isValidId(accountId)) {
      let error = {
        statusCode: 400
      }
      throw error
    }

    const filter = {
      _id: accountId
    }

    return Account.model.findOne(filter).select(Account.selectFilters)
  }).then((result) => {
    if (!result) {
      let error = {
        statusCode: 404
      }
      throw error
    }

    const account = result

    const statusMessage = 'Successfully get account'
    const output = { account }
    return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 200, statusMessage, output)
  }).catch((error) => {
    if (typeof error !== 'string' && error.statusCode && error.statusCode === 400) {
      const statusMessage = 'Failed to get account as account id was not provided'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 404) {
      const statusMessage = 'Failed to get account as account with given account id does not exist'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 404, statusMessage)
    } else {
      const statusMessage = 'Error encountered when getting account: ' + error
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 500, statusMessage)
    }
  })
}
