'use strict'

const bcrypt = require('bcryptjs')

const constants = require('../lib/constants')

const FUNC_NAME = '[account/changePassword]'
const accountHelper = require('../helpers/accountHelper')
const outputHelper = require('../helpers/outputHelper')

const db = require('../database/connectDatabase')
const Account = (process.env.IS_TEST) ? require('../../defaults/account') : require('../models/account')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  const accountId = event.pathParameters.id
  const passwords = event.body
  let oldPassword
  let newPassword

  db.connect().then(() => {
    if (!passwords || !outputHelper.isJson(passwords) || !accountId || !outputHelper.isValidId(accountId)) {
      let error = {
        statusCode: 400
      }
      throw error
    }
    const parsedPasswords = JSON.parse(passwords)
    oldPassword = parsedPasswords.oldPassword
    newPassword = parsedPasswords.newPassword

    if (!oldPassword || !newPassword) {
      let error = {
        statusCode: 400
      }
      throw error
    } else if (newPassword.length < 6) {
      let error = {
        statusCode: 400,
        subStatusCode: 1
      }
      throw error
    }

    const filter = {
      _id: accountId
    }

    return Account.model.findOne(filter)
  }).then((result) => {
    if (!result) {
      let error = {
        statusCode: 404
      }
      throw error
    }
    const account = result
    const comparePasswordsResult = bcrypt.compare(oldPassword, account.password)
    const newPasswordResult = bcrypt.hash(newPassword, constants.SALT_ROUNDS)

    return Promise.all([comparePasswordsResult, newPasswordResult])
  }).then((result) => {
    const comparePasswordsResult = result[0]
    const newPassword = result[1]

    if (!comparePasswordsResult) {
      let error = {
        statusCode: 401
      }
      throw error
    }

    const filter = {
      _id: accountId
    }

    const newFields = {
      modifiedAt: Math.floor(Date.now() / 1000),
      password: newPassword
    }

    return Account.model.findOneAndUpdate(filter, newFields, { new: true }).select(Account.selectFilters)
  }).then((result) => {
    const account = result

    const statusMessage = 'Successfully changed password'

    const output = { account }
    return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 200, statusMessage, output)
  }).catch((error) => {
    if (typeof error !== 'string' && error.statusCode && error.statusCode === 400 && error.subStatusCode && error.subStatusCode === 1) {
      const statusMessage = 'Failed to change password as fields were invalid'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 400) {
      const statusMessage = 'Failed to change password as accountId, old password or new password was not provided'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 401) {
      const statusMessage = 'Failed to change password as the old password was wrong'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 401, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 404) {
      const statusMessage = 'Failed to change password as account with given account id does not exist'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 404, statusMessage)
    } else {
      const statusMessage = 'Error encountered when changing password: ' + error
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 500, statusMessage)
    }
  })
}
