'use strict'

const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const constants = require('../lib/constants')

const db = require('../database/connectDatabase')
const Account = (process.env.IS_TEST) ? require('../../defaults/account') : require('../models/account')
const Guest = (process.env.IS_TEST) ? require('../../defaults/guest') : require('../models/guest')

const FUNC_NAME = '[account/loginAccount]'
const accountHelper = require('../helpers/accountHelper')
const outputHelper = require('../helpers/outputHelper')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  let body
  let username
  let password
  let token
  let notificationArn
  const now = Math.floor(Date.now() / 1000)

  db.connect().then(() => {
    if (!event.body || !outputHelper.isJson(event.body)) {
      let error = {
        statusCode: 400
      }
      throw error
    }
    body = JSON.parse(event.body)
    username = body.username
    password = body.password

    if (!username || !password) {
      let error = {
        statusCode: 400
      }
      throw error
    }

    const filter = {
      username: username
    }

    return Account.model.findOne(filter)
  }).then((result) => {
    if (!result) {
      let error = {
        statusCode: 404
      }
      throw error
    }
    const account = result

    const permissions = {
      type: account.type,
      tokenId: account._id
    }

    const accessToken =
    jwt.sign({ tokenType: 'access', permissions: permissions }, process.env.JWT_ACCESS_TOKEN_SECRET, { expiresIn: constants.ACCESS_TOKEN_EXPIRY })

    const refreshToken =
    jwt.sign({ tokenType: 'refresh', permissions: permissions, tokenSignedAt: now }, process.env.JWT_REFRESH_TOKEN_SECRET)

    return Promise.all([bcrypt.compare(password, account.password), accessToken, refreshToken])
  }).then((resolutionResult) => {
    if (!resolutionResult[0]) {
      let error = {
        statusCode: 401
      }
      throw error
    }

    const accessToken = resolutionResult[1]
    const refreshToken = resolutionResult[2]

    const filter = {
      username: username
    }

    const newFields = {
      refreshToken: refreshToken
    }

    return Promise.all([Account.model.findOneAndUpdate(filter, newFields, { new: true }).select(Account.selectFilters), accessToken, refreshToken])
  }).then((result) => {
    const account = result[0]
    const accessToken = result[1]
    const refreshToken = result[2]

    const statusMessage = 'Successfully logged in account'
    const output = { accessToken: accessToken, refreshToken: refreshToken, account }
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 200, statusMessage, output)
  }).catch((error) => {
    if (typeof error !== 'string' && error.statusCode && error.statusCode === 400) {
      const statusMessage = 'Failed to login as username, password, token or notification arn was not provided'
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && (error.statusCode === 404 || error.statusCode === 401)) {
      const statusMessage = 'Failed to login as username or password was wrong'
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 401, statusMessage)
    } else {
      const statusMessage = 'Error encountered when logging in: ' + error
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 500, statusMessage)
    }
  })
}
