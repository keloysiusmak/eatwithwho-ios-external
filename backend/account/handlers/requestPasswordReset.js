'use strict'

const jwt = require('jsonwebtoken')

const aws = require('aws-sdk')
const ses = new aws.SES({region: 'ap-southeast-2'})

const constants = require('../lib/constants')

const db = require('../database/connectDatabase')

const FUNC_NAME = '[account/requestPasswordReset]'
const accountHelper = require('../helpers/accountHelper')
const outputHelper = require('../helpers/outputHelper')

const Account = (process.env.IS_TEST) ? require('../../defaults/account') : require('../models/account')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  const fieldsToUpdate = event.body

  db.connect().then(() => {
    if (!fieldsToUpdate || !outputHelper.isJson(fieldsToUpdate)) {
      let error = {
        statusCode: 400
      }
      throw error
    }

    let parsedFieldsToUpdate = JSON.parse(fieldsToUpdate)

    if (!parsedFieldsToUpdate.username) {
      let error = {
        statusCode: 400
      }
      throw error
    }

    const filter = {
      username: parsedFieldsToUpdate.username
    }

    return Account.model.findOne(filter).select('username email type firstName _id')
  }).then((result) => {
    let account = result

    if (account) {

      const permissions = {
        type: account.type,
        username: account.username
      }

      const requestPasswordResetToken =
      jwt.sign({ tokenType: 'reset', permissions: permissions }, process.env.JWT_RESET_PASSWORD_SECRET, { expiresIn: constants.RESET_PASSWORD_EXPIRY })
      
      const params = {
        Destination: {
          ToAddresses: ["sius_max@hotmail.com"]
        },
        Message: {
          Body: {
            Html: { 
              Data: "<html>Hey " + account.firstName + ",<br/><br/>You recently requested for a password reset.<br/><br/>Click on the following link to reset your password: <a href=\"http://localhost:8080/#/reset/" + account.username + "?token=" + requestPasswordResetToken + "\">" + "http://localhost:8080/#/reset/" + account.username + "?token=" + requestPasswordResetToken + "</a><br/><br/>With Love,<br/>eatwithwho</html>"
            }
          },
          Subject: { 
            Data: "Password Reset Request"
          }
        },
        Source: "no-reply@keloysiusmak.com"
      }

      if (!(process.env.IS_TEST)) {
        ses.sendEmail(params, function (err, data) {
          if (err) {
            let error = {
              statusCode: 502
            }
            throw error
          }
        })
      }
    }

    const statusMessage = 'Successfully request password reset'
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 200, statusMessage)
  }).catch((error) => {
    if (typeof error !== 'string' && error.statusCode && error.statusCode === 400) {
      const statusMessage = 'Failed to request for password reset as username was not provided'
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 502) {
      const statusMessage = 'Failed to request for password reset as verification email failed to send'
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 502, statusMessage)
    } else {
      const statusMessage = 'Error encountered when requesting for password reset: ' + error
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 500, statusMessage)
    }
  })
}
