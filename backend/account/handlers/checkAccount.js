'use strict'

const db = require('../database/connectDatabase')
const Account = (process.env.IS_TEST) ? require('../../defaults/account') : require('../models/account')

const FUNC_NAME = '[account/checkAccount]'
const accountHelper = require('../helpers/accountHelper')
const outputHelper = require('../helpers/outputHelper')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false

  db.connect().then(() => {
    if (!event.body || !outputHelper.isJson(event.body)) {
      let error = {
        statusCode: 400
      }
      throw error
    }

    let body = JSON.parse(event.body)

    if (!body.username) {
      let error = {
        statusCode: 400
      }
      throw error
    } else if (!accountHelper.isValidUsername(body.username)) {
      let error = {
        statusCode: 400,
        subStatusCode: 1
      }
      throw error
    }

    return Account.model.find({
      username: body.username
    })
  }).then((result) => {
    let output = {
      'validUsername': (result.length == 0)
    }
    const statusMessage = 'Successfully checked account'
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 200, statusMessage, output)
  }).catch((error) => {
    if (typeof error !== 'string' && error.statusCode && error.statusCode === 400 && error.subStatusCode && error.subStatusCode === 1) {
      const statusMessage = 'Failed to check account as fields were invalid'
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 400) {
      const statusMessage = 'Failed to check account as fields were missing'
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 400, statusMessage)
    } else {
      const statusMessage = 'Error encountered when checking account: ' + error
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 500, statusMessage)
    }
  })
}
