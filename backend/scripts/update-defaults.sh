cp -v defaults/outputHelper.js account/helpers/outputHelper.js
cp -v defaults/outputHelper.js eatwith/helpers/outputHelper.js
cp -v defaults/outputHelper.js advertisement/helpers/outputHelper.js
cp -v defaults/outputHelper.js auth/helpers/outputHelper.js
cp -v defaults/outputHelper.js budget/helpers/outputHelper.js
cp -v defaults/outputHelper.js checklist/helpers/outputHelper.js
cp -v defaults/outputHelper.js comment/helpers/outputHelper.js
cp -v defaults/outputHelper.js couple/helpers/outputHelper.js
cp -v defaults/outputHelper.js cron/helpers/outputHelper.js
cp -v defaults/outputHelper.js file/helpers/outputHelper.js
cp -v defaults/outputHelper.js guest/helpers/outputHelper.js
cp -v defaults/outputHelper.js guestlist/helpers/outputHelper.js
cp -v defaults/outputHelper.js internal/helpers/outputHelper.js
cp -v defaults/outputHelper.js mail/helpers/outputHelper.js
cp -v defaults/outputHelper.js notebook/helpers/outputHelper.js
cp -v defaults/outputHelper.js notification/helpers/outputHelper.js
cp -v defaults/outputHelper.js payment/helpers/outputHelper.js
cp -v defaults/outputHelper.js quotation/helpers/outputHelper.js
cp -v defaults/outputHelper.js record/helpers/outputHelper.js
cp -v defaults/outputHelper.js schedule/helpers/outputHelper.js
cp -v defaults/outputHelper.js table/helpers/outputHelper.js
cp -v defaults/outputHelper.js todo/helpers/outputHelper.js
cp -v defaults/outputHelper.js validation/helpers/outputHelper.js
cp -v defaults/outputHelper.js vendor/helpers/outputHelper.js

cp -v lib/defaults.js account/lib/defaults.js
cp -v lib/defaults.js eatwith/lib/defaults.js
cp -v lib/defaults.js auth/lib/defaults.js
cp -v lib/defaults.js comment/lib/defaults.js
cp -v lib/defaults.js budget/lib/defaults.js
cp -v lib/defaults.js couple/lib/defaults.js
cp -v lib/defaults.js guest/lib/defaults.js
cp -v lib/defaults.js guestlist/lib/defaults.js
cp -v lib/defaults.js mail/lib/defaults.js
cp -v lib/defaults.js notebook/lib/defaults.js
cp -v lib/defaults.js notification/lib/defaults.js
cp -v lib/defaults.js quotation/lib/defaults.js
cp -v lib/defaults.js record/lib/defaults.js
cp -v lib/defaults.js schedule/lib/defaults.js
cp -v lib/defaults.js table/lib/defaults.js
cp -v lib/defaults.js validation/lib/defaults.js
cp -v lib/defaults.js vendor/lib/defaults.js

cd account/helpers && chmod -R 777 ./ &
cd eatwith/helpers && chmod -R 777 ./ &
cd advertisement/helpers && chmod -R 777 ./ &
cd auth/helpers && chmod -R 777 ./ &
cd budget/helpers && chmod -R 777 ./ &
cd checklist/helpers && chmod -R 777 ./ &
cd comment/helpers && chmod -R 777 ./ &
cd couple/helpers && chmod -R 777 ./ &
cd cron/helpers && chmod -R 777 ./ &
cd file/helpers && chmod -R 777 ./ &
cd guest/helpers && chmod -R 777 ./ &
cd guestlist/helpers && chmod -R 777 ./ &
cd internal/helpers && chmod -R 777 ./ &
cd mail/helpers && chmod -R 777 ./ &
cd notebook/helpers && chmod -R 777 ./ &
cd notification/helpers && chmod -R 777 ./ &
cd payment/helpers && chmod -R 777 ./ &
cd record/helpers && chmod -R 777 ./ &
cd schedule/helpers && chmod -R 777 ./ &
cd table/helpers && chmod -R 777 ./ &
cd todo/helpers && chmod -R 777 ./ &
cd validation/helpers && chmod -R 777 ./ &
cd vendor/helpers && chmod -R 777 ./ &

cd budget/lib && chmod -R 777 ./ &
