cp -v defaults/eatwith.js account/models/eatwith.js
cp -v defaults/eatwith.js eatwith/models/eatwith.js
cp -v defaults/eatwith.js cron/models/eatwith.js
cp -v defaults/eatwith.js schedule/models/eatwith.js
cp -v defaults/eatwith.js validation/models/eatwith.js

cp -v defaults/advertisement.js advertisement/models/advertisement.js

cp -v defaults/file.js account/models/file.js
cp -v defaults/file.js eatwith/models/file.js
cp -v defaults/file.js cron/models/file.js
cp -v defaults/file.js file/models/file.js
cp -v defaults/file.js schedule/models/file.js
cp -v defaults/file.js validation/models/file.js

cp -v defaults/internal.js internal/models/internal.js

cp -v defaults/mail.js couple/models/mail.js
cp -v defaults/mail.js mail/models/mail.js

cp -v defaults/permission.js eatwith/models/permission.js
cp -v defaults/permission.js checklist/models/permission.js
cp -v defaults/permission.js cron/models/permission.js
cp -v defaults/permission.js todo/models/permission.js
cp -v defaults/permission.js validation/models/permission.js

cp -v defaults/schedule.js account/models/schedule.js
cp -v defaults/schedule.js eatwith/models/schedule.js
cp -v defaults/schedule.js file/models/schedule.js
cp -v defaults/schedule.js schedule/models/schedule.js
cp -v defaults/schedule.js validation/models/schedule.js

cp -v defaults/todo.js account/models/todo.js
cp -v defaults/todo.js eatwith/models/todo.js
cp -v defaults/todo.js comment/models/todo.js
cp -v defaults/todo.js schedule/models/todo.js
cp -v defaults/todo.js todo/models/todo.js
cp -v defaults/todo.js validation/models/todo.js

cd eatwith/models && chmod -R 777 ./ &
cd advertisement/models && chmod -R 777 ./ &
cd auth/models && chmod -R 777 ./ &
cd budget/models && chmod -R 777 ./ &
cd cron/models && chmod -R 777 ./ &
cd file/models && chmod -R 777 ./ &
cd notification/models && chmod -R 777 ./ &
cd schedule/models && chmod -R 777 ./ &
cd todo/models && chmod -R 777 ./ &
cd validation/models && chmod -R 777 ./ &
