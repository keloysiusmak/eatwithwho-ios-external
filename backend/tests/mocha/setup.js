const mongoose = require('mongoose')
const Account = require('../../defaults/account')
const Activity = require('../../defaults/eatwith')
const Advertisement = require('../../defaults/advertisement')
const Budget = require('../../defaults/budget')
const Checklist = require('../../defaults/checklist')
const Couple = require('../../defaults/couple')
const File = require('../../defaults/file')
const Guest = require('../../defaults/guest')
const Guestlist = require('../../defaults/guestlist')
const Mail = require('../../defaults/mail')
const Notebook = require('../../defaults/notebook')
const Notification = require('../../defaults/notification')
const Payment = require('../../defaults/payment')
const Quotation = require('../../defaults/quotation')
const Record = require('../../defaults/record')
const Schedule = require('../../defaults/schedule')
const Table = require('../../defaults/table')
const todo = require('../../defaults/todo')
const Vendor = require('../../defaults/vendor')

const mms = require('mongodb-memory-server');
const mongoServer = new mms.MongoMemoryServer();

mongoose.Promise = Promise;

mongoServer.getConnectionString().then(testMongoUrl => {
  process.env.MONGO_DB = testMongoUrl
  let promises = []

  mongoose.connect(testMongoUrl, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true }).then(() => {
    promises.push(Notification.model.create({
      _id: '4c252f67ce278c93918ea222',
      type: 1,
      recipients: [
        '4bc764e34d95ee70b93cedd6'
      ],
      params: ['rand'],
      createdAt: 1545940839,
      modifiedAt: 1545940839
    }))
    promises.push(Vendor.model.create({
      _id : '4c7e3e2e6343394236b7ff7b',
      name : "Title",
      desc : 'Body',
      websiteUrl: 'abcdef',
      imageUrls: [
        'url1'
      ],
      email: 'a@b.com',
      countryCode: '65',
      number: '12345667',
      createdAt : 1551777326,
      modifiedAt : 1551777326
    }))
    promises.push(Mail.model.create({
      _id : '4c7e3e2e6343394236b7ff7b',
      title : "Title",
      body : 'Body',
      recipients: [
        '4bc764e34d95ee70b93cedd6'
      ],
      sender: '4bc764e34d95ee70b93cedd6',
      read: [],
      starred: [],
      createdAt : 1551777326,
      modifiedAt : 1551777326
    }))
    promises.push(Advertisement.model.create({
      _id : '4cabef02fd3a85d77e891084',
      title : 'Test Title',
      desc : 'Test Desc.',
      imageUrl : 'http://zhkphoto.com/assets/photos/david-abigail_25.jpg',
      targetUrl : 'http://zhkphoto.com',
      createdAt : 1554771634,
      modifiedAt : 1554771634
    }))
    promises.push(Account.model.create({
      _id: '4bc764e34d95ee70b93cedd6',
      username: 'aaaaaa',
      password: '$2a$10$A1J8XrbozmuAaK5Tn0IecefZf3vcOm6LVvDbNZdoFNHOyS6FbZziO',
      refreshToken: null,
      type: 'couple',
      typeId: '4bbce6c22bb510278220de7a',
      email: 'a@b.com',
      number: 91234567,
      title: 'mr',
      firstName: 'User',
      lastName: 'Test',
      modifiedAt: 1554003684,
      createdAt: 1539106498,
      validRefreshToken: 1554002924
    }))

    promises.push(Guestlist.model.create({
      _id: '4bbce6c22bb510278220fe7a',
      guests: [
        '4bbce6c22bb510278220de72',
        '4bbce6c22bb510278220de73',
        '4bbce6c22bb510278220de75',
        '4bc764e34d95ee70b93cedd6'
      ],
      tables: [
        '4bbce6c22bb510278220de6a'
      ],
      maxGuests: 100,
      linkSharing: true,
      modifiedAt: 1554003772,
      createdAt: 1539106498
    }))

    promises.push(Table.model.create({
      _id: '4bbce6c22bb510278220de6a',
      number: 1,
      name: 'Ab',
      size: 10,
      guests: ['4bbce6c22bb510278220de73'],
      modifiedAt: 1554003772,
      createdAt: 1539106498
    }))

    promises.push(Table.model.create({
      _id: '4bbce6c22bb510278220de6b',
      number: 2,
      name: 'Abc',
      size: 10,
      guests: ['4bbce6c22bb510278220de72'],
      modifiedAt: 1554003772,
      createdAt: 1539106498
    }))

    promises.push(Table.model.create({
      _id: '4bbce6c22bb510278220de6d',
      number : 3,
      name: 'Abd',
      size: 10,
      guests: ['4bbce6c22bb510278220de75'],
      modifiedAt: 1554003772,
      createdAt: 1539106498
    }))

    promises.push(Checklist.model.create({
      _id: '4bbce6c22bb510279220fe7a',
      todos: [
        '4bbce89ae620a3278eb4828a'
      ],
      modifiedAt: 1554003772,
      createdAt: 1539106498
    }))

    promises.push(Notebook.model.create({
      _id: '4bfce6c22bb510279220fe7a',
      quotations: [
        '4bbce89ae620a3278eb4829a'
      ],
      modifiedAt: 1554003772,
      createdAt: 1539106498
    }))

    promises.push(Couple.model.create({
      _id: '4bbce6c22bb510278220de7a',
      guestlist: '4bbce6c22bb510278220fe7a',
      checklist: '4bbce6c22bb510279220fe7a',
      schedule: '4bbce6c22bb510278220de75',
      budget: '4bbce6c22bb510278220de74',
      notebook: '4bfce6c22bb510279220fe7a',
      filesSize: 14,
      hashtag: 'abcdef',
      eatwithTitle: 'mr',
      eatwithFirstName: 'Ab',
      eatwithLastName: 'Cd',
      venue: 'abcdef',
      modifiedAt: 1554003772,
      createdAt: 1539106498
    }))

    promises.push(Couple.model.create({
      _id: '4bbce6c22bb510278220de70',
      filesSize: 0,
      modifiedAt: 1554003772,
      createdAt: 1539106498
    }))

    promises.push(Budget.model.create({
      _id: '4bbce6c22bb510278220de74',
      currency: 'SGD',
      records: [
        '4bbce7f726d5ad2782a95e4f'
      ],
      initialBudget: 500,
      modifiedAt: 1554003938,
      createdAt: 1539106498,
      payments: [
        '4c8f27cb90c04c9f1449bab1'
      ]
    }))

    promises.push(Record.model.create({
      _id: '4bbce7f726d5ad2782a95e4f',
      type: 'expenditure',
      isDeleted: false,
      name: 'RecordName',
      value: 15,
      date: 1541354530,
      modifiedAt: 1554003944,
      createdAt: 1539106807,
      category: 'videography',
      deletedAt: 0,
      payment: null,
      note: 'Abc',
      isPaid: false,
      dueDate: 4
    }))

    promises.push(Payment.model.create({
      _id: '4c8f27cb90c04c9f1449bab1',
      type: 'creditcard',
      isDirectory: false,
      name: 'CardName',
      desc: 'CardDesc',
      modifiedAt: 1552885707,
      createdAt: 1552885707
    }))

    promises.push(Payment.model.create({
      _id: '4c8f27cb90c04c9f1449bab2',
      type: 'creditcard',
      isDirectory: true,
      name: 'CardName',
      desc: 'CardDesc',
      modifiedAt: 1552885707,
      createdAt: 1552885707
    }))

    promises.push(Schedule.model.create({
      _id: '4bbce6c22bb510278220de75',
      activities: [
        '4bbce88f4eed3b278e7cc7f9'
      ],
      date: 1564873205,
      modifiedAt: 1553744032,
      createdAt: 1539106498
    }))

    promises.push(Activity.model.create({
      _id: '4bbce88f4eed3b278e7cc7f9',
      type: 'others',
      isDeleted: false,
      assignedGuests: [],
      files: [
        '4c7e3e2e6343394236b7ff7b'
      ],
      name: 'ActivityName',
      desc: 'ActivityDesc',
      startTime: 1596297800,
      endTime: 1596297801,
      modifiedAt: 1554003860,
      createdAt: 1539106959,
      tag: '',
      location: ''
    }))

    promises.push(todo.model.create({
      _id: '4bbce89ae620a3278eb4828a',
      name: 'todoName',
      status: 'completed',
      modifiedAt: 1551915066,
      createdAt: 1539106970
    }))

    promises.push(Quotation.model.create({
      _id: '4bbce89ae620a3278eb4829a',
      name: 'Test',
      category: 'photography',
      pointOfContact: 'abc',
      number: 99999,
      countryCode: '65',
      email: 'a@b.com',
      links: [],
      modifiedAt: 1551915066,
      createdAt: 1539106970
    }))

    promises.push(Guest.model.create({
      _id: '4bbce6c22bb510278220de72',
      title: 'mr', 
      firstName: 'ValidName', 
      lastName: 'ValidName', 
      email: 'ta@tb.com', 
      number: 12345678, 
      status: 'attending', 
      note: 'test', 
      addressLine1: 'Singapore', 
      addressLine2: 'Singapore', 
      dietaryPreference: 'test', 
      countryCode: '65', 
      postalCode: 999999, 
      additionalGuests: [{
        firstName: 'test', 
        lastName: 'test', 
        title: 'mr', 
        dietaryPreference: 'food', 
        babyChair: true
      }]
    }))

    promises.push(Guest.model.create({
      _id: '4bbce6c22bb510278220de73',
      title: 'mr', 
      firstName: 'ValidName', 
      lastName: 'ValidName', 
      email: 'ta@tb.com', 
      number: 12345678, 
      status: 'attending', 
      note: 'test', 
      addressLine1: 'Singapore', 
      addressLine2: 'Singapore', 
      dietaryPreference: 'test', 
      countryCode: '65', 
      postalCode: 999999, 
      additionalGuests: [{
        firstName: 'test', 
        lastName: 'test', 
        title: 'mr', 
        dietaryPreference: 'food', 
        babyChair: true
      }]
    }))

    promises.push(Guest.model.create({
      _id: '4bbce6c22bb510278220de75',
      title: 'mr', 
      firstName: 'ValidName', 
      lastName: 'ValidName', 
      email: 'ta@tb.com', 
      number: 12345678, 
      status: 'pending', 
      note: 'test', 
      addressLine1: 'Singapore', 
      addressLine2: 'Singapore', 
      dietaryPreference: 'test', 
      countryCode: '65', 
      postalCode: 999999, 
      additionalGuests: [{
        firstName: 'test', 
        lastName: 'test', 
        title: 'mr', 
        dietaryPreference: 'food', 
        babyChair: true
      }]
    }))

    promises.push(Guest.model.create({ 
      _id: '4bc764e34d95ee70b93cedd6',
      title: 'mr', 
      firstName: 'ValidName', 
      lastName: 'ValidName', 
      email: 'ta@tb.com', 
      number: 12345678, 
      status: 'attending', 
      note: 'test', 
      addressLine1: 'Singapore', 
      addressLine2: 'Singapore', 
      dietaryPreference: 'test', 
      countryCode: '65', 
      postalCode: 999999, 
      additionalGuests: [{
        firstName: 'test', 
        lastName: 'test', 
        title: 'mr', 
        dietaryPreference: 'food', 
        babyChair: true
      }]
    }))
    return Promise.all(promises).then(() => run())
  })
})