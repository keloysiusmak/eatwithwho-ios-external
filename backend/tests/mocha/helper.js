const mongoose = require('mongoose')

const Account = require('../../defaults/account')
const Activity = require('../../defaults/eatwith')
const Advertisement = require('../../defaults/advertisement')
const Budget = require('../../defaults/budget')
const Checklist = require('../../defaults/checklist')
const Couple = require('../../defaults/couple')
const File = require('../../defaults/file')
const Guest = require('../../defaults/guest')
const Guestlist = require('../../defaults/guestlist')
const Mail = require('../../defaults/mail')
const Notification = require('../../defaults/notification')
const Notebook = require('../../defaults/notebook')
const Payment = require('../../defaults/payment')
const Quotation = require('../../defaults/quotation')
const Record = require('../../defaults/record')
const Schedule = require('../../defaults/schedule')
const Table = require('../../defaults/table')
const todo = require('../../defaults/todo')
const Vendor = require('../../defaults/vendor')

function isModelType(model, obj) {
  return model.selectFilters.split(' ').filter(key => {
    return obj[key] == null
  }).length == 0
}

function isAccount(obj) {
  return isModelType(Account, obj)
}

function isAdvertisement(obj) {
  return isModelType(Advertisement, obj) || Advertisement.lightSelectFilters.split(' ').filter(key => {
    return obj[key] == null
  }).length == 0
}

function isBudget(obj) {
  return isModelType(Budget, obj)
}
function isChecklist(obj) {
  return isModelType(Checklist, obj)
}

function isCouple(obj) {
  return isModelType(Couple, obj)
}

function isGuest(obj) {
  return isModelType(Guest, obj)
}

function isGuestlist(obj) {
  return isModelType(Guestlist, obj)
}

function isMail(obj) {
  return isModelType(Mail, obj)
}

function isNotebook(obj) {
  return isModelType(Notebook, obj)
}

function isNotification(obj) {
  return isModelType(Notification, obj)
}

function isQuotation(obj) {
  return isModelType(Quotation, obj)
}

function isTable(obj) {
  return isModelType(Table, obj)
}

function istodo(obj) {
  return isModelType(todo, obj)
}

function isValidId (str) {
  return mongoose.Types.ObjectId.isValid(str)
}

function isVendor(obj) {
  return isModelType(Vendor, obj)
}

module.exports = {
  isAccount,
  isAdvertisement,
  isBudget,
  isChecklist,
  isCouple,
  isGuest,
  isGuestlist,
  isMail,
  isNotebook,
  isNotification,
  isQuotation,
  isTable,
  istodo,
  isValidId,
  isVendor
}
