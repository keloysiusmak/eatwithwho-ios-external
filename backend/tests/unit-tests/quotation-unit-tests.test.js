var expect = require('chai').expect
var helper = require('../mocha/helper')

let quotationId = '4bbce89ae620a3278eb4829a'
let notebookId = '4bfce6c22bb510279220fe7a'

let validPayload

beforeEach(function(done) {
  validPayload = { 
    name: 'Vendor Name', 
    category: 'photography', 
    note: 'Note', 
    price: 10, 
    email: 'a@b.com',
    countryCode: '65',
    number: 123456,
    pointOfContact: 'Name',
    status: 'rejected',
    links: ['http://www.google.com']
   }
  done()
})

describe('getQuotations', () => {
  const getQuotations = require('../../quotation/handlers/getQuotations').main

  it('should return 200 and an array of quotation objects for a valid input', (done) => {
    let event = { pathParameters: { id: notebookId } }
    let context = {}
    return getQuotations(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(result.data.quotations.length).to.equal(1)
        result.data.quotations.forEach(quotation => {
          console.log(quotation)
          expect(helper.isQuotation(quotation)).to.equal(true)
        })
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid notebook id', (done) => {
    let event = { pathParameters: { id: notebookId.substr(0, notebookId.length - 1) + 'x' } }
    let context = {}
    return getQuotations(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent notebook id', (done) => {
    let event = { pathParameters: { id: notebookId.substr(0, notebookId.length - 1) + 'c' } }
    let context = {}
    return getQuotations(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('createQuotation', () => {
  const createQuotation = require('../../quotation/handlers/createQuotation').main

  it('should return 200 and a quotation object for a valid input', (done) => {
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isQuotation(result.data.quotation)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent notebook id', (done) => {
    let event = { pathParameters: { id: notebookId.substr(0, notebookId.length - 1) + 'b' }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid notebook id', (done) => {
    let event = { pathParameters: { id: notebookId.substr(0, notebookId.length - 1) + 'x' }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for missing input', (done) => {
    let event = { pathParameters: { id: notebookId } }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing name', (done) => {
    delete validPayload.name
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid name', (done) => {
    validPayload.name = 'V'
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid note', (done) => {
    validPayload.note = 'a'.repeat(801)
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing category', (done) => {
    delete validPayload.category
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid category', (done) => {
    validPayload.category = 'invalid'
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid price (string)', (done) => {
    validPayload.price = 'A'
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid price (less than 0)', (done) => {
    validPayload.price = -1
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid price (more than 1000000)', (done) => {
    validPayload.price = 10000000
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid email (number)', (done) => {
    validPayload.email = 123
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid email', (done) => {
    validPayload.email = 'a'
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid number (length longer than 10)', (done) => {
    validPayload.number = 99999999999
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid point of contact (length shorter than 2)', (done) => {
    validPayload.pointOfContact = 'a'
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid point of contact (length longer than 60)', (done) => {
    validPayload.pointOfContact = 'a'.repeat(161)
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid status (number)', (done) => {
    validPayload.status = 1
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid status', (done) => {
    validPayload.status = 'invalid'
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid links (number)', (done) => {
    validPayload.links = [123]
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid links (string)', (done) => {
    validPayload.links = ['abc']
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid links (invalid array)', (done) => {
    validPayload.links = [1, 'http://www.google.com']
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid links (duplicates)', (done) => {
    validPayload.links = ['http://www.google.com', 'http://www.google.com']
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('updateQuotation', () => {
  const updateQuotation = require('../../quotation/handlers/updateQuotation').main

  it('should return 200 and a quotation object for a valid input', (done) => {
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)
        expect(result.statusCode).to.equal(200)
        expect(helper.isQuotation(result.data.quotation)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid quotation id', (done) => {
    let event = { pathParameters: { id: quotationId.substr(0, quotationId.length - 1) + 'x' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for an non-existent quotation id', (done) => {
    let event = { pathParameters: { id: quotationId.substr(0, quotationId.length - 1) + 'b' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for missing input', (done) => {
    let event = { pathParameters: { id: quotationId } }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid name', (done) => {
    validPayload.name = 'a'
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid note', (done) => {
    validPayload.note = 'v'.repeat(801)
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid category', (done) => {
    validPayload.category = 'invalid'
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid price (string)', (done) => {
    validPayload.price = 'a'
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid price (less than 0)', (done) => {
    validPayload.price = -1
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid price (more than 1000000)', (done) => {
    validPayload.price = 99999999999
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid email (number)', (done) => {
    validPayload.email = 9
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid email', (done) => {
    validPayload.email = 'abc'
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid number (length longer than 10)', (done) => {
    validPayload.number = 99999999999
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid point of contact (length shorter than 2)', (done) => {
    validPayload.pointOfContact = 'a'
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid point of contact (length longer than 60)', (done) => {
    validPayload.pointOfContact = 'a'.repeat(161)
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid status (number)', (done) => {
    validPayload.status = 1
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid status', (done) => {
    validPayload.status = 'abc'
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid links (number)', (done) => {
    validPayload.links = [1]
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid links (string)', (done) => {
    validPayload.links = ['abc']
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid links (invalid array)', (done) => {
    validPayload.links = [1, 'http://www.google.com']
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid links (array contains duplicates)', (done) => {
    validPayload.links = ['http://www.google.com', 'http://www.google.com']
    let event = { pathParameters: { id: quotationId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('deleteQuotation', () => {
  const deleteQuotation = require('../../quotation/handlers/deleteQuotation').main

  it('should return 200 and quotation object for a valid input', (done) => {
    let event = { pathParameters: { id: quotationId } }
    let context = {}
    return deleteQuotation(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(200)
        expect(helper.isQuotation(result.data.quotation)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid quotation id', (done) => {
    let event = { pathParameters: { id: quotationId.substr(0, quotationId.length - 1) + 'x' } }
    let context = {}
    return deleteQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent quotation id', (done) => {
    let event = { pathParameters: { id: quotationId.substr(0, quotationId.length - 1) + 'b' } }
    let context = {}
    return deleteQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 200 and a quotation object for a valid input when quotation is deleted', (done) => {
    let event = { pathParameters: { id: quotationId } }
    let context = {}
    return deleteQuotation(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isQuotation(result.data.quotation)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('restoreQuotation', () => {
  const restoreQuotation = require('../../quotation/handlers/restoreQuotation').main

  it('should return 200 and quotation object for a valid input', (done) => {
    let event = { pathParameters: { id: quotationId } }
    let context = {}
    return restoreQuotation(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(200)
        expect(helper.isQuotation(result.data.quotation)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid quotation id', (done) => {
    let event = { pathParameters: { id: quotationId.substr(0, quotationId.length - 1) + 'x' } }
    let context = {}
    return restoreQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent quotation id', (done) => {
    let event = { pathParameters: { id: quotationId.substr(0, quotationId.length - 1) + 'b' } }
    let context = {}
    return restoreQuotation(event, context, (_, res) => {
      try {
        expect(JSON.parse(res.body).statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 200 and a quotation object for a valid input when quotation is not deleted', (done) => {
    let event = { pathParameters: { id: quotationId } }
    let context = {}
    return restoreQuotation(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isQuotation(result.data.quotation)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})
