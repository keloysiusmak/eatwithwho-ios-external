var expect = require('chai').expect
var helper = require('../mocha/helper')

let accountId = '4bc764e34d95ee70b93cedd6'
let mailId = '4c7e3e2e6343394236b7ff7b'

let validPayload

beforeEach(function(done) {
  validPayload = {
    read: '4bc764e34d95ee70b93cedd6',
    star: '4bc764e34d95ee70b93cedd6'
  }
  done()
})
describe('getMails', () => {
  const getMails = require('../../mail/handlers/getMails').main

  it('should return 200 for a valid input', (done) => {
    let event = { pathParameters: { id: accountId } }
    let context = {}
    return getMails(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(result.data.mails.length).to.equal(1)
        result.data.mails.forEach(mailObj => {
          expect(helper.isMail(mailObj)).to.equal(true)
        })
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid account id', (done) => {
    let event = { pathParameters: { id: accountId.substr(0, accountId.length - 1) + 'x' } }
    let context = {}
    return getMails(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('updateMail', () => {
  const updateMail = require('../../mail/handlers/updateMail').main

  it('should return 200 for a valid input and update starred and read', (done) => {
    let event = { pathParameters: { id: mailId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateMail(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isMail(result.data.mail)).to.equal(true)
        expect(result.data.mail.read).to.contain(accountId)
        expect(result.data.mail.starred).to.contain(accountId)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 200 for a valid input and remove from starred and read', (done) => {
    validPayload.unread = validPayload.read
    delete validPayload.read
    validPayload.unstar = validPayload.star
    delete validPayload.star

    let event = { pathParameters: { id: mailId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateMail(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isMail(result.data.mail)).to.equal(true)
        expect(result.data.mail.read).to.not.contain(accountId)
        expect(result.data.mail.starred).to.not.contain(accountId)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid mail id', (done) => {
    let event = { pathParameters: { id: mailId.substr(0, mailId.length - 1) + 'x' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateMail(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent mail id', (done) => {
    let event = { pathParameters: { id: mailId.substr(0, mailId.length - 1) + 'c' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateMail(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid user id', (done) => {
    validPayload.read = validPayload.read.substr(0, validPayload.read.length - 1) + 'x'
    let event = { pathParameters: { id: mailId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateMail(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})
