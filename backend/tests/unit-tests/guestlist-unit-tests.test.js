var expect = require('chai').expect
var helper = require('../mocha/helper')

let coupleId = '4bbce6c22bb510278220de70'
let guestlistId = '4bbce6c22bb510278220fe7a'

let validPayload

beforeEach(function(done) {
  validPayload = { 
    dietaryPreferences: ['food'],
    maxGuests: 100,
    linkSharing: true
  }
  done()
})

describe('createGuestlist', () => {
  const createGuestlist = require('../../guestlist/handlers/createGuestlist').main

  it('should return 200 and a guestlist object for a valid input', (done) => {
    let event = { pathParameters: { id: coupleId } }
    let context = {}
    return createGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isGuestlist(result.data.guestlist)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent couple id', (done) => {
    let event = { pathParameters: { id: coupleId.substr(0, coupleId.length - 1) + 'c' } }
    let context = {}
    return createGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid couple id', (done) => {
    let event = { pathParameters: { id: coupleId.substr(0, coupleId.length - 1) + 'x' } }
    let context = {}
    return createGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 409 for a couple with a guestlist', (done) => {
    let event = { pathParameters: { id: coupleId } }
    let context = {}
    return createGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(409)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('getGuestlist', () => {
  const getGuestlist = require('../../guestlist/handlers/getGuestlist').main

  it('should return 200 and a guestlist object for a valid input', (done) => {
    let event = { pathParameters: { id: guestlistId } }
    let context = {}
    return getGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isGuestlist(result.data.guestlist)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guestlist id', (done) => {
    let event = { pathParameters: { id: guestlistId.substr(0, guestlistId.length - 1) + 'x' } }
    let context = {}
    return getGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent guestlist id', (done) => {
    let event = { pathParameters: { id: guestlistId.substr(0, guestlistId.length - 1) + 'c' } }
    let context = {}
    return getGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('updateGuestlist', () => {
  const updateGuestlist = require('../../guestlist/handlers/updateGuestlist').main

  it('should return 200 and a guestlist object for a valid input', (done) => {
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for missing input', (done) => {
    let event = { pathParameters: { id: guestlistId } }
    let context = {}
    return updateGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guestlist id', (done) => {
    let event = { pathParameters: { id: guestlistId.substr(0, guestlistId.length - 1) + 'x' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent guestlist id', (done) => {
    let event = { pathParameters: { id: guestlistId.substr(0, guestlistId.length - 1) + 'c' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid maxGuests (string)', (done) => {
    validPayload.maxGuests = 'abc'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid maxGuests (less than 0)', (done) => {
    validPayload.maxGuests = -1
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid maxGuests (more than 1000)', (done) => {
    validPayload.maxGuests = 1001
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid dietaryPreferences (array contains duplicates)', (done) => {
    validPayload.dietaryPreferences = ['abc', 'abc', '123']
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid link sharing (string)', (done) => {
    validPayload.linkSharing = 'abc'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid link sharing (number)', (done) => {
    validPayload.linkSharing = 123
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuestlist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})