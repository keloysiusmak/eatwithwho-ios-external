var expect = require('chai').expect
var helper = require('../mocha/helper')

let coupleId = '4bbce6c22bb510278220de7a'

describe('getNotifications', () => {
  const getNotifications = require('../../notification/handlers/getNotifications').main

  it('should return 200 for a valid input', (done) => {
    let event = { pathParameters: { id: coupleId } }
    let context = {}
    return getNotifications(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        result.data.notifications.forEach(notificationObj => {
          expect(helper.isNotification(notificationObj)).to.equal(true)
        })
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid couple id', (done) => {
    let event = { pathParameters: { id: coupleId.substr(0, coupleId.length - 1) + 'x' } }
    let context = {}
    return getNotifications(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})