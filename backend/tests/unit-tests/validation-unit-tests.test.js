var expect = require('chai').expect
var helper = require('../mocha/helper')

describe('validatePermissions', () => {
  const validatePermissions = require('../../validation/handlers/validatePermissions').main

  it('Valid Test', (done) => {
    let event = {}
    let context = {}
    return validatePermissions(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })
})

describe('validateRelationships', () => {
  const validateRelationships = require('../../validation/handlers/validateRelationships').main

  it('Valid Test', (done) => {
    let event = {}
    let context = {}
    return validateRelationships(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })
})
