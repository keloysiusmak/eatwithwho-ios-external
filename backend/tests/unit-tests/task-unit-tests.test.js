var expect = require('chai').expect
var helper = require('../mocha/helper')

let todoId = '4bbce89ae620a3278eb4828a'
let checklistId = '4bbce6c22bb510279220fe7a'

let validPayload

beforeEach(function(done) {
  validPayload = { 
    name: 'ValidName', 
    status: 'completed' 
  }
  done()
})

describe('gettodos', () => {
  const gettodos = require('../../todo/handlers/gettodos').main

  it('should return 200 and an array of todo objects for a valid input', (done) => {
    let event = { pathParameters: { id: checklistId } }
    let context = {}
    return gettodos(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(result.data.todos.length).to.equal(1)
        result.data.todos.forEach(todo => {
          expect(helper.istodo(todo)).to.equal(true)
        })
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid checklist id', (done) => {
    let event = { pathParameters: { id: checklistId.substr(0, checklistId.length - 1) + 'x' } }
    let context = {}
    return gettodos(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent checklist id', (done) => {
    let event = { pathParameters: { id: checklistId.substr(0, checklistId.length - 1) + 'c' } }
    let context = {}
    return gettodos(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('createtodo', () => {
  const createtodo = require('../../todo/handlers/createtodo').main

  it('should return 200 and a todo object for a valid input', (done) => {
    let event = { pathParameters: { id: checklistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createtodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(200)
        expect(helper.istodo(result.data.todo)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent checklist id', (done) => {
    let event = { pathParameters: { id: checklistId.substr(0, checklistId.length - 1) + 'b' }, body: JSON.stringify(validPayload) }
    let context = {}
    return createtodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid checklist id', (done) => {
    let event = { pathParameters: { id: checklistId.substr(0, checklistId.length - 1) + 'x' }, body: JSON.stringify(validPayload) }
    let context = {}
    return createtodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for missing input', (done) => {
    let event = { pathParameters: { id: checklistId } }
    let context = {}
    return createtodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid name', (done) => {
    validPayload.name = 'a'
    let event = { pathParameters: { id: checklistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createtodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing name', (done) => {
    delete validPayload.name
    let event = { pathParameters: { id: checklistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createtodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid status', (done) => {
    validPayload.status = 'a'
    let event = { pathParameters: { id: checklistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createtodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing status', (done) => {
    delete validPayload.status
    let event = { pathParameters: { id: checklistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createtodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('updatetodo', () => {
  const updatetodo = require('../../todo/handlers/updatetodo').main

  it('should return 200 and a todo object for a valid input', (done) => {
    let event = { pathParameters: { id: todoId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updatetodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(200)
        expect(helper.istodo(result.data.todo)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid todo id', (done) => {
    let event = { pathParameters: { id: todoId.substr(0, todoId.length - 1) + 'x' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updatetodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for an non-existent todo id', (done) => {
    let event = { pathParameters: { id: todoId.substr(0, todoId.length - 1) + 'b' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updatetodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for missing input', (done) => {
    let event = { pathParameters: { id: todoId } }
    let context = {}
    return updatetodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid name', (done) => {
    validPayload.name = 'a'
    let event = { pathParameters: { id: checklistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updatetodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid status', (done) => {
    validPayload.status = 'a'
    let event = { pathParameters: { id: checklistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updatetodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('deletetodo', () => {
  const deletetodo = require('../../todo/handlers/deletetodo').main

  it('should return 200 and todo object for a valid input', (done) => {
    let event = { pathParameters: { id: todoId } }
    let context = {}
    return deletetodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(200)
        expect(helper.istodo(result.data.todo)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid todo id', (done) => {
    let event = { pathParameters: { id: todoId.substr(0, todoId.length - 1) + 'x' } }
    let context = {}
    return deletetodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent todo id', (done) => {
    let event = { pathParameters: { id: todoId.substr(0, todoId.length - 1) + 'b' } }
    let context = {}
    return deletetodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 200 and a todo object for a valid input when todo is deleted', (done) => {
    let event = { pathParameters: { id: todoId } }
    let context = {}
    return deletetodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.istodo(result.data.todo)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('restoretodo', () => {
  const restoretodo = require('../../todo/handlers/restoretodo').main

  it('should return 200 and todo object for a valid input', (done) => {
    let event = { pathParameters: { id: todoId } }
    let context = {}
    return restoretodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(200)
        expect(helper.istodo(result.data.todo)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid todo id', (done) => {
    let event = { pathParameters: { id: todoId.substr(0, todoId.length - 1) + 'x' } }
    let context = {}
    return restoretodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent todo id', (done) => {
    let event = { pathParameters: { id: todoId.substr(0, todoId.length - 1) + 'b' } }
    let context = {}
    return restoretodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(JSON.parse(res.body).statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 200 and a todo object for a valid input when todo is not deleted', (done) => {
    let event = { pathParameters: { id: todoId } }
    let context = {}
    return restoretodo(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.istodo(result.data.todo)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})
