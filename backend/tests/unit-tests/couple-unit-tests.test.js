var expect = require('chai').expect
var helper = require('../mocha/helper')

let coupleId = '4bbce6c22bb510278220de7a'
let coupleHashtag = 'abcdef'

let validPayload

beforeEach(function(done) {
  validPayload = { 
    eatwithDay: 1579234801,
    hashtag: 'abcdee',
    eatwithTitle: 'mr',
    eatwithFirstName: 'Ab',
    eatwithLastName: 'Cd',
    venue: 'abcdef'
  }
  done()
})

describe('getCouple', () => {
  const getCouple = require('../../couple/handlers/getCouple').main

  it('should return 200 and a couple object for a valid input', (done) => {
    let event = { pathParameters: { id: coupleId } }
    let context = {}
    return getCouple(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isCouple(result.data.couple)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent couple id', (done) => {
    let event = { pathParameters: { id: coupleId.substr(0, coupleId.length - 1) + 'b' } }
    let context = {}
    return getCouple(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid couple id', (done) => {
    let event = { pathParameters: { id: coupleId.substr(0, coupleId.length - 1) + 'x' } }
    let context = {}
    return getCouple(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing couple id', (done) => {
    let event = { pathParameters: { } }
    let context = {}
    return getCouple(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('getCoupleByHashtag', () => {
  const getCoupleByHashtag = require('../../couple/handlers/getCoupleByHashtag').main

  it('should return 200 and a couple object for a valid input', (done) => {
    let event = { pathParameters: { hashtag: coupleHashtag } }
    let context = {}
    return getCoupleByHashtag(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isCouple(result.data.couple)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent couple id', (done) => {
    let event = { pathParameters: { hashtag: coupleHashtag.substr(0, coupleHashtag.length - 1) + 'b' } }
    let context = {}
    return getCoupleByHashtag(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing couple hashtag', (done) => {
    let event = { pathParameters: { } }
    let context = {}
    return getCoupleByHashtag(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('updateCouple', () => {
  const updateCouple = require('../../couple/handlers/updateCouple').main

  it('should return 200 and a couple object for a valid input', (done) => {
    let event = { pathParameters: { id: coupleId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateCouple(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isCouple(result.data.couple)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for missing input', (done) => {
    let event = { pathParameters: { id: coupleId } }
    let context = {}
    return updateCouple(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent couple id', (done) => {
    let event = { pathParameters: { id: coupleId.substr(0, coupleId.length - 1) + 'b' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateCouple(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid couple id', (done) => {
    let event = { pathParameters: { id: coupleId.substr(0, coupleId.length - 1) + 'x' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateCouple(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for missing input', (done) => {
    let event = { pathParameters: { id: coupleId } }
    let context = {}
    return updateCouple(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 409 for an invalid eatwith day', (done) => {
    validPayload.eatwithDay = 'abc'
    let event = { pathParameters: { id: coupleId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateCouple(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid eatwith title', (done) => {
    validPayload.eatwithTitle = 'mx'
    let event = { pathParameters: { id: coupleId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateCouple(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid venue', (done) => {
    validPayload.venue = 'm'.repeat(121)
    let event = { pathParameters: { id: coupleId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateCouple(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('checkHashtag', () => {
  const checkHashtag = require('../../couple/handlers/checkHashtag').main

  it('should return 200 for a valid input', (done) => {
    let event = { pathParameters: { hashtag: 'abcdea' } }
    let context = {}
    return checkHashtag(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing input', (done) => {
    let event = { pathParameters: {} }
    let context = {}
    return checkHashtag(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 409 for an existing hashtag', (done) => {
    let event = { pathParameters: { hashtag: 'abcdee' } }
    let context = {}
    return checkHashtag(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(409)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})
