var expect = require('chai').expect
var helper = require('../mocha/helper')

let recordId = '4bbce7f726d5ad2782a95e4f'
let budgetId = '4bbce6c22bb510278220de74'
let guestId = '4bbce6c22bb510278220de72'

describe('addRecord', () => {
  const addRecord = require('../../record/handlers/addRecord').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ name: 'ValidName', type: 'expenditure', value: 1, date: 1, category: 'transport' }) }
    let context = {}
    return addRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Valid Test - assignedGuest', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ name: 'ValidName', type: 'expenditure', value: 1, date: 1, category: 'transport', assignedGuest: guestId }) }
    let context = {}
    return addRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Non-existent budgetId', (done) => {
    let event = { pathParameters: { id: budgetId.substr(0, budgetId.length - 1) + 'a' }, body: JSON.stringify({ name: 'ValidName', type: 'expenditure', value: 1, date: 1, category: 'transport' }) }
    let context = {}
    return addRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })

  it('Invalid Test - Invalid budgetId', (done) => {
    let event = { pathParameters: { id: budgetId.substr(0, budgetId.length - 1) + 'x' }, body: JSON.stringify({ name: 'ValidName', type: 'expenditure', value: 1, date: 1, category: 'transport' }) }
    let context = {}
    return addRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid name', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ name: 'V', type: 'expenditure', value: 1, date: 1, category: 'transport' }) }
    let context = {}
    return addRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Missing name', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ type: 'expenditure', value: 1, date: 1, category: 'transport' }) }
    let context = {}
    return addRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid type', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ name: 'ValidName', type: 'e', value: 1, date: 1, category: 'transport' }) }
    let context = {}
    return addRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Missing type', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ name: 'ValidName', value: 1, date: 1, category: 'transport' }) }
    let context = {}
    return addRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid value', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ name: 'ValidName', type: 'expenditure', value: 0, date: 1, category: 'transport' }) }
    let context = {}
    return addRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Missing value', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ name: 'ValidName', type: 'expenditure', date: 1, category: 'transport' }) }
    let context = {}
    return addRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid date', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ name: 'ValidName', type: 'expenditure', value: 1, date: 0, category: 'transport' }) }
    let context = {}
    return addRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Missing date', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ name: 'ValidName', type: 'expenditure', value: 1, category: 'transport' }) }
    let context = {}
    return addRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid category', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ name: 'ValidName', type: 'expenditure', value: 1, date: 1, category: 't' }) }
    let context = {}
    return addRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Missing category', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ name: 'ValidName', type: 'expenditure', value: 1, date: 1 }) }
    let context = {}
    return addRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid partialValue', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ name: 'ValidName', type: 'expenditure', value: 1, date: 1, category: 'transport', partialValue: -1 }) }
    let context = {}
    return addRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid partialValue', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ name: 'ValidName', type: 'expenditure', value: 1, date: 1, category: 'transport', partialValue: 2 }) }
    let context = {}
    return addRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })
})

describe('deleteRecord', () => {
  const deleteRecord = require('../../record/handlers/deleteRecord').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: recordId } }
    let context = {}
    return deleteRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Invalid recordId', (done) => {
    let event = { pathParameters: { id: recordId.substr(0, recordId.length - 1) + 'x' } }
    let context = {}
    return deleteRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Non-existent recordId', (done) => {
    let event = { pathParameters: { id: recordId.substr(0, recordId.length - 1) + 'a' } }
    let context = {}
    return deleteRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })

  it('Invalid Test - Already deleted record', (done) => {
    let event = { pathParameters: { id: recordId } }
    let context = {}
    return deleteRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })
})

describe('restoreRecord', () => {
  const restoreRecord = require('../../record/handlers/restoreRecord').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: recordId } }
    let context = {}
    return restoreRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Invalid recordId', (done) => {
    let event = { pathParameters: { id: recordId.substr(0, recordId.length - 1) + 'x' } }
    let context = {}
    return restoreRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Non-existent recordId', (done) => {
    let event = { pathParameters: { id: recordId.substr(0, recordId.length - 1) + 'a' } }
    let context = {}
    return restoreRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })

  it('Invalid Test - Already restored record', (done) => {
    let event = { pathParameters: { id: recordId } }
    let context = {}
    return restoreRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })
})

describe('getRecord', () => {
  const getRecord = require('../../record/handlers/getRecord').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: recordId } }
    let context = {}
    return getRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Invalid recordId', (done) => {
    let event = { pathParameters: { id: recordId.substr(0, recordId.length - 1) + 'x' } }
    let context = {}
    return getRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Non-existent recordId', (done) => {
    let event = { pathParameters: { id: recordId.substr(0, recordId.length - 1) + 'a' } }
    let context = {}
    return getRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })
})

describe('updateRecord', () => {
  const updateRecord = require('../../record/handlers/updateRecord').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: recordId }, body: JSON.stringify({ name: 'ValidName' }) }
    let context = {}
    return updateRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Valid Test - Empty body', (done) => {
    let event = { pathParameters: { id: recordId }, body: JSON.stringify({}) }
    let context = {}
    return updateRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Invalid recordId', (done) => {
    let event = { pathParameters: { id: recordId.substr(0, recordId.length - 1) + 'x' }, body: JSON.stringify({ name: 'ValidName' }) }
    let context = {}
    return updateRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Non-existent recordId', (done) => {
    let event = { pathParameters: { id: recordId.substr(0, recordId.length - 1) + 'a' }, body: JSON.stringify({ name: 'ValidName' }) }
    let context = {}
    return updateRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })

  it('Invalid Test - Missing fields', (done) => {
    let event = { pathParameters: { id: recordId } }
    let context = {}
    return updateRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })
  it('Invalid Test - Invalid name', (done) => {
    let event = { pathParameters: { id: recordId }, body: JSON.stringify({ name: 'V' }) }
    let context = {}
    return updateRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid type', (done) => {
    let event = { pathParameters: { id: recordId }, body: JSON.stringify({ type: 't' }) }
    let context = {}
    return updateRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid value', (done) => {
    let event = { pathParameters: { id: recordId }, body: JSON.stringify({ value: 0 }) }
    let context = {}
    return updateRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid partialValue', (done) => {
    let event = { pathParameters: { id: recordId }, body: JSON.stringify({ partialValue: -1 }) }
    let context = {}
    return updateRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid partialValue', (done) => {
    let event = { pathParameters: { id: recordId }, body: JSON.stringify({ partialValue: 16 }) }
    let context = {}
    return updateRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid date', (done) => {
    let event = { pathParameters: { id: recordId }, body: JSON.stringify({ date: 0 }) }
    let context = {}
    return updateRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid category', (done) => {
    let event = { pathParameters: { id: recordId }, body: JSON.stringify({ category: 'a' }) }
    let context = {}
    return updateRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid dueDate', (done) => {
    let event = { pathParameters: { id: recordId }, body: JSON.stringify({ dueDate: -1 }) }
    let context = {}
    return updateRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid isPaid', (done) => {
    let event = { pathParameters: { id: recordId }, body: JSON.stringify({ isPaid: 'a' }) }
    let context = {}
    return updateRecord(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })
})
