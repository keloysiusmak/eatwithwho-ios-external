var expect = require('chai').expect
var helper = require('../mocha/helper')

describe('deleteActivities', () => {
  const deleteActivities = require('../../cron/handlers/deleteActivities').main

  it('Valid Test', (done) => {
    let event = {}
    let context = {}
    return deleteActivities(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })
})

describe('deleteGuests', () => {
  const deleteGuests = require('../../cron/handlers/deleteGuests').main

  it('Valid Test', (done) => {
    let event = {}
    let context = {}
    return deleteGuests(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })
})

describe('deleteRecords', () => {
  const deleteRecords = require('../../cron/handlers/deleteRecords').main

  it('Valid Test', (done) => {
    let event = {}
    let context = {}
    return deleteRecords(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })
})
