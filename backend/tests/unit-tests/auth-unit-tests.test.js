var expect = require('chai').expect
var helper = require('../mocha/helper')
const Account = require('../../defaults/account')
const jwt = require('jsonwebtoken')

let guestId = '4bc764e34d95ee70b93cedd6'
let accountId = '4bc764e34d95ee70b93cedd6'

describe('getApiToken', () => {
  const getApiToken = require('../../auth/handlers/getApiToken').main

  it('should return 200 and an api token string for a valid input', (done) => {
    let event = { headers: { 'api-key': 'test' } }
    let context = {}
    return getApiToken(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)
        const decodedApiToken = jwt.verify(result.data.apiToken, process.env.JWT_API_TOKEN_SECRET)

        expect(result.statusCode).to.equal(200)
        expect(decodedApiToken.tokenType).to.equal('api')
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 401 for an invalid api-key', (done) => {
    let event = { headers: { 'api-key': 'wrong' } }
    let context = {}
    return getApiToken(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(401)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing api-key', (done) => {
    let event = { headers: {} }
    let context = {}
    return getApiToken(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('checkApiToken', () => {
  const checkApiToken = require('../../auth/handlers/checkApiToken').main

  it('should return 200 for a valid input', (done) => {
    let event = { headers: { 'api-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlblR5cGUiOiJhcGkiLCJpYXQiOjE1NTQwMDkzOTgsImV4cCI6OTU1OTE5MzM5OH0.wbuzeg57G9pNEDieOPN8V8SswJu1e_XDxax09OjisA0' } }
    let context = {}
    return checkApiToken(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 401 for an invalid api token', (done) => {
    let event = { headers: { 'api-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlblR5cGUiOiJhcGkiLCJpYXQiOjE1NTQwMDkzOTgsImV4cCI6OTU1OTE5MzM5OH0.wbuzeg57G9pNEDieOPN8V8SswJu1e_XDxax09OjisA1' } }
    let context = {}
    return checkApiToken(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(401)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing api token', (done) => {
    let event = { headers: {} }
    let context = {}
    return checkApiToken(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('refreshAccessToken', () => {
  const refreshAccessToken = require('../../auth/handlers/refreshAccessToken').main

  it('should return 200 and an access token string for a valid input', (done) => {
    Account.model.update({ _id: '4bc764e34d95ee70b93cedd6' }, {
      refreshToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlblR5cGUiOiJyZWZyZXNoIiwicGVybWlzc2lvbnMiOnsidHlwZSI6ImNvdXBsZSIsInRva2VuSWQiOiI0YmM3NjRlMzRkOTVlZTcwYjkzY2VkZDYifSwidG9rZW5TaWduZWRBdCI6MTU2NDEzMzU5NSwiaWF0IjoxNTY0MTMzNTk2fQ.0CsfiWVyGGZbLnQtraNr7_c-qLb1LUlN3GkCtdExBBs'
    }).then(() => {
      let event = { headers: { 'refresh-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlblR5cGUiOiJyZWZyZXNoIiwicGVybWlzc2lvbnMiOnsidHlwZSI6ImNvdXBsZSIsInRva2VuSWQiOiI0YmM3NjRlMzRkOTVlZTcwYjkzY2VkZDYifSwidG9rZW5TaWduZWRBdCI6MTU2NDEzMzU5NSwiaWF0IjoxNTY0MTMzNTk2fQ.0CsfiWVyGGZbLnQtraNr7_c-qLb1LUlN3GkCtdExBBs' } }
      let context = {}
      return refreshAccessToken(event, context, (_, res) => {
        try {
          const result = JSON.parse(res.body)
          expect(result.statusCode).to.equal(200)
    
          const decodedAccessToken = jwt.verify(result.data.accessToken, process.env.JWT_ACCESS_TOKEN_SECRET)

          expect(decodedAccessToken.tokenType).to.equal('access')
          expect(decodedAccessToken.permissions.type).to.equal('couple')
          done()
        } catch (err) {
          done(err)
        }
      })
    })
  })

  it('should return 401 for an invalid token id', (done) => {
    let event = { headers: { 'refresh-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlblR5cGUiOiJyZWZyZXNoIiwicGVybWlzc2lvbnMiOnsidHlwZSI6ImNvdXBsZSIsInRva2VuSWQiOiI0YmQ3NjRlMzRkOTVlZTcwYjkzY2VkZDYifSwidG9rZW5TaWduZWRBdCI6MTU2NDEzMzU5NSwiaWF0IjoxNTY0MTMzNTk2fQ.uzj0rRk6tw0LXKXFSmbIEnTIbAXlxcSWF8wQ433YoJA' } }
    let context = {}
    return refreshAccessToken(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(401)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 401 for an invalid refresh token', (done) => {
    let event = { headers: { 'refresh-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlblR5cGUiOiJyZWZyZXNoIiwicGVybWlzc2lvbnMiOnsidHlwZSI6ImNvdXBsZSIsInRva2VuSWQiOiI1ZDM1NzVlZjljMGM0Nzg2OGEwMTU4M2QifSwidG9rZW5TaWduZWRBdCI6MTU2NDEzMzU5NSwiaWF0IjoxNTY0MTMzNTk2fQ.HyVWqda-MewrhsbqAWiuNjrrnlzsW4n6IN9VbncsHhs' } }
    let context = {}
    return refreshAccessToken(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(401)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing refresh token', (done) => {
    let event = { headers: {} }
    let context = {}
    return refreshAccessToken(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})
