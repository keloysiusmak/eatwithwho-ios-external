var expect = require('chai').expect
var helper = require('../mocha/helper')

let accountId = '4bbce89ae620a3278eb4828a'

let validPayload

beforeEach(function(done) {
  validPayload = { 
    accountId,
    type: 'bug', 
    route: 'testroute/',
    message: 'Test Message' 
  }
  done()
})

describe('report', () => {
  const report = require('../../internal/handlers/report').main

  it('should return 200 for a valid input', (done) => {
    let event = { body: JSON.stringify(validPayload) }
    let context = {}
    return report(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for missing input', (done) => {
    let event = {}
    let context = {}
    return report(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing account id', (done) => {
    delete validPayload.accountId
    let event = { body: JSON.stringify(validPayload) }
    let context = {}
    return report(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid account id', (done) => {
    validPayload.accountId = 'ab'
    let event = { body: JSON.stringify(validPayload) }
    let context = {}
    return report(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing route', (done) => {
    delete validPayload.route
    let event = { body: JSON.stringify(validPayload) }
    let context = {}
    return report(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a message with over 120 characters', (done) => {
    validPayload.route = 'a'.repeat(121)
    let event = { body: JSON.stringify(validPayload) }
    let context = {}
    return report(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing message', (done) => {
    delete validPayload.message
    let event = { body: JSON.stringify(validPayload) }
    let context = {}
    return report(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a message with over 600 characters', (done) => {
    validPayload.message = 'a'.repeat(601)
    let event = { body: JSON.stringify(validPayload) }
    let context = {}
    return report(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid type', (done) => {
    validPayload.type = 'a'
    let event = { body: JSON.stringify(validPayload) }
    let context = {}
    return report(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})