var expect = require('chai').expect
var helper = require('../mocha/helper')

let eatwithId = '4bbce88f4eed3b278e7cc7f9'
let fileId = '4c7e3e2e6343394236b7ff7b'

describe('uploadFile', () => {
  const uploadFile = require('../../file/handlers/uploadFile').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: eatwithId }, body: JSON.stringify({ fileName: 'abc.png', fileSize: 3 }) }
    let context = {}
    return uploadFile(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Non-existent eatwithId', (done) => {
    let event = { pathParameters: { id: eatwithId.substr(0, eatwithId.length - 1) + 'b' }, body: JSON.stringify({ fileName: 'abc.png', fileSize: 3 }) }
    let context = {}
    return uploadFile(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })

  it('Invalid Test - Invalid eatwithId', (done) => {
    let event = { pathParameters: { id: eatwithId.substr(0, eatwithId.length - 1) + 'x' }, body: JSON.stringify({ fileName: 'abc.png', fileSize: 3 }) }
    let context = {}
    return uploadFile(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid fileName', (done) => {
    let event = { pathParameters: { id: eatwithId }, body: JSON.stringify({ fileName: '', fileSize: 3 }) }
    let context = {}
    return uploadFile(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Missing fileName', (done) => {
    let event = { pathParameters: { id: eatwithId }, body: JSON.stringify({ fileSize: 3 }) }
    let context = {}
    return uploadFile(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid fileSize', (done) => {
    let event = { pathParameters: { id: eatwithId }, body: JSON.stringify({ fileName: 'abc.png', fileSize: 'a' }) }
    let context = {}
    return uploadFile(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid fileSize', (done) => {
    let event = { pathParameters: { id: eatwithId }, body: JSON.stringify({ fileName: 'abcd.png', fileSize: 4124012481024012401 }) }
    let context = {}
    return uploadFile(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(409)
      done()
    })
  })

  it('Invalid Test - Missing fileSize', (done) => {
    let event = { pathParameters: { id: eatwithId }, body: JSON.stringify({ fileName: 'abc.png' }) }
    let context = {}
    return uploadFile(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })
})

describe('deleteFile', () => {
  const deleteFile = require('../../file/handlers/deleteFile').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: fileId} }
    let context = {}
    return deleteFile(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Invalid fileId', (done) => {
    let event = { pathParameters: { id: fileId.substr(0, fileId.length - 1) + 'x' } }
    let context = {}
    return deleteFile(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Non-existent fileId', (done) => {
    let event = { pathParameters: { id: fileId.substr(0, fileId.length - 1) + 'c' } }
    let context = {}
    return deleteFile(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })
})
