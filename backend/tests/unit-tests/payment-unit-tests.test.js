var expect = require('chai').expect
var helper = require('../mocha/helper')

let paymentId = '4c8f27cb90c04c9f1449bab1'
let paymentDirectoryId = '4c8f27cb90c04c9f1449bab2'
let budgetId = '4bbce6c22bb510278220de74'

describe('getPaymentDirectory', () => {
  const getPaymentDirectory = require('../../payment/handlers/getPaymentDirectory').main

  it('Valid Test', (done) => {
    let event = { }
    let context = {}
    return getPaymentDirectory(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })
})

describe('addPayment', () => {
  const addPayment = require('../../payment/handlers/addPayment').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ name: 'ValidName', desc: 'ValidDesc' }) }
    let context = {}
    return addPayment(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Non-existent budgetId', (done) => {
    let event = { pathParameters: { id: budgetId.substr(0, budgetId.length - 1) + 'a' }, body: JSON.stringify({ name: 'ValidName', desc: 'ValidDesc' }) }
    let context = {}
    return addPayment(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })

  it('Invalid Test - Invalid budgetId', (done) => {
    let event = { pathParameters: { id: budgetId.substr(0, budgetId.length - 1) + 'x' }, body: JSON.stringify({ name: 'ValidName', desc: 'ValidDesc' }) }
    let context = {}
    return addPayment(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid name', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ name: 'A', desc: 'ValidDesc' }) }
    let context = {}
    return addPayment(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Missing name', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ desc: 'ValidDesc' }) }
    let context = {}
    return addPayment(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Missing desc', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ name: 'ValidName' }) }
    let context = {}
    return addPayment(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })
})

describe('addPaymentsFromDirectory', () => {
  const addPaymentsFromDirectory = require('../../payment/handlers/addPaymentsFromDirectory').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ paymentIds: [paymentDirectoryId] })  }
    let context = {}
    return addPaymentsFromDirectory(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Non-existent budgetId', (done) => {
    let event = { pathParameters: { id: budgetId.substr(0, budgetId.length - 1) + 'a' }, body: JSON.stringify({ paymentIds: [paymentDirectoryId] }) }
    let context = {}
    return addPaymentsFromDirectory(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })

  it('Invalid Test - Invalid budgetId', (done) => {
    let event = { pathParameters: { id: budgetId.substr(0, budgetId.length - 1) + 'x' }, body: JSON.stringify({ paymentIds: [paymentDirectoryId] }) }
    let context = {}
    return addPaymentsFromDirectory(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid paymentIds', (done) => {
    let event = { pathParameters: { id: budgetId.substr(0, budgetId.length - 1) + 'x' }, body: JSON.stringify({ paymentIds: [paymentDirectoryId.substr(0, paymentDirectoryId.length - 1) + 'x'] }) }
    let context = {}
    return addPaymentsFromDirectory(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })
})

describe('updatePayment', () => {
  const updatePayment = require('../../payment/handlers/updatePayment').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: paymentId }, body: JSON.stringify({ name: 'ValidName' }) }
    let context = {}
    return updatePayment(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: paymentId }, body: JSON.stringify({ desc: 'ValidDesc' }) }
    let context = {}
    return updatePayment(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Valid Test - Empty body', (done) => {
    let event = { pathParameters: { id: paymentId }, body: JSON.stringify({}) }
    let context = {}
    return updatePayment(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Invalid paymentId', (done) => {
    let event = { pathParameters: { id: paymentId.substr(0, paymentId.length - 1) + 'x' }, body: JSON.stringify({ name: 'ValidName' }) }
    let context = {}
    return updatePayment(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Non-existent paymentId', (done) => {
    let event = { pathParameters: { id: paymentId.substr(0, paymentId.length - 1) + 'a' }, body: JSON.stringify({ name: 'ValidName' }) }
    let context = {}
    return updatePayment(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })

  it('Invalid Test - Missing fields', (done) => {
    let event = { pathParameters: { id: paymentId } }
    let context = {}
    return updatePayment(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid name', (done) => {
    let event = { pathParameters: { id: paymentId }, body: JSON.stringify({ name: 'V' }) }
    let context = {}
    return updatePayment(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })
})

describe('deletePayment', () => {
  const deletePayment = require('../../payment/handlers/deletePayment').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: paymentId } }
    let context = {}
    return deletePayment(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Invalid paymentId', (done) => {
    let event = { pathParameters: { id: paymentId.substr(0, paymentId.length - 1) + 'x' } }
    let context = {}
    return deletePayment(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Non-existent paymentId', (done) => {
    let event = { pathParameters: { id: paymentId.substr(0, paymentId.length - 1) + 'a' } }
    let context = {}
    return deletePayment(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })
})
