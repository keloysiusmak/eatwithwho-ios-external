var expect = require('chai').expect
var helper = require('../mocha/helper')

let scheduleId = '4bbce6c22bb510278220de75'

describe('getSchedule', () => {
  const getSchedule = require('../../schedule/handlers/getSchedule').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: scheduleId } }
    let context = {}
    return getSchedule(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Non-existent scheduleId', (done) => {
    let event = { pathParameters: { id: scheduleId.substr(0, scheduleId.length - 1) + 'a' } }
    let context = {}
    return getSchedule(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })

  it('Invalid Test - Invalid scheduleId', (done) => {
    let event = { pathParameters: { id: scheduleId.substr(0, scheduleId.length - 1) + 'x' } }
    let context = {}
    return getSchedule(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })
})

describe('updateSchedule', () => {
  const updateSchedule = require('../../schedule/handlers/updateSchedule').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: scheduleId }, body: JSON.stringify({ date: 1 }) }
    let context = {}
    return updateSchedule(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: scheduleId }, body: JSON.stringify({ date: 1 }) }
    let context = {}
    return updateSchedule(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Non-existent scheduleId', (done) => {
    let event = { pathParameters: { id: scheduleId.substr(0, scheduleId.length - 1) + 'a' }, body: JSON.stringify({ name: 'ValidName', date: 1 }) }
    let context = {}
    return updateSchedule(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })

  it('Invalid Test - Invalid scheduleId', (done) => {
    let event = { pathParameters: { id: scheduleId.substr(0, scheduleId.length - 1) + 'x' }, body: JSON.stringify({ name: 'ValidName', date: 1 }) }
    let context = {}
    return updateSchedule(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid date', (done) => {
    let event = { pathParameters: { id: scheduleId }, body: JSON.stringify({ date: 0 }) }
    let context = {}
    return updateSchedule(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid date', (done) => {
    let event = { pathParameters: { id: scheduleId }, body: JSON.stringify({ date: 'A' }) }
    let context = {}
    return updateSchedule(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })
})
