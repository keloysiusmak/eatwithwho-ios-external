var expect = require('chai').expect
var helper = require('../mocha/helper')

let validPayload

beforeEach(function(done) {
  validPayload = { 
    light: true
  }
  done()
})

describe('getVendors', () => {
  const getVendors = require('../../vendor/handlers/getVendors').main

  it('should return 200 and an vendor objects for a valid input', (done) => {
    delete validPayload.light
    let event = { body: JSON.stringify(validPayload) }
    let context = {}
    return getVendors(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        for (const vendor of result.data.vendors) {
          expect(helper.isVendor(vendor)).to.equal(true)
        }
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})