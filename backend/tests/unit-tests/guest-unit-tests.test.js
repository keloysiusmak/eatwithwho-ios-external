var expect = require('chai').expect
var helper = require('../mocha/helper')

let guestId = '4bbce6c22bb510278220de72'
let guestlistId = '4bbce6c22bb510278220fe7a'

let validPayload

beforeEach(function(done) {
  validPayload = {
    title: 'mr', 
    firstName: 'ValidName', 
    lastName: 'ValidName', 
    email: 'ta@tb.com', 
    number: 12345678, 
    status: 'attending', 
    note: 'test', 
    addressLine1: 'Singapore', 
    addressLine2: 'Singapore', 
    dietaryPreference: 'test', 
    countryCode: '65', 
    postalCode: 999999, 
    additionalGuests: [{
      firstName: 'test', 
      lastName: 'test', 
      title: 'mr', 
      dietaryPreference: 'food', 
      babyChair: true
    }]
  }
  done()
})

describe('getGuests', () => {
  const getGuests = require('../../guest/handlers/getGuests').main

  it('should return 200 and an array of guest objects for a valid input', (done) => {
    let event = { pathParameters: { id: guestlistId } }
    let context = {}
    return getGuests(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(result.data.guests.length).to.equal(4)
        result.data.guests.forEach(guest => {
          expect(helper.isGuest(guest)).to.equal(true)
        })
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guestlist id', (done) => {
    let event = { pathParameters: { id: guestlistId.substr(0, guestlistId.length - 1) + 'x' } }
    let context = {}
    return getGuests(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent guestlist id', (done) => {
    let event = { pathParameters: { id: guestlistId.substr(0, guestlistId.length - 1) + 'c' } }
    let context = {}
    return getGuests(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('sendGuestReminders', () => {
  const sendGuestReminders = require('../../guest/handlers/sendGuestReminders').main

  it('should return 200 for a valid input', (done) => {
    let event = { pathParameters: { id: guestlistId } }
    let context = {}
    return sendGuestReminders(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guestlist id', (done) => {
    let event = { pathParameters: { id: guestlistId.substr(0, guestlistId.length - 1) + 'x' } }
    let context = {}
    return sendGuestReminders(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent guestlist id', (done) => {
    let event = { pathParameters: { id: guestlistId.substr(0, guestlistId.length - 1) + 'c' } }
    let context = {}
    return sendGuestReminders(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('createGuest', () => {
  const createGuest = require('../../guest/handlers/createGuest').main

  it('should return 200 and a guest object for a valid input', (done) => {
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isGuest(result.data.guest)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent guestlist id', (done) => {
    let event = { pathParameters: { id: guestlistId.substr(0, guestlistId.length - 1) + 'c' }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guestlist id', (done) => {
    let event = { pathParameters: { id: guestlistId.substr(0, guestlistId.length - 1) + 'x' }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for missing input', (done) => {
    let event = { pathParameters: { id: guestlistId } }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid title', (done) => {
    validPayload.title = 'mx'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid first name', (done) => {
    validPayload.firstName = 'a'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing first name', (done) => {
    delete validPayload.firstName
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid last name', (done) => {
    validPayload.lastName = 'b'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing last name', (done) => {
    delete validPayload.lastName
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid email', (done) => {
    validPayload.email = 'c'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid number (length longer than 10)', (done) => {
    validPayload.number = 99999999999
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid number (string)', (done) => {
    validPayload.number = 'V'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid status', (done) => {
    validPayload.status = 'abc'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid additional guests (string)', (done) => {
    validPayload.additionalGuests = 'abc'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid additional guests (number)', (done) => {
    validPayload.additionalGuests = 21
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid additional guests (invalid array)', (done) => {
    validPayload.additionalGuests = [21]
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid additional guests (invalid array object - invalid title)', (done) => {
    validPayload.additionalGuests[0].title = 'mx'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid additional guests (invalid array object - invalid babyChair)', (done) => {
    validPayload.additionalGuests[0].babyChair = 'alse'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid additional guests (invalid array object - invalid firstName)', (done) => {
    validPayload.additionalGuests[0].firstName = 'a'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid additional guests (invalid array object - invalid lastName)', (done) => {
    validPayload.additionalGuests[0].lastName = 'a'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid note', (done) => {
    validPayload.note = 'a'.repeat(281)
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid addressLine1', (done) => {
    validPayload.addressLine1 = 'a'.repeat(281)
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid country code', (done) => {
    validPayload.countryCode = '9999'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid country code (number)', (done) => {
    validPayload.countryCode = 9999
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid country code (letters)', (done) => {
    validPayload.countryCode = 'abc'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid postal code', (done) => {
    validPayload.postalCode = 9999999
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid postal code (string)', (done) => {
    validPayload.postalCode = 'abc'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('deleteGuest', () => {
  const deleteGuest = require('../../guest/handlers/deleteGuest').main

  it('should return 200 and guest object for a valid input', (done) => {
    let event = { pathParameters: { id: guestId } }
    let context = {}
    return deleteGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isGuest(result.data.guest)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guest id', (done) => {
    let event = { pathParameters: { id: guestId.substr(0, guestId.length - 1) + 'x' } }
    let context = {}
    return deleteGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent guest id', (done) => {
    let event = { pathParameters: { id: guestId.substr(0, guestId.length - 1) + 'c' } }
    let context = {}
    return deleteGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 200 and a guest object for a valid input when guest is deleted', (done) => {
    let event = { pathParameters: { id: guestId } }
    let context = {}
    return deleteGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isGuest(result.data.guest)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('restoreGuest', () => {
  const restoreGuest = require('../../guest/handlers/restoreGuest').main

  it('should return 200 and a guest object for a valid input', (done) => {
    let event = { pathParameters: { id: guestId } }
    let context = {}
    return restoreGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isGuest(result.data.guest)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guest id', (done) => {
    let event = { pathParameters: { id: guestId.substr(0, guestId.length - 1) + 'x' } }
    let context = {}
    return restoreGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent guest id', (done) => {
    let event = { pathParameters: { id: guestId.substr(0, guestId.length - 1) + 'c' } }
    let context = {}
    return restoreGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 200 and a guest object for a valid input when guest is not deleted', (done) => {
    let event = { pathParameters: { id: guestId } }
    let context = {}
    return restoreGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isGuest(result.data.guest)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('getGuest', () => {
  const getGuest = require('../../guest/handlers/getGuest').main

  it('should return 200 and a guest object for a valid input', (done) => {
    let event = { pathParameters: { id: guestId } }
    let context = {}
    return getGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isGuest(result.data.guest)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guest id', (done) => {
    let event = { pathParameters: { id: guestId.substr(0, guestId.length - 1) + 'x' } }
    let context = {}
    return getGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent guest id', (done) => {
    let event = { pathParameters: { id: guestId.substr(0, guestId.length - 1) + 'c' } }
    let context = {}
    return getGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('updateGuest', () => {
  const updateGuest = require('../../guest/handlers/updateGuest').main

  it('should return 200 and a guest object for a valid input', (done) => {
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for missing input', (done) => {
    let event = { pathParameters: { id: guestId } }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guest id', (done) => {
    let event = { pathParameters: { id: guestId.substr(0, guestId.length - 1) + 'x' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent guest id', (done) => {
    let event = { pathParameters: { id: guestId.substr(0, guestId.length - 1) + 'c' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid title', (done) => {
    validPayload.title = 'mx'
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid first name', (done) => {
    validPayload.firstName = 'a'
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid last name', (done) => {
    validPayload.lastName = 'v'
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid email', (done) => {
    validPayload.email = 'a'
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid number (length longer than 10)', (done) => {
    validPayload.number = 10000000000
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid number (string)', (done) => {
    validPayload.number = 'abc'
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid status', (done) => {
    validPayload.status = 'abc'
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid additional guests (string)', (done) => {
    validPayload.additionalGuests = 'abc'
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid additional guests (number)', (done) => {
    validPayload.additionalGuests = 1
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid note', (done) => {
    validPayload.note = 'a'.repeat(281)
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid addressLine1', (done) => {
    validPayload.addressLine1 = 'a'.repeat(61)
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid addressLine2', (done) => {
    validPayload.addressLine2 = 'a'.repeat(61)
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid country code', (done) => {
    validPayload.countryCode = '1000'
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid country code (number)', (done) => {
    validPayload.countryCode = 1000
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid country code (letters)', (done) => {
    validPayload.countryCode = 'abc'
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid postal code', (done) => {
    validPayload.postalCode = 1000000
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid postal code (string)', (done) => {
    validPayload.postalCode = 1234567
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid additional guests (invalid array)', (done) => {
    validPayload.additionalGuests = [21]
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid additional guests (invalid array object - invalid babyChair)', (done) => {
    validPayload.additionalGuests[0].babyChair = 'truax'
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid additional guests (invalid array object - invalid firstName)', (done) => {
    validPayload.additionalGuests[0].firstName = 'a'
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid additional guests (invalid array object - invalid lastName)', (done) => {
    validPayload.additionalGuests[0].lastName = 'a'
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for invalid additional guests (invalid array object - invalid title)', (done) => {
    validPayload.additionalGuests[0].title = 'mx'
    let event = { pathParameters: { id: guestId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateGuest(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})
