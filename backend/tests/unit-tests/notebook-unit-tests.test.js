var expect = require('chai').expect
var helper = require('../mocha/helper')

let coupleId = '4bbce6c22bb510278220de70'
let notebookId = '4bfce6c22bb510279220fe7a'

describe('createNotebook', () => {
  const createNotebook = require('../../notebook/handlers/createNotebook').main

  it('should return 200 and a notebook object for a valid input', (done) => {
    let event = { pathParameters: { id: coupleId } }
    let context = {}
    return createNotebook(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isNotebook(result.data.notebook)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent couple id', (done) => {
    let event = { pathParameters: { id: coupleId.substr(0, coupleId.length - 1) + 'c' } }
    let context = {}
    return createNotebook(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid couple id', (done) => {
    let event = { pathParameters: { id: coupleId.substr(0, coupleId.length - 1) + 'x' } }
    let context = {}
    return createNotebook(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 409 for a couple with a notebook', (done) => {
    let event = { pathParameters: { id: coupleId } }
    let context = {}
    return createNotebook(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(409)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

})

describe('getNotebook', () => {
  const getNotebook = require('../../notebook/handlers/getNotebook').main

  it('should return 200 and a notebook object for a valid input', (done) => {
    let event = { pathParameters: { id: notebookId } }
    let context = {}
    return getNotebook(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isNotebook(result.data.notebook)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid notebook id', (done) => {
    let event = { pathParameters: { id: notebookId.substr(0, notebookId.length - 1) + 'x' } }
    let context = {}
    return getNotebook(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent notebook id', (done) => {
    let event = { pathParameters: { id: notebookId.substr(0, notebookId.length - 1) + 'c' } }
    let context = {}
    return getNotebook(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('updateNotebook', () => {
  const updateNotebook = require('../../notebook/handlers/updateNotebook').main

  it('should return 200 and a notebook object for a valid input', (done) => {
    let event = { pathParameters: { id: notebookId }, body: JSON.stringify({ photographyMetrics: ['test'] }) }
    let context = {}
    return updateNotebook(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for missing input', (done) => {
    let event = { pathParameters: { id: notebookId } }
    let context = {}
    return updateNotebook(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid notebook id', (done) => {
    let event = { pathParameters: { id: notebookId.substr(0, notebookId.length - 1) + 'x' }, body: JSON.stringify({ photographyMetrics: ['test'] }) }
    let context = {}
    return updateNotebook(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent notebook id', (done) => {
    let event = { pathParameters: { id: notebookId.substr(0, notebookId.length - 1) + 'c' }, body: JSON.stringify({ photographyMetrics: ['test'] }) }
    let context = {}
    return updateNotebook(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})
