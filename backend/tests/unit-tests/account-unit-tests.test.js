var expect = require('chai').expect
var helper = require('../mocha/helper')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

let accountId = '4bc764e34d95ee70b93cedd6'

let validPayload

beforeEach(function(done) {
  validPayload = {
    title: 'mr',
    username: 'abcdef',
    password: '123456',
    firstName: 'Alston',
    lastName: 'Lee',
    lang: 'en',
    countryCode: '65',
    number: 8125555,
    email: 'alston2@lee.com',
    walkthroughs: ['checklist']
  }
  done()
})

describe('checkAccount', () => {
  const checkAccount = require('../../account/handlers/checkAccount').main

  it('should return 200 and a valid username boolean for a valid input', (done) => {
    let event = { body: JSON.stringify({ username: 'abcdeg' }) }
    let context = {}
    return checkAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(result.data.validUsername).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing username', (done) => {
    let event = { body: JSON.stringify({}) }
    let context = {}
    return checkAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid username', (done) => {
    let event = { body: JSON.stringify({ username: 'a' }) }
    let context = {}
    return checkAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 200 and a valid username boolean for an existing username', (done) => {
    let event = { body: JSON.stringify({ username: 'aaaaaa' }) }
    let context = {}
    return checkAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(result.data.validUsername).to.equal(false)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('changePassword', () => {
  const changePassword = require('../../account/handlers/changePassword').main

  it('should return 200 and an account object for a vaid input', (done) => {
    const newPassword = 'abcdef'
    let event = { pathParameters: { id: accountId }, body: JSON.stringify({ oldPassword: 'b', newPassword: newPassword }) }
    let context = {}
    return changePassword(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(true).to.equal(helper.isAccount(result.data.account))
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid account id', (done) => {
    let event = { pathParameters: { id: accountId.substr(0, accountId.length - 1) + 'x' }, body: JSON.stringify({ oldPassword: 'abcdef', newPassword: 'abcdef' }) }
    let context = {}
    return changePassword(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent account id', (done) => {
    let event = { pathParameters: { id: accountId.substr(0, accountId.length - 1) + 'a' }, body: JSON.stringify({ oldPassword: 'abcdef', newPassword: 'abcdef' }) }
    let context = {}
    return changePassword(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing account id', (done) => {
    let event = { pathParameters: {}, body: JSON.stringify({ oldPassword: 'abcdef', newPassword: 'abcdef' }) }
    let context = {}
    return changePassword(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing old password', (done) => {
    let event = { pathParameters: { id: accountId }, body: JSON.stringify({ newPassword: 'abcdef' }) }
    let context = {}
    return changePassword(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid new password', (done) => {
    let event = { pathParameters: { id: accountId }, body: JSON.stringify({ oldPassword: 'abcdef', newPassword: 'a' }) }
    let context = {}
    return changePassword(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing new password', (done) => {
    let event = { pathParameters: { id: accountId }, body: JSON.stringify({ oldPassword: 'abcdef' }) }
    let context = {}
    return changePassword(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('getAccount', () => {
  const getAccount = require('../../account/handlers/getAccount').main

  it('should return 200 and an account object for a valid input', (done) => {
    let event = { pathParameters: { id: accountId } }
    let context = {}
    return getAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isAccount(result.data.account)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid account id', (done) => {
    let event = { pathParameters: { id: accountId.substr(0, accountId.length - 1) + 'x' } }
    let context = {}
    return getAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent account id', (done) => {
    let event = { pathParameters: { id: accountId.substr(0, accountId.length - 1) + 'a' } }
    let context = {}
    return getAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing account id', (done) => {
    let event = { pathParameters: {} }
    let context = {}
    return getAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('loginAccount', () => {
  const loginAccount = require('../../account/handlers/loginAccount').main

  it('should return 200, an account object, an access token string and a refresh token string for a valid input', (done) => {
    let event = { pathParameters: { id: accountId }, body: JSON.stringify({ username: 'aaaaaa', password: 'abcdef' }) }
    let context = {}
    return loginAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)
        const decodedAccessToken = jwt.verify(result.data.accessToken, process.env.JWT_ACCESS_TOKEN_SECRET)
        const decodedRefreshToken = jwt.verify(result.data.refreshToken, process.env.JWT_REFRESH_TOKEN_SECRET)

        expect(result.statusCode).to.equal(200)
        expect(helper.isAccount(result.data.account)).to.equal(true)
        expect(decodedAccessToken.tokenType).to.equal('access')
        expect(decodedRefreshToken.tokenType).to.equal('refresh')
        expect(decodedAccessToken.permissions.type).to.equal('couple')
        expect(decodedRefreshToken.permissions.type).to.equal('couple')
        expect(decodedAccessToken.permissions.tokenId).to.equal(accountId)
        expect(decodedRefreshToken.permissions.tokenId).to.equal(accountId)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 401 for an invalid password', (done) => {
    let event = { pathParameters: { id: accountId }, body: JSON.stringify({ username: 'a', password: 'x' }) }
    let context = {}
    return loginAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(401)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 401 for an invalid username', (done) => {
    let event = { pathParameters: { id: accountId }, body: JSON.stringify({ username: 'b', password: 'abcdef' }) }
    let context = {}
    return loginAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(401)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing username', (done) => {
    let event = { body: JSON.stringify({ password: 'a' }) }
    let context = {}
    return loginAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing password', (done) => {
    let event = { body: JSON.stringify({ username: 'a' }) }
    let context = {}
    return loginAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('registerAccount', () => {
  const registerAccount = require('../../account/handlers/registerAccount').main

  it('should return 200 and an account object for a valid input', (done) => {
    validPayload.email = 'alston@lee.com'
    validPayload.username = 'validusername'
    let event = { pathParameters: { type: 'couple' }, body: JSON.stringify(validPayload) }
    let context = {}
    return registerAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isAccount(result.data.account)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid username', (done) => {
    validPayload.username = 'abc'
    let event = { pathParameters: { type: 'couple' }, body: JSON.stringify(validPayload) }
    let context = {}
    return registerAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 409 for an existing username', (done) => {
    validPayload.username = 'aaaaaa'
    let event = { pathParameters: { type: 'couple' }, body: JSON.stringify(validPayload) }
    let context = {}
    return registerAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(409)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing username', (done) => {
    delete validPayload.username
    let event = { pathParameters: { type: 'couple' }, body: JSON.stringify(validPayload) }
    let context = {}
    return registerAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid password', (done) => {
    validPayload.password = 'test'
    let event = { pathParameters: { type: 'couple' }, body: JSON.stringify(validPayload) }
    let context = {}
    return registerAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing password', (done) => {
    delete validPayload.password
    let event = { pathParameters: { type: 'couple' }, body: JSON.stringify(validPayload) }
    let context = {}
    return registerAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an missing title', (done) => {
    delete validPayload.title
    let event = { pathParameters: { type: 'couple' }, body: JSON.stringify(validPayload) }
    let context = {}
    return registerAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid title', (done) => {
    validPayload.title = 'mx'
    let event = { pathParameters: { type: 'couple' }, body: JSON.stringify(validPayload) }
    let context = {}
    return registerAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid first name', (done) => {
    validPayload.firstName = 't'
    let event = { pathParameters: { type: 'couple' }, body: JSON.stringify(validPayload) }
    let context = {}
    return registerAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing first name', (done) => {
    delete validPayload.firstName
    let event = { pathParameters: { type: 'couple' }, body: JSON.stringify(validPayload) }
    let context = {}
    return registerAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid last name', (done) => {
    validPayload.lastName = 't'
    let event = { pathParameters: { type: 'couple' }, body: JSON.stringify(validPayload) }
    let context = {}
    return registerAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing last name', (done) => {
    delete validPayload.lastName
    let event = { pathParameters: { type: 'couple' }, body: JSON.stringify(validPayload) }
    let context = {}
    return registerAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid number (length longer than 10)', (done) => {
    validPayload.number = 9999999999999
    let event = { pathParameters: { id: accountId }, body: JSON.stringify(validPayload) }
    let context = {}
    return registerAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid number (string)', (done) => {
    validPayload.number = 'v'
    let event = { pathParameters: { id: accountId }, body: JSON.stringify(validPayload) }
    let context = {}
    return registerAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid email', (done) => {
    validPayload.email = 't'
    let event = { pathParameters: { type: 'couple' }, body: JSON.stringify(validPayload) }
    let context = {}
    return registerAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing email', (done) => {
    delete validPayload.email
    let event = { pathParameters: { type: 'couple' }, body: JSON.stringify(validPayload) }
    let context = {}
    return registerAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('updateAccount', () => {
  const updateAccount = require('../../account/handlers/updateAccount').main

  it('should return 200 and an account object for a valid input', (done) => {
    let event = { pathParameters: { id: accountId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isAccount(result.data.account)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid account id', (done) => {
    let event = { pathParameters: { id: accountId.substr(0, accountId.length - 1) + 'x' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)
        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent account id', (done) => {
    let event = { pathParameters: { id: accountId.substr(0, accountId.length - 1) + 'a' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing account id', (done) => {
    let event = { pathParameters: {}, body: JSON.stringify(validPayload) }
    let context = {}
    return updateAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for missing input', (done) => {
    let event = { pathParameters: { id: accountId } }
    let context = {}
    return updateAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid language', (done) => {
    validPayload.lang = 'ex'
    let event = { pathParameters: { id: accountId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid country code', (done) => {
    validPayload.countryCode = '1000'
    let event = { pathParameters: { id: accountId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid country code (number)', (done) => {
    validPayload.countryCode = 1000
    let event = { pathParameters: { id: accountId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid country code (letters)', (done) => {
    validPayload.countryCode = 'abc'
    let event = { pathParameters: { id: accountId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid title', (done) => {
    validPayload.title = 'mx'
    let event = { pathParameters: { id: accountId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid first name', (done) => {
    validPayload.firstName = 'a'
    let event = { pathParameters: { id: accountId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid last name', (done) => {
    validPayload.lastName = 'a'
    let event = { pathParameters: { id: accountId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid number (length longer than 10)', (done) => {
    validPayload.number = 99999999999
    let event = { pathParameters: { id: accountId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid number (string)', (done) => {
    validPayload.number = 'V'
    let event = { pathParameters: { id: accountId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateAccount(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('requestPasswordReset', () => {
  const requestPasswordReset = require('../../account/handlers/requestPasswordReset').main

  it('should return 200 for a valid input', (done) => {
    const newPassword = 'abcdef'
    let event = { body: JSON.stringify({ username: 'aaaaaa' }) }
    let context = {}
    return requestPasswordReset(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 200 for a non-existent username', (done) => {
    const newPassword = 'abcdef'
    let event = { body: JSON.stringify({ username: 'bbbbbb' }) }
    let context = {}
    return requestPasswordReset(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing input', (done) => {
    let event = {}
    let context = {}
    return requestPasswordReset(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for missing username', (done) => {
    let event = { body: JSON.stringify({}) }
    let context = {}
    return requestPasswordReset(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('resetPassword', () => {
  const resetPassword = require('../../account/handlers/resetPassword').main

  it('should return 200 and an account object for a vaid input', (done) => {
    const newPassword = 'abcdef'
    let event = { pathParameters: { id: validPayload.username }, body: JSON.stringify({ password: newPassword }) }
    let context = {}
    return resetPassword(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(true).to.equal(helper.isAccount(result.data.account))
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent account id', (done) => {
    let event = { pathParameters: { id: validPayload.username.substr(0, validPayload.username.length - 1) + 'x' }, body: JSON.stringify({ password: 'abcdef' }) }
    let context = {}
    return resetPassword(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing account id', (done) => {
    let event = { pathParameters: {}, body: JSON.stringify({ password: 'abcdef' }) }
    let context = {}
    return resetPassword(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid new password', (done) => {
    let event = { pathParameters: { id: validPayload.username }, body: JSON.stringify({ password: 'a' }) }
    let context = {}
    return resetPassword(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a missing new password', (done) => {
    let event = { pathParameters: { id: validPayload.username }, body: JSON.stringify({ }) }
    let context = {}
    return resetPassword(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})
