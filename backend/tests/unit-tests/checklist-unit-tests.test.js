var expect = require('chai').expect
var helper = require('../mocha/helper')

let coupleId = '4bbce6c22bb510278220de70'
let checklistId = '4bbce6c22bb510279220fe7a'

let validPayload

beforeEach(function(done) {
  validPayload = { 
    todos: ['4bbce6c22bb510279220fe7b']
  }
  done()
})

describe('createChecklist', () => {
  const createChecklist = require('../../checklist/handlers/createChecklist').main

  it('should return 200 and a checklist object for a valid input', (done) => {
    let event = { pathParameters: { id: coupleId } }
    let context = {}
    return createChecklist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isChecklist(result.data.checklist)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent couple id', (done) => {
    let event = { pathParameters: { id: coupleId.substr(0, coupleId.length - 1) + 'c' } }
    let context = {}
    return createChecklist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid couple id', (done) => {
    let event = { pathParameters: { id: coupleId.substr(0, coupleId.length - 1) + 'x' } }
    let context = {}
    return createChecklist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 409 for a couple with a checklist', (done) => {
    let event = { pathParameters: { id: coupleId } }
    let context = {}
    return createChecklist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(409)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

})

describe('getChecklist', () => {
  const getChecklist = require('../../checklist/handlers/getChecklist').main

  it('should return 200 and a checklist object for a valid input', (done) => {
    let event = { pathParameters: { id: checklistId } }
    let context = {}
    return getChecklist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isChecklist(result.data.checklist)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid checklist id', (done) => {
    let event = { pathParameters: { id: checklistId.substr(0, checklistId.length - 1) + 'x' } }
    let context = {}
    return getChecklist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent checklist id', (done) => {
    let event = { pathParameters: { id: checklistId.substr(0, checklistId.length - 1) + 'c' } }
    let context = {}
    return getChecklist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('updateChecklist', () => {
  const updateChecklist = require('../../checklist/handlers/updateChecklist').main

  it('should return 200 and a checklist object for a valid input', (done) => {
    let event = { pathParameters: { id: checklistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateChecklist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for missing input', (done) => {
    let event = { pathParameters: { id: checklistId } }
    let context = {}
    return updateChecklist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid checklist id', (done) => {
    let event = { pathParameters: { id: checklistId.substr(0, checklistId.length - 1) + 'x' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateChecklist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent checklist id', (done) => {
    let event = { pathParameters: { id: checklistId.substr(0, checklistId.length - 1) + 'c' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateChecklist(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})
