var expect = require('chai').expect
var helper = require('../mocha/helper')

let budgetId = '4bbce6c22bb510278220de74'
let coupleId = '4bbce6c22bb510278220de70'

describe('createBudget', () => {
  const createBudget = require('../../budget/handlers/createBudget').main

  it('should return 200 and a budget object for a valid input', (done) => {
    let event = { pathParameters: { id: coupleId } }
    let context = {}
    return createBudget(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isBudget(result.data.budget)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent couple id', (done) => {
    let event = { pathParameters: { id: coupleId.substr(0, coupleId.length - 1) + 'c' } }
    let context = {}
    return createBudget(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid couple id', (done) => {
    let event = { pathParameters: { id: coupleId.substr(0, coupleId.length - 1) + 'x' } }
    let context = {}
    return createBudget(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 409 for a couple with a budget', (done) => {
    let event = { pathParameters: { id: coupleId } }
    let context = {}
    return createBudget(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(409)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('getBudget', () => {
  const getBudget = require('../../budget/handlers/getBudget').main

  it('should return 200 and a budget object for a valid input', (done) => {
    let event = { pathParameters: { id: budgetId } }
    let context = {}
    return getBudget(event, context, (_, res) => {
        try {
          const result = JSON.parse(res.body)

          expect(result.statusCode).to.equal(200)
          done()
        } catch (err) {
          done(err)
        }
    })
  })

  it('should return 400 for a non-existent budget id', (done) => {
    let event = { pathParameters: { id: budgetId.substr(0, budgetId.length - 1) + 'a' } }
    let context = {}
    return getBudget(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid budget id', (done) => {
    let event = { pathParameters: { id: budgetId.substr(0, budgetId.length - 1) + 'x' } }
    let context = {}
    return getBudget(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('updateBudget', () => {
  const updateBudget = require('../../budget/handlers/updateBudget').main

  it('should return 200 and a budget object for a valid input', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ budgetBudget: 1, currency: 'SEK' }) }
    let context = {}
    return updateBudget(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for a non-existent budget id', (done) => {
    let event = { pathParameters: { id: budgetId.substr(0, budgetId.length - 1) + 'a' }, body: JSON.stringify({ budgetBudget: 1 }) }
    let context = {}
    return updateBudget(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid budget id', (done) => {
    let event = { pathParameters: { id: budgetId.substr(0, budgetId.length - 1) + 'x' }, body: JSON.stringify({ budgetBudget: 1 }) }
    let context = {}
    return updateBudget(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guestlistBudget (less than 1)', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ guestlistBudget: 0 }) }
    let context = {}
    return updateBudget(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guestlistBudget (more than 10,000,000)', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ guestlistBudget: 10000001 }) }
    let context = {}
    return updateBudget(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guestlistBudget (string)', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ guestlistBudget: 'A' }) }
    let context = {}
    return updateBudget(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid currency (invalid)', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ currency: 'ABC' }) }
    let context = {}
    return updateBudget(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid currency (number)', (done) => {
    let event = { pathParameters: { id: budgetId }, body: JSON.stringify({ currency: 1 }) }
    let context = {}
    return updateBudget(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})
