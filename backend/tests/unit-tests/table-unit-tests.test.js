var expect = require('chai').expect
var helper = require('../mocha/helper')

let tableId = '4bbce6c22bb510278220de6a'
let guestlistId = '4bbce6c22bb510278220fe7a'

let validPayload

beforeEach(function(done) {
  validPayload = { 
    number: 1,
    name: 'ValidName',
    size: 10,
    guests: ['4bbce6c22bb510278220de75'] 
  }
  done()
})

describe('getTables', () => {
  const getTables = require('../../table/handlers/getTables').main

  it('should return 200 and an array of table objects for a valid input', (done) => {
    let event = { pathParameters: { id: guestlistId } }
    let context = {}
    return getTables(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        result.data.tables.forEach(table => {
          expect(helper.isTable(table)).to.equal(true)
        })
        expect(result.data.tables.length).to.equal(1)
        expect(result.data.tables[0].guests.length).to.equal(1)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guestlist id', (done) => {
    let event = { pathParameters: { id: guestlistId.substr(0, guestlistId.length - 1) + 'x' } }
    let context = {}
    return getTables(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent guestlist id', (done) => {
    let event = { pathParameters: { id: guestlistId.substr(0, guestlistId.length - 1) + 'c' } }
    let context = {}
    return getTables(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('createTable', () => {
  const getTable = require('../../table/handlers/getTable').main
  const createTable = require('../../table/handlers/createTable').main

  it('should return 200 and a table object for a valid input', (done) => {
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isTable(result.data.table)).to.equal(true)
        expect(result.data.table.guests.length).to.equal(1)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should pull guests assigned to another table for a valid input', (done) => {
    let event = { pathParameters: { id: tableId.substr(0, tableId.length - 1) + 'd' } }
    let context = {}
    return getTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isTable(result.data.table)).to.equal(true)
        expect(result.data.table.guests.length).to.equal(0)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent guestlist id', (done) => {
    let event = { pathParameters: { id: guestlistId.substr(0, guestlistId.length - 1) + 'c' }, body: JSON.stringify(validPayload) }
    let context = {}
    return createTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guestlist id', (done) => {
    let event = { pathParameters: { id: guestlistId.substr(0, guestlistId.length - 1) + 'x' }, body: JSON.stringify(validPayload) }
    let context = {}
    return createTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid size (string)', (done) => {
    validPayload.size = 'abc'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid size (less than 1)', (done) => {
    validPayload.size = 0
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid size (more than 30)', (done) => {
    validPayload.size = 31
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid name', (done) => {
    validPayload.name = 'V'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an missing name', (done) => {
    delete validPayload.name
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an missing number', (done) => {
    delete validPayload.number
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid number', (done) => {
    validPayload.number = 'a'
    let event = { pathParameters: { id: guestlistId }, body: JSON.stringify(validPayload) }
    let context = {}
    return createTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('deleteTable', () => {
  const deleteTable = require('../../table/handlers/deleteTable').main

  it('should return 200 and table object for a valid input', (done) => {
    let event = { pathParameters: { id: tableId } }
    let context = {}
    return deleteTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isTable(result.data.table)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid table id', (done) => {
    let event = { pathParameters: { id: tableId.substr(0, tableId.length - 1) + 'x' } }
    let context = {}
    return deleteTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent table id', (done) => {
    let event = { pathParameters: { id: tableId.substr(0, tableId.length - 1) + 'c' } }
    let context = {}
    return deleteTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 200 and a table object for a valid input when table is deleted', (done) => {
    let event = { pathParameters: { id: tableId } }
    let context = {}
    return deleteTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isTable(result.data.table)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('restoreTable', () => {
  const restoreTable = require('../../table/handlers/restoreTable').main

  it('should return 200 and a table object for a valid input', (done) => {
    let event = { pathParameters: { id: tableId } }
    let context = {}
    return restoreTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isTable(result.data.table)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid table id', (done) => {
    let event = { pathParameters: { id: tableId.substr(0, tableId.length - 1) + 'x' } }
    let context = {}
    return restoreTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent table id', (done) => {
    let event = { pathParameters: { id: tableId.substr(0, tableId.length - 1) + 'c' } }
    let context = {}
    return restoreTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 200 and a table object for a valid input when table is not deleted', (done) => {
    let event = { pathParameters: { id: tableId } }
    let context = {}
    return restoreTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isTable(result.data.table)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('getTable', () => {
  const getTable = require('../../table/handlers/getTable').main

  it('should return 200 and a table object for a valid input', (done) => {
    let event = { pathParameters: { id: tableId } }
    let context = {}
    return getTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isTable(result.data.table)).to.equal(true)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid table id', (done) => {
    let event = { pathParameters: { id: tableId.substr(0, tableId.length - 1) + 'x' } }
    let context = {}
    return getTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent table id', (done) => {
    let event = { pathParameters: { id: tableId.substr(0, tableId.length - 1) + 'c' } }
    let context = {}
    return getTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('updateTable', () => {
  const getTable = require('../../table/handlers/getTable').main
  const updateTable = require('../../table/handlers/updateTable').main

  it('should return 200 and a table object for a valid input', (done) => {
    validPayload.guests = ['4bbce6c22bb510278220de72']
    let event = { pathParameters: { id: tableId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isTable(result.data.table)).to.equal(true)
        expect(result.data.table.guests.length).to.equal(1)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should pull guests assigned to another table for a valid input', (done) => {
    let event = { pathParameters: { id: tableId.substr(0, tableId.length - 1) + 'b' } }
    let context = {}
    return getTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isTable(result.data.table)).to.equal(true)
        expect(result.data.table.guests.length).to.equal(0)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for missing input', (done) => {
    let event = { pathParameters: { id: tableId } }
    let context = {}
    return updateTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid table id', (done) => {
    let event = { pathParameters: { id: tableId.substr(0, tableId.length - 1) + 'x' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 404 for a non-existent table id', (done) => {
    let event = { pathParameters: { id: tableId.substr(0, tableId.length - 1) + 'c' }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(404)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid size (string)', (done) => {
    validPayload.size = 'abc'
    let event = { pathParameters: { id: tableId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid size (less than 1)', (done) => {
    validPayload.size = 0
    let event = { pathParameters: { id: tableId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guests (string)', (done) => {
    validPayload.guests = 'abc'
    let event = { pathParameters: { id: tableId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guests (number)', (done) => {
    validPayload.guests = 1
    let event = { pathParameters: { id: tableId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guests (array with invalid id)', (done) => {
    validPayload.guests = ['1']
    let event = { pathParameters: { id: tableId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid guests (array with one invalid id)', (done) => {
    validPayload.guests = [tableId, '1']
    let event = { pathParameters: { id: tableId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid name', (done) => {
    validPayload.name = 'a'
    let event = { pathParameters: { id: tableId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 400 for an invalid number', (done) => {
    validPayload.number = 'a'
    let event = { pathParameters: { id: tableId }, body: JSON.stringify(validPayload) }
    let context = {}
    return updateTable(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(400)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})
