var expect = require('chai').expect
var helper = require('../mocha/helper')

let validPayload

beforeEach(function(done) {
  validPayload = { 
    light: true
  }
  done()
})

describe('getAdvertisement', () => {
  const getAdvertisement = require('../../advertisement/handlers/getAdvertisement').main

  it('should return 200 and an advertisement object for a valid input', (done) => {
    delete validPayload.light
    let event = { body: JSON.stringify(validPayload) }
    let context = {}
    return getAdvertisement(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isAdvertisement(result.data.advertisement)).to.equal(true)
        expect(result.data.advertisement.desc).to.not.equal(undefined)
        expect(result.data.advertisement.imageUrl).to.not.equal(undefined)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 200 and an advertisement object for a false light input', (done) => {
    validPayload.light = false
    let event = { body: JSON.stringify(validPayload) }
    let context = {}
    return getAdvertisement(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isAdvertisement(result.data.advertisement)).to.equal(true)
        expect(result.data.advertisement.desc).to.not.equal(undefined)
        expect(result.data.advertisement.imageUrl).to.not.equal(undefined)
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  it('should return 200 and a light advertisement object for a true light input', (done) => {
    let event = { body: JSON.stringify(validPayload) }
    let context = {}
    return getAdvertisement(event, context, (_, res) => {
      try {
        const result = JSON.parse(res.body)

        expect(result.statusCode).to.equal(200)
        expect(helper.isAdvertisement(result.data.advertisement)).to.equal(true)
        expect(result.data.advertisement.desc).to.equal(undefined)
        expect(result.data.advertisement.imageUrl).to.equal(undefined)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})