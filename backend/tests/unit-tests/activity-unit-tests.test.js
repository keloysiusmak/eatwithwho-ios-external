var expect = require('chai').expect
var helper = require('../mocha/helper')

let eatwithId = '4bbce88f4eed3b278e7cc7f9'
let scheduleId = '4bbce6c22bb510278220de75'

describe('addActivity', () => {
  const addActivity = require('../../eatwith/handlers/addActivity').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: scheduleId }, body: JSON.stringify({ name: 'ValidName', startTime: 1, endTime: 2, category: 'others', desc: 'ValidDesc' }) }
    let context = {}
    return addActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Non-existent scheduleId', (done) => {
    let event = { pathParameters: { id: scheduleId.substr(0, scheduleId.length - 1) + 'a' }, body: JSON.stringify({ name: 'ValidName', startTime: 1, endTime: 2, category: 'others', desc: 'ValidDesc' }) }
    let context = {}
    return addActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })

  it('Invalid Test - Invalid scheduleId', (done) => {
    let event = { pathParameters: { id: scheduleId.substr(0, scheduleId.length - 1) + 'x' }, body: JSON.stringify({ name: 'ValidName', startTime: 1, endTime: 2, category: 'others', desc: 'ValidDesc' }) }
    let context = {}
    return addActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid name', (done) => {
    let event = { pathParameters: { id: scheduleId }, body: JSON.stringify({ name: 'A', startTime: 1, endTime: 2, category: 'others', desc: 'ValidDesc' }) }
    let context = {}
    return addActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Missing name', (done) => {
    let event = { pathParameters: { id: scheduleId }, body: JSON.stringify({ startTime: 1, endTime: 2, category: 'others', desc: 'ValidDesc' }) }
    let context = {}
    return addActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Identical startTime and endTime', (done) => {
    let event = { pathParameters: { id: scheduleId }, body: JSON.stringify({ name: 'ValidName', startTime: 1, endTime: 1, category: 'others', desc: 'ValidDesc' }) }
    let context = {}
    return addActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid startTime and endTime', (done) => {
    let event = { pathParameters: { id: scheduleId }, body: JSON.stringify({ name: 'ValidName', startTime: 2, endTime: 1, category: 'others', desc: 'ValidDesc' }) }
    let context = {}
    return addActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Missing startTime', (done) => {
    let event = { pathParameters: { id: scheduleId }, body: JSON.stringify({ name: 'ValidName', endTime: 2, category: 'others', desc: 'ValidDesc' }) }
    let context = {}
    return addActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Missing endTime', (done) => {
    let event = { pathParameters: { id: scheduleId }, body: JSON.stringify({ name: 'ValidName', startTime: 1, category: 'others', desc: 'ValidDesc' }) }
    let context = {}
    return addActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid category', (done) => {
    let event = { pathParameters: { id: scheduleId }, body: JSON.stringify({ name: 'ValidName', startTime: 1, endTime: 2, category: 'abc', desc: 'ValidDesc' }) }
    let context = {}
    return addActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Missing category', (done) => {
    let event = { pathParameters: { id: scheduleId }, body: JSON.stringify({ name: 'ValidName', startTime: 1, endTime: 2, desc: 'ValidDesc' }) }
    let context = {}
    return addActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Missing desc', (done) => {
    let event = { pathParameters: { id: scheduleId }, body: JSON.stringify({ name: 'ValidName', startTime: 1, endTime: 2, category: 'others' }) }
    let context = {}
    return addActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })
})

describe('deleteActivity', () => {
  const deleteActivity = require('../../eatwith/handlers/deleteActivity').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: eatwithId } }
    let context = {}
    return deleteActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Invalid eatwithId', (done) => {
    let event = { pathParameters: { id: eatwithId.substr(0, eatwithId.length - 1) + 'x' } }
    let context = {}
    return deleteActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Non-existent eatwithId', (done) => {
    let event = { pathParameters: { id: eatwithId.substr(0, eatwithId.length - 1) + 'a' } }
    let context = {}
    return deleteActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })

  it('Invalid Test - Already deleted eatwith', (done) => {
    let event = { pathParameters: { id: eatwithId } }
    let context = {}
    return deleteActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })
})

describe('restoreActivity', () => {
  const restoreActivity = require('../../eatwith/handlers/restoreActivity').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: eatwithId } }
    let context = {}
    return restoreActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Invalid eatwithId', (done) => {
    let event = { pathParameters: { id: eatwithId.substr(0, eatwithId.length - 1) + 'x' } }
    let context = {}
    return restoreActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Non-existent eatwithId', (done) => {
    let event = { pathParameters: { id: eatwithId.substr(0, eatwithId.length - 1) + 'a' } }
    let context = {}
    return restoreActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })

  it('Invalid Test - Already restored eatwith', (done) => {
    let event = { pathParameters: { id: eatwithId } }
    let context = {}
    return restoreActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })
})

describe('getActivity', () => {
  const getActivity = require('../../eatwith/handlers/getActivity').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: eatwithId } }
    let context = {}
    return getActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Invalid eatwithId', (done) => {
    let event = { pathParameters: { id: eatwithId.substr(0, eatwithId.length - 1) + 'x' } }
    let context = {}
    return getActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Non-existent eatwithId', (done) => {
    let event = { pathParameters: { id: eatwithId.substr(0, eatwithId.length - 1) + 'a' } }
    let context = {}
    return getActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })
})

describe('updateActivity', () => {
  const updateActivity = require('../../eatwith/handlers/updateActivity').main

  it('Valid Test', (done) => {
    let event = { pathParameters: { id: eatwithId }, body: JSON.stringify({ name: 'ValidName' }) }
    let context = {}
    return updateActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Valid Test - Empty body', (done) => {
    let event = { pathParameters: { id: eatwithId }, body: JSON.stringify({}) }
    let context = {}
    return updateActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(200)
      done()
    })
  })

  it('Invalid Test - Invalid eatwithId', (done) => {
    let event = { pathParameters: { id: eatwithId.substr(0, eatwithId.length - 1) + 'x' }, body: JSON.stringify({ name: 'ValidName' }) }
    let context = {}
    return updateActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Non-existent eatwithId', (done) => {
    let event = { pathParameters: { id: eatwithId.substr(0, eatwithId.length - 1) + 'a' }, body: JSON.stringify({ name: 'ValidName' }) }
    let context = {}
    return updateActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(404)
      done()
    })
  })

  it('Invalid Test - Missing fields', (done) => {
    let event = { pathParameters: { id: eatwithId } }
    let context = {}
    return updateActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid name', (done) => {
    let event = { pathParameters: { id: eatwithId }, body: JSON.stringify({ name: 'V' }) }
    let context = {}
    return updateActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid category', (done) => {
    let event = { pathParameters: { id: eatwithId }, body: JSON.stringify({ category: 't' }) }
    let context = {}
    return updateActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Identical startTime and endTime', (done) => {
    let event = { pathParameters: { id: eatwithId }, body: JSON.stringify({ startTime: 1, endTime: 1 }) }
    let context = {}
    return updateActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })

  it('Invalid Test - Invalid startTime and endTime', (done) => {
    let event = { pathParameters: { id: eatwithId }, body: JSON.stringify({ startTime: 2, endTime: 1 }) }
    let context = {}
    return updateActivity(event, context, (_, res) => {
      expect(JSON.parse(res.body).statusCode).to.equal(400)
      done()
    })
  })
})
