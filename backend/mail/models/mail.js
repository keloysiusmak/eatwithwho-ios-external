'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const mailSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  body: {
    type: String,
    required: true
  },
  recipients: {
    type: Array,
    items: {
      type: Schema.Types.ObjectId,
      ref: 'account'
    },
    default: []
  },
  read: {
    type: Array,
    items: {
      type: Schema.Types.ObjectId,
      ref: 'account'
    },
    default: []
  },
  starred: {
    type: Array,
    items: {
      type: Schema.Types.ObjectId,
      ref: 'account'
    },
    default: []
  },
  sender: {
    type: Schema.Types.ObjectId,
    ref: 'account',
    required: true
  },
  createdAt: {
    type: Number,
    required: true
  },
  modifiedAt: {
    type: Number,
    required: true
  }
})

module.exports.model = mongoose.model('Mail', mailSchema)
module.exports.selectFilters = '_id title body sender read starred createdAt modifiedAt'
