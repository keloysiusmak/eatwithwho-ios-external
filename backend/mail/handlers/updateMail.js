'use strict'

const FUNC_NAME = '[mail/updateMail]'
const mailHelper = require('../helpers/mailHelper')
const outputHelper = require('../helpers/outputHelper')

const db = require('../database/connectDatabase')
const Mail = (process.env.IS_TEST) ? require('../../defaults/mail') : require('../models/mail')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  const mailId = event.pathParameters.id
  const fieldsToUpdate = event.body
  const now = Math.floor(Date.now() / 1000)

  db.connect().then(() => {
    if (!mailId || !outputHelper.isValidId(mailId) || !fieldsToUpdate || !outputHelper.isJson(fieldsToUpdate)) {
      let error = {
        statusCode: 400
      }
      throw error
    }

    const parsedFieldsToUpdate = JSON.parse(fieldsToUpdate)
    const permittedFields = ['read', 'unread', 'star', 'unstar']

    const fields = []

    permittedFields.forEach((field) => {
      if ((field in parsedFieldsToUpdate)) {
        if (!outputHelper.isValidId(parsedFieldsToUpdate[field])) {
          let error = {
            statusCode: 400
          }
          throw error
        }
        fields.push(parsedFieldsToUpdate[field])
      }
    })

    if (parsedFieldsToUpdate['read'] && parsedFieldsToUpdate['unread'] && parsedFieldsToUpdate['read'] === parsedFieldsToUpdate['unread']) {
      let error = {
        statusCode: 400
      }
      throw error
    }
    
    if (parsedFieldsToUpdate['star'] && parsedFieldsToUpdate['unstar'] && parsedFieldsToUpdate['star'] === parsedFieldsToUpdate['unstar']) {
      let error = {
        statusCode: 400
      }
      throw error
    }

    let addObj = {}
    let pullObj = {}

    if (parsedFieldsToUpdate['read']) {
      addObj.read = parsedFieldsToUpdate['read']
    }

    if (parsedFieldsToUpdate['star']) {
      addObj.starred = parsedFieldsToUpdate['star']
    }

    if (parsedFieldsToUpdate['unread']) {
      pullObj.read = parsedFieldsToUpdate['unread']
    }

    if (parsedFieldsToUpdate['unstar']) {
      pullObj.starred = parsedFieldsToUpdate['unstar']
    }

    const filter = {
      _id: mailId,
      recipients: { 
        $all: [...new Set(fields)]
      }
    }

    const newFields = {
      modifiedAt: now,
      $addToSet: addObj,
      $pull: pullObj
    }

    return Mail.model.findOneAndUpdate(filter, newFields, { new: true }).select(Mail.selectFilters)
  }).then((result) => {
    if (!result) {
      let error = {
        statusCode: 404
      }
      throw error
    }

    const mail = result

    const statusMessage = 'Successfully updated mail'
    const output = { mail }
    return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 200, statusMessage, output)
  }).catch((error) => {
    if (typeof error !== 'string' && error.statusCode && error.statusCode === 400) {
      const statusMessage = 'Failed to update mail as mail id or fields was not provided'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 401) {
      const statusMessage = 'Failed to update mail as mail cannot be modified'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 401, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 404) {
      const statusMessage = 'Failed to update mail as mail with given mail id does not exist'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 404, statusMessage)
    } else {
      const statusMessage = 'Error encountered when updating mail: ' + error
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 500, statusMessage)
    }
  })
}
