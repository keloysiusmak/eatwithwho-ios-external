'use strict'

const FUNC_NAME = '[mail/getMails]'
const outputHelper = require('../helpers/outputHelper')

const db = require('../database/connectDatabase')
const Mail = (process.env.IS_TEST) ? require('../../defaults/mail') : require('../models/mail')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  const accountId = event.pathParameters.id

  db.connect().then(() => {
    if (!accountId || !outputHelper.isValidId(accountId)) {
      let error = {
        statusCode: 400
      }
      throw error
    }

    let filter = {
      recipients: accountId
    }

    return Mail.model.find(filter).sort({ createdAt: -1 }).limit(10).select(Mail.selectFilters)
  }).then((result) => {
    const mails = result

    const statusMessage = 'Successfully get mail'
    const output = { mails }
    return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 200, statusMessage, output)
  }).catch((error) => {
    if (typeof error !== 'string' && error.statusCode && error.statusCode === 400) {
      const statusMessage = 'Failed to get notifications as account id was not provided'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 400, statusMessage)
    } else {
      const statusMessage = 'Error encountered when getting notifications: ' + error
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 500, statusMessage)
    }
  })
}
