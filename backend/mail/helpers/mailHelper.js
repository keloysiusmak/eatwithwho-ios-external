const outputHelper = require('./outputHelper')

function isValidMail (Mail, isCreate, mail) {
  let errors = new Mail(mail).validateSync()
  let error = (errors && !isCreate) ? Object.keys(mail).filter(key => {
    return Object.keys(errors.errors).filter(errorKey => errorKey.includes(key)).length > 0
  }).length > 0 : errors
  if (error) {
    return false
  } else {
    return true
  }
}

module.exports = {
  isValidMail
}
