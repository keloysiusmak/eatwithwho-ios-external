'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const permissionSchema = new Schema({
  accountId: {
    type: Schema.Types.ObjectId,
    ref: 'account'
  },
  guestId: {
    type: Schema.Types.ObjectId,
    ref: 'guest'
  },
  permissions: {
    type: Object,
    additionalProperties: {
      type: Array,
      items: {
        type: Schema.Types.ObjectId
      }
    }
  },
  createdAt: {
    type: Number,
    required: true
  },
  modifiedAt: {
    type: Number,
    required: true
  }
})

module.exports.model = mongoose.model('Permission', permissionSchema)
