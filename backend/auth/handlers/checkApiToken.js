const jwt = require('jsonwebtoken')

const FUNC_NAME = '[auth/checkApiToken]'
const outputHelper = require('../helpers/outputHelper')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  const apiToken = event.headers['api-token']

  if (!apiToken) {
    const statusMessage = 'Failed to check api token as the api token was not provided'
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 400, statusMessage)
  }

  try {
    const decoded = jwt.verify(apiToken, process.env.JWT_API_TOKEN_SECRET)
    if (decoded.tokenType !== 'api') {
      const statusMessage = 'Failed to check api token as it was invalid'
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 401, statusMessage)
    } else {
      const statusMessage = 'Successfully checked api token'
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 200, statusMessage)
    }
  } catch (error) {
    const statusMessage = 'Failed to check api token as it was invalid'
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 401, statusMessage)
  }
}
