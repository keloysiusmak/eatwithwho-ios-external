const jwt = require('jsonwebtoken')

const FUNC_NAME = '[auth/verifyApiToken]'

const generatePolicy = (principalId, effect, resource) => {
  const authResponse = {}
  authResponse.principalId = principalId
  if (effect && resource) {
    const policyDocument = {}
    policyDocument.Version = '2012-10-17'
    policyDocument.Statement = []
    const statementOne = {}
    statementOne.Action = 'execute-api:Invoke'
    statementOne.Effect = effect
    statementOne.Resource = resource
    policyDocument.Statement[0] = statementOne
    authResponse.policyDocument = policyDocument
  }
  return authResponse
}

module.exports.main = (event, context, callback) => {
  if (process.env.STATUS === 'offline') {
    return callback(null, generatePolicy(1, 'Deny', event.methodArn))
  }
  context.callbackWaitsForEmptyEventLoop = false
  const apiToken = (event.authorizationToken !== undefined) ? event.authorizationToken : event.headers['api-token']

  if (!apiToken) {
    const statusMessage = 'Failed to verify api token as the api token was not provided'
    console.log(FUNC_NAME, statusMessage)
    return callback(null, generatePolicy(1, 'Deny', event.methodArn))
  } else {
    try {
      const decoded = jwt.verify(apiToken, process.env.JWT_API_TOKEN_SECRET)
      if (decoded.tokenType !== 'api') {
        const statusMessage = 'Failed to verify api token as it was invalid'
        console.log(FUNC_NAME, statusMessage)
        return callback(null, generatePolicy(1, 'Deny', event.methodArn))
      }

      const statusMessage = 'Successfully verified api token'
      console.log(FUNC_NAME, statusMessage)
      return callback(null, generatePolicy(1, 'Allow', event.methodArn))
    } catch (error) {
      const statusMessage = 'Error encountered when verifying token: ' + error
      console.log(FUNC_NAME, statusMessage)
      return callback(null, generatePolicy(1, 'Deny', event.methodArn))
    }
  }
}
