const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')

const db = require('../database/connectDatabase')
const Permission = (process.env.IS_TEST) ? require('../../defaults/permission') : require('../models/permission')

const FUNC_NAME = '[auth/verifyAccessToken]'

function isJson (str) {
  try {
    JSON.parse(str)
  } catch (e) {
    return false
  }
  return true
}

function getRequiredPermissions (method) {
  var validObjectId = /^[a-fA-F0-9]{24}$/
  let requiredPermissions = 'none'
  if (method[1] === 'account') {
    if (validObjectId.test(method[2])) {
      requiredPermissions = {
        type: ['couple', 'admin'],
        account: method[2]
      }
    }
  } else if (method[1] === 'schedule') {
    if (validObjectId.test(method[2])) {
      requiredPermissions = {
        type: ['couple', 'admin'],
        schedule: method[2]
      }
    }
  } else if (method[1] === 'guestlist') {
    if (validObjectId.test(method[2])) {
      requiredPermissions = {
        type: ['couple', 'admin'],
        guestlist: method[2]
      }
    }
  } else if (method[1] === 'couple') {
    if (validObjectId.test(method[2])) {
      requiredPermissions = {
        type: ['couple', 'admin'],
        couple: method[2]
      }
    }
  } else if (method[1] === 'file') {
    if (validObjectId.test(method[2])) {
      requiredPermissions = {
        type: ['couple', 'admin'],
        file: method[2]
      }
    }
  } else if (method[1] === 'payment') {
    if (validObjectId.test(method[2])) {
      requiredPermissions = {
        type: ['couple', 'admin'],
        payment: method[2]
      }
    }
  } else if (method[1] === 'guest') {
    if (validObjectId.test(method[2])) {
      if (method[0] === 'GET') {
        requiredPermissions = {
          type: ['guest', 'couple', 'admin'],
          guest: method[2]
        }
      } else {
        requiredPermissions = {
          type: ['couple', 'admin'],
          guest: method[2]
        }
      }
    }
  } else if (method[1] === 'eatwith') {
    if (validObjectId.test(method[2])) {
      requiredPermissions = {
        type: ['couple', 'admin'],
        eatwith: method[2]
      }
    }
  } else if (method[1] === 'table') {
    if (validObjectId.test(method[2])) {
      requiredPermissions = {
        type: ['couple', 'admin'],
        table: method[2]
      }
    }
  } else if (method[1] === 'budget') {
    if (validObjectId.test(method[2])) {
      requiredPermissions = {
        type: ['couple', 'admin'],
        budget: method[2]
      }
    }
  } else if (method[1] === 'comment') {
    if (validObjectId.test(method[2])) {
      requiredPermissions = {
        type: ['couple', 'admin'],
        comment: method[2]
      }
    } else if (validObjectId.test(method[3])) {
      requiredPermissions = {
        type: ['couple', 'admin']
      }
      requiredPermissions[method[2]] = method[3]
    }
  } else if (method[1] === 'record') {
    if (validObjectId.test(method[2])) {
      requiredPermissions = {
        type: ['couple', 'admin'],
        record: method[2]
      }
    }
  } else if (method[1] === 'todo') {
    if (validObjectId.test(method[2])) {
      if (method[0] === 'PUT') {
        requiredPermissions = {
          type: ['guest', 'couple', 'admin'],
          todo: method[2]
        }
      } else {
        requiredPermissions = {
          type: ['couple', 'admin'],
          todo: method[2]
        }
      }
    }
  }

  return requiredPermissions
}

function checkPermissions (grantedPermissions, method) {
  const requiredPermissions = getRequiredPermissions(method)
  const tokenId = grantedPermissions.tokenId
  if (!tokenId) {
    return new Promise((resolve, reject) => {
      resolve(false)
    })
  }
  if (requiredPermissions === 'none') {
    return new Promise((resolve, reject) => {
      resolve(true)
    })
  } else {
    if (!(requiredPermissions.type.includes(grantedPermissions.type))) {
      return new Promise((resolve, reject) => {
        resolve(false)
      })
    }

    const type = grantedPermissions.type

    delete requiredPermissions.type
    delete grantedPermissions.type

    return db.connect().then(() => {
      let filter

      if (type === 'couple' || type === 'admin') {
        filter = {
          accountId: new mongoose.Types.ObjectId(tokenId)
        }
      } else {
        filter = {
          guestId: new mongoose.Types.ObjectId(tokenId)
        }
      }

      let selectFilters = []

      for (const key in requiredPermissions) {
        if (key !== 'type') {
          selectFilters.push('permissions.' + key)
        }
      }
      selectFilters = selectFilters.join(' ')

      return Permission.model.findOne(filter, selectFilters)
    }).then((result) => {
      if (!result) {
        return false
      }
      const permission = result

      for (const key in permission.permissions) {
        if (key !== '_id') grantedPermissions[key] = permission.permissions[key]
      }

      let allowed = true
      if (grantedPermissions.type !== 'admin') {
        for (const key in requiredPermissions) {
          if (isJson(requiredPermissions[key]) && Array.isArray(requiredPermissions[key])) {
            const requiredPermissionsArray = JSON.parse(requiredPermissions[key])
            requiredPermissionsArray.forEach(requiredPermission => {
              if (!(grantedPermissions[key] && grantedPermissions[key].includes(requiredPermission))) {
                allowed = false
              }
            })
          } else {
            if (!(grantedPermissions[key] && grantedPermissions[key].includes(requiredPermissions[key]))) {
              allowed = false
            }
          }
        }
      }

      return allowed
    })
  }
}

const generatePolicy = (principalId, effect, resource) => {
  const authResponse = {}
  authResponse.principalId = principalId
  if (effect && resource) {
    const policyDocument = {}
    policyDocument.Version = '2012-10-17'
    policyDocument.Statement = []
    const statementOne = {}
    statementOne.Action = 'execute-api:Invoke'
    statementOne.Effect = effect
    statementOne.Resource = resource
    policyDocument.Statement[0] = statementOne
    authResponse.policyDocument = policyDocument
  }
  return authResponse
}

module.exports.main = (event, context, callback) => {
  if (process.env.STATUS === 'offline') {
    return callback(null, generatePolicy(1, 'Deny', event.methodArn))
  }
  context.callbackWaitsForEmptyEventLoop = false
  const accessToken = (event.authorizationToken !== undefined) ? event.authorizationToken : (event.headers) ? event.headers['access-token'] : null
  const method = event.methodArn.split('/').splice(2)

  if (!accessToken) {
    const statusMessage = 'Failed to verify access token as the access token was not provided'
    console.log(FUNC_NAME, statusMessage)
    return callback(null, generatePolicy(1, 'Deny', event.methodArn))
  } else {
    try {
      const decoded = jwt.verify(accessToken, process.env.JWT_ACCESS_TOKEN_SECRET)
      if (decoded.tokenType !== 'access') {
        const statusMessage = 'Failed to verify access token as it was invalid'
        console.log(FUNC_NAME, statusMessage)
        return callback(null, generatePolicy(1, 'Deny', event.methodArn))
      }

      const accessPermissions = decoded.permissions

      checkPermissions(accessPermissions, method).then((result) => {
        if (result) {
          const statusMessage = 'Successfully verified access token'
          console.log(FUNC_NAME, statusMessage)
          return callback(null, generatePolicy(accessPermissions.tokenId, 'Allow', event.methodArn))
        } else {
          const statusMessage = 'Failed to verify access token as it was invalid'
          console.log(FUNC_NAME, statusMessage)
          return callback(null, generatePolicy(1, 'Deny', event.methodArn))
        }
      })
    } catch (error) {
      const statusMessage = 'Error encountered when verifying access token: ' + error
      console.log(FUNC_NAME, statusMessage)
      return callback(null, generatePolicy(1, 'Deny', event.methodArn))
    }
  }
}
