const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')

const db = require('../database/connectDatabase')

const FUNC_NAME = '[auth/verifyResetToken]'

function checkPermissions (grantedPermissions, method) {
  const requiredPermissions = {
    type: ['couple', 'admin'],
    account: method[2]
  }

  const username = grantedPermissions.username
  if (!username) {
    return new Promise((resolve, reject) => {
      resolve(false)
    })
  }
  if (requiredPermissions === 'none') {
    return new Promise((resolve, reject) => {
      resolve(true)
    })
  } else {
    if (!(requiredPermissions.type.includes(grantedPermissions.type))) {
      return new Promise((resolve, reject) => {
        resolve(false)
      })
    }

    return new Promise((resolve, reject) => {
      resolve(requiredPermissions.account === grantedPermissions.username)
    })
  }
}

const generatePolicy = (principalId, effect, resource) => {
  const authResponse = {}
  authResponse.principalId = principalId
  if (effect && resource) {
    const policyDocument = {}
    policyDocument.Version = '2012-10-17'
    policyDocument.Statement = []
    const statementOne = {}
    statementOne.Action = 'execute-api:Invoke'
    statementOne.Effect = effect
    statementOne.Resource = resource
    policyDocument.Statement[0] = statementOne
    authResponse.policyDocument = policyDocument
  }
  return authResponse
}

module.exports.main = (event, context, callback) => {
  if (process.env.STATUS === 'offline') {
    return callback(null, generatePolicy(1, 'Deny', event.methodArn))
  }
  context.callbackWaitsForEmptyEventLoop = false
  const resetPasswordToken = (event.authorizationToken !== undefined) ? event.authorizationToken : (event.headers) ? event.headers['reset-token'] : null
  const method = event.methodArn.split('/').splice(2)

  if (!resetPasswordToken) {
    const statusMessage = 'Failed to verify password reset token as the password reset token was not provided'
    console.log(FUNC_NAME, statusMessage)
    return callback(null, generatePolicy(1, 'Deny', event.methodArn))
  } else {
    try {
      const decoded = jwt.verify(resetPasswordToken, process.env.JWT_RESET_PASSWORD_SECRET)
      if (decoded.tokenType !== 'reset') {
        const statusMessage = 'Failed to verify password reset token as it was invalid'
        console.log(FUNC_NAME, statusMessage)
        return callback(null, generatePolicy(1, 'Deny', event.methodArn))
      }

      const accessPermissions = decoded.permissions

      checkPermissions(accessPermissions, method).then((result) => {
        if (result) {
          const statusMessage = 'Successfully verified password reset token'
          console.log(FUNC_NAME, statusMessage)
          return callback(null, generatePolicy(accessPermissions.username, 'Allow', event.methodArn))
        } else {
          const statusMessage = 'Failed to verify password reset token as it was invalid'
          console.log(FUNC_NAME, statusMessage)
          return callback(null, generatePolicy(1, 'Deny', event.methodArn))
        }
      })
    } catch (error) {
      const statusMessage = 'Error encountered when verifying password reset token: ' + error
      console.log(FUNC_NAME, statusMessage)
      return callback(null, generatePolicy(1, 'Deny', event.methodArn))
    }
  }
}
