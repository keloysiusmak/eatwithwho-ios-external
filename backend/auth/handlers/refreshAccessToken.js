const constants = require('../lib/constants')
const jwt = require('jsonwebtoken')

const db = require('../database/connectDatabase')
const Account = (process.env.IS_TEST) ? require('../../defaults/account') : require('../models/account')

const FUNC_NAME = '[auth/refreshAccessToken]'
const outputHelper = require('../helpers/outputHelper')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  const refreshToken = event.headers['refresh-token']

  if (!refreshToken) {
    const statusMessage = 'Failed to refresh access token as the refresh token was not provided'
    return outputHelper.formatOutput(callback, 'refresh-token', FUNC_NAME, 400, statusMessage)
  }

  try {
    const decoded = jwt.verify(refreshToken, process.env.JWT_REFRESH_TOKEN_SECRET)
    const tokenSignedAt = decoded.tokenSignedAt
    const accountId = decoded.permissions.tokenId

    db.connect().then(() => {
      return Account.model.findOne({ _id: accountId, refreshToken: refreshToken })
    }).then((result) => {
      const account = result
      
      if (!account) {
        const statusMessage = 'Failed to refresh access token as it was invalid'
        return outputHelper.formatOutput(callback, 'refresh-token', FUNC_NAME, 401, statusMessage)
      }

      const accessToken =
      jwt.sign({ tokenType: 'access', permissions: decoded.permissions }, process.env.JWT_ACCESS_TOKEN_SECRET, { expiresIn: constants.ACCESS_TOKEN_EXPIRY })

      const statusMessage = 'Successfully refreshed access token'
      const output = { 'accessToken': accessToken }
      return outputHelper.formatOutput(callback, 'refresh-token', FUNC_NAME, 200, statusMessage, output)
    }).catch((error) => {
      const statusMessage = 'Error encountered when refreshing access token: ' + error
      return outputHelper.formatOutput(callback, 'refresh-token', FUNC_NAME, 500, statusMessage)
    })
  } catch (error) {
    const statusMessage = 'Failed to refresh access token as the refresh token was invalid'
    return outputHelper.formatOutput(callback, 'refresh-token', FUNC_NAME, 401, statusMessage)
  }
}
