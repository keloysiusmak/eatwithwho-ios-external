const constants = require('../lib/constants')
const jwt = require('jsonwebtoken')

const FUNC_NAME = '[auth/getApiToken]'
const outputHelper = require('../helpers/outputHelper')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  const apiKey = event.headers['api-key']

  if (!apiKey) {
    const statusMessage = 'Failed to get api token as the api key was not provided'
    return outputHelper.formatOutput(callback, 'api-key', FUNC_NAME, 400, statusMessage)
  }

  if (apiKey !== process.env.DEVELOPER_KEYS_API_KEY) {
    const statusMessage = 'Failed to get api token as the api key is wrong'
    return outputHelper.formatOutput(callback, 'api-key', FUNC_NAME, 401, statusMessage)
  }

  try {
    const token = jwt.sign({ tokenType: 'api' }, process.env.JWT_API_TOKEN_SECRET, { expiresIn: constants.API_TOKEN_EXPIRY })

    const statusMessage = 'Successfully obtained api token'
    const output = { apiToken: token }
    return outputHelper.formatOutput(callback, 'api-key', FUNC_NAME, 200, statusMessage, output)
  } catch (error) {
    const statusMessage = 'Error encountered when signing token: ' + error
    return outputHelper.formatOutput(callback, 'api-key', FUNC_NAME, 500, statusMessage)
  }
}
