'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const notificationSchema = new Schema({
  type: {
    type: Number,
    required: true
  },
  params: {
    type: Array,
    items: {
      type: String
    },
    default: []
  },
  recipients: {
    type: Array,
    items: {
      type: Schema.Types.ObjectId,
      ref: 'account'
    }
  },
  createdAt: {
    type: Number,
    required: true
  },
  modifiedAt: {
    type: Number,
    required: true
  }
})

module.exports.model = mongoose.model('Notification', notificationSchema)
