'use strict'

const FUNC_NAME = '[notification/getNotifications]'
const outputHelper = require('../helpers/outputHelper')

const db = require('../database/connectDatabase')
const Notification = (process.env.IS_TEST) ? require('../../defaults/notification') : require('../models/notification')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  const coupleId = event.pathParameters.id

  db.connect().then(() => {
    if (!coupleId || !outputHelper.isValidId(coupleId)) {
      let error = {
        statusCode: 400
      }
      throw error
    }

    let filter = {
      type: 1,
      recipients: coupleId
    }

    return Notification.model.find(filter).sort({ createdAt: -1 }).limit(5)
  }).then((result) => {
    const notifications = result

    const statusMessage = 'Successfully get notifications'
    const output = { notifications }
    return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 200, statusMessage, output)
  }).catch((error) => {
    if (typeof error !== 'string' && error.statusCode && error.statusCode === 400) {
      const statusMessage = 'Failed to get notifications as couple id was not provided'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 400, statusMessage)
    } else {
      const statusMessage = 'Error encountered when getting notifications: ' + error
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 500, statusMessage)
    }
  })
}
