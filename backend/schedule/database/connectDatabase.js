const mongoose = require('mongoose')
let cachedDb

module.exports.connect = () => {
  if (cachedDb) {
    return Promise.resolve(cachedDb)
  }

  return mongoose.connect(process.env.MONGO_DB, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true }) // keep the connection string in an env var
    .then(client => {
      cachedDb = client
      return cachedDb
    })
}
