'use strict'

const db = require('../database/connectDatabase')
const Activity = (process.env.IS_TEST) ? require('../../defaults/eatwith') : require('../models/eatwith')
const File = (process.env.IS_TEST) ? require('../../defaults/file') : require('../models/file')
const Guest = (process.env.IS_TEST) ? require('../../defaults/guest') : require('../models/guest')
const Payment = (process.env.IS_TEST) ? require('../../defaults/payment') : require('../models/payment')
const Schedule = (process.env.IS_TEST) ? require('../../defaults/schedule') : require('../models/schedule')

const FUNC_NAME = '[schedule/getSchedule]'
const scheduleHelper = require('../helpers/scheduleHelper')
const outputHelper = require('../helpers/outputHelper')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  const scheduleId = event.pathParameters.id

  db.connect().then(() => {
    if (!scheduleId || !outputHelper.isValidId(scheduleId)) {
      let error = {
        statusCode: 400
      }
      throw error
    }

    const filter = {
      _id: scheduleId
    }

    return Schedule.findOne(filter).populate(scheduleHelper.populateFilters)
  }).then((result) => {
    if (!result) {
      let error = {
        statusCode: 404
      }
      throw error
    }
    const schedule = result

    const statusMessage = 'Successfully get schedule'
    const output = { schedule }
    return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 200, statusMessage, output)
  }).catch((error) => {
    if (typeof error !== 'string' && error.statusCode && error.statusCode === 400) {
      const statusMessage = 'Failed to get schedule as schedule id was not provided'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 400, statusMessage)
    } if (typeof error !== 'string' && error.statusCode && error.statusCode === 404) {
      const statusMessage = 'Failed to get schedule as schedule with given schedule id does not exist'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 404, statusMessage)
    } else {
      const statusMessage = 'Error encountered when getting schedule: ' + error
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 500, statusMessage)
    }
  })
}
