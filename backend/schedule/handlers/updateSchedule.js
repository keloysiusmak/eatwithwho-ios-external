'use strict'

const db = require('../database/connectDatabase')
const Activity = (process.env.IS_TEST) ? require('../../defaults/eatwith') : require('../models/eatwith')
const File = (process.env.IS_TEST) ? require('../../defaults/file') : require('../models/file')
const Guest = (process.env.IS_TEST) ? require('../../defaults/guest') : require('../models/guest')
const Payment = (process.env.IS_TEST) ? require('../../defaults/payment') : require('../models/payment')
const Schedule = (process.env.IS_TEST) ? require('../../defaults/schedule') : require('../models/schedule')

const FUNC_NAME = '[schedule/updateSchedule]'
const scheduleHelper = require('../helpers/scheduleHelper')
const outputHelper = require('../helpers/outputHelper')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  const scheduleId = event.pathParameters.id
  const fieldsToUpdate = event.body

  db.connect().then(() => {
    if (!scheduleId || !outputHelper.isValidId(scheduleId) || !fieldsToUpdate || !outputHelper.isJson(fieldsToUpdate)) {
      let error = {
        statusCode: 400
      }
      throw error
    }

    const parsedFieldsToUpdate = JSON.parse(fieldsToUpdate)

    const permittedFields = ['date', 'advertisementHidePreferences']

    const newFields = {
      modifiedAt: Math.floor(Date.now() / 1000)
    }

    permittedFields.forEach((field) => {
      if ((field in parsedFieldsToUpdate)) {
        newFields[field] = parsedFieldsToUpdate[field]
      }
    })

    const filter = {
      _id: scheduleId
    }

    if (!(scheduleHelper.isValidSchedule(newFields))) {
      let error = {
        statusCode: 400,
        subStatusCode: 1
      }
      throw error
    }

    return Schedule.findOneAndUpdate(filter, newFields, { new: true }).populate(scheduleHelper.populateFilters)
  }).then((result) => {
    if (!result) {
      let error = {
        statusCode: 404
      }
      throw error
    }
    const schedule = result

    const statusMessage = 'Successfully updated schedule'
    const output = { schedule }
    return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 200, statusMessage, output)
  }).catch((error) => {
    if (typeof error !== 'string' && error.statusCode && error.statusCode === 400 && error.subStatusCode && error.subStatusCode === 1) {
      const statusMessage = 'Failed to update schedule as fields were invalid'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 400) {
      const statusMessage = 'Failed to update schedule as schedule id or fields was not provided'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 404) {
      const statusMessage = 'Failed to update schedule as schedule with given schedule id does not exist'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 404, statusMessage)
    } else {
      const statusMessage = 'Error encountered when updating schedule: ' + error
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 500, statusMessage)
    }
  })
}
