const outputHelper = require('./outputHelper')

function isValidSchedule (schedule) {
  if (schedule.name && (schedule.name.length < 2 || schedule.name.length > 60)) {
    return false
  }
  if (schedule.date && Number.isNaN(Number(schedule.date))) {
    return false
  }
  if (!Number.isNaN(Number(schedule.date)) && (schedule.date < 1)) {
    return false
  }
  return true
}

/*
const populateFilters = {
  path: 'activities',
  model: Activity,
  match: { isDeleted: false },
  populate: [{
    path: 'files',
    model: File
  }]
}*/

module.exports = {
  isValidSchedule,
  //populateFilters
}
