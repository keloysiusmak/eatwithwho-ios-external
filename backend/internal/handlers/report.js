'use strict'

const FUNC_NAME = '[internal/report]'
const internalHelper = require('../helpers/internalHelper')
const outputHelper = require('../helpers/outputHelper')

const db = require('../database/connectDatabase')
const Internal = (process.env.IS_TEST) ? require('../../defaults/internal') : require('../models/internal')

module.exports.main = (event, context, callback) => {

  context.callbackWaitsForEmptyEventLoop = false
  const fieldsToAdd = event.body
  const now = Math.floor(Date.now() / 1000)

  db.connect().then(() => {
    if (!fieldsToAdd || !outputHelper.isJson(fieldsToAdd)) {
      let error = {
        statusCode: 400
      }
      throw error
    }

    let parsedFieldsToAdd = JSON.parse(fieldsToAdd)

    const permittedFields = Internal.permittedFields
    Object.keys(parsedFieldsToAdd).filter(key => {
      if ((!(permittedFields.includes(key))) || (parsedFieldsToAdd[key] === null)) {
        delete parsedFieldsToAdd[key]
      }
    })
    parsedFieldsToAdd.createdAt = now

    if (!(internalHelper.isValidInternal(Internal.model, true, parsedFieldsToAdd))) {
      let error = {
        statusCode: 400,
        subStatusCode: 1
      }
      throw error
    }

    return Internal.model.create(parsedFieldsToAdd)
  }).then(() => {
    const statusMessage = 'Successfully added internal message'
    return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 200, statusMessage)
  }).catch((error) => {
    if (typeof error !== 'string' && error.statusCode && error.statusCode === 400 && error.subStatusCode && error.subStatusCode === 1) {
      const statusMessage = 'Failed to add internal message as fields were invalid'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 400) {
      const statusMessage = 'Failed to add internal message as required fields was not provided'
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 400, statusMessage)
    } else {
      const statusMessage = 'Error encountered when adding internal message: ' + error
      return outputHelper.formatOutput(callback, 'access-token', FUNC_NAME, 500, statusMessage)
    }
  })
}
