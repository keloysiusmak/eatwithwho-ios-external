function isValidInternal (Internal, isCreate, internal) {
  let errors = new Internal(internal).validateSync()
  let error = (errors && !isCreate) ? Object.keys(internal).filter(key => errors.errors[key]).length > 0 : errors
  if (error) {
    return false
  } else {
    return true
  }	
}

module.exports = {
  isValidInternal
}
