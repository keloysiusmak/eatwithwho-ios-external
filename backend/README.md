# eatwithwho-backend

[![Build Status](https://travis-ci.com/keloysiusmak/eatwithwho-backend.svg?token=GWXiGcQF9aR6uxYrpWRw&branch=master)](https://travis-ci.com/keloysiusmak/eatwithwho-backend)

## Installation

1.  Make sure _**npm**_ and _**mongoDB**_ is installed on your computer.
2. Install serverless with `npm install -g serverless`
3.  Go to https://github.com/keloysiusmak/eatwithwho-backend and clone the repo into a directory of your choice.
4.  Create a new database in MongoDB (use Robo3T for a GUI, or connect to a running instance of mongoDB using `mongo`).
5.  Make a copy of _**&lt;INSTALLED_DIR>/config/config-template.yml**_ into _**&lt;INSTALLED_DIR>/config/config.yml**_ .
6.  Replace the value of _**&lt;MONGO DB URL>**_ with the URL for your mongo installation.
7.  Replace the fields _**&lt;API KEY>**_, _**&lt;API TOKEN SECRET>**_, _**&lt;ACCESS TOKEN SECRET>**_ and _**&lt;REFRESH TOKEN SECRET>**_ with your own values.
8.  Obtain a copy of _**&lt;AWS_SECRET_ACCESS_KEY>**_ and _**&lt;AWS_ACCESS_KEY_ID>**_ from your manager and replace the corresponding value.

## Running

1.  Go into the directory you copied the Back End into.
2.  Run _**npm install**_. 
3.  Start mongoDB by running _**mongod**_.
4.  Run _**npm run offline**_ to run the Back End locally.

# Architecture

The Back End is divided into several Services, each Service handling a particular portion of the eatwithwho API. For instance, the Schedule Service handles all functionality pertaining to schedules, such as Create/Read/Update/Delete _(CRUD)_. There are several key components of each Service, and they are detailed as such.

## /database

Inside this folder lies the database connection functionality. It is hardly modified, and is identical across all Services.

## /handlers

This is where all the functionality lies. Each function is packed in one particular file, named after the function. 

## /helpers

Frequently used code across functions in a particular Service are extracted into a helper file to ensure consistency. An output helper file is also a staple of this helpers folder, to assist with output formatting across all Services. The output helper file is identical across all Services.

## /models 

Herein lies all the Mongoose models relevant for a particular Service. These models are duplicated from a central repository of models, and should not be directly modified without modifying the central repository of models, to ensure consistency across all models in different Services.

## serverless.yml

This file, not to be confused with the parent **serverless.yml** residing in the root directory, contains information with regards to the stack deployment on AWS Lambda.

There exists a separate **serverless.yml** residing in the root directory of the application. This **serverless.yml** is the master controller of the serverless functions in the offline mode, and should be used when running the Back End locally.

A typical **serverless.yml** entry looks as such.

    getSchedule:

        handler: schedule/handlers/getSchedule.main

        events:

          - http:

              path: schedule/{id}

              method: get

              cors: true

              authorizer: ${self:custom.authorizer.accessToken}

              request:

                parameters:

                   paths:

                     id: true

The first line specifies the name of the entry, in this instance, **getSchedule**. The name of the entry should match the name of the handler.

The handler property is then specified to be the **main** function of the handler. We use main in the interim, to provide the leeway for us to add more functionality in the future if the need arises.

Under http, and under path, the method and URL that the function listens to is specified.  The URL of the host is prepended to this, so running this locally would yield a URL similar to

    GET http://localhost:8000/schedule/123

The authorizer specifies which authorizer function to use. For generic routes not requiring permission based control, we use the apiToken authorizer while for anything else, we use the accessToken authorizer.

In parameters and paths, we see that **id** is set to be true. This signifies that the **{id}** in the path is accessible in the function via **events.pathParameters.id** _(more on this later)._

# Bash Scripts

We have in place several bash scripts to automate certain processes and also to ensure data uniformity.

## /deploy.sh 

This script contains information needed for us to automate the deployment of our backend onto Lamda. **Do not modify this script.**

## /lint.sh

This script allows us to automatically run the linter on all the various Services.

## /update-defaults.sh

This allows us to update the output helper and any other file that is required consistently across all Services.

## /update-models.sh

This script allows us to deploy the same Mongoose model across all the Services, ensuring that all services use the same Mongoose model.

## /tests.sh

This script allows us to run all our tests, while supplying the tests with the appropriate environment variables and configurations.

# Response Codes

These are some common response codes we use in eatwithwho. It can be accessed from the Front End via the **statusCode** field in the response. Each error comes with an associated error message, along with any output from the functions. This can be found in the **error** field and the **message** field in the response respectively.

## 200

200 signifies that a request has successfully completed without any errors, and that the function is running as intended.

## 400

400 signifies that a request input is malformed, from reasons such as an invalid JSON to an invalid Mongoose Id. It signifies that the fault of failure lies with the client, not the server.

## 401

401 signifies that the supplied authentication token does not have sufficient permissions to execute the request.

## 404

404 signifies that the requested object is not found.

## 409

409 signifies that there exists a conflict between the suggested change in state and the presently existing state. For instance, emails that should be unique to a particular friend cannot be reused for another friend.

## 500

500 signifies that there has been an error with the server. This could persist in the form of badly written functionality, malfunctioning components or a combination of various factors.

# Anatomy of a Handler

## Dependencies

Each handler is consistently written to ensure uniformity across all handlers for code consistency. For this section, we will be referring to the **updateSchedule** handler.

We start each handler by importing the database connection helper, along with any mongoose models.

    const db = require('../database/connectDatabase')

    const Activity = (process.env.IS_TEST) ? require('../../defaults/eatwith') : require('../models/eatwith')

    const File = (process.env.IS_TEST) ? require('../../defaults/file') : require('../models/file')

Next, the _FUNC_NAME_ is specified. The _FUNC_NAME_ is used for logging using our output helper. The _FUNC_NAME_ should be written in this format:

    [SERVICE_NAME/FUNC_NAME]

Following which, the output helper and the Service specific helper should be imported, for use throughout the handler.

## Function Body

The expected export name for all functions should be **main**. The function will eventually return a promise, and through promise chaining through out the function body, we finally return an output with the formatted response.

Any path parameters in the URL of the request can be accessed using **pathParameters**. For instance, **id** can be accessed using the **id** property of **pathParameters** for this following request. For more information, please refer to the section under _Anatomy of a Handler_.

    PUT /schedule/:id

Any JSON body for PUT and POST requests can be accessed via **event.body**. Our naming convention for POST requests are fieldsToAdd, while our naming conventions for PUT requests are fieldsToUpdate

**db.connect()** is used to open up a connection transaction with the database, to allow for CRUD requests to and from the database.

We first start by validating if the request is a valid request by checking if a valid Mongoose Id is supplied _(if necessary)_, and if a valid JSON is supplied for PUT and POST requests. Should any one of these two not be met, we return an **400** error. This is usually done in this form.

    if (!scheduleId || !scheduleHelper.isValidId(scheduleId) || !fieldsToUpdate || !scheduleHelper.isJson(fieldsToUpdate)) {

        let error = {

          statusCode: 400

        }

        throw error

    }

For PUT and POST requests, we have several permitted fields that are allowed to be passed on for further processing. This is usually specified in **permittedFields**, as an array of strings representing the valid keys. POST requests have a further field called **requiredFields**, which must be present, if not an error with status code **400** is thrown.

When performing **find** or **findOne** operations on the various Mongoose models, this involves returning a populated object. To ensure the consistency of the returned objects, we obtain the filters for both **select()** and **populate()** from the helper specific to the particular Service. This ensures the consistency of the returned objects.

The returned object should be wrapped in a larger JSON, with the name of the model as the key. For instance, the Schedule object should be returned under the key **schedule** in the JSON object.

## Error Handling

Errors can happen anywhere in the message body, or be thrown by validation checks. This errors should be handled in a catch block at the end of the promise, and each error should be individually handled using the output helper. All other errors should fall into the category with error code of **500**.

Status messages are our way of letting the client know what the error is, and should be clearly worded to be as articulate as possible.
