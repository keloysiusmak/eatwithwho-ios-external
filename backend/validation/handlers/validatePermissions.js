'use strict'

const db = require('../database/connectDatabase')
const Account = (process.env.IS_TEST) ? require('../../defaults/account') : require('../models/account')
const Activity = (process.env.IS_TEST) ? require('../../defaults/eatwith') : require('../models/eatwith')
const Budget = (process.env.IS_TEST) ? require('../../defaults/budget') : require('../models/budget')
const Couple = (process.env.IS_TEST) ? require('../../defaults/couple') : require('../models/couple')
const Guest = (process.env.IS_TEST) ? require('../../defaults/guest') : require('../models/guest')
const Payment = (process.env.IS_TEST) ? require('../../defaults/payment') : require('../models/payment')
const Permission = (process.env.IS_TEST) ? require('../../defaults/permission') : require('../models/permission')
const Record = (process.env.IS_TEST) ? require('../../defaults/record') : require('../models/record')
const Schedule = (process.env.IS_TEST) ? require('../../defaults/schedule') : require('../models/schedule')

const FUNC_NAME = '[validation/validatePermissions]'
const outputHelper = require('../helpers/outputHelper')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false

  db.connect().then(() => {
    const populateFilters = [{
      path: 'accountId',
      model: Account
    }, {
      path: 'guestId',
      model: Guest
    }]
    return Permission.find().populate(populateFilters)
  }).then((result) => {
    const permissionDocuments = result

    permissionDocuments.forEach(doc => {
      if (doc.accountId) {
        const accountId = JSON.parse(JSON.stringify(doc.accountId._id))
        const permissions = doc.permissions

        Couple.findOne({ coupleGuests: doc.accountId.guest }).then((couple) => {
          return Promise.all([
            couple._id,
            Schedule.findOne({ _id: couple.schedule }).populate({
              path: 'activities',
              model: Activity
            }),
            Budget.findOne({ _id: couple.budget }).populate([{
              path: 'records',
              model: Record
            }, {
              path: 'payments',
              model: Payment
            }])])
        }).then((result) => {
          const couple = result[0]
          const schedule = result[1]
          const budget = result[2]
          if (!permissions.couple.includes(JSON.parse(JSON.stringify(couple._id)))) {
            console.log('[validation/validatePermissions] - [accountId ' + accountId + '] - Error: Missing Couple Id (' + couple._id + ')')
          }
          if (!permissions.account.includes(accountId)) {
            console.log('[validation/validatePermissions] - [accountId ' + accountId + '] - Error: Missing Account Id (' + accountId + ')')
          }
          if (!permissions.budget.includes(JSON.parse(JSON.stringify(budget._id)))) {
            console.log('[validation/validatePermissions] - [accountId ' + accountId + '] - Error: Missing Budget Id  (' + budget._id + ')')
          }
          if (!permissions.schedule.includes(JSON.parse(JSON.stringify(schedule._id)))) {
            console.log('[validation/validatePermissions] - [accountId ' + accountId + '] - Error: Missing Schedule Id (' + schedule._id + ')')
          }
          if (schedule.activities && schedule.activities.length > 0) {
            schedule.activities.forEach(eatwith => {
              if (!permissions.eatwith.includes(JSON.parse(JSON.stringify(eatwith._id)))) {
                console.log('[validation/validatePermissions] - [accountId ' + accountId + '] - Error: Missing Activity Id (' + eatwith._id + ')')
              }
              if (eatwith.files && eatwith.files.length > 0) {
                eatwith.files.forEach(file => {
                  if (!permissions.file.includes(JSON.parse(JSON.stringify(file)))) {
                    console.log('[validation/validatePermissions] - [accountId ' + accountId + '] - Error: Missing File Id (' + file + ')')
                  }
                })
              }
            })
            if (budget.records && budget.records.length > 0) {
              budget.records.forEach(record => {
                if (!permissions.record.includes(JSON.parse(JSON.stringify(record._id)))) {
                  console.log('[validation/validatePermissions] - [accountId ' + accountId + '] - Error: Missing Record Id (' + record._id + ')')
                }
              })
              if (budget.payments && budget.payments.length > 0) {
                budget.payments.forEach(payment => {
                  if (!permissions.payment.includes(JSON.parse(JSON.stringify(payment)))) {
                    console.log('[validation/validatePermissions] - [accountId ' + accountId + '] - Error: Missing Payment Id (' + payment + ')')
                  }
                })
              }
            }
            if (couple.guests && couple.guests.length > 0) {
              couple.guests.forEach(guest => {
                if (!permissions.guest.includes(JSON.parse(JSON.stringify(guest._id)))) {
                  console.log('[validation/validatePermissions] - [accountId ' + accountId + '] - Error: Missing Guest Id (' + guest._id + ')')
                }
              })
            }
          }
        })
      } else if (doc.guestId) {
        const guestId = JSON.parse(JSON.stringify(doc.guestId._id))
        const permissions = doc.permissions

        //
      }
    })

    const statusMessage = 'Successfully validated permissions'
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 200, statusMessage)
  }).catch((error) => {
    const statusMessage = 'Error encountered when validating permissions: ' + error
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 500, statusMessage)
  })
}
