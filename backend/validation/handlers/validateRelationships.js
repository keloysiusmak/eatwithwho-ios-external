'use strict'

const db = require('../database/connectDatabase')
const Activity = (process.env.IS_TEST) ? require('../../defaults/eatwith') : require('../models/eatwith')
const Budget = (process.env.IS_TEST) ? require('../../defaults/budget') : require('../models/budget')
const Payment = (process.env.IS_TEST) ? require('../../defaults/payment') : require('../models/payment')
const Couple = (process.env.IS_TEST) ? require('../../defaults/couple') : require('../models/couple')
const File = (process.env.IS_TEST) ? require('../../defaults/file') : require('../models/file')
const Guest = (process.env.IS_TEST) ? require('../../defaults/guest') : require('../models/guest')
const Record = (process.env.IS_TEST) ? require('../../defaults/record') : require('../models/record')
const Schedule = (process.env.IS_TEST) ? require('../../defaults/schedule') : require('../models/schedule')
const todo = (process.env.IS_TEST) ? require('../../defaults/todo') : require('../models/todo')

const FUNC_NAME = '[validation/validateRelationships]'
const outputHelper = require('../helpers/outputHelper')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false

  db.connect().then(() => {
    return Promise.all([Activity.find(), File.find(), Guest.find(), Record.find(), todo.find(), Payment.find()])
  }).then((result) => {
    const activities = result[0]
    const files = result[1]
    const guests = result[2]
    const records = result[3]
    const todos = result[4]
    const payments = result[5]

    activities.forEach(eatwith => {
      Schedule.find({ activities: JSON.parse(JSON.stringify(eatwith._id)) }).then((result) => {
        if (result.length !== 1) {
          console.log('[validation/validateRelationships] - [eatwithId ' + JSON.parse(JSON.stringify(eatwith._id)) + '] - Error: Incorrect many-to-one relationship (Count: ' + result.count + ')')
        }
      })
    })
    files.forEach(file => {
      Activity.find({ files: JSON.parse(JSON.stringify(file._id)) }).then((result) => {
        if (result.length !== 1) {
          console.log('[validation/validateRelationships] - [fileId ' + JSON.parse(JSON.stringify(file._id)) + '] - Error: Incorrect many-to-one relationship (Count: ' + result.count + ')')
        }
      })
    })
    guests.forEach(guest => {
      Couple.find({ guests: JSON.parse(JSON.stringify(guest._id)) }).then((result) => {
        if (result.length !== 1) {
          console.log('[validation/validateRelationships] - [guestId ' + JSON.parse(JSON.stringify(guest._id)) + '] - Error: Incorrect many-to-one relationship (Count: ' + result.count + ')')
        }
      })
    })
    records.forEach(record => {
      Budget.find({ records: JSON.parse(JSON.stringify(record._id)) }).then((result) => {
        if (result.length !== 1) {
          console.log('[validation/validateRelationships] - [recordId ' + JSON.parse(JSON.stringify(record._id)) + '] - Error: Incorrect many-to-one relationship (Count: ' + result.count + ')')
        }
      })
    })
    payments.forEach(payment => {
      Budget.find({ payments: JSON.parse(JSON.stringify(payment._id)) }).then((result) => {
        if (result.length !== 1) {
          console.log('[validation/validateRelationships] - [paymentId ' + JSON.parse(JSON.stringify(payment._id)) + '] - Error: Incorrect many-to-one relationship (Count: ' + result.count + ')')
        }
      })
    })
    return Promise.all([Budget.find(), Couple.find(), Schedule.find()])
  }).then((result) => {
    const budgets = result[0]
    const couples = result[1]
    const schedules = result[2]

    budgets.forEach(budget => {
      if (budget.records && budget.records.length > 0) {
        budget.records.forEach(recordId => {
          Record.findOne({ _id: recordId }).then((result) => {
            if (!result) {
              console.log('[validation/validateRelationships] - [budgetId ' + JSON.parse(JSON.stringify(budget._id)) + '] - Error: Incorrect one-to-many relationship (Invalid recordId: ' + recordId + ')')
            }
          })
        })
      }
    })

    couples.forEach(couple => {
      if (!couple.coupleGuests || couple.coupleGuests.length !== 2) {
        console.log('[validation/validateRelationships] - [coupleId ' + JSON.parse(JSON.stringify(couple._id)) + '] - Error: Couple does not have two guests (Count: ' + couple.coupleGuests.length + ')')
      }
      if (couple.coupleGuests && couple.coupleGuests.length > 0) {
        couple.coupleGuests.forEach(guestId => {
          Guest.findOne({ _id: guestId }).then((result) => {
            if (!result) {
              console.log('[validation/validateRelationships] - [coupleId ' + JSON.parse(JSON.stringify(couple._id)) + '] - Error: Incorrect one-to-many relationship (Invalid guestId: ' + guestId + ')')
            }
          })
        })
      }

      if (couple.guests && couple.guests.length > 0) {
        couple.guests.forEach(guestId => {
          Guest.findOne({ _id: guestId }).then((result) => {
            if (!result) {
              console.log('[validation/validateRelationships] - [coupleId ' + JSON.parse(JSON.stringify(couple._id)) + '] - Error: Incorrect one-to-many relationship (Invalid guestId: ' + guestId + ')')
            }
          })
        })
      }
    })

    schedules.forEach(schedule => {
      if (schedule.activities && schedule.activities.length > 0) {
        schedule.activities.forEach(eatwithId => {
          Activity.findOne({ _id: eatwithId }).then((result) => {
            if (!result) {
              console.log('[validation/validateRelationships] - [scheduleId ' + JSON.parse(JSON.stringify(schedule._id)) + '] - Error: Incorrect one-to-many relationship (Invalid eatwithId: ' + eatwithId + ')')
            }
          })
        })
      }
    })

    const statusMessage = 'Successfully validated relationships'
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 200, statusMessage)
  }).catch((error) => {
    const statusMessage = 'Error encountered when validating relationships: ' + error
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 500, statusMessage)
  })
}
