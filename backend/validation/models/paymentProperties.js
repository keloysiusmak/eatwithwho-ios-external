'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const paymentSchema = new Schema({
  desc: {
    type: String,
    default: ''
  },
  subDesc: {
    type: String,
    default: ''
  },
  percentageValue: {
    type: Number,
    default: 0
  },
  fixedValue: {
    type: Number,
    default: 0
  },
  min: {
    type: Number,
    default: 0
  },
  max: {
    type: Number,
    default: 0
  }
})

module.exports.model = mongoose.model('Payment', paymentSchema)
