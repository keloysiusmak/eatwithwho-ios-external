'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const paymentSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  desc: {
    type: String
  },
  isDirectory: {
    type: Boolean,
    default: false
  },
  type: {
    type: String,
    enum: ['creditcard', 'cash', 'debitcard'],
    default: 'creditcard'
  },
  url: {
    type: String
  },
  cashback: {
    type: Schema.Types.ObjectId,
    ref: 'paymentProperties'
  },
  rebate: {
    type: Schema.Types.ObjectId,
    ref: 'paymentProperties'
  },
  properties: {
    type: Array,
    items: {
      type: Schema.Types.ObjectId,
      ref: 'paymentProperty'
    }
  },
  createdAt: {
    type: Number,
    required: true
  },
  modifiedAt: {
    type: Number,
    required: true
  }
})

module.exports.model = mongoose.model('Payment', paymentSchema)
