'use strict'

const defaults = require('../lib/defaults')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const vendorSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    maxlength: 60
  },
  desc: {
    type: String,
    required: true,
    trim: true,
    maxlength: 1000
  },
  countryCode: {
    type: String,
    default: '65',
    enum: defaults.countryCodes,
    required: () => this.number
  },
  number: {
    type: Number,
    default: 0,
    max: 9999999999,
    required: () => this.countryCode
  },
  email: {
    type: String,
    default: "",
    trim: true,
    validate: {
      validator: (email) => /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(email)
    }
  },
  websiteUrl: {
    type: String,
    default: null
  },
  imageUrls: {
    type: Array,
    default: [],
    items: {
      type: String
    }
  },
  createdAt: {
    type: Number,
    required: true
  },
  modifiedAt: {
    type: Number,
    required: true
  }
})

module.exports.model = mongoose.model('Vendor', vendorSchema)
module.exports.selectFields = 'name desc countryCode number email websiteUrl imageUrls createdAt'
