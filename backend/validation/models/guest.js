'use strict'

const defaults = require('../lib/defaults')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const additionalGuestSchema = new Schema({
  title: {
    type: String,
    enum: defaults.titles,
    required: true
  },
  firstName: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 30,
    trim: true
  },
  lastName: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 30,
    trim: true
  },
  babyChair: {
    type: Boolean,
    required: true,
    default: false
  },
  dietaryPreference: {
    type: String,
    default: 'unspecified'
  }
})

const guestSchema = new Schema({
  title: {
    type: String,
    enum: ['mr', 'mrs', 'ms', 'dr'],
    required: true
  },
  firstName: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 30,
    trim: true
  },
  lastName: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 30,
    trim: true
  },
  number: {
    type: Number,
    default: 0,
    max: 9999999999,
    required: () => this.countryCode
  },
  email: {
    type: String,
    default: "",
    trim: true,
    validate: {
      validator: (email) => /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(email)
    }
  },
  additionalGuests: [additionalGuestSchema],
  dietaryPreference: {
    type: String,
    default: 'unspecified'
  },
  addressLine1: {
    type: String,
    default: '',
    maxlength: 60,
    required: () => this.addressLine2 || this.postalCode
  },
  addressLine2: {
    type: String,
    maxlength: 60,
    default: ''
  },
  countryCode: {
    type: String,
    default: '65',
    enum: defaults.countryCodes,
    required: () => this.number
  },
  postalCode: {
    type: Number,
    validate: {
      validator: (code) => code >= 0 && code < 1000000
    },
    required: () => this.addressLine1
  },
  dietaryPreference: {
    type: String,
    default: 'none'
  },
  note: {
    type: String,
    default: '',
    maxlength: 280
  },
  status: {
    type: String,
    default: 'pending',
    enum: ['pending', 'attending', 'notattending']
  },
  joinedWithLink: {
    type: Boolean,
    default: false
  },
  deletedAt: {
    type: Number,
    required: true,
    default: 0
  },
  createdAt: {
    type: Number,
    required: true,
    default: Math.floor(Date.now() / 1000)
  },
  modifiedAt: {
    type: Number,
    required: true,
    default: Math.floor(Date.now() / 1000)
  }
})

module.exports.model = mongoose.model('Guest', guestSchema)
module.exports.permittedFields = ['title', 'firstName', 'lastName', 'email', 'number', 'status', 'note', 'addressLine1', 'addressLine2', 'dietaryPreference', 'additionalGuests', 'postalCode', 'countryCode']
module.exports.selectFilters = 'title firstName lastName countryCode number email addressLine1 addressLine2 postalCode note status dietaryPreference additionalGuests _id joinedWithLink modifiedAt deletedAt'
