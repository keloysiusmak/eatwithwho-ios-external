'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const fileSchema = new Schema({
  fileStorageName: {
    type: String,
    required: true
  },
  fileName: {
    type: String,
    required: true
  },
  fileSize: {
    type: Number,
    required: true,
    default: '0'
  },
  createdAt: {
    type: Number,
    required: true
  },
  modifiedAt: {
    type: Number,
    required: true
  }
})

module.exports.model = mongoose.model('File', fileSchema)
