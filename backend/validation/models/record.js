'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const categories = ['transport', 'photography', 'videography', 'banquet', 'flowers', 'clothing', 'makeup', 'gifts', 'decoration', 'others']

const recordSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  type: {
    type: String,
    enum: ['expenditure', 'income'],
    default: 'expenditure',
    required: true
  },
  category: {
    type: String,
    enum: categories,
    default: 'others',
    required: true
  },
  value: {
    type: Number,
    required: true,
    default: 0
  },
  partialValue: {
    type: Number,
    required: true,
    default: 0
  },
  isPaid: {
    type: Boolean,
    default: true
  },
  dueDate: {
    type: Number,
    default: 0
  },
  payment: {
    type: Schema.Types.ObjectId,
    ref: 'Payment',
    default: null
  },
  date: {
    type: Number,
    required: true
  },
  note: {
    type: String,
    default: ''
  },
  deletedAt: {
    type: Number,
    required: true,
    default: 0
  },
  createdAt: {
    type: Number,
    required: true
  },
  modifiedAt: {
    type: Number,
    required: true
  }
})

module.exports.model = mongoose.model('Record', recordSchema)
module.exports.categories = categories
