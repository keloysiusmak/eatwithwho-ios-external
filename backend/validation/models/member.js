'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const guestSchema = new Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  reference: {
    type: String,
    required: true,
    unique: true,
    length: 8
  },
  gender: {
    type: String,
    enum: ['male', 'female', 'unspecified'],
    required: true,
    default: 'unspecified'
  },
  number: {
    type: String
  },
  email: {
    type: String
  },
  additionalGuests: {
    type: Number,
    default: 0
  },
  note: {
    type: String,
    default: ''
  },
  status: {
    type: String,
    default: 'pending',
    enum: ['pending', 'attending', 'notattending']
  },
  imageType: {
    type: String,
    default: 'none',
    required: true,
    enum: ['none', 'jpg', 'png', 'jpeg']
  },
  isDeleted: {
    type: Boolean,
    required: true,
    default: false
  },
  deletedAt: {
    type: Number,
    required: true,
    default: 0
  },
  createdAt: {
    type: Number,
    required: true
  },
  modifiedAt: {
    type: Number,
    required: true
  }
})

module.exports.model = mongoose.model('Guest', guestSchema)
