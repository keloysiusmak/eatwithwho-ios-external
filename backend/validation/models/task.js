'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const todoSchema = new Schema({
  name: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 60,
    trim: true
  },
  status: {
    type: String,
    required: true,
    enum: ['completed', 'inprogress', 'notstarted']
  },
  dueDate: {
    type: Number,
    default: 0
  },
  deletedAt: {
    type: Number,
    required: true,
    default: 0
  },
  createdAt: {
    type: Number,
    required: true,
    default: Math.floor(Date.now() / 1000)
  },
  modifiedAt: {
    type: Number,
    required: true,
    default: Math.floor(Date.now() / 1000)
  }
})

module.exports.model = mongoose.model('todo', todoSchema)
module.exports.permittedFields = ['name', 'status', 'dueDate']
module.exports.selectFilters = 'name status dueDate _id modifiedAt deletedAt'
