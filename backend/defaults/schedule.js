'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const scheduleSchema = new Schema({
  date: {
    type: Number,
    required: true
  },
  activities: {
    type: Array,
    items: {
      type: Schema.Types.ObjectId,
      ref: 'eatwith',
      required: true
    }
  },
  advertisementHidePreferences: {
    type: Array,
    items: {
      type: String
    },
    default: []
  },
  createdAt: {
    type: Number,
    required: true
  },
  modifiedAt: {
    type: Number,
    required: true
  }
})

module.exports.model = mongoose.model('Schedule', scheduleSchema)
