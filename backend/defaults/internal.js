'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const internalSchema = new Schema({
  accountId: {
    type: Schema.Types.ObjectId,
    required: true
  },
  type: {
    type: String,
    required: true,
    enum: ['bug', 'feature']
  },
  route: {
    type: String,
    maxlength: 120,
    required: true
  },
  message: {
    type: String,
    maxlength: 600,
    required: true
  },
  createdAt: {
    type: Number,
    required: true
  }
})

module.exports.model = mongoose.model('Internal', internalSchema)
module.exports.permittedFields = ['accountId', 'type', 'route', 'message', 'createdAt']
