'use strict'

const defaults = require('../lib/defaults')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const accountSchema = new Schema({
  username: {
    type: String,
    required: true,
    minlength: 6,
    maxlength: 30,
    trim: true
  },
  password: {
    type: String,
    required: true
  },
  refreshToken: {
    type: String,
    default: null,
  },
  lang: {
    type: String,
    enum: defaults.languages,
    default: 'en',
    required: true
  },
  title: {
    type: String,
    enum: defaults.titles,
    required: true
  },
  firstName: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 30,
    trim: true
  },
  lastName: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 30,
    trim: true
  },
  email: {
    type: String,
    required: true,
    unique: true,
    default: "",
    trim: true,
    validate: {
      validator: (email) => email === "" || /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(email)
    }
  },
  countryCode: {
    type: String,
    default: '65',
    enum: defaults.countryCodes,
    required: () => this.number
  },
  number: {
    type: Number,
    default: 0,
    max: 9999999999,
    required: () => this.countryCode
  },
  type: {
    type: String,
    enum: ['couple']
  },
  typeId: {
    type: Schema.Types.ObjectId
  },
  createdAt: {
    type: Number,
    required: true,
    default: Math.floor(Date.now() / 1000)
  },
  modifiedAt: {
    type: Number,
    required: true,
    default: Math.floor(Date.now() / 1000)
  }
})

module.exports.schema = accountSchema
module.exports.model = mongoose.model('Account', accountSchema)
module.exports.permittedFields = ['title', 'firstName', 'lastName', 'password', 'countryCode', 'number', 'username', 'email', 'lang']
module.exports.selectFilters = 'username title firstName lastName countryCode number type typeId lang email _id createdAt modifiedAt'
