'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const advertisementSchema = new Schema({
  title: {
    type: String,
    required: true,
    trim: true
  },
  desc: {
    type: String,
    required: true,
    trim: true
  },
  imageUrl: {
    type: String,
    trim: true,
    default: null
  },
  targetUrl: {
    type: String,
    required: true,
    trim: true
  },
  lightViews: {
    type: Number,
    default: 0
  },
  views: {
    type: Number,
    default: 0
  },
  createdAt: {
    type: Number,
    required: true
  },
  modifiedAt: {
    type: Number,
    required: true
  }
})

module.exports.model = mongoose.model('Advertisement', advertisementSchema)
module.exports.lightSelectFilters = 'title targetUrl'
module.exports.selectFilters = 'title desc imageUrl targetUrl'

