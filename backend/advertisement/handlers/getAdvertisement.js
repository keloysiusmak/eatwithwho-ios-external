'use strict'

const db = require('../database/connectDatabase')
const Advertisement = (process.env.IS_TEST) ? require('../../defaults/advertisement') : require('../models/advertisement')

const FUNC_NAME = '[advertisement/getAdvertisement]'
const outputHelper = require('../helpers/outputHelper')

module.exports.main = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  const fields = event.body
  let parsedFields

  db.connect().then(() => {
    if (!fields || !outputHelper.isJson(fields)) {
      let error = {
        statusCode: 400
      }
      throw error
    }

    parsedFields = JSON.parse(fields)

    return Advertisement.model.countDocuments();
  }).then((count) => {
    if (count === 0) {
      let error = {
        statusCode: 404
      }
      throw error
    }
    var random = Math.floor(Math.random() * count);
    return Advertisement.model.findOne().skip(random);
  }).then((result) => {
    let newFields
    if (parsedFields.light) {
      newFields = {
        $inc: { views: 1 }
      }
    } else {
      newFields = {
        $inc: { lightViews: 1 }
      }
    }

    return Advertisement.model.findOneAndUpdate({_id: result._id}, newFields, {new: true}).select((parsedFields.light) ? Advertisement.lightSelectFilters : Advertisement.selectFilters)
  }).then((result) => {
    const advertisement = result

    const statusMessage = 'Successfully get advertisement'
    const output = { advertisement, advertisementType: 'default' }
    return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 200, statusMessage, output)
  }).catch((error) => {
    if (typeof error !== 'string' && error.statusCode && error.statusCode === 400) {
      const statusMessage = 'Failed to get advertisement as fields were invalid'
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 400, statusMessage)
    } else if (typeof error !== 'string' && error.statusCode && error.statusCode === 404) {
      const statusMessage = 'Failed to get advertisement as no advertisements exist'
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 404, statusMessage)
    } else {
      const statusMessage = 'Error encountered when getting advertisement: ' + error
      return outputHelper.formatOutput(callback, 'api-token', FUNC_NAME, 500, statusMessage)
    }
  })
}
