/*
 *  Copyright (c) 2014, Facebook, Inc.
 *  All rights reserved.
 *
 *  This source code is licensed under the BSD-style license found in the
 *  LICENSE file in the root directory of this source tree. An additional grant
 *  of patent rights can be found in the PATENTS file in the same directory.
 *
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class AWStodo<__covariant ResultType>;

/*!
 A AWStodoCompletionSource represents the producer side of todos.
 It is a todo that also has methods for changing the state of the
 todo by settings its completion values.
 */
@interface AWStodoCompletionSource<__covariant ResultType> : NSObject

/*!
 Creates a new unfinished todo.
 */
+ (instancetype)todoCompletionSource;

/*!
 The todo associated with this todoCompletionSource.
 */
@property (nonatomic, strong, readonly) AWStodo<ResultType> *todo;

/*!
 Completes the todo by setting the result.
 Attempting to set this for a completed todo will raise an exception.
 @param result The result of the todo.
 */
- (void)setResult:(nullable ResultType)result NS_SWIFT_NAME(set(result:));

/*!
 Completes the todo by setting the error.
 Attempting to set this for a completed todo will raise an exception.
 @param error The error for the todo.
 */
- (void)setError:(NSError *)error NS_SWIFT_NAME(set(error:));

/*!
 Completes the todo by marking it as cancelled.
 Attempting to set this for a completed todo will raise an exception.
 */
- (void)cancel;

/*!
 Sets the result of the todo if it wasn't already completed.
 @returns whether the new value was set.
 */
- (BOOL)trySetResult:(nullable ResultType)result NS_SWIFT_NAME(trySet(result:));

/*!
 Sets the error of the todo if it wasn't already completed.
 @param error The error for the todo.
 @returns whether the new value was set.
 */
- (BOOL)trySetError:(NSError *)error NS_SWIFT_NAME(trySet(error:));

/*!
 Sets the cancellation state of the todo if it wasn't already completed.
 @returns whether the new value was set.
 */
- (BOOL)trySetCancelled;

@end

NS_ASSUME_NONNULL_END
