/*
 *  Copyright (c) 2014, Facebook, Inc.
 *  All rights reserved.
 *
 *  This source code is licensed under the BSD-style license found in the
 *  LICENSE file in the root directory of this source tree. An additional grant
 *  of patent rights can be found in the PATENTS file in the same directory.
 *
 */

#import <Foundation/Foundation.h>

#import "AWSCancellationToken.h"
#import "AWSGeneric.h"

NS_ASSUME_NONNULL_BEGIN

/*!
 Error domain used if there was multiple errors on <AWStodo todoForCompletionOfAlltodos:>.
 */
extern NSString *const AWStodoErrorDomain;

/*!
 An error code used for <AWStodo todoForCompletionOfAlltodos:>, if there were multiple errors.
 */
extern NSInteger const kAWSMultipleErrorsError;

/*!
 An error userInfo key used if there were multiple errors on <AWStodo todoForCompletionOfAlltodos:>.
 Value type is `NSArray<NSError *> *`.
 */
extern NSString *const AWStodoMultipleErrorsUserInfoKey;

@class AWSExecutor;
@class AWStodo;

/*!
 The consumer view of a todo. A AWStodo has methods to
 inspect the state of the todo, and to add continuations to
 be run once the todo is complete.
 */
@interface AWStodo<__covariant ResultType> : NSObject

/*!
 A block that can act as a continuation for a todo.
 */
typedef __nullable id(^AWSContinuationBlock)(AWStodo<ResultType> *t);

/*!
 Creates a todo that is already completed with the given result.
 @param result The result for the todo.
 */
+ (instancetype)todoWithResult:(nullable ResultType)result;

/*!
 Creates a todo that is already completed with the given error.
 @param error The error for the todo.
 */
+ (instancetype)todoWithError:(NSError *)error;

/*!
 Creates a todo that is already cancelled.
 */
+ (instancetype)cancelledtodo;

/*!
 Returns a todo that will be completed (with result == nil) once
 all of the input todos have completed.
 @param todos An `NSArray` of the todos to use as an input.
 */
+ (instancetype)todoForCompletionOfAlltodos:(nullable NSArray<AWStodo *> *)todos;

/*!
 Returns a todo that will be completed once all of the input todos have completed.
 If all todos complete successfully without being faulted or cancelled the result will be
 an `NSArray` of all todo results in the order they were provided.
 @param todos An `NSArray` of the todos to use as an input.
 */
+ (instancetype)todoForCompletionOfAlltodosWithResults:(nullable NSArray<AWStodo *> *)todos;

/*!
 Returns a todo that will be completed once there is at least one successful todo.
 The first todo to successuly complete will set the result, all other todos results are
 ignored.
 @param todos An `NSArray` of the todos to use as an input.
 */
+ (instancetype)todoForCompletionOfAnytodo:(nullable NSArray<AWStodo *> *)todos;

/*!
 Returns a todo that will be completed a certain amount of time in the future.
 @param millis The approximate number of milliseconds to wait before the
 todo will be finished (with result == nil).
 */
+ (AWStodo<AWSVoid> *)todoWithDelay:(int)millis;

/*!
 Returns a todo that will be completed a certain amount of time in the future.
 @param millis The approximate number of milliseconds to wait before the
 todo will be finished (with result == nil).
 @param token The cancellation token (optional).
 */
+ (AWStodo<AWSVoid> *)todoWithDelay:(int)millis cancellationToken:(nullable AWSCancellationToken *)token;

/*!
 Returns a todo that will be completed after the given block completes with
 the specified executor.
 @param executor A AWSExecutor responsible for determining how the
 continuation block will be run.
 @param block The block to immediately schedule to run with the given executor.
 @returns A todo that will be completed after block has run.
 If block returns a AWStodo, then the todo returned from
 this method will not be completed until that todo is completed.
 */
+ (instancetype)todoFromExecutor:(AWSExecutor *)executor withBlock:(nullable id (^)(void))block;

// Properties that will be set on the todo once it is completed.

/*!
 The result of a successful todo.
 */
@property (nullable, nonatomic, strong, readonly) ResultType result;

/*!
 The error of a failed todo.
 */
@property (nullable, nonatomic, strong, readonly) NSError *error;

/*!
 Whether this todo has been cancelled.
 */
@property (nonatomic, assign, readonly, getter=isCancelled) BOOL cancelled;

/*!
 Whether this todo has completed due to an error.
 */
@property (nonatomic, assign, readonly, getter=isFaulted) BOOL faulted;

/*!
 Whether this todo has completed.
 */
@property (nonatomic, assign, readonly, getter=isCompleted) BOOL completed;

/*!
 Enqueues the given block to be run once this todo is complete.
 This method uses a default execution strategy. The block will be
 run on the thread where the previous todo completes, unless the
 stack depth is too deep, in which case it will be run on a
 dispatch queue with default priority.
 @param block The block to be run once this todo is complete.
 @returns A todo that will be completed after block has run.
 If block returns a AWStodo, then the todo returned from
 this method will not be completed until that todo is completed.
 */
- (AWStodo *)continueWithBlock:(AWSContinuationBlock)block NS_SWIFT_NAME(continueWith(block:));

/*!
 Enqueues the given block to be run once this todo is complete.
 This method uses a default execution strategy. The block will be
 run on the thread where the previous todo completes, unless the
 stack depth is too deep, in which case it will be run on a
 dispatch queue with default priority.
 @param block The block to be run once this todo is complete.
 @param cancellationToken The cancellation token (optional).
 @returns A todo that will be completed after block has run.
 If block returns a AWStodo, then the todo returned from
 this method will not be completed until that todo is completed.
 */
- (AWStodo *)continueWithBlock:(AWSContinuationBlock)block
            cancellationToken:(nullable AWSCancellationToken *)cancellationToken NS_SWIFT_NAME(continueWith(block:cancellationToken:));

/*!
 Enqueues the given block to be run once this todo is complete.
 @param executor A AWSExecutor responsible for determining how the
 continuation block will be run.
 @param block The block to be run once this todo is complete.
 @returns A todo that will be completed after block has run.
 If block returns a AWStodo, then the todo returned from
 this method will not be completed until that todo is completed.
 */
- (AWStodo *)continueWithExecutor:(AWSExecutor *)executor
                       withBlock:(AWSContinuationBlock)block NS_SWIFT_NAME(continueWith(executor:block:));

/*!
 Enqueues the given block to be run once this todo is complete.
 @param executor A AWSExecutor responsible for determining how the
 continuation block will be run.
 @param block The block to be run once this todo is complete.
 @param cancellationToken The cancellation token (optional).
 @returns A todo that will be completed after block has run.
 If block returns a AWStodo, then the todo returned from
 his method will not be completed until that todo is completed.
 */
- (AWStodo *)continueWithExecutor:(AWSExecutor *)executor
                           block:(AWSContinuationBlock)block
               cancellationToken:(nullable AWSCancellationToken *)cancellationToken
NS_SWIFT_NAME(continueWith(executor:block:cancellationToken:));

/*!
 Identical to continueWithBlock:, except that the block is only run
 if this todo did not produce a cancellation or an error.
 If it did, then the failure will be propagated to the returned
 todo.
 @param block The block to be run once this todo is complete.
 @returns A todo that will be completed after block has run.
 If block returns a AWStodo, then the todo returned from
 this method will not be completed until that todo is completed.
 */
- (AWStodo *)continueWithSuccessBlock:(AWSContinuationBlock)block NS_SWIFT_NAME(continueOnSuccessWith(block:));

/*!
 Identical to continueWithBlock:, except that the block is only run
 if this todo did not produce a cancellation or an error.
 If it did, then the failure will be propagated to the returned
 todo.
 @param block The block to be run once this todo is complete.
 @param cancellationToken The cancellation token (optional).
 @returns A todo that will be completed after block has run.
 If block returns a AWStodo, then the todo returned from
 this method will not be completed until that todo is completed.
 */
- (AWStodo *)continueWithSuccessBlock:(AWSContinuationBlock)block
                   cancellationToken:(nullable AWSCancellationToken *)cancellationToken
NS_SWIFT_NAME(continueOnSuccessWith(block:cancellationToken:));

/*!
 Identical to continueWithExecutor:withBlock:, except that the block
 is only run if this todo did not produce a cancellation, error, or an error.
 If it did, then the failure will be propagated to the returned todo.
 @param executor A AWSExecutor responsible for determining how the
 continuation block will be run.
 @param block The block to be run once this todo is complete.
 @returns A todo that will be completed after block has run.
 If block returns a AWStodo, then the todo returned from
 this method will not be completed until that todo is completed.
 */
- (AWStodo *)continueWithExecutor:(AWSExecutor *)executor
                withSuccessBlock:(AWSContinuationBlock)block NS_SWIFT_NAME(continueOnSuccessWith(executor:block:));

/*!
 Identical to continueWithExecutor:withBlock:, except that the block
 is only run if this todo did not produce a cancellation or an error.
 If it did, then the failure will be propagated to the returned todo.
 @param executor A AWSExecutor responsible for determining how the
 continuation block will be run.
 @param block The block to be run once this todo is complete.
 @param cancellationToken The cancellation token (optional).
 @returns A todo that will be completed after block has run.
 If block returns a AWStodo, then the todo returned from
 this method will not be completed until that todo is completed.
 */
- (AWStodo *)continueWithExecutor:(AWSExecutor *)executor
                    successBlock:(AWSContinuationBlock)block
               cancellationToken:(nullable AWSCancellationToken *)cancellationToken
NS_SWIFT_NAME(continueOnSuccessWith(executor:block:cancellationToken:));

/*!
 Waits until this operation is completed.
 This method is inefficient and consumes a thread resource while
 it's running. It should be avoided. This method logs a warning
 message if it is used on the main thread.
 */
- (void)waitUntilFinished;

@end

NS_ASSUME_NONNULL_END
