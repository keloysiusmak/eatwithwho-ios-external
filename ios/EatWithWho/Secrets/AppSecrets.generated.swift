// Generated using Sourcery 0.16.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

public struct AppSecrets {
    let apiKey: String
    let env: String
}

public let appSecrets = AppSecrets(apiKey: "eatwithwhoApiKey", env: "1")
