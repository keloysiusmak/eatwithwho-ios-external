// Generated using Sourcery 0.16.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

public struct ApiConfig {
    let scheme: String
    let port: Int
    let host: String
    let stage: String
}

public let authApiConfig = ApiConfig(scheme: "http", port: 3000, host: "localhost", stage: "debug")
public let accountApiConfig = ApiConfig(scheme: "http", port: 3000, host: "localhost", stage: "debug")
public let budgetApiConfig = ApiConfig(scheme: "http", port: 3000, host: "localhost", stage: "debug")
public let coupleApiConfig = ApiConfig(scheme: "http", port: 3000, host: "localhost", stage: "debug")
public let friendApiConfig = ApiConfig(scheme: "http", port: 3000, host: "localhost", stage: "debug")
public let notificationApiConfig = ApiConfig(scheme: "http", port: 3000, host: "localhost", stage: "debug")
public let recordApiConfig = ApiConfig(scheme: "http", port: 3000, host: "localhost", stage: "debug")
public let eatwithApiConfig = ApiConfig(scheme: "http", port: 3000, host: "localhost", stage: "debug")
public let scheduleApiConfig = ApiConfig(scheme: "http", port: 3000, host: "localhost", stage: "debug")
public let fileApiConfig = ApiConfig(scheme: "http", port: 3000, host: "localhost", stage: "debug")
public let todoApiConfig = ApiConfig(scheme: "http", port: 3000, host: "localhost", stage: "debug")
public let paymentApiConfig = ApiConfig(scheme: "http", port: 3000, host: "localhost", stage: "debug")
public let statusApiConfig = ApiConfig(scheme: "http", port: 3000, host: "localhost", stage: "debug")
