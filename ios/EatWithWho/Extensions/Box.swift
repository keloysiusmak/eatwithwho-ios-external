//
//  Box.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 9/3/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import Foundation
import CardParts

class Box: CardPartsViewController {

    let imageView = CardPartImageView(frame: CGRect(x: 0, y: 0, width: 48, height: 48))
    let pretitlePart = CardPartTextView(type: .title)
    let titlePart = CardPartTextView(type: .title)
    let subtitlePart = CardPartTextView(type: .normal)
    let buttonPart = CardPartButtonView()
    let boxStack = CardPartStackView()

    func setValues(pretitle: String = "", title: String, subtitle: String, button: String, image: String) {
        pretitlePart.text = pretitle
        titlePart.text = title
        subtitlePart.text = subtitle
        buttonPart.setTitle(button, for: .normal)
        imageView.imageName = image
    }

    func setValues(pretitle: String = "", title: String, subtitle: String, button: String, imageUrl: String) {
        pretitlePart.text = pretitle
        titlePart.text = title
        subtitlePart.text = subtitle
        buttonPart.setTitle(button, for: .normal)
        imageView.load(urlString: imageUrl)
    }

    override func viewDidLoad() {
        self.view.layer.borderColor = UIColor.lighterGray.cgColor
        self.view.layer.borderWidth = 0.5

        boxStack.axis = .vertical
        boxStack.spacing = 12.0

        imageView.margins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        imageView.view!.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.widthAnchor.constraint(equalToConstant: self.view.layer.frame.width).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: self.view.layer.frame.width / 2.5).isActive = true
        imageView.layer.cornerRadius = 10
        if #available(iOS 11.0, *) {
            self.view.layer.cornerRadius = 10.0
            imageView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }

        pretitlePart.font = pretitlePart.font.withSize(14.0)
        pretitlePart.textColor = UIColor.lightGray

        let outerBoxStack = CardPartStackView()
        outerBoxStack.axis = .vertical
        outerBoxStack.spacing = 0.0
        outerBoxStack.margins = UIEdgeInsets(top: 18, left: 18, bottom: 18, right: 18)

        titlePart.font = titlePart.font.withSize(21.0)
        titlePart.textColor = UIColor.darkGray

        subtitlePart.font = subtitlePart.font.withSize(14.0)
        subtitlePart.textColor = UIColor.gray
        subtitlePart.lineSpacing = 2.0

        buttonPart.backgroundColor = nil
        buttonPart.titleLabel?.font = CardPartTextView(type: .title).font.withSize(14.0)
        buttonPart.setTitleColor(UIColor.darkGray, for: .normal)
        if (pretitlePart.text != "") {
            outerBoxStack.addArrangedSubview(pretitlePart)
        }
        boxStack.addArrangedSubview(titlePart)
        boxStack.addArrangedSubview(subtitlePart)
        boxStack.addArrangedSubview(buttonPart)

        outerBoxStack.addArrangedSubview(boxStack)
        setupCardParts([imageView, outerBoxStack])
    }
}
