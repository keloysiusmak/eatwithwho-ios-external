//
//  Page.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 10/3/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import Foundation
import CardParts

class Page: CardPartsViewController {
    let innerpage = CardPartStackView()
    let page = CardPartStackView()
    let imageStack = CardPartStackView()
    let imagePart = CardPartImageView()
    let titlePart = CardPartTextView(type: .title)
    let subtitlePart = CardPartTextView(type: .normal)

    func setValues(title: String, subtitle: String, image: String) -> CardPartStackView {
        titlePart.text = title
        imagePart.imageName = image
        subtitlePart.text = subtitle

        innerpage.axis = .vertical
        innerpage.spacing = 12.0
        innerpage.alignment = .center

        imageStack.axis = .horizontal
        imageStack.alignment = .center
        imageStack.distribution = .equalCentering

        imagePart.margins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        imagePart.clipsToBounds = true
        imagePart.contentMode = .scaleAspectFill
        imagePart.image = imagePart.image!.withRenderingMode(.alwaysTemplate)
        imagePart.tintColor = UIColor.pastelRed
        imagePart.widthAnchor.constraint(equalToConstant: 100).isActive = true
        imagePart.heightAnchor.constraint(equalToConstant: 100).isActive = true

        imageStack.addArrangedSubview(imagePart)

        titlePart.font = titlePart.font.withSize(28.0)
        titlePart.textColor = UIColor.darkGray
        titlePart.textAlignment = .center

        subtitlePart.font = subtitlePart.font.withSize(18.0)
        subtitlePart.textColor = UIColor.gray
        subtitlePart.textAlignment = .center

        innerpage.addArrangedSubview(imageStack)
        innerpage.addArrangedSubview(titlePart)
        innerpage.addArrangedSubview(subtitlePart)

        page.addArrangedSubview(innerpage)
        page.alignment = .center
        page.distribution = .equalCentering

        return page
    }
}
