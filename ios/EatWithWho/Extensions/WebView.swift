//
//  WebView.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 9/3/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import Foundation
import CardParts
import WebKit

class WebView: CardPartsViewController {
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func setURL(url: URL) {
        let webView = WKWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        self.view.addSubview(webView)
        webView.load(URLRequest(url: url))
    }
}
