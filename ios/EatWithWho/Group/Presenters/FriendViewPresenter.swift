//
//  FriendViewPresenter.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 17/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

protocol FriendView: NSObjectProtocol {
    func presentValues(friend: Friend)
    func dismiss()
}

class FriendViewPresenter {
    private let friendService: FriendService
    weak private var friendView: FriendView?

    init(friendService: FriendService) {
        self.friendService = friendService
    }

    func attachView(view: FriendView) {
        friendView = view
    }

    func detachView() {
        friendView = nil
    }

    func loadFriend(friendId: String, callback: @escaping (Response) -> Void) {
        let lastSyncFriend = UserDefaults.standard.integer(forKey: "lastSyncFriend")
        let lastUpdatedFriend = UserDefaults.standard.integer(forKey: "lastUpdatedFriend")

        if (friendId == UserDefaults.standard.string(forKey: "friendId") && lastSyncFriend != 0 && lastUpdatedFriend != 0 && lastSyncFriend >= lastUpdatedFriend) {
            UserDefaults.standard.set(NSDate().timeIntervalSince1970, forKey: "lastSyncFriend")
            let friend = NSKeyedUnarchiver.unarchiveObject(withFile: Friend.ArchiveURL.path) as? Friend
            if (friend == nil) {
                getFriend(friendId: friendId, callback: callback)
            } else {
                self.friendView?.presentValues(friend: friend!)
            }
        } else {
            getFriend(friendId: friendId, callback: callback)
        }
    }

    func getFriend(friendId: String, callback: @escaping (Response) -> Void) {
        friendService.getFriend(friendId: friendId) { (result) in
            switch result {
            case .success(let response):
                if (response.statusCode == 200) {
                    self.friendView?.presentValues(friend: (response.data?.friend)!)
                    let friend: Friend
                    if (friendId == UserDefaults.standard.string(forKey: "friendId")) {
                        friend = (response.data?.friend)!
                        NSKeyedArchiver.archiveRootObject(friend, toFile: Friend.ArchiveURL.path)
                    }
                }
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func deleteFriend(friendId: String, callback: @escaping (Response) -> Void) {
        friendService.deleteFriend(friendId: friendId) { (result) in
            switch result {
            case .success(let response):
                callback(response)
                self.friendView?.dismiss()
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func updateFriend(friendId: String, firstName: String, lastName: String, email: String, number: String, gender: String, callback: @escaping (Response) -> Void) {
        friendService.updateFriend(friendId: friendId, firstName: firstName, lastName: lastName, email: email, number: number, gender: gender) { (result) in
            switch result {
            case .success(let response):
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func uploadPicture(friendId: String, imageType: String, callback: @escaping (Response) -> Void) {
        friendService.uploadPicture(friendId: friendId, imageType: imageType) { (result) in
            switch result {
            case .success(let response):
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func deletePicture(friendId: String, callback: @escaping (Response) -> Void) {
        friendService.deletePicture(friendId: friendId) { (result) in
            switch result {
            case .success(let response):
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }
}
