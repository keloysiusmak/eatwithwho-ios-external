//
//  FriendsViewPresenter.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 17/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

protocol FriendsView: NSObjectProtocol {
    func presentValues(coupleFriends: [Friend], friends: [Friend])
}

class FriendsViewPresenter {
    private let coupleService: CoupleService
    private let friendService: FriendService
    weak private var friendsView: FriendsView?

    init(coupleService: CoupleService, friendService: FriendService) {
        self.coupleService = coupleService
        self.friendService = friendService
    }

    func attachView(view: FriendsView) {
        friendsView = view
    }

    func detachView() {
        friendsView = nil
    }

    func loadFriends(userId: String, callback: @escaping (Response) -> Void) {
        let lastSyncFriends = UserDefaults.standard.integer(forKey: "lastSyncFriends")
        let lastUpdatedFriends = UserDefaults.standard.integer(forKey: "lastUpdatedFriends")
        if (userId == UserDefaults.standard.string(forKey: "userId") && lastSyncFriends != 0 && lastUpdatedFriends != 0 && lastSyncFriends >= lastUpdatedFriends) {
            let couple = NSKeyedUnarchiver.unarchiveObject(withFile: Couple.ArchiveURL.path) as? Couple
            if (couple == nil) {
                getFriends(userId: userId, callback: callback)
            } else {
                self.friendsView?.presentValues(coupleFriends: (couple?.coupleFriends)!, friends: (couple?.friends)!)
                UserDefaults.standard.set((couple?.friends)!.count, forKey: "coupleFriendCount")
            }
        } else {
            getFriends(userId: userId, callback: callback)
        }
    }

    func getFriends(userId: String, callback: @escaping (Response) -> Void) {
        friendService.getFriends(userId: userId) { (result) in
            switch result {
            case .success(let response):
                if (response.statusCode == 200) {
                    UserDefaults.standard.set(NSDate().timeIntervalSince1970, forKey: "lastSyncFriends")
                    self.friendsView?.presentValues(coupleFriends: (response.data?.coupleFriends)!, friends: (response.data?.friends)!)
                    UserDefaults.standard.set((response.data?.friends)!.count, forKey: "coupleFriendCount")
                    if (userId == UserDefaults.standard.string(forKey: "userId")) {
                        let couple = NSKeyedUnarchiver.unarchiveObject(withFile: Couple.ArchiveURL.path) as? Couple
                        if (couple != nil) {
                            couple?.coupleFriends = (response.data?.coupleFriends)!
                            couple?.friends = (response.data?.friends)!
                            NSKeyedArchiver.archiveRootObject(couple! as Any, toFile: Couple.ArchiveURL.path)
                        }
                    }
                }
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func addFriend(userId: String, firstName: String, lastName: String, email: String, number: String, gender: String, callback: @escaping (Response) -> Void) {
        friendService.addFriend(userId: userId, firstName: firstName, lastName: lastName, email: email, number: number, gender: gender) { (result) in
            switch result {
            case .success(let response):
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }
}
