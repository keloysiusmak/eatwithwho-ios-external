//
//  ProfileViewPresenter.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 17/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

protocol ProfileView: NSObjectProtocol {
    func presentValues(account: Account)
    func presentValues(schedule: Schedule)
}

class ProfileViewPresenter {
    private let accountService: AccountService
    private let scheduleService: ScheduleService
    weak private var profileView: ProfileView?

    init(accountService: AccountService, scheduleService: ScheduleService) {
        self.accountService = accountService
        self.scheduleService = scheduleService
    }

    func attachView(view: ProfileView) {
        profileView = view
    }

    func detachView() {
        profileView = nil
    }

    func loadAccount(accountId: String, callback: @escaping (Response) -> Void) {
        let lastSyncAccount = UserDefaults.standard.integer(forKey: "lastSyncAccount")
        let lastUpdatedAccount = UserDefaults.standard.integer(forKey: "lastUpdatedAccount")
        if (accountId == UserDefaults.standard.string(forKey: "accountId") && lastSyncAccount != 0 && lastUpdatedAccount != 0 && lastSyncAccount >= lastUpdatedAccount) {
            let account = NSKeyedUnarchiver.unarchiveObject(withFile: Account.ArchiveURL.path) as? Account
            if (account == nil) {
                getAccount(accountId: accountId, callback: callback)
            } else {
                self.profileView?.presentValues(account: account!)
            }
        } else {
            getAccount(accountId: accountId, callback: callback)
        }
    }
    func getAccount(accountId: String, callback: @escaping (Response) -> Void) {
        accountService.getAccount(accountId: accountId) { (result) in
            switch result {
            case .success(let response):
                if (response.statusCode == 200) {
                    UserDefaults.standard.set(NSDate().timeIntervalSince1970, forKey: "lastSyncAccount")
                    let account = response.data?.account
                    self.profileView?.presentValues(account: account!)
                    if (accountId == UserDefaults.standard.string(forKey: "accountId")) {
                        NSKeyedArchiver.archiveRootObject(account!, toFile: Account.ArchiveURL.path)
                    }
                }
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }
    func logoutAccount(accountId: String, callback: @escaping (Response) -> Void) {
        accountService.logoutAccount(accountId: accountId) { (result) in
            switch result {
            case .success(let response):
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func loadSchedule(scheduleId: String, callback: @escaping (Response) -> Void) {
        let lastSyncSchedule = UserDefaults.standard.integer(forKey: "lastSyncSchedule")
        let lastUpdatedSchedule = UserDefaults.standard.integer(forKey: "lastUpdatedSchedule")
        if (scheduleId == UserDefaults.standard.string(forKey: "scheduleId") && lastSyncSchedule != 0 && lastUpdatedSchedule != 0 && lastSyncSchedule >= lastUpdatedSchedule) {
            let schedule = NSKeyedUnarchiver.unarchiveObject(withFile: Schedule.ArchiveURL.path) as? Schedule
            if (schedule == nil) {
                getSchedule(scheduleId: scheduleId, callback: callback)
            } else {
                self.profileView?.presentValues(schedule: schedule!)
            }
        } else {
            getSchedule(scheduleId: scheduleId, callback: callback)
        }
    }

    func getSchedule(scheduleId: String, callback: @escaping (Response) -> Void) {
        scheduleService.getSchedule(scheduleId: scheduleId) { (result) in
            switch result {
            case .success(let response):
                if (response.statusCode == 200) {
                    UserDefaults.standard.set(NSDate().timeIntervalSince1970, forKey: "lastSyncSchedule")
                    let schedule = response.data?.schedule
                    self.profileView?.presentValues(schedule: schedule!)
                    if (scheduleId == UserDefaults.standard.string(forKey: "scheduleId")) {
                        NSKeyedArchiver.archiveRootObject(schedule!, toFile: Schedule.ArchiveURL.path)
                    }
                }
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func changePassword(accountId: String, oldPassword: String, newPassword: String, callback: @escaping (Response) -> Void) {
        accountService.changePassword(accountId: accountId, oldPassword: oldPassword, newPassword: newPassword) { (result) in
            switch result {
            case .success(let response):
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }
}
