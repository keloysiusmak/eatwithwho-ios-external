//
//  eatwithPresenter.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 17/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

protocol ScheduleView: NSObjectProtocol {
    func presentValues(schedule: Schedule, eatwiths: [eatwith], name: String)
}

class ScheduleViewPresenter {
    private let scheduleService: ScheduleService
    weak private var scheduleView: ScheduleView?

    init(scheduleService: ScheduleService) {
        self.scheduleService = scheduleService
    }

    func attachView(view: ScheduleView) {
        scheduleView = view
    }

    func detachView() {
        scheduleView = nil
    }

    func loadSchedule(scheduleId: String, callback: @escaping (Response) -> Void) {
        let lastSyncSchedule = UserDefaults.standard.integer(forKey: "lastSyncSchedule")
        let lastUpdatedSchedule = UserDefaults.standard.integer(forKey: "lastUpdatedSchedule")
        if (scheduleId == UserDefaults.standard.string(forKey: "scheduleId") && lastSyncSchedule != 0 && lastUpdatedSchedule != 0 && lastSyncSchedule >= lastUpdatedSchedule) {
            let schedule = NSKeyedUnarchiver.unarchiveObject(withFile: Schedule.ArchiveURL.path) as? Schedule
            if (schedule == nil) {
                getSchedule(scheduleId: scheduleId, callback: callback)
            } else {
                self.scheduleView?.presentValues(schedule: schedule!, eatwiths: schedule?.eatwiths ?? [], name: schedule?.name ?? "My Schedule")
            }
        } else {
            getSchedule(scheduleId: scheduleId, callback: callback)
        }
    }

    func getSchedule(scheduleId: String, callback: @escaping (Response) -> Void) {
        scheduleService.getSchedule(scheduleId: scheduleId) { (result) in
            switch result {
            case .success(let response):
                if (response.statusCode == 200) {
                    UserDefaults.standard.set(NSDate().timeIntervalSince1970, forKey: "lastSyncSchedule")
                    let schedule = response.data?.schedule
                    self.scheduleView?.presentValues(schedule: schedule!, eatwiths: schedule!.eatwiths, name: schedule!.name)
                    if (scheduleId == UserDefaults.standard.string(forKey: "scheduleId")) {
                        NSKeyedArchiver.archiveRootObject(schedule!, toFile: Schedule.ArchiveURL.path)
                    }
                }
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func updateSchedule(scheduleId: String, name: String, date: Date, callback: @escaping (Response) -> Void) {
        scheduleService.updateSchedule(scheduleId: scheduleId, name: name, date: date) { (result) in
            switch result {
            case .success(let response):
                if (response.statusCode == 200) {
                    let schedule = response.data?.schedule
                    if (scheduleId == UserDefaults.standard.string(forKey: "scheduleId")) {
                        NSKeyedArchiver.archiveRootObject(schedule!, toFile: Schedule.ArchiveURL.path)
                    }
                }
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func addeatwith(name: String, desc: String, location: String, geolocation: String, category: String, startTime: Date, endTime: Date, callback: @escaping (Response) -> Void) {
        scheduleService.addeatwith(name: name, desc: desc, location: location, geolocation: geolocation, category: category, startTime: startTime, endTime: endTime) { (result) in
            switch result {
            case .success(let response):
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }
}
