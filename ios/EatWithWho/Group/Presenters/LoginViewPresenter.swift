//
//  LoginViewPresenter.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 17/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

protocol LoginView: NSObjectProtocol {
}

class LoginViewPresenter {
    private let authService: AuthService
    private let accountService: AccountService
    private let friendService: FriendService
    weak private var loginView: LoginView?

    init(authService: AuthService, accountService: AccountService, friendService: FriendService) {
        self.authService = authService
        self.accountService = accountService
        self.friendService = friendService
    }

    func attachView(view: LoginView) {
        loginView = view
    }

    func detachView() {
        loginView = nil
    }

    func registerAccount(username1: String, password1: String, firstName1: String, lastName1: String, gender1: String, email1: String, number1: String, username2: String, password2: String, firstName2: String, lastName2: String, gender2: String, email2: String, number2: String, callback: @escaping (Response) -> Void) {
        accountService.registerAccount(username1: username1, password1: password1, firstName1: firstName1, lastName1: lastName1, gender1: gender1, email1: email1, number1: number1, username2: username2, password2: password2, firstName2: firstName2, lastName2: lastName2, gender2: gender2, email2: email2, number2: number2) { (result) in
            switch result {
            case .success(let response):
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func loginAccount(username: String, password: String, token: String, notificationArn: String, callback: @escaping (Response) -> Void) {
        accountService.loginAccount(username: username, password: password, token: token, notificationArn: notificationArn) { (result) in
            switch result {
            case .success(let response):
                if (response.statusCode == 200) {
                    let timestamp = NSDate().timeIntervalSince1970
                    UserDefaults.standard.set("couple", forKey: "loggedInAs")
                    UserDefaults.standard.set(timestamp, forKey: "tokenLastSigned")
                    UserDefaults.standard.set(NSDate().timeIntervalSince1970, forKey: "accessTokenExpiry")
                    UserDefaults.standard.set(response.data?.accessToken, forKey: "accessToken")
                    UserDefaults.standard.set(response.data?.refreshToken, forKey: "refreshToken")
                    UserDefaults.standard.set(response.data?.account?._id, forKey: "accountId")
                    UserDefaults.standard.set(response.data?.account?.type, forKey: "accountType")
                    UserDefaults.standard.set(response.data?.account?.friend._id, forKey: "friendId")
                    UserDefaults.standard.set(response.data?.account?.friend.imageType, forKey: "friendImageType")
                    UserDefaults.standard.set(response.data?.account?.friend.imageType, forKey: "friendModifiedAt")
                    UserDefaults.standard.set(response.data?.account?.friend.firstName, forKey: "friendFirstName")
                    UserDefaults.standard.set(response.data?.account?.friend.lastName, forKey: "friendLastName")
                    NSKeyedArchiver.archiveRootObject(response.data?.account! as Any, toFile: Account.ArchiveURL.path)
                }
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func checkAccount(username1: String, username2: String, callback: @escaping (Response) -> Void) {
        accountService.checkAccount(username1: username1, username2: username2) { (result) in
            switch result {
            case .success(let response):
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func getFriend(friendId: String, callback: @escaping (Response) -> Void) {
        friendService.getFriend(friendId: friendId) { (result) in
            switch result {
            case .success(let response):
                if (response.statusCode == 200) {
                    UserDefaults.standard.set(response.data?.friend?._id, forKey: "friendId")
                    UserDefaults.standard.set(response.data?.friend?.firstName, forKey: "friendFirstName")
                    UserDefaults.standard.set(response.data?.friend?.lastName, forKey: "friendLastName")
                }
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func getFriendId(reference: String, token: String, notificationArn: String, callback: @escaping (Response) -> Void) {
        friendService.getFriendId(reference: reference, token: token, notificationArn: notificationArn) { (result) in
            switch result {
            case .success(let response):
                if (response.statusCode == 200) {
                    let timestamp = NSDate().timeIntervalSince1970
                    UserDefaults.standard.set(timestamp, forKey: "tokenLastSigned")
                }
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func getFriendAccessToken(friendId: String, callback: @escaping (Response) -> Void) {
        authService.getFriendAccessToken(friendId: friendId) { (result) in
            switch result {
            case .success(let response):
                if (response.statusCode == 200) {
                    UserDefaults.standard.set("friend", forKey: "loggedInAs")
                    UserDefaults.standard.set(response.data?.accessToken, forKey: "accessToken")
                }
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func getAccountType(accountId: String, accountType: String, callback: @escaping (Response) -> Void) {
        accountService.getAccountType(accountId: accountId) { (result) in
            switch result {
            case .success(let response):
                if(response.statusCode == 200) {
                    if (accountType == "couple") {
                        UserDefaults.standard.set(response.data?.couple?._id, forKey: "userId")
                        UserDefaults.standard.set(response.data?.couple?.subscription, forKey: "coupleSubscription")
                        UserDefaults.standard.set(response.data?.couple?.filesSize, forKey: "coupleFilesSize")
                        UserDefaults.standard.set(response.data?.couple?.schedule, forKey: "scheduleId")
                        UserDefaults.standard.set(response.data?.couple?.budget, forKey: "budgetId")
                        NSKeyedArchiver.archiveRootObject(response.data?.couple! as Any, toFile: Couple.ArchiveURL.path)
                        callback(response)
                    }
                } else {
                    callback(response)
                }
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func getInitializers(accountId: String, callback: @escaping (Response) -> Void) {
        accountService.getInitializers(accountId: accountId) { (result) in
            switch result {
            case .success(let response):
                if(response.statusCode == 200) {
                    UserDefaults.standard.set(response.data?.defaults?.currency, forKey: "currency")
                    UserDefaults.standard.set(response.data?.timestamps?.account, forKey: "lastUpdatedAccount")
                    UserDefaults.standard.set(response.data?.timestamps?.budget, forKey: "lastUpdatedBudget")
                    UserDefaults.standard.set(response.data?.timestamps?.couple, forKey: "lastUpdatedCouple")
                    UserDefaults.standard.set(response.data?.timestamps?.schedule, forKey: "lastUpdatedSchedule")
                    UserDefaults.standard.set(response.data?.timestamps?.friends, forKey: "lastUpdatedFriends")
                    UserDefaults.standard.set(response.data?.timestamps?.guests, forKey: "lastUpdatedGuests")
                    UserDefaults.standard.set(response.data?.timestamps?.todos, forKey: "lastUpdatedtodos")
                    UserDefaults.standard.set(response.data?.isEmpty?.budget, forKey: "isEmptyBudget")
                    UserDefaults.standard.set(response.data?.isEmpty?.schedule, forKey: "isEmptySchedule")
                    callback(response)
                } else {
                    callback(response)
                }
            case .failure:
                callback(ResponseError().response)
            }
        }
    }
}
