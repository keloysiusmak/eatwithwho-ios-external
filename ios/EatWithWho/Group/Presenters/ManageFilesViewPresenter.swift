//
//  ManageFilesViewPresenter.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 17/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

class ManageFilesViewPresenter {
    private let fileService: FileService

    init(fileService: FileService) {
        self.fileService = fileService
    }

    func uploadFile(eatwithId: String, fileName: String, fileSize: Int, callback: @escaping (Response) -> Void) {
        fileService.uploadFile(eatwithId: eatwithId, fileName: fileName, fileSize: fileSize) { (result) in
            switch result {
            case .success(let response):
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }

    func deleteFile(fileId: String, callback: @escaping (Response) -> Void) {
        fileService.deleteFile(fileId: fileId) { (result) in
            switch result {
            case .success(let response):
                callback(response)
            case .failure:
                callback(ResponseError().response)
            }
        }
    }
}
