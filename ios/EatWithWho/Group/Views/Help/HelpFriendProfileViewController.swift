//
//  HelpFriendProfileViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 9/3/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import Foundation
import CardParts
import WebKit

class HelpFriendProfileViewController: CardsViewController {

    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.backgroundColor = UIColor.white

        let cancelButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissWindow))

        navigationItem.leftBarButtonItem = cancelButton

        let pageCard = HelpFriendProfileViewPageCard()
        pageCard.readyButton.addTarget(self, action: #selector(dismissWindow), for: .touchUpInside)

        loadCards(cards: [pageCard])
    }

    @objc func dismissWindow() {
        self.dismiss(animated: true, completion: nil)
    }
}

class HelpFriendProfileViewPageCard: CardPartsViewController, CardPartPagedViewDelegate, TransparentCardTrait, NoTopBottomMarginsCardTrait {

    let readyButton = CardPartButtonView()

    func didMoveToPage(page: Int) {
        //
    }
    override func viewDidLoad() {
        var initialPages: [CardPartStackView] = []

        let page1 = Page().setValues(title: "Building smarter eatwiths.", subtitle: "Here at EatWithWho, we remain steadfast in our goal to build smarter eatwiths through technology. We are constantly evolving and developing our products, so the stresses of eatwith planning become a thing of the past.", image: "help-profile-page1")

        initialPages.append(page1)

        let height = view.frame.height - 260
        let cardPartPages = CardPartPagedView(withPages: initialPages, andHeight: height)
        cardPartPages.delegate = self
        cardPartPages.layer.borderWidth = 0
        cardPartPages.margins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        readyButton.backgroundColor = UIColor.pastelRed
        readyButton.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        readyButton.titleLabel?.font = readyButton.titleLabel?.font.withSize(14.0)
        readyButton.layer.cornerRadius = 10.0
        readyButton.contentHorizontalAlignment = .center
        readyButton.setTitleColor(UIColor.white, for: .normal)
        readyButton.setTitle("Take The Leap", for: .normal)

        setupCardParts([cardPartPages, CardPartSpacerView(height: 10), readyButton])
    }
}
