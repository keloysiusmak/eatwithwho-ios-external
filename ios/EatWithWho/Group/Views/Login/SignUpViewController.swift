//
//  SignUpViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 28/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import Bond

class SignUpViewController: CardsViewController {

    var signUpCard = SignUpViewSignUpCard()
    var loginViewPresenter: LoginViewPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.backgroundColor = UIColor.white

        signUpCard.setViewPresenter(loginViewPresenter: loginViewPresenter!)

        loadCards(cards: [signUpCard])
    }

    func setViewPresenter(loginViewPresenter: LoginViewPresenter) {
        self.loginViewPresenter = loginViewPresenter
    }

    override func willMove(toParent: UIViewController?) {
        if (toParent == nil) {
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.navigationController?.navigationBar.titleTextAttributes = [
                NSAttributedString.Key.foregroundColor: UIColor.white
            ]
            self.navigationController?.navigationBar.setValue(false, forKey: "hidesShadow")
        } else {
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
            self.navigationController?.navigationBar.tintColor = UIColor.pastelRed
            self.navigationController?.navigationBar.titleTextAttributes = [
                NSAttributedString.Key.foregroundColor: UIColor.pastelRed
            ]
            self.navigationController?.navigationBar.setValue(false, forKey: "hidesShadow")
        }
    }
}

class SignUpViewSignUpCard: CardPartsViewController, TransparentCardTrait, UIPickerViewDataSource, UIPickerViewDelegate {

    let titlePart = CardPartTextView(type: .title)
    let writeupPart = CardPartTextView(type: .normal)
    let usernameLabel = CardPartLabel()
    let usernamePart = CardPartTextField()
    let passwordLabel = CardPartLabel()
    let passwordPart = CardPartTextField()
    let firstNameLabel = CardPartLabel()
    let firstNamePart = CardPartTextField()
    let lastNameLabel = CardPartLabel()
    let lastNamePart = CardPartTextField()
    let genderLabel = CardPartLabel()
    let genderPart = CardPartTextField()
    let emailLabel = CardPartLabel()
    let emailPart = CardPartTextField()
    let numberLabel = CardPartLabel()
    let numberPart = CardPartTextField()
    let genderPicker = UIPickerView()

    let usernameLabel2 = CardPartLabel()
    let usernamePart2 = CardPartTextField()
    let firstNameLabel2 = CardPartLabel()
    let firstNamePart2 = CardPartTextField()
    let lastNameLabel2 = CardPartLabel()
    let lastNamePart2 = CardPartTextField()
    let genderLabel2 = CardPartLabel()
    let genderPart2 = CardPartTextField()
    let emailLabel2 = CardPartLabel()
    let emailPart2 = CardPartTextField()
    let numberLabel2 = CardPartLabel()
    let numberPart2 = CardPartTextField()
    let genderPicker2 = UIPickerView()

    let tncCheckbox = Checkbox(frame: CGRect(x: 32, y: 32, width: 25, height: 25))

    let firstPageStackView = CardPartStackView()
    let secondPageStackView = CardPartStackView()
    let thirdPageStackView = CardPartStackView()
    let warningStackView = CardPartStackView()
    let warningLabel = CardPartTextView(type: .normal)
    let tncWarningLabel = CardPartTextView(type: .normal)
    let errorWarningLabel = CardPartTextView(type: .normal)

    let stackView = CardPartStackView()

    let tncLabel = CardPartTextView(type: .small)

    let genders = ["male", "female"]

    var loginViewPresenter: LoginViewPresenter?

    func setViewPresenter(loginViewPresenter: LoginViewPresenter) {
        self.loginViewPresenter = loginViewPresenter
    }

    @objc func backFirstPage() {
        self.loadPage(page: 1)
    }

    @objc func backSecondPage() {
        self.loadPage(page: 2)
    }

    @objc func backThirdPage() {
        self.loadPage(page: 3)
    }

    @objc func loadFirstPage() {
        self.loadPage(page: 1)
    }

    @objc func loadSecondPage() {
        errorWarningLabel.isHidden = true
        warningLabel.isHidden = true
        var hasErrors = false
        if (!Helper.isValidPassword(self.passwordPart.text!)) {
            passwordLabel.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid password. Passwords must be between 6 and 30 characters in length."
            hasErrors = true
        } else {
            passwordLabel.textColor = UIColor.gray
        }

        if (!Helper.isValidUsername(self.usernamePart2.text!)) {
            usernameLabel2.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid username. Usernames must be between 6 and 30 characters in length."
            hasErrors = true
        } else {
            usernameLabel2.textColor = UIColor.gray
        }

        if (!Helper.isValidUsername(self.usernamePart.text!)) {
            usernameLabel.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid username. Usernames must be between 6 and 30 characters in length."
            hasErrors = true
        } else {
            usernameLabel.textColor = UIColor.gray
        }

        if (!hasErrors) {
            if (self.usernamePart.text! == self.usernamePart2.text!) {
                usernameLabel.textColor = UIColor.pastelRed
                usernameLabel2.textColor = UIColor.pastelRed
                hasErrors = true
                warningLabel.text = "The two usernames you have entered are identical. Please try something different."
                warningLabel.isHidden = false
            }
        } else {
            warningLabel.isHidden = false
        }
        if (!hasErrors) {
            loginViewPresenter!.checkAccount(username1: self.usernamePart.text!, username2: self.usernamePart2.text!) { response in
                if (response.statusCode == 200) {
                    if (!(response.data!.validUsernames)!) {
                        self.usernameLabel.textColor = UIColor.pastelRed
                        self.usernameLabel2.textColor = UIColor.pastelRed
                        self.warningLabel.text = "There exists a username with the given usernames. Please try something different."
                        self.warningLabel.isHidden = false
                    } else {
                        self.loadPage(page: 2)
                    }
                } else {
                    self.errorWarningLabel.text = "Something went wrong. Please try again later."
                    self.errorWarningLabel.isHidden = false
                }
            }
        }
    }

    @objc func loadThirdPage() {
        errorWarningLabel.isHidden = true
        warningLabel.isHidden = true
        var hasErrors = false
        if (!self.numberPart.text!.isEmpty && !Helper.isValidNumber(self.numberPart.text!)) {
            numberLabel.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid number. Numbers must be between 6 and 30 characters in length."
            hasErrors = true
        } else {
            numberLabel.textColor = UIColor.gray
        }
        if (!self.emailPart.text!.isEmpty && !Helper.isValidEmail(self.emailPart.text!)) {
            emailLabel.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid email. Emails must be shorter than 120 characters in length."
            hasErrors = true
        } else {
            emailLabel.textColor = UIColor.gray
        }
        if (!self.genderPart.text!.isEmpty && Helper.isValidGender(self.genderPart.text!.lowercased())) {
            genderLabel.textColor = UIColor.pastelRed
            hasErrors = true
            warningLabel.text = "You did not enter a valid gender."
        } else {
            genderLabel.textColor = UIColor.gray
        }
        if (!self.lastNamePart.text!.isEmpty && !Helper.isValidName(self.lastNamePart.text!)) {
            lastNameLabel.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid last name. Names must be between 2 and 60 characters in length."
            hasErrors = true
        } else {
            lastNameLabel.textColor = UIColor.gray
        }
        if (!self.firstNamePart.text!.isEmpty && !Helper.isValidName(self.firstNamePart.text!)) {
            firstNameLabel.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid first name. Names must be between 2 and 60 characters in length."
            hasErrors = true
        } else {
            firstNameLabel.textColor = UIColor.gray
        }
        if (!hasErrors) {
            self.loadPage(page: 3)
        } else {
            warningLabel.isHidden = false
        }
    }

    @objc func loadFourthPage() {
        errorWarningLabel.isHidden = true
        warningLabel.isHidden = true
        var hasErrors = false
        if (!self.numberPart2.text!.isEmpty && !Helper.isValidNumber(self.numberPart2.text!)) {
            numberLabel2.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid number. Numbers must be between 6 and 30 characters in length."
            hasErrors = true
        } else {
            numberLabel2.textColor = UIColor.gray
        }
        if (!self.emailPart2.text!.isEmpty && !Helper.isValidEmail(self.emailPart2.text!)) {
            emailLabel2.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid email. Emails must be shorter than 120 characters in length."
            hasErrors = true
        } else {
            emailLabel2.textColor = UIColor.gray
        }
        if (!self.genderPart2.text!.isEmpty && Helper.isValidGender(self.genderPart2.text!.lowercased())) {
            genderLabel2.textColor = UIColor.pastelRed
            hasErrors = true
            warningLabel.text = "You did not enter a valid gender."
        } else {
            genderLabel2.textColor = UIColor.gray
        }
        if (!self.lastNamePart2.text!.isEmpty && !Helper.isValidName(self.lastNamePart2.text!)) {
            lastNameLabel2.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid last name. Names must be between 2 and 60 characters in length."
            hasErrors = true
        } else {
            lastNameLabel2.textColor = UIColor.gray
        }
        if (!self.firstNamePart2.text!.isEmpty && !Helper.isValidName(self.firstNamePart2.text!)) {
            firstNameLabel2.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid first name. Names must be between 2 and 60 characters in length."
            hasErrors = true
        } else {
            firstNameLabel2.textColor = UIColor.gray
        }
        if (!hasErrors) {
            self.loadPage(page: 4)
        } else {
            warningLabel.isHidden = false
        }
    }

    @objc func loadPage(page: Int) {
        errorWarningLabel.isHidden = true
        warningLabel.isHidden = true
        firstPageStackView.isHidden = true
        secondPageStackView.isHidden = true
        thirdPageStackView.isHidden = true
        warningStackView.isHidden = true
        if (page == 1) {
            writeupPart.text = "We just need a couple of details from you and you'll be on your way to organizing your eatwith, the EatWithWho way.\n\nTo start, we need some login details for you and your eatwith."
            firstPageStackView.isHidden = false
        } else if (page == 2) {
            writeupPart.text = "We want to know more about you!\n\nFeel free to do this later, we know you're excited to get started."
            secondPageStackView.isHidden = false
        } else if (page == 3) {
            writeupPart.text = "We'd like to know about your eatwith as well!\n\nAgain, feel free to do this later."
            thirdPageStackView.isHidden = false
        } else if (page == 4) {
            writeupPart.text = "All's good, it seems like we're ready to get going.\n\nYour privacy on our platform is extremely important to us. If you agree with our Terms and Conditions and Privacy Policy, let's get started."
            warningStackView.isHidden = false
        }
    }

    override func viewDidLoad() {
        firstPageStackView.isHidden = true
        secondPageStackView.isHidden = true
        thirdPageStackView.isHidden = true
        warningStackView.isHidden = true
        stackView.axis = .vertical

        titlePart.text = "Welcome onboard."
        titlePart.textColor = UIColor.darkGray
        titlePart.lineHeightMultiple = 0.75
        titlePart.font = titlePart.font?.withSize(48.0)
        stackView.addArrangedSubview(titlePart)

        writeupPart.textColor = UIColor.darkGray
        writeupPart.font = writeupPart.font?.withSize(16.0)
        stackView.addArrangedSubview(writeupPart)

        tncWarningLabel.isHidden = true
        warningLabel.isHidden = true
        errorWarningLabel.isHidden = true
        stackView.addArrangedSubview(errorWarningLabel)
        stackView.addArrangedSubview(tncWarningLabel)
        stackView.addArrangedSubview(warningLabel)

        firstPageStackView.axis = .vertical
        firstPageStackView.spacing = 24.0

        let separatorStack = CardPartStackView()
        separatorStack.axis = .horizontal
        separatorStack.addArrangedSubview(CardPartSeparatorView())
        separatorStack.distribution = .fillEqually
        separatorStack.alignment = .center
        let loginLabel = CardPartTextView(type: .detail)
        loginLabel.text = "LOGIN DETAILS"
        loginLabel.textColor = UIColor.darkGray
        loginLabel.textAlignment = .center
        separatorStack.addArrangedSubview(loginLabel)
        separatorStack.addArrangedSubview(CardPartSeparatorView())
        firstPageStackView.addArrangedSubview(separatorStack)

        let usernameStackView = CardPartStackView()
        usernameStackView.axis = .vertical
        usernameStackView.spacing = 6.0
        usernameLabel.text = "YOUR USERNAME"
        usernameLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        usernameLabel.textColor = UIColor.gray
        usernamePart.placeholder = "Username"
        usernamePart.maxLength = 60
        usernamePart.font = Helper.theme.normalTextFont.withSize(24.0)
        usernamePart.textColor = UIColor.darkGray
        usernamePart.autocapitalizationType = UITextAutocapitalizationType.none
        usernameStackView.addArrangedSubview(usernameLabel)
        usernameStackView.addArrangedSubview(usernamePart)
        firstPageStackView.addArrangedSubview(usernameStackView)

        let usernameStackView2 = CardPartStackView()
        usernameStackView2.axis = .vertical
        usernameStackView2.spacing = 6.0
        usernameLabel2.text = "YOUR PARTNER'S USERNAME"
        usernameLabel2.font = Helper.theme.detailTextFont.withSize(14.0)
        usernameLabel2.textColor = UIColor.gray
        usernamePart2.placeholder = "Username"
        usernamePart2.maxLength = 60
        usernamePart2.font = Helper.theme.normalTextFont.withSize(24.0)
        usernamePart2.textColor = UIColor.darkGray
        usernamePart2.autocapitalizationType = UITextAutocapitalizationType.none
        usernameStackView2.addArrangedSubview(usernameLabel2)
        usernameStackView2.addArrangedSubview(usernamePart2)
        firstPageStackView.addArrangedSubview(usernameStackView2)

        let passwordStackView = CardPartStackView()
        passwordStackView.axis = .vertical
        passwordStackView.spacing = 6.0
        passwordLabel.text = "PASSWORD"
        passwordLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        passwordLabel.textColor = UIColor.gray
        passwordPart.placeholder = "Password"
        passwordPart.maxLength = 60
        passwordPart.font = Helper.theme.normalTextFont.withSize(24.0)
        passwordPart.textColor = UIColor.darkGray
        passwordPart.isSecureTextEntry = true
        passwordPart.autocapitalizationType = UITextAutocapitalizationType.none
        passwordStackView.addArrangedSubview(passwordLabel)
        passwordStackView.addArrangedSubview(passwordPart)
        firstPageStackView.addArrangedSubview(passwordStackView)

        let firstPageButton = CardPartButtonView()
        firstPageButton.backgroundColor = UIColor.darkGray
        firstPageButton.titleLabel?.font = firstPageButton.titleLabel?.font.withSize(14.0)
        firstPageButton.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        firstPageButton.contentHorizontalAlignment = .center
        firstPageButton.setTitleColor(UIColor.white, for: .normal)
        firstPageButton.setTitle("Continue", for: .normal)
        firstPageButton.addTarget(self, action: #selector(loadSecondPage), for: .touchUpInside)
        firstPageButton.layer.cornerRadius = 10.0
        firstPageStackView.addArrangedSubview(firstPageButton)

        secondPageStackView.axis = .vertical
        secondPageStackView.spacing = 24.0

        let separatorStack2 = CardPartStackView()
        separatorStack2.axis = .horizontal
        separatorStack2.addArrangedSubview(CardPartSeparatorView())
        separatorStack2.distribution = .fillEqually
        separatorStack2.alignment = .center
        let loginLabel2 = CardPartTextView(type: .detail)
        loginLabel2.text = "YOUR DETAILS"
        loginLabel2.textColor = UIColor.darkGray
        loginLabel2.textAlignment = .center
        separatorStack2.addArrangedSubview(loginLabel2)
        separatorStack2.addArrangedSubview(CardPartSeparatorView())
        secondPageStackView.addArrangedSubview(separatorStack2)

        let firstNameStackView = CardPartStackView()
        firstNameStackView.axis = .vertical
        firstNameStackView.spacing = 6.0
        firstNameLabel.text = "FIRST NAME"
        firstNameLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        firstNameLabel.textColor = UIColor.gray
        firstNamePart.placeholder = "First Name"
        firstNamePart.maxLength = 60
        firstNamePart.font = Helper.theme.normalTextFont.withSize(24.0)
        firstNamePart.textColor = UIColor.darkGray
        firstNamePart.autocapitalizationType = UITextAutocapitalizationType.none
        firstNameStackView.addArrangedSubview(firstNameLabel)
        firstNameStackView.addArrangedSubview(firstNamePart)
        secondPageStackView.addArrangedSubview(firstNameStackView)

        let lastNameStackView = CardPartStackView()
        lastNameStackView.axis = .vertical
        lastNameStackView.spacing = 6.0
        lastNameLabel.text = "LAST NAME"
        lastNameLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        lastNameLabel.textColor = UIColor.gray
        lastNamePart.placeholder = "Last Name"
        lastNamePart.maxLength = 60
        lastNamePart.font = Helper.theme.normalTextFont.withSize(24.0)
        lastNamePart.textColor = UIColor.darkGray
        lastNamePart.autocapitalizationType = UITextAutocapitalizationType.none
        lastNameStackView.addArrangedSubview(lastNameLabel)
        lastNameStackView.addArrangedSubview(lastNamePart)
        secondPageStackView.addArrangedSubview(lastNameStackView)

        let genderStackView = CardPartStackView()
        genderStackView.axis = .vertical
        genderStackView.spacing = 6.0
        genderPicker.dataSource = self
        genderPicker.delegate = self
        genderLabel.text = "GENDER"
        genderLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        genderLabel.textColor = UIColor.gray
        genderPart.placeholder = "Gender"
        genderPart.maxLength = 60
        genderPart.font = Helper.theme.normalTextFont.withSize(24.0)
        genderPart.textColor = UIColor.darkGray
        genderPart.autocapitalizationType = UITextAutocapitalizationType.none
        genderPart.inputView = genderPicker
        let genderToolbar = UIToolbar()
        genderToolbar.sizeToFit()
        let genderDoneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClicked))
        genderToolbar.setItems([genderDoneButton], animated: true)
        genderPart.inputAccessoryView = genderToolbar

        genderStackView.addArrangedSubview(genderLabel)
        genderStackView.addArrangedSubview(genderPart)
        secondPageStackView.addArrangedSubview(genderStackView)

        let emailStackView = CardPartStackView()
        emailStackView.axis = .vertical
        emailStackView.spacing = 6.0
        emailLabel.text = "EMAIL"
        emailLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        emailLabel.textColor = UIColor.gray
        emailPart.placeholder = "Email"
        emailPart.maxLength = 60
        emailPart.font = Helper.theme.normalTextFont.withSize(24.0)
        emailPart.textColor = UIColor.darkGray
        emailPart.autocapitalizationType = UITextAutocapitalizationType.none
        emailStackView.addArrangedSubview(emailLabel)
        emailStackView.addArrangedSubview(emailPart)
        secondPageStackView.addArrangedSubview(emailStackView)

        let numberStackView = CardPartStackView()
        numberStackView.axis = .vertical
        numberStackView.spacing = 6.0
        numberLabel.text = "NUMBER"
        numberLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        numberLabel.textColor = UIColor.gray
        numberPart.placeholder = "Number"
        numberPart.maxLength = 60
        numberPart.font = Helper.theme.normalTextFont.withSize(24.0)
        numberPart.textColor = UIColor.darkGray
        numberPart.autocapitalizationType = UITextAutocapitalizationType.none
        numberStackView.addArrangedSubview(numberLabel)
        numberStackView.addArrangedSubview(numberPart)
        secondPageStackView.addArrangedSubview(numberStackView)

        let secondPagingStackView = CardPartStackView()
        secondPagingStackView.spacing = 6.0
        secondPagingStackView.axis = .vertical

        let secondPageBackButton = CardPartButtonView()
        secondPageBackButton.backgroundColor = UIColor.pastel
        secondPageBackButton.titleLabel?.font = secondPageBackButton.titleLabel?.font.withSize(14.0)
        secondPageBackButton.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        secondPageBackButton.contentHorizontalAlignment = .center
        secondPageBackButton.setTitleColor(UIColor.darkGray, for: .normal)
        secondPageBackButton.setTitle("Go Back", for: .normal)
        secondPageBackButton.addTarget(self, action: #selector(backFirstPage), for: .touchUpInside)
        secondPageBackButton.layer.cornerRadius = 10.0
        secondPagingStackView.addArrangedSubview(secondPageBackButton)

        let secondPageButton = CardPartButtonView()
        secondPageButton.backgroundColor = UIColor.darkGray
        secondPageButton.titleLabel?.font = secondPageButton.titleLabel?.font.withSize(14.0)
        secondPageButton.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        secondPageButton.contentHorizontalAlignment = .center
        secondPageButton.setTitleColor(UIColor.white, for: .normal)
        secondPageButton.setTitle("Continue", for: .normal)
        secondPageButton.addTarget(self, action: #selector(loadThirdPage), for: .touchUpInside)
        secondPageButton.layer.cornerRadius = 10.0
        secondPagingStackView.addArrangedSubview(secondPageButton)
        secondPageStackView.addArrangedSubview(secondPagingStackView)

        thirdPageStackView.axis = .vertical
        thirdPageStackView.spacing = 24.0

        let separatorStack3 = CardPartStackView()
        separatorStack3.axis = .horizontal
        separatorStack3.addArrangedSubview(CardPartSeparatorView())
        separatorStack3.distribution = .fillEqually
        separatorStack3.alignment = .center
        let loginLabel3 = CardPartTextView(type: .detail)
        loginLabel3.text = "YOUR PARTNER'S DETAILS"
        loginLabel3.textColor = UIColor.darkGray
        loginLabel3.textAlignment = .center
        separatorStack3.addArrangedSubview(loginLabel3)
        separatorStack3.addArrangedSubview(CardPartSeparatorView())
        thirdPageStackView.addArrangedSubview(separatorStack3)

        let firstNameStackView2 = CardPartStackView()
        firstNameStackView2.axis = .vertical
        firstNameStackView2.spacing = 6.0
        firstNameLabel2.text = "FIRST NAME"
        firstNameLabel2.font = Helper.theme.detailTextFont.withSize(14.0)
        firstNameLabel2.textColor = UIColor.gray
        firstNamePart2.placeholder = "First Name"
        firstNamePart2.maxLength = 60
        firstNamePart2.font = Helper.theme.normalTextFont.withSize(24.0)
        firstNamePart2.textColor = UIColor.darkGray
        firstNamePart2.autocapitalizationType = UITextAutocapitalizationType.none
        firstNameStackView2.addArrangedSubview(firstNameLabel2)
        firstNameStackView2.addArrangedSubview(firstNamePart2)
        thirdPageStackView.addArrangedSubview(firstNameStackView2)

        let lastNameStackView2 = CardPartStackView()
        lastNameStackView2.axis = .vertical
        lastNameStackView2.spacing = 6.0
        lastNameLabel2.text = "LAST NAME"
        lastNameLabel2.font = Helper.theme.detailTextFont.withSize(14.0)
        lastNameLabel2.textColor = UIColor.gray
        lastNamePart2.placeholder = "Last Name"
        lastNamePart2.maxLength = 60
        lastNamePart2.font = Helper.theme.normalTextFont.withSize(24.0)
        lastNamePart2.textColor = UIColor.darkGray
        lastNamePart2.autocapitalizationType = UITextAutocapitalizationType.none
        lastNameStackView2.addArrangedSubview(lastNameLabel2)
        lastNameStackView2.addArrangedSubview(lastNamePart2)
        thirdPageStackView.addArrangedSubview(lastNameStackView2)

        let genderStackView2 = CardPartStackView()
        genderStackView2.axis = .vertical
        genderStackView2.spacing = 6.0
        genderPicker2.dataSource = self
        genderPicker2.delegate = self
        genderLabel2.text = "GENDER"
        genderLabel2.font = Helper.theme.detailTextFont.withSize(14.0)
        genderLabel2.textColor = UIColor.gray
        genderPart2.placeholder = "Gender"
        genderPart2.maxLength = 60
        genderPart2.font = Helper.theme.normalTextFont.withSize(24.0)
        genderPart2.textColor = UIColor.darkGray
        genderPart2.autocapitalizationType = UITextAutocapitalizationType.none
        genderPart2.inputView = genderPicker2
        let genderToolbar2 = UIToolbar()
        genderToolbar2.sizeToFit()
        let genderDoneButton2 = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClicked))
        genderToolbar2.setItems([genderDoneButton2], animated: true)
        genderPart2.inputAccessoryView = genderToolbar2

        genderStackView2.addArrangedSubview(genderLabel2)
        genderStackView2.addArrangedSubview(genderPart2)
        thirdPageStackView.addArrangedSubview(genderStackView2)

        let emailStackView2 = CardPartStackView()
        emailStackView2.axis = .vertical
        emailStackView2.spacing = 6.0
        emailLabel2.text = "EMAIL"
        emailLabel2.font = Helper.theme.detailTextFont.withSize(14.0)
        emailLabel2.textColor = UIColor.gray
        emailPart2.placeholder = "Email"
        emailPart2.maxLength = 60
        emailPart2.font = Helper.theme.normalTextFont.withSize(24.0)
        emailPart2.textColor = UIColor.darkGray
        emailPart2.autocapitalizationType = UITextAutocapitalizationType.none
        emailStackView2.addArrangedSubview(emailLabel2)
        emailStackView2.addArrangedSubview(emailPart2)
        thirdPageStackView.addArrangedSubview(emailStackView2)

        let numberStackView2 = CardPartStackView()
        numberStackView2.axis = .vertical
        numberStackView2.spacing = 6.0
        numberLabel2.text = "NUMBER"
        numberLabel2.font = Helper.theme.detailTextFont.withSize(14.0)
        numberLabel2.textColor = UIColor.gray
        numberPart2.placeholder = "Number"
        numberPart2.maxLength = 60
        numberPart2.font = Helper.theme.normalTextFont.withSize(24.0)
        numberPart2.textColor = UIColor.darkGray
        numberPart2.autocapitalizationType = UITextAutocapitalizationType.none
        numberStackView2.addArrangedSubview(numberLabel2)
        numberStackView2.addArrangedSubview(numberPart2)
        thirdPageStackView.addArrangedSubview(numberStackView2)

        let thirdPagingStackView = CardPartStackView()
        thirdPagingStackView.spacing = 6.0
        thirdPagingStackView.axis = .vertical

        let thirdPageBackButton = CardPartButtonView()
        thirdPageBackButton.backgroundColor = UIColor.pastel
        thirdPageBackButton.titleLabel?.font = thirdPageBackButton.titleLabel?.font.withSize(14.0)
        thirdPageBackButton.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        thirdPageBackButton.contentHorizontalAlignment = .center
        thirdPageBackButton.setTitleColor(UIColor.darkGray, for: .normal)
        thirdPageBackButton.setTitle("Go Back", for: .normal)
        thirdPageBackButton.addTarget(self, action: #selector(backSecondPage), for: .touchUpInside)
        thirdPageBackButton.layer.cornerRadius = 10.0
        thirdPagingStackView.addArrangedSubview(thirdPageBackButton)

        let thirdPageButton = CardPartButtonView()
        thirdPageButton.backgroundColor = UIColor.darkGray
        thirdPageButton.titleLabel?.font = thirdPageButton.titleLabel?.font.withSize(14.0)
        thirdPageButton.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        thirdPageButton.contentHorizontalAlignment = .center
        thirdPageButton.setTitleColor(UIColor.white, for: .normal)
        thirdPageButton.setTitle("Continue", for: .normal)
        thirdPageButton.addTarget(self, action: #selector(loadFourthPage), for: .touchUpInside)
        thirdPageButton.layer.cornerRadius = 10.0
        thirdPagingStackView.addArrangedSubview(thirdPageButton)
        thirdPageStackView.addArrangedSubview(thirdPagingStackView)

        let fourthPagingStackView = CardPartStackView()
        fourthPagingStackView.spacing = 6.0
        fourthPagingStackView.axis = .vertical

        warningLabel.text = "You have missing or invalid fields."
        warningLabel.font = warningLabel.font?.withSize(14.0)
        warningLabel.textColor = UIColor.pastelRed

        tncWarningLabel.text = "Please read and accept the terms and conditions."
        tncWarningLabel.font = tncWarningLabel.font?.withSize(14.0)
        tncWarningLabel.textColor = UIColor.pastelRed

        errorWarningLabel.font = errorWarningLabel.font?.withSize(14.0)
        errorWarningLabel.textColor = UIColor.pastelRed

        warningStackView.axis = .vertical
        warningStackView.spacing = 24.0

        let tncStackView = CardPartStackView()
        tncStackView.axis = .horizontal
        tncStackView.spacing = 6.0
        tncCheckbox.borderStyle = .circle
        tncCheckbox.checkmarkStyle = .circle
        tncCheckbox.borderWidth = 1
        tncCheckbox.checkmarkSize = 0.7
        tncCheckbox.checkmarkColor = .pastelRed
        tncCheckbox.uncheckedBorderColor = UIColor.lightGray
        tncCheckbox.checkedBorderColor = UIColor.lightGray
        tncCheckbox.heightAnchor.constraint(equalToConstant: 25).isActive = true
        tncCheckbox.widthAnchor.constraint(equalToConstant: 25).isActive = true
        let tncText = NSMutableAttributedString(string: "I accept the Terms and Conditions.", attributes: [NSAttributedString.Key.font: tncLabel.font!.withSize(16.0)])
        tncText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.pastelRed, range: NSRange(location: 13, length: 20))
        tncLabel.attributedText = tncText
        tncStackView.addArrangedSubview(tncCheckbox)
        tncStackView.addArrangedSubview(tncLabel)
        let spacerView = UIView()
        spacerView.setContentHuggingPriority(.defaultLow, for: .horizontal)

        tncStackView.addArrangedSubview(spacerView)
        warningStackView.addArrangedSubview(tncStackView)

        let fourthPageBackButton = CardPartButtonView()
        fourthPageBackButton.backgroundColor = UIColor.pastel
        fourthPageBackButton.titleLabel?.font = fourthPageBackButton.titleLabel?.font.withSize(14.0)
        fourthPageBackButton.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        fourthPageBackButton.contentHorizontalAlignment = .center
        fourthPageBackButton.setTitleColor(UIColor.darkGray, for: .normal)
        fourthPageBackButton.setTitle("Go Back", for: .normal)
        fourthPageBackButton.addTarget(self, action: #selector(backThirdPage), for: .touchUpInside)
        fourthPageBackButton.layer.cornerRadius = 10.0
        fourthPagingStackView.addArrangedSubview(fourthPageBackButton)

        let signUpButtonPart = CardPartButtonView()
        signUpButtonPart.backgroundColor = UIColor.pastelRed
        signUpButtonPart.titleLabel?.font = signUpButtonPart.titleLabel?.font.withSize(14.0)
        signUpButtonPart.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        signUpButtonPart.contentHorizontalAlignment = .center
        signUpButtonPart.setTitleColor(UIColor.white, for: .normal)
        signUpButtonPart.setTitle("Sign Up", for: .normal)
        signUpButtonPart.addTarget(self, action: #selector(signUp), for: .touchUpInside)
        signUpButtonPart.layer.cornerRadius = 10.0
        fourthPagingStackView.addArrangedSubview(signUpButtonPart)
        warningStackView.addArrangedSubview(fourthPagingStackView)

        stackView.spacing = 24.0

        stackView.addArrangedSubview(firstPageStackView)
        stackView.addArrangedSubview(secondPageStackView)
        stackView.addArrangedSubview(thirdPageStackView)
        stackView.addArrangedSubview(warningStackView)
        stackView.addArrangedSubview(CardPartSpacerView(height: 240.0))

        setupCardParts([stackView])
        self.loadPage(page: 1)
    }

    @objc func signUp() {
        errorWarningLabel.isHidden = true
        tncWarningLabel.isHidden = true
        warningLabel.isHidden = true

        var hasErrors = false
        if (!self.numberPart2.text!.isEmpty && !Helper.isValidNumber(self.numberPart2.text!)) {
            numberLabel2.textColor = UIColor.pastelRed
            hasErrors = true
            warningLabel.text = "You did not enter a valid number. Numbers must be between 6 and 30 characters in length."
        } else {
            numberLabel2.textColor = UIColor.gray
        }
        if (!self.emailPart2.text!.isEmpty && !Helper.isValidEmail(self.emailPart2.text!)) {
            emailLabel2.textColor = UIColor.pastelRed
            hasErrors = true
            warningLabel.text = "You did not enter a valid email. Emails must be shorter than 120 characters in length."
        } else {
            emailLabel2.textColor = UIColor.gray
        }
        if (!self.genderPart2.text!.isEmpty && Helper.isValidGender(self.genderPart2.text!.lowercased())) {
            genderLabel2.textColor = UIColor.pastelRed
            hasErrors = true
            warningLabel.text = "You did not enter a valid gender."
        } else {
            genderLabel2.textColor = UIColor.gray
        }
        if (!self.lastNamePart2.text!.isEmpty && !Helper.isValidName(self.lastNamePart2.text!)) {
            lastNameLabel2.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid last name. Names must be between 2 and 60 characters in length."
            hasErrors = true
        } else {
            lastNameLabel2.textColor = UIColor.gray
        }
        if (!self.firstNamePart2.text!.isEmpty && !Helper.isValidName(self.firstNamePart2.text!)) {
            firstNameLabel2.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid first name. Names must be between 2 and 60 characters in length."
            hasErrors = true
        } else {
            firstNameLabel2.textColor = UIColor.gray
        }
        if (!Helper.isValidUsername(self.usernamePart2.text!)) {
            usernameLabel2.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid username. Usernames must be between 6 and 30 characters in length."
            hasErrors = true
        } else {
            usernameLabel2.textColor = UIColor.gray
        }
        if (!self.numberPart.text!.isEmpty && !Helper.isValidNumber(self.numberPart.text!)) {
            numberLabel.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid number. Numbers must be between 6 and 30 characters in length."
            hasErrors = true
        } else {
            numberLabel.textColor = UIColor.gray
        }
        if (!self.emailPart.text!.isEmpty && !Helper.isValidEmail(self.emailPart.text!)) {
            emailLabel.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid email. Emails must be shorter than 120 characters in length."
            hasErrors = true
        } else {
            emailLabel.textColor = UIColor.gray
        }
        if (!self.genderPart.text!.isEmpty && Helper.isValidGender(self.genderPart.text!.lowercased())) {
            genderLabel.textColor = UIColor.pastelRed
            hasErrors = true
            warningLabel.text = "You did not enter a valid gender."
        } else {
            genderLabel.textColor = UIColor.gray
        }
        if (!self.lastNamePart.text!.isEmpty && !Helper.isValidName(self.lastNamePart.text!)) {
            lastNameLabel.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid last name. Names must be between 2 and 60 characters in length."
            hasErrors = true
        } else {
            lastNameLabel.textColor = UIColor.gray
        }
        if (!self.firstNamePart.text!.isEmpty && !Helper.isValidName(self.firstNamePart.text!)) {
            firstNameLabel.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid first name. Names must be between 2 and 60 characters in length."
            hasErrors = true
        } else {
            firstNameLabel.textColor = UIColor.gray
        }
        if (!Helper.isValidPassword(self.passwordPart.text!)) {
            passwordLabel.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid password. Passwords must be between 6 and 30 characters in length."
            hasErrors = true
        } else {
            passwordLabel.textColor = UIColor.gray
        }
        if (!Helper.isValidUsername(self.usernamePart.text!)) {
            usernameLabel.textColor = UIColor.pastelRed
            warningLabel.text = "You did not enter a valid username. Usernames must be between 6 and 30 characters in length."
            hasErrors = true
        } else {
            usernameLabel.textColor = UIColor.gray
        }

        if (!hasErrors) {
            if (self.usernamePart.text! == self.usernamePart2.text!) {
                usernameLabel.textColor = UIColor.pastelRed
                usernameLabel2.textColor = UIColor.pastelRed
                hasErrors = true
                self.errorWarningLabel.text = "The two usernames you have entered are identical. Please try something different."
            }
        }

        if (hasErrors) {
            warningLabel.isHidden = false
        } else if (!self.tncCheckbox.isChecked) {
            tncWarningLabel.isHidden = false
        } else {
            view.endEditing(true)
            loginViewPresenter!.registerAccount(
             username1: self.usernamePart.text!,
             password1: self.passwordPart.text!,
             firstName1: Helper.isValidName(self.firstNamePart.text!) ? self.firstNamePart.text! : "Your",
             lastName1: Helper.isValidName(self.lastNamePart.text!) ? self.lastNamePart.text! : "Name",
             gender1: Helper.isValidGender(self.genderPart.text!.lowercased()) ? self.genderPart.text!.lowercased() : "female",
             email1: Helper.isValidEmail(self.emailPart.text!) ? self.emailPart.text! : "youremail@email.com",
             number1: Helper.isValidNumber(self.numberPart.text!) ? self.numberPart.text! : "91234567",
             username2: self.usernamePart2.text!,
             password2: self.passwordPart.text!,
             firstName2: Helper.isValidName(self.firstNamePart2.text!) ? self.firstNamePart2.text! : "Your Partner's",
             lastName2: Helper.isValidName(self.lastNamePart2.text!) ? self.lastNamePart2.text! : "Name",
             gender2: Helper.isValidGender(self.genderPart2.text!.lowercased()) ? self.genderPart2.text!.lowercased() : "male",
             email2: Helper.isValidEmail(self.emailPart2.text!) ? self.emailPart2.text! : "youreatwithsemail@email.com",
             number2: Helper.isValidNumber(self.numberPart2.text!) ? self.numberPart2.text! : "81234567"
            ) { result in
                if (result.statusCode == 200) {
                    let alert = UIAlertController(title: "Account Successfully Created", message: "We've got everything set up. You're now ready to login to make use of what EatWithWho has to offer!", preferredStyle: .alert)

                    self.present(alert, animated: true)

                    DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                        alert.dismiss(animated: true, completion: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                } else if (result.statusCode == 409) {
                    self.errorWarningLabel.text = "There exists an account with the usernames or emails provided."
                    self.errorWarningLabel.isHidden = false
                } else {
                    self.errorWarningLabel.text = "Something went wrong. Please try again later."
                    self.errorWarningLabel.isHidden = false
                }
             }
        }
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genders.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genders[row].capitalizingFirstLetter()
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (pickerView == genderPicker) {
            genderPart.text = genders[row].capitalizingFirstLetter()
        } else {
            genderPart2.text = genders[row].capitalizingFirstLetter()
        }
    }

    @objc func doneClicked() {
        self.view.endEditing(true)
    }
}
