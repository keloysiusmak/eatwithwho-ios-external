//
//  LoginViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 17/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import Bond

class LoginViewController: CardsViewController, LoginView {

    var loginCard = LoginViewLoginCard()
    let loginViewPresenter = LoginViewPresenter(authService: AuthService(), accountService: AccountService(), friendService: FriendService())

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        self.navigationController?.navigationBar.setValue(false, forKey: "hidesShadow")

        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.collectionView.bounds.width, height: self.collectionView.bounds.height))

        let pastelView = PastelView(frame: view.bounds)

        // Custom Direction
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight

        // Custom Duration
        pastelView.animationDuration = 3.0

        // Custom Color
        pastelView.setColors([UIColor(red: 156/255, green: 39/255, blue: 176/255, alpha: 1.0),
                              UIColor(red: 255/255, green: 64/255, blue: 129/255, alpha: 1.0),
                              UIColor(red: 123/255, green: 31/255, blue: 162/255, alpha: 1.0)])

        pastelView.startAnimation()
        view.insertSubview(pastelView, at: 0)

        self.collectionView.backgroundView = view
//        self.collectionView.backgroundColor = UIColor.pastelRed

        loginViewPresenter.attachView(view: self)

        loginCard.setViewPresenter(loginViewPresenter: loginViewPresenter)

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()

        let signUpButton = UIBarButtonItem(title: "Register", style: .plain, target: self, action: #selector(signUp))
        navigationItem.rightBarButtonItem = signUpButton

        loadCards(cards: [loginCard])
    }

    @objc func signUp() {
        let signUpVC = SignUpViewController()

        signUpVC.setViewPresenter(loginViewPresenter: loginViewPresenter)

        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
}

class LoginViewLoginCard: CardPartsViewController, RoundedCardTrait, TransparentCardTrait {
    var currency: String?
    let errorText = CardPartTextView(type: .title)
    let usernamePart = CardPartTextField()
    let passwordPart = CardPartTextField()
    let loginButtonPart = CardPartButtonView()
    let ai = UIeatwithIndicatorView.init(style: .white)
    let ai_spacer = CardPartSpacerView(height: 14)
    let referenceLoginButtonPart = CardPartButtonView()
    var loginViewPresenter: LoginViewPresenter?
    let usernameStackView = CardPartStackView()
    let passwordStackView = CardPartStackView()
    let friendLoginPart = CardPartButtonView()
    let coupleLoginPart = CardPartButtonView()
    let forgotButtonPart = CardPartButtonView()
    let loginLabel = CardPartTextView(type: .detail)
    let referenceStackView = CardPartStackView()
    let referenceParts = [CardPartTextField(), CardPartTextField(), CardPartTextField(), CardPartTextField(), CardPartTextField(), CardPartTextField()]
    var referenceCounter = 0

    func cornerRadius() -> CGFloat {
        return 20.0
    }

    func setViewPresenter(loginViewPresenter: LoginViewPresenter) {
        self.loginViewPresenter = loginViewPresenter
    }

    override func viewDidLoad() {
        usernamePart.becomeFirstResponder()
        let stackView = CardPartStackView()
        stackView.axis = .vertical
        stackView.margins.top = 60.0

        let logoView = CardPartImageView(frame: CGRect(x: 0, y: 400, width: 400, height: 42))
        logoView.imageName = "logo"
        logoView.layer.masksToBounds = true
        logoView.widthAnchor.constraint(equalToConstant: 400.0).isActive = true
        logoView.heightAnchor.constraint(equalToConstant: 42.0).isActive = true
        logoView.contentMode = .scaleAspectFit
        logoView.image = logoView.image!.withRenderingMode(.alwaysTemplate)
        logoView.tintColor = UIColor.pastel
        logoView.isUserInteractionEnabled = true
        stackView.addArrangedSubview(logoView)
        stackView.addArrangedSubview(CardPartSpacerView(height: 6))

        let separatorStack = CardPartStackView()
        separatorStack.axis = .horizontal
        separatorStack.addArrangedSubview(CardPartSeparatorView())
        separatorStack.distribution = .fillEqually
        separatorStack.alignment = .center
        loginLabel.text = "LOGIN"
        loginLabel.font = Helper.theme.detailTextFont
        loginLabel.textColor = UIColor.white
        loginLabel.textAlignment = .center
        separatorStack.addArrangedSubview(loginLabel)
        separatorStack.addArrangedSubview(CardPartSeparatorView())
        stackView.addArrangedSubview(separatorStack)

        errorText.text = ""
        errorText.font = Helper.theme.detailTextFont.withSize(14.0)
        errorText.textColor = UIColor.white
        errorText.isHidden = true
        stackView.addArrangedSubview(errorText)

        usernameStackView.axis = .vertical
        usernameStackView.spacing = 6.0
        usernamePart.placeholder = "Username"
        usernamePart.maxLength = 60
        usernamePart.font = Helper.theme.normalTextFont.withSize(48.0)
        usernamePart.textColor = UIColor.white
        usernamePart.autocapitalizationType = UITextAutocapitalizationType.none
        usernameStackView.addArrangedSubview(usernamePart)
        stackView.addArrangedSubview(usernameStackView)

        passwordStackView.axis = .vertical
        passwordStackView.spacing = 6.0
        passwordPart.placeholder = "Password"
        passwordPart.maxLength = 60
        passwordPart.isSecureTextEntry = true
        passwordPart.font = Helper.theme.normalTextFont.withSize(48.0)
        passwordPart.textColor = UIColor.white
        passwordStackView.addArrangedSubview(passwordPart)
        stackView.addArrangedSubview(passwordStackView)

        loginButtonPart.backgroundColor = nil
        loginButtonPart.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        loginButtonPart.titleLabel?.font = Helper.theme.normalTextFont.withSize(14.0)
        loginButtonPart.contentHorizontalAlignment = .center
        loginButtonPart.setTitleColor(UIColor.darkGray, for: .normal)
        loginButtonPart.layer.cornerRadius = 10.0
        loginButtonPart.backgroundColor = UIColor.white
        loginButtonPart.setTitle("Log In", for: .normal)
        loginButtonPart.addTarget(self, action: #selector(login), for: .touchUpInside)
        stackView.addArrangedSubview(loginButtonPart)

        referenceStackView.axis = .horizontal
        referenceStackView.spacing = 8.0
        referenceStackView.distribution = .fillEqually
        for referencePart in referenceParts {
            referencePart.maxLength = 1
            referencePart.font = referencePart.font?.withSize(36.0)
            referencePart.textColor = UIColor.white
            referencePart.layer.borderColor = UIColor.white.cgColor
            referencePart.layer.borderWidth = 0.5
            referencePart.layer.cornerRadius = 12.0
            referencePart.heightAnchor.constraint(equalToConstant: 60).isActive = true
            referencePart.textAlignment = .center
            referencePart.autocapitalizationType = .allCharacters
            referencePart.addTarget(self, action: #selector(enterReference), for: .editingChanged)
            referencePart.addTarget(self, action: #selector(beginEnterReference), for: .editingDidBegin)
            referencePart.autocorrectionType = .no
            referenceStackView.addArrangedSubview(referencePart)
        }
        stackView.addArrangedSubview(referenceStackView)
        referenceStackView.isHidden = true

        referenceLoginButtonPart.backgroundColor = nil
        referenceLoginButtonPart.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        referenceLoginButtonPart.titleLabel?.font = Helper.theme.normalTextFont.withSize(14.0)
        referenceLoginButtonPart.contentHorizontalAlignment = .center
        referenceLoginButtonPart.setTitleColor(UIColor.darkGray, for: .normal)
        referenceLoginButtonPart.backgroundColor = UIColor.white
        referenceLoginButtonPart.layer.cornerRadius = 10.0
        referenceLoginButtonPart.setTitle("Log In", for: .normal)
        referenceLoginButtonPart.addTarget(self, action: #selector(referenceLogin), for: .touchUpInside)
        stackView.addArrangedSubview(referenceLoginButtonPart)
        referenceLoginButtonPart.isHidden = true

        let bottomStack = CardPartStackView()
        bottomStack.axis = .vertical
        bottomStack.spacing = 6.0

        bottomStack.addArrangedSubview(ai)
        bottomStack.addArrangedSubview(ai_spacer)
        ai_spacer.isHidden = true

        friendLoginPart.backgroundColor = nil
        friendLoginPart.titleLabel?.font = Helper.theme.normalTextFont.withSize(14.0)
        friendLoginPart.contentHorizontalAlignment = .center
        friendLoginPart.setTitleColor(UIColor.white, for: .normal)
        let friendLoginText = NSMutableAttributedString(string: "Login with a Friend Code", attributes: [NSAttributedString.Key.font: CardPartTextView(type: .detail).font.withSize(14.0), NSAttributedString.Key.foregroundColor: UIColor.white])
        friendLoginText.addAttribute(NSAttributedString.Key.font, value: CardPartTextView(type: .title).font.withSize(14.0), range: NSRange(location: 13, length: 11))
        friendLoginPart.setAttributedTitle(friendLoginText, for: .normal)
        friendLoginPart.addTarget(self, action: #selector(friendLogin), for: .touchUpInside)
        bottomStack.addArrangedSubview(friendLoginPart)

        coupleLoginPart.backgroundColor = nil
        coupleLoginPart.titleLabel?.font = coupleLoginPart.titleLabel?.font.withSize(14.0)
        coupleLoginPart.contentHorizontalAlignment = .center
        coupleLoginPart.setTitleColor(UIColor.white, for: .normal)
        let coupleLoginText = NSMutableAttributedString(string: "Login with Couple Credentials", attributes: [NSAttributedString.Key.font: CardPartTextView(type: .detail).font.withSize(14.0), NSAttributedString.Key.foregroundColor: UIColor.white])
        coupleLoginText.addAttribute(NSAttributedString.Key.font, value: CardPartTextView(type: .title).font.withSize(14.0), range: NSRange(location: 11, length: 18))
        coupleLoginPart.setAttributedTitle(coupleLoginText, for: .normal)
        coupleLoginPart.addTarget(self, action: #selector(coupleLogin), for: .touchUpInside)
        bottomStack.addArrangedSubview(coupleLoginPart)
        coupleLoginPart.isHidden = true

        forgotButtonPart.backgroundColor = nil
        forgotButtonPart.titleLabel?.font = Helper.theme.normalTextFont.withSize(14.0)
        forgotButtonPart.contentHorizontalAlignment = .center
        forgotButtonPart.setTitleColor(UIColor.white, for: .normal)
        forgotButtonPart.setTitle("Forgot your password?", for: .normal)
        forgotButtonPart.addTarget(self, action: #selector(forgotPassword), for: .touchUpInside)
        bottomStack.addArrangedSubview(forgotButtonPart)

        stackView.addArrangedSubview(bottomStack)

        stackView.spacing = 24.0

        setupCardParts([stackView])

        //self.friendLogin()
    }
    @objc func enterReference() {
        if ((self.referenceParts[self.referenceCounter].text?.count)! > 0) {
            if (self.referenceCounter < 5) {
                self.referenceCounter = self.referenceCounter + 1
            }
        } else if (self.referenceCounter > 0) {
            self.referenceCounter = self.referenceCounter - 1
        }
        self.referenceParts[self.referenceCounter].becomeFirstResponder()
    }
    @objc
    func beginEnterReference() {
        self.referenceParts[self.referenceCounter].becomeFirstResponder()
    }

    @objc func forgotPassword() {
        let forgotPasswordVC = ForgotPasswordViewController()

        self.navigationController?.pushViewController(forgotPasswordVC, animated: true)
    }

    @objc func friendLogin() {
        self.coupleLoginPart.isHidden = false
        self.friendLoginPart.isHidden = true
        self.usernameStackView.isHidden = true
        self.passwordStackView.isHidden = true
        self.loginButtonPart.isHidden = true
        self.referenceStackView.isHidden = false
        self.referenceLoginButtonPart.isHidden = false
        self.forgotButtonPart.isHidden = true
        self.ai.isHidden = true
        self.ai_spacer.isHidden = true
    }

    @objc func coupleLogin() {
        self.coupleLoginPart.isHidden = true
        self.friendLoginPart.isHidden = false
        self.usernameStackView.isHidden = false
        self.passwordStackView.isHidden = false
        self.loginButtonPart.isHidden = false
        self.referenceStackView.isHidden = true
        self.referenceLoginButtonPart.isHidden = true
        self.forgotButtonPart.isHidden = false
        self.ai.isHidden = true
        self.ai_spacer.isHidden = true
    }

    @objc func referenceLogin() {
        self.referenceLoginButtonPart.isHidden = true
        self.ai.isHidden = false
        self.ai_spacer.isHidden = false
        self.ai.startAnimating()
        var reference = self.referenceParts.reduce("") { (result, referencePart) -> String in
            return result + (referencePart.text ?? "")
        }
        reference = reference.lowercased()
        self.errorText.isHidden = true

        let token = UserDefaults.standard.string(forKey: "deviceTokenForSNS") ?? ""
        let notificationArn = UserDefaults.standard.string(forKey: "endpointArnForSNS") ?? ""

        if (!reference.isEmpty && reference.count == 6) {
            self.loginViewPresenter!.getFriendId(reference: reference, token: token, notificationArn: notificationArn) { result in
                if (result.statusCode == 200) {
                    let friendId = result.data?.friendId
                    self.loginViewPresenter!.getFriendAccessToken(friendId: friendId!) { (_) in
                        self.loginViewPresenter!.getFriend(friendId: friendId!) { _ in
                            let appDelegateTemp = UIApplication.shared.delegate as? AppDelegate
                            appDelegateTemp?.window?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Friend")
                        }
                    }
                } else if (result.statusCode == 404) {
                    self.referenceLoginButtonPart.isHidden = false
                    self.ai.isHidden = true
                    self.ai_spacer.isHidden = true
                    self.errorText.isHidden = false
                    self.errorText.text = "You have entered an incorrect friend id."
                } else {
                    self.referenceLoginButtonPart.isHidden = false
                    self.ai.isHidden = true
                    self.ai_spacer.isHidden = true
                    self.errorText.isHidden = false
                    self.errorText.text = "Something went wrong. Please try again later."
                }
            }
        } else {
            self.referenceLoginButtonPart.isHidden = false
            self.ai.isHidden = true
            self.ai_spacer.isHidden = true
            self.errorText.isHidden = false
            self.errorText.text = "You have missing fields."
        }
    }
    @objc
    func login() {
        self.loginButtonPart.isHidden = true
        self.ai.isHidden = false
        self.ai_spacer.isHidden = false
        self.ai.startAnimating()
        let username = self.usernamePart.text!
        let password = self.passwordPart.text!
        self.errorText.isHidden = true

        let token = UserDefaults.standard.string(forKey: "deviceTokenForSNS") ?? ""
        let notificationArn = UserDefaults.standard.string(forKey: "endpointArnForSNS") ?? ""

        if (!username.isEmpty && !password.isEmpty) {
            self.loginViewPresenter!.loginAccount(username: username, password: password, token: token, notificationArn: notificationArn) { result in
                if (result.statusCode == 200) {
                    self.loginViewPresenter!.getAccountType(accountId: UserDefaults.standard.string(forKey: "accountId")!, accountType: UserDefaults.standard.string(forKey: "accountType")!) { (result) in
                        if (result.statusCode == 200) {
                            self.loginViewPresenter!.getInitializers(accountId: UserDefaults.standard.string(forKey: "accountId")!) { (_) in
                                let appDelegateTemp = UIApplication.shared.delegate as? AppDelegate
                                appDelegateTemp?.window?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Main")
                            }
                        } else {
                            self.loginButtonPart.isHidden = false
                            self.ai.isHidden = true
                            self.ai_spacer.isHidden = true
                            self.errorText.isHidden = false
                            self.errorText.text = "Something went wrong. Please try again later."
                        }
                    }
                } else if (result.statusCode == 401) {
                    self.loginButtonPart.isHidden = false
                    self.ai.isHidden = true
                    self.ai_spacer.isHidden = true
                    self.errorText.isHidden = false
                    self.errorText.text = "Your login credentials were incorrect."
                } else {
                    self.loginButtonPart.isHidden = false
                    self.ai.isHidden = true
                    self.ai_spacer.isHidden = true
                    self.errorText.isHidden = false
                    self.errorText.text = "Something went wrong. Please try again later."
                }
            }
        } else {
            self.loginButtonPart.isHidden = false
            self.ai.isHidden = true
            self.ai_spacer.isHidden = true
            self.errorText.isHidden = false
            self.errorText.text = "You have missing fields."
        }
    }
}
