//
//  ForgotPasswordViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 28/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import Bond

class ForgotPasswordViewController: CardsViewController {

    var forgotPasswordCard = ForgotPasswordViewForgotPasswordCard()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.backgroundColor = UIColor.white

        loadCards(cards: [forgotPasswordCard])
    }

    override func willMove(toParent: UIViewController?) {
        if (toParent == nil) {
            self.navigationController?.navigationBar.barTintColor = UIColor.pastelRed
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.navigationController?.navigationBar.titleTextAttributes = [
                NSAttributedString.Key.foregroundColor: UIColor.white
            ]
            self.navigationController?.navigationBar.setValue(false, forKey: "hidesShadow")
        } else {
            self.navigationController?.navigationBar.barTintColor = UIColor.white
            self.navigationController?.navigationBar.tintColor = UIColor.pastelRed
            self.navigationController?.navigationBar.titleTextAttributes = [
                NSAttributedString.Key.foregroundColor: UIColor.pastelRed
            ]
            self.navigationController?.navigationBar.setValue(false, forKey: "hidesShadow")
        }
    }
}

class ForgotPasswordViewForgotPasswordCard: CardPartsViewController, TransparentCardTrait {

    let titlePart = CardPartTextView(type: .title)
    let writeupPart = CardPartTextView(type: .normal)
    let emailLabel = CardPartTextView(type: .detail)
    let emailPart = CardPartTextField()

    override func viewDidLoad() {
        emailPart.becomeFirstResponder()
        let stackView = CardPartStackView()
        stackView.axis = .vertical

        titlePart.text = "Did you forget your password?"
        titlePart.lineHeightMultiple = 0.75
        titlePart.textColor = UIColor.darkGray
        titlePart.font = titlePart.font?.withSize(48.0)
        stackView.addArrangedSubview(titlePart)

        writeupPart.text = "Don't worry, we've got you covered. Fill in the email you used to sign up with your account and we'll drop you an email with more details on how you can recover your account."
        writeupPart.textColor = UIColor.darkGray
        writeupPart.font = writeupPart.font?.withSize(16.0)
        stackView.addArrangedSubview(writeupPart)

        let separatorStack = CardPartStackView()
        separatorStack.axis = .horizontal
        separatorStack.addArrangedSubview(CardPartSeparatorView())
        separatorStack.distribution = .fillEqually
        separatorStack.alignment = .center
        let loginLabel = CardPartTextView(type: .detail)
        loginLabel.text = "RESET YOUR PASSWORD"
        loginLabel.textColor = UIColor.darkGray
        loginLabel.textAlignment = .center
        separatorStack.addArrangedSubview(loginLabel)
        separatorStack.addArrangedSubview(CardPartSeparatorView())
        stackView.addArrangedSubview(separatorStack)

        let emailStackView = CardPartStackView()
        emailStackView.axis = .vertical
        emailStackView.spacing = 6.0
        emailLabel.text = "LAST NAME"
        emailLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        emailLabel.textColor = UIColor.gray
        emailPart.placeholder = "Last Name"
        emailPart.maxLength = 60
        emailPart.font = Helper.theme.normalTextFont.withSize(24.0)
        emailPart.textColor = UIColor.darkGray
        emailPart.autocapitalizationType = UITextAutocapitalizationType.none
        emailStackView.addArrangedSubview(emailLabel)
        emailStackView.addArrangedSubview(emailPart)
        stackView.addArrangedSubview(emailStackView)

        let resetButtonPart = CardPartButtonView()
        resetButtonPart.backgroundColor = UIColor.pastelRed
        resetButtonPart.titleLabel?.font = resetButtonPart.titleLabel?.font.withSize(14.0)
        resetButtonPart.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        resetButtonPart.contentHorizontalAlignment = .center
        resetButtonPart.setTitleColor(UIColor.white, for: .normal)
        resetButtonPart.setTitle("Reset Your Password", for: .normal)
        resetButtonPart.addTarget(self, action: #selector(resetPassword), for: .touchUpInside)
        resetButtonPart.layer.cornerRadius = 10.0
        stackView.addArrangedSubview(resetButtonPart)

        stackView.spacing = 24.0

        setupCardParts([stackView])
    }

    @objc func resetPassword() {
        view.endEditing(true)
    }
}
