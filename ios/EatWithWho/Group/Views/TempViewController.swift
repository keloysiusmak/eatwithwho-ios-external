//
//  TempViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 17/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import UIKit

class TempViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        Helper.deleteUserDefaults()

        do {
            try FileManager.default.removeItem(atPath: Budget.ArchiveURL.path)
            try FileManager.default.removeItem(atPath: Couple.ArchiveURL.path)
            try FileManager.default.removeItem(atPath: Schedule.ArchiveURL.path)
        } catch {
            //
        }

        let appDelegateTemp = UIApplication.shared.delegate as? AppDelegate
        appDelegateTemp?.window?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Login")
    }

}
