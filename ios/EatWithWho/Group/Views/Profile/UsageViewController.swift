//
//  UsageViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 02/03/20.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import RxDataSources
import Bond

class UsageViewController: CardsViewController, UsageView, NVeatwithIndicatorViewable {

    let usageCard = UsageViewUsageCard()

    private let usageViewPresenter = UsageViewPresenter(coupleService: CoupleService())

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: UIControlEvents.valueChanged)

        return refreshControl
    }()

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {

        self.usageViewPresenter.getUsage(userId: UserDefaults.standard.string(forKey: "userId")!) {_ in
            refreshControl.endRefreshing()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        NVeatwithIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
        self.collectionView.addSubview(refreshControl)
        self.collectionView.backgroundColor = UIColor.white

        usageViewPresenter.attachView(view: self)
        usageViewPresenter.loadUsage(userId: UserDefaults.standard.string(forKey: "userId")!) { _ in

        }
        NetworkManager.sharedInstance.reachability.whenReachable = { _ in
            self.usageViewPresenter.loadUsage(userId: UserDefaults.standard.string(forKey: "userId")!) { _ in

            }
        }
    }

    func presentValues(filesSize: Int, friendsCount: Int) {
        usageCard.setValues(filesSize: filesSize, friendsCount: friendsCount)

        loadCards(cards: [usageCard])
        NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
    }
}

class UsageViewUsageCard: CardPartsViewController, TransparentCardTrait, NoTopBottomMarginsCardTrait {

    let storageTitleBar = CardPartTextView(type: .title)
    let storageSubtitleBar = CardPartTextView(type: .detail)
    let storagePercentageBar = CardPartBarView()
    let storageUsedPart = CardPartTextView(type: .title)
    let storageRemainingPart = CardPartTextView(type: .title)
    let storageQuotaPart = CardPartTextView(type: .title)

    let friendsTitleBar = CardPartTextView(type: .title)
    let friendsSubtitleBar = CardPartTextView(type: .detail)
    let friendsPercentageBar = CardPartBarView()
    let friendsUsedPart = CardPartTextView(type: .title)
    let friendsRemainingPart = CardPartTextView(type: .title)
    let friendsQuotaPart = CardPartTextView(type: .title)

    let moreTitleBar = CardPartTextView(type: .title)
    let moreSubtitleBar = CardPartTextView(type: .detail)

    let subscription = UserDefaults.standard.string(forKey: "coupleSubscription")!

    var filesSize: Int? = 0

    override func viewDidLoad() {

        let usageStack = CardPartStackView()
        usageStack.axis = .vertical
        usageStack.spacing = 6.0

        storageTitleBar.text = "File Storage"
        storageTitleBar.font = storageTitleBar.font.withSize(24.0)
        storageTitleBar.textColor = UIColor.darkGray

        storageSubtitleBar.text = "Shows how much space you have used, and how much you have left."
        storageSubtitleBar.font = storageSubtitleBar.font.withSize(14.0)
        storageSubtitleBar.textColor = UIColor.gray

        let storageBottomStack = CardPartStackView()
        storageBottomStack.axis = .horizontal
        storageBottomStack.distribution = .fillEqually
        storageBottomStack.alignment = .top

        storageUsedPart.font = storageUsedPart.font.withSize(14.0)
        storageUsedPart.textColor = UIColor.black

        storageRemainingPart.font = storageRemainingPart.font.withSize(14.0)
        storageRemainingPart.textColor = UIColor.black

        let storageLeftUsageStack = CardPartStackView()
        storageLeftUsageStack.axis = .vertical
        storageLeftUsageStack.spacing = 6.0

        storageLeftUsageStack.addArrangedSubview(storageUsedPart)
        storageLeftUsageStack.addArrangedSubview(storageRemainingPart)

        storageQuotaPart.font = storageQuotaPart.font.withSize(14.0)
        storageQuotaPart.textColor = UIColor.black
        storageQuotaPart.textAlignment = .right

        storagePercentageBar.barHeight = 2.0
        storagePercentageBar.barColor = UIColor.pastelGreen
        storagePercentageBar.verticalLine.removeFromSuperlayer()
        storagePercentageBar.heightAnchor.constraint(equalToConstant: 2.0).isActive = true

        storageBottomStack.addArrangedSubview(storageLeftUsageStack)
        storageBottomStack.addArrangedSubview(storageQuotaPart)

        friendsTitleBar.text = "Friends"
        friendsTitleBar.font = friendsTitleBar.font.withSize(24.0)
        friendsTitleBar.textColor = UIColor.darkGray

        friendsSubtitleBar.text = "Shows how many friends you've assigned to your eatwith."
        friendsSubtitleBar.font = friendsSubtitleBar.font.withSize(14.0)
        friendsSubtitleBar.textColor = UIColor.gray

        let friendsBottomStack = CardPartStackView()
        friendsBottomStack.axis = .horizontal
        friendsBottomStack.distribution = .fillEqually
        friendsBottomStack.alignment = .top

        friendsUsedPart.font = friendsUsedPart.font.withSize(14.0)
        friendsUsedPart.textColor = UIColor.black

        friendsRemainingPart.font = friendsRemainingPart.font.withSize(14.0)
        friendsRemainingPart.textColor = UIColor.black

        let friendsLeftUsageStack = CardPartStackView()
        friendsLeftUsageStack.axis = .vertical
        friendsLeftUsageStack.spacing = 6.0

        friendsLeftUsageStack.addArrangedSubview(friendsUsedPart)
        friendsLeftUsageStack.addArrangedSubview(friendsRemainingPart)

        friendsQuotaPart.font = friendsQuotaPart.font.withSize(14.0)
        friendsQuotaPart.textColor = UIColor.black
        friendsQuotaPart.textAlignment = .right

        friendsPercentageBar.barHeight = 2.0
        friendsPercentageBar.barColor = UIColor.pastelGreen
        friendsPercentageBar.verticalLine.removeFromSuperlayer()
        friendsPercentageBar.heightAnchor.constraint(equalToConstant: 2.0).isActive = true

        friendsBottomStack.addArrangedSubview(friendsLeftUsageStack)
        friendsBottomStack.addArrangedSubview(friendsQuotaPart)

        moreTitleBar.text = "Do you need more from us?"
        moreTitleBar.font = moreTitleBar.font.withSize(24.0)
        moreTitleBar.textColor = UIColor.darkGray

        moreSubtitleBar.text = "We've got you covered. Check out our other plans and find a plan that works for you."
        moreSubtitleBar.font = moreSubtitleBar.font.withSize(14.0)
        moreSubtitleBar.textColor = UIColor.gray

        let moreButton = CardPartButtonView()

        moreButton.backgroundColor = UIColor.pastelRed
        moreButton.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        moreButton.titleLabel?.font = moreButton.titleLabel?.font.withSize(14.0)
        moreButton.layer.cornerRadius = 10.0
        moreButton.contentHorizontalAlignment = .center
        moreButton.setTitleColor(UIColor.white, for: .normal)
        moreButton.setTitle("Our Plans", for: .normal)
        moreButton.margins.top = 24.0
        moreButton.margins.bottom = 6.0
        moreButton.addTarget(self, action: #selector(comparePlans), for: .touchUpInside)

        usageStack.addArrangedSubview(storageTitleBar)
        usageStack.addArrangedSubview(storageSubtitleBar)
        usageStack.addArrangedSubview(CardPartSpacerView(height: 14))
        usageStack.addArrangedSubview(storagePercentageBar)
        usageStack.addArrangedSubview(storageBottomStack)
        usageStack.addArrangedSubview(CardPartSpacerView(height: 14))
        usageStack.addArrangedSubview(CardPartSeparatorView())
        usageStack.addArrangedSubview(CardPartSpacerView(height: 14))
        usageStack.addArrangedSubview(friendsTitleBar)
        usageStack.addArrangedSubview(friendsSubtitleBar)
        usageStack.addArrangedSubview(CardPartSpacerView(height: 14))
        usageStack.addArrangedSubview(friendsPercentageBar)
        usageStack.addArrangedSubview(friendsBottomStack)
        usageStack.addArrangedSubview(CardPartSpacerView(height: 14))
        usageStack.addArrangedSubview(CardPartSeparatorView())
        usageStack.addArrangedSubview(CardPartSpacerView(height: 14))
        usageStack.addArrangedSubview(moreTitleBar)
        usageStack.addArrangedSubview(moreSubtitleBar)
        usageStack.addArrangedSubview(CardPartSpacerView(height: 14))
        usageStack.addArrangedSubview(moreButton)

        setupCardParts([usageStack])
    }

    @objc func comparePlans() {
        NetworkManager.isReachable { _ in
            let openPlansVC = PlansViewController()

            self.navigationController?.pushViewController(openPlansVC, animated: true)
        }
        NetworkManager.isUnreachable { _ in
            let alert = UIAlertController(title: "No Internet Connection", message: "", preferredStyle: .alert)

            self.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }

    func setValues(filesSize: Int, friendsCount: Int) {
        let storageQuotaSize = Helper.getCoupleSubscription(subscription).maxFilesSize

        let storageUsedText = ByteCountFormatter.string(fromByteCount: Int64(filesSize), countStyle: .file)
        let storageUsedAttrText = NSMutableAttributedString(string: "Used: " + storageUsedText, attributes: [NSAttributedString.Key.font: CardPartTextView(type: .title).font.withSize(16.0)])
        storageUsedAttrText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray, range: NSRange(location: 0, length: 5))
        storageUsedAttrText.addAttribute(NSAttributedString.Key.font, value: Helper.theme.detailTextFont, range: NSRange(location: 0, length: 5))
        storageUsedPart.attributedText = storageUsedAttrText

        let storageRemainingText = ByteCountFormatter.string(fromByteCount: Int64(storageQuotaSize - filesSize), countStyle: .file)
        let storageRemainingAttrText = NSMutableAttributedString(string: "Remaining: " + storageRemainingText, attributes: [NSAttributedString.Key.font: CardPartTextView(type: .title).font.withSize(16.0)])
        storageRemainingAttrText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray, range: NSRange(location: 0, length: 10))
        storageRemainingAttrText.addAttribute(NSAttributedString.Key.font, value: Helper.theme.detailTextFont, range: NSRange(location: 0, length: 10))
        storageRemainingPart.attributedText = storageRemainingAttrText

        let storageQuotaText = ByteCountFormatter.string(fromByteCount: Int64(storageQuotaSize), countStyle: .file)
        let storageQuotaAttrText = NSMutableAttributedString(string: "Quota: " + storageQuotaText, attributes: [NSAttributedString.Key.font: CardPartTextView(type: .title).font.withSize(16.0)])
        storageQuotaAttrText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray, range: NSRange(location: 0, length: 6))
        storageQuotaAttrText.addAttribute(NSAttributedString.Key.font, value: Helper.theme.detailTextFont, range: NSRange(location: 0, length: 6))
        storageQuotaPart.attributedText = storageQuotaAttrText

        let storagePercentage = Double(filesSize) / Double(storageQuotaSize)
        storagePercentageBar.percent = storagePercentage

        let friendsQuotaCount = Helper.getCoupleSubscription(subscription).maxFriends

        let friendsUsedAttrText = NSMutableAttributedString(string: "Used: " + String(friendsCount), attributes: [NSAttributedString.Key.font: CardPartTextView(type: .title).font.withSize(16.0)])
        friendsUsedAttrText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray, range: NSRange(location: 0, length: 5))
        friendsUsedAttrText.addAttribute(NSAttributedString.Key.font, value: Helper.theme.detailTextFont, range: NSRange(location: 0, length: 5))
        friendsUsedPart.attributedText = friendsUsedAttrText

        let friendsRemainingAttrText = NSMutableAttributedString(string: "Remaining: " + String(friendsQuotaCount - friendsCount), attributes: [NSAttributedString.Key.font: CardPartTextView(type: .title).font.withSize(16.0)])
        friendsRemainingAttrText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray, range: NSRange(location: 0, length: 10))
        friendsRemainingAttrText.addAttribute(NSAttributedString.Key.font, value: Helper.theme.detailTextFont, range: NSRange(location: 0, length: 10))
        friendsRemainingPart.attributedText = friendsRemainingAttrText

        let friendsQuotaAttrText = NSMutableAttributedString(string: "Quota: " + String(friendsQuotaCount), attributes: [NSAttributedString.Key.font: CardPartTextView(type: .title).font.withSize(16.0)])
        friendsQuotaAttrText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray, range: NSRange(location: 0, length: 6))
        friendsQuotaAttrText.addAttribute(NSAttributedString.Key.font, value: Helper.theme.detailTextFont, range: NSRange(location: 0, length: 6))
        friendsQuotaPart.attributedText = friendsQuotaAttrText

        let friendsPercentage = Double(friendsCount) / Double(friendsQuotaCount)
        friendsPercentageBar.percent = friendsPercentage
    }
}
