//
//  ProfileViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 02/03/20.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import Bond

class ProfileViewController: CardsViewController, ProfileView, NVeatwithIndicatorViewable {

    var account: Account?
    var schedule: Schedule?
    let newProfileCard = ProfileViewProfileCard()

    let profileViewPresenter = ProfileViewPresenter(accountService: AccountService(), scheduleService: ScheduleService())

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: UIControlEvents.valueChanged)

        return refreshControl
    }()

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {

        self.profileViewPresenter.getAccount(accountId: UserDefaults.standard.string(forKey: "accountId")!) {_ in
            refreshControl.endRefreshing()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.addSubview(refreshControl)
        NVeatwithIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)

        self.collectionView.backgroundColor = UIColor.white

        profileViewPresenter.attachView(view: self)
        profileViewPresenter.loadAccount(accountId: UserDefaults.standard.string(forKey: "accountId")!) { _ in

        }
        profileViewPresenter.loadSchedule(scheduleId: UserDefaults.standard.string(forKey: "scheduleId")!) { _ in

        }

        NetworkManager.sharedInstance.reachability.whenReachable = { _ in
            self.profileViewPresenter.loadAccount(accountId: UserDefaults.standard.string(forKey: "accountId")!) { _ in

            }
            self.profileViewPresenter.loadSchedule(scheduleId: UserDefaults.standard.string(forKey: "scheduleId")!) { _ in

            }
        }
        let imageView = CardPartImageView(frame: CGRect(x: 0, y: 400, width: 24, height: 24))
        imageView.imageName = "logout"
        imageView.layer.masksToBounds = true
        imageView.widthAnchor.constraint(equalToConstant: 24.0).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
        imageView.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(logout))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tap)

        navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: imageView)
    }

    func securitySettings() {
        NetworkManager.isReachable { _ in
            let manageProfileVC = ManageProfileViewController()
            manageProfileVC.initViewPresenter(profileViewPresenter: self.profileViewPresenter)

            let nc = UINavigationController(rootViewController: manageProfileVC)
            self.navigationController?.present(nc, animated: true, completion: { () -> Void   in

            })
        }
        NetworkManager.isUnreachable { _ in
            let alert = UIAlertController(title: "No Internet Connection", message: "", preferredStyle: .alert)

            self.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }

    @objc func logout() {
        newProfileCard.logout()
    }

    func presentValues(account: Account) {
        self.account = account
        newProfileCard.setValues(account: account)

    }

    func presentValues(schedule: Schedule) {
        self.schedule = schedule
        newProfileCard.setValues(schedule: schedule)

        var cards: [CardController] = []
        self.newProfileCard.setController(controller: self)
        cards.append(self.newProfileCard)

        self.loadCards(cards: cards)

        NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
    }
}

class ProfileViewProfileCard: CardPartsViewController, TransparentCardTrait, NoTopBottomMarginsCardTrait {
    var namePart = CardPartTextView(type: .title)
    var joinedDatePart = CardPartTextView(type: .detail)

    var stackView = CardPartStackView()
    var logoutButtonPart = CardPartButtonView()
    var account: Account?
    var controller: ProfileViewController?
    var schedule: Schedule?
    let percentageBar = CardPartBarView()
    let profileTitle = CardPartTextView(type: .detail)
    let profileDays = CardPartTextView(type: .detail)

    let subscriptionTitle = CardPartTextView(type: .detail)
    let subscriptionSubtitle = CardPartTextView(type: .normal)

    public func setController(controller: ProfileViewController) {
        self.controller = controller
    }

    public func setValues(account: Account) {
        self.account = account
        namePart.text = account.friend.firstName + " " + account.friend.lastName
        joinedDatePart.text = "Friend Since " + Helper.formatDate(text: String(account.createdAt), format: "d MMMM YYYY")

        if (self.schedule != nil) {
            let percentage = Double(Int(NSDate().timeIntervalSince1970) - account.friend.createdAt) / Double(self.schedule!.date - account.friend.createdAt)
            percentageBar.percent = percentage
            resetPercentageBar()
        }
    }
    public func setValues(schedule: Schedule) {
        self.schedule = schedule
        if (self.account != nil) {
            let percentage = Double(Int(NSDate().timeIntervalSince1970) - self.account!.friend.createdAt) / Double(schedule.date - self.account!.friend.createdAt)
            percentageBar.percent = percentage
            resetPercentageBar()
        }
        let numberOfDays = Helper.timeDifferenceFull(time1: schedule.date, time2: Int(NSDate().timeIntervalSince1970), allowedUnits: [.day])

        let daysText = NSMutableAttributedString(string: numberOfDays.uppercased() + " TO " + schedule.name.uppercased(), attributes: [NSAttributedString.Key.font: CardPartTextView(type: .detail).font.withSize(12.0), NSAttributedString.Key.foregroundColor: UIColor.gray])
        daysText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range: NSRange(location: 0, length: numberOfDays.count))
        daysText.addAttribute(NSAttributedString.Key.font, value: CardPartTextView(type: .detail).font.withSize(12.0), range: NSRange(location: 0, length: numberOfDays.count))
        profileDays.attributedText = daysText

    }
    private func resetPercentageBar() {
        let percentage = percentageBar.percent
        if (percentage < 0.6) {
            percentageBar.barColor = UIColor.pastelGreen
            profileTitle.text = "Planning early helps to alleviate much of the stresses that come with eatwith planning."
        } else if (percentage < 0.8) {
            percentageBar.barColor = UIColor.pastelOrange
            profileTitle.text = "Your eatwith is coming up pretty soon, and we're so excited for you."
        } else {
            let numberOfDays = Helper.timeDifferenceFull(time1: self.schedule!.date, time2: Int(NSDate().timeIntervalSince1970), allowedUnits: [.day])
            percentageBar.barColor = UIColor.pastelRed
            profileTitle.text = "The countdown has begun! " + numberOfDays + " to " + self.schedule!.name + ". Are you ready?"
        }
    }

    override func viewDidLoad() {
        stackView.axis = .horizontal
        stackView.spacing = 20.0

        let rect = CGRect(origin: .zero, size: CGSize(width: 50.0, height: 50.0))
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        UIColor.darkGray.setFill()
        UIRectFill(rect)

        let imageView = CardPartImageView(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
        if (UserDefaults.standard.string(forKey: "friendImageType") != nil && UserDefaults.standard.string(forKey: "friendImageType") != "none") {
            imageView.setImage(friendId: UserDefaults.standard.string(forKey: "friendId")!, imageType: UserDefaults.standard.string(forKey: "friendImageType")!, modifiedAt: UserDefaults.standard.integer(forKey: "friendModifiedAt"))
        } else {
            imageView.imageName = "default"
        }
        imageView.layer.cornerRadius = 35.0
        imageView.layer.masksToBounds = true
        imageView.widthAnchor.constraint(equalToConstant: 70.0).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 70.0).isActive = true
        imageView.contentMode = .scaleAspectFill

        namePart.font = namePart.font.withSize(24.0)
        namePart.textColor = UIColor.black

        joinedDatePart.font = joinedDatePart.font.withSize(14.0)

        let nameStack = CardPartStackView()
        nameStack.axis = .vertical
        nameStack.spacing = 0.0
        nameStack.addArrangedSubview(namePart)
        nameStack.addArrangedSubview(joinedDatePart)
        stackView.alignment = .center
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(nameStack)

        let spacerView = UIView()
        spacerView.setContentHuggingPriority(.defaultLow, for: .horizontal)

        stackView.addArrangedSubview(spacerView)

        logoutButtonPart.backgroundColor = UIColor.pastelGreen
        logoutButtonPart.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        logoutButtonPart.titleLabel?.font = logoutButtonPart.titleLabel?.font.withSize(16.0)
        logoutButtonPart.contentHorizontalAlignment = .center
        logoutButtonPart.setTitleColor(UIColor.white, for: .normal)
        logoutButtonPart.layer.cornerRadius = 24.0
        logoutButtonPart.setTitle("Logout", for: .normal)
        logoutButtonPart.addTarget(self, action: #selector(logout), for: .touchUpInside)

        let subscriptionStackView = CardPartStackView()
        subscriptionStackView.axis = .vertical
        subscriptionStackView.spacing = 4.0
        subscriptionStackView.margins.top = 20.0

        subscriptionTitle.font = subscriptionTitle.font.withSize(21.0)
        subscriptionTitle.textColor = UIColor.black
        subscriptionSubtitle.font = subscriptionSubtitle.font.withSize(14.0)
        subscriptionSubtitle.textColor = UIColor.gray

        let subscriptionFormatted = Helper.formatCoupleSubscription("free")

        let subscriptionTitleText = NSMutableAttributedString(string: "Subscription: " + subscriptionFormatted, attributes: [NSAttributedString.Key.font: CardPartTextView(type: .detail).font.withSize(21.0), NSAttributedString.Key.foregroundColor: UIColor.black])
        subscriptionTitleText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.pastelRed, range: NSRange(location: 14, length: subscriptionFormatted.count))
        subscriptionTitleText.addAttribute(NSAttributedString.Key.font, value: CardPartTextView(type: .detail).font.withSize(21.0), range: NSRange(location: 14, length: subscriptionFormatted.count))
        subscriptionTitle.attributedText = subscriptionTitleText

        subscriptionSubtitle.text = Helper.formatCoupleSubscriptionWriteup("free")

        let comparePlansButtonPart = CardPartButtonView()

        comparePlansButtonPart.backgroundColor = UIColor.pastel
        comparePlansButtonPart.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        comparePlansButtonPart.titleLabel?.font = comparePlansButtonPart.titleLabel?.font.withSize(14.0)
        comparePlansButtonPart.layer.cornerRadius = 10.0
        comparePlansButtonPart.contentHorizontalAlignment = .center
        comparePlansButtonPart.setTitleColor(UIColor.darkGray, for: .normal)
        comparePlansButtonPart.setTitle("Compare Plans", for: .normal)
        comparePlansButtonPart.margins.top = 24.0
        comparePlansButtonPart.margins.bottom = 6.0
        comparePlansButtonPart.addTarget(self, action: #selector(comparePlans), for: .touchUpInside)

        subscriptionStackView.addArrangedSubview(subscriptionTitle)
        subscriptionStackView.addArrangedSubview(subscriptionSubtitle)
        subscriptionStackView.addArrangedSubview(CardPartSpacerView(height: 12.0))
        subscriptionStackView.addArrangedSubview(comparePlansButtonPart)

        let profileStackView = CardPartStackView()
        profileStackView.axis = .vertical
        profileStackView.spacing = 4.0
        profileStackView.margins.top = 20.0

        profileTitle.font = profileTitle.font.withSize(18.0)
        profileTitle.textColor = UIColor.black

        let profileWriteup = CardPartTextView(type: .normal)
        profileWriteup.font = profileWriteup.font.withSize(14.0)
        profileWriteup.textColor = UIColor.gray
        profileWriteup.text = "Find out more on how you can maximize what EatWithWho can offer. Let's make eatwiths smarter."

        let profileButton = CardPartButtonView()
        profileButton.backgroundColor = nil
        profileButton.titleLabel?.font = profileTitle.font.withSize(14.0)
        profileButton.setTitleColor(UIColor.pastelRed, for: .normal)
        profileButton.setTitle("Show Me More", for: .normal)
        //profileButton.addTarget(self, action: #selector(showQuickstart), for: .touchUpInside)

        percentageBar.barHeight = 2.0
        percentageBar.verticalLine.removeFromSuperlayer()
        percentageBar.heightAnchor.constraint(equalToConstant: 6.0).isActive = true

        profileDays.textAlignment = .right
        profileDays.textColor = .gray

        profileStackView.addArrangedSubview(percentageBar)
        profileStackView.addArrangedSubview(profileDays)
        profileStackView.addArrangedSubview(CardPartSpacerView(height: 16.0))
        profileStackView.addArrangedSubview(CardPartSeparatorView())
        profileStackView.addArrangedSubview(CardPartSpacerView(height: 12.0))
        profileStackView.addArrangedSubview(subscriptionStackView)
        profileStackView.addArrangedSubview(CardPartSpacerView(height: 16.0))
        profileStackView.addArrangedSubview(CardPartSeparatorView())
        profileStackView.addArrangedSubview(CardPartSpacerView(height: 12.0))
        profileStackView.addArrangedSubview(profileTitle)
        profileStackView.addArrangedSubview(profileWriteup)
        profileStackView.addArrangedSubview(profileButton)
        profileStackView.addArrangedSubview(CardPartSpacerView(height: 12.0))
        profileStackView.addArrangedSubview(CardPartSeparatorView())

        let linksStack = CardPartStackView()
        linksStack.axis = .vertical
        linksStack.spacing = 8.0

        let securitySettingsButtonPart = CardPartButtonView()

        securitySettingsButtonPart.backgroundColor = UIColor.pastel
        securitySettingsButtonPart.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        securitySettingsButtonPart.titleLabel?.font = securitySettingsButtonPart.titleLabel?.font.withSize(14.0)
        securitySettingsButtonPart.layer.cornerRadius = 10.0
        securitySettingsButtonPart.contentHorizontalAlignment = .center
        securitySettingsButtonPart.setTitleColor(UIColor.darkGray, for: .normal)
        securitySettingsButtonPart.setTitle("Security Settings", for: .normal)
        securitySettingsButtonPart.margins.top = 24.0
        securitySettingsButtonPart.margins.bottom = 6.0
        securitySettingsButtonPart.addTarget(self, action: #selector(securitySettings), for: .touchUpInside)

        let subscriptionUsageButtonPart = CardPartButtonView()

        subscriptionUsageButtonPart.backgroundColor = UIColor.pastel
        subscriptionUsageButtonPart.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        subscriptionUsageButtonPart.titleLabel?.font = subscriptionUsageButtonPart.titleLabel?.font.withSize(14.0)
        subscriptionUsageButtonPart.layer.cornerRadius = 10.0
        subscriptionUsageButtonPart.contentHorizontalAlignment = .center
        subscriptionUsageButtonPart.setTitleColor(UIColor.darkGray, for: .normal)
        subscriptionUsageButtonPart.setTitle("Subscription Usage", for: .normal)
        subscriptionUsageButtonPart.margins.top = 24.0
        subscriptionUsageButtonPart.margins.bottom = 6.0
        subscriptionUsageButtonPart.addTarget(self, action: #selector(subscriptionUsage), for: .touchUpInside)

        let logoutLinkPart = CardPartButtonView()

        logoutLinkPart.backgroundColor = UIColor.pastelRed
        logoutLinkPart.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        logoutLinkPart.titleLabel?.font = logoutLinkPart.titleLabel?.font.withSize(14.0)
        logoutLinkPart.layer.cornerRadius = 10.0
        logoutLinkPart.contentHorizontalAlignment = .center
        logoutLinkPart.setTitleColor(UIColor.white, for: .normal)
        logoutLinkPart.setTitle("Logout", for: .normal)
        logoutLinkPart.margins.top = 24.0
        logoutLinkPart.margins.bottom = 6.0
        logoutLinkPart.addTarget(self, action: #selector(logout), for: .touchUpInside)

        linksStack.addArrangedSubview(subscriptionUsageButtonPart)
        linksStack.addArrangedSubview(securitySettingsButtonPart)
        linksStack.addArrangedSubview(logoutLinkPart)

        setupCardParts([stackView, profileStackView, CardPartSpacerView(height: 12), linksStack])
    }
    @objc func securitySettings() {
        self.controller?.securitySettings()
    }

    @objc func comparePlans() {
        NetworkManager.isReachable { _ in
            let openPlansVC = PlansViewController()

            self.navigationController?.pushViewController(openPlansVC, animated: true)
        }
        NetworkManager.isUnreachable { _ in
            let alert = UIAlertController(title: "No Internet Connection", message: "", preferredStyle: .alert)

            self.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            alert.dismiss(animated: true, completion: nil)
 }
        }
    }

    @objc func subscriptionUsage() {
        NetworkManager.isReachable { _ in
            let subscriptionUsageVC = UsageViewController()

            self.navigationController?.pushViewController(subscriptionUsageVC, animated: true)
        }
        NetworkManager.isUnreachable { _ in
            let alert = UIAlertController(title: "No Internet Connection", message: "", preferredStyle: .alert)

            self.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }

    @objc func logout() {
        NVeatwithIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
        self.controller!.profileViewPresenter.logoutAccount(accountId: UserDefaults.standard.string(forKey: "accountId")!) {_ in
            Helper.deleteUserDefaults()
            NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()

            let appDelegateTemp = UIApplication.shared.delegate as? AppDelegate
            appDelegateTemp?.window?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
        }
    }
}
