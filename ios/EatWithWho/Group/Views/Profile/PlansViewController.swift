//
//  PlansViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 02/03/20.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import RxDataSources
import Bond

class PlansViewController: CardsViewController, PlansView {

    let plansCard = PlansViewPlansCard()

    private let plansViewPresenter = PlansViewPresenter()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = "Plans"

        self.collectionView.backgroundColor = UIColor.white

        plansViewPresenter.attachView(view: self)

        loadCards(cards: [plansCard])
    }
}

class PlansViewPlansCard: CardPartsViewController, TransparentCardTrait, NoTopBottomMarginsCardTrait, CardPartCollectionViewDelegte {

    lazy var collectionViewPart = CardPartCollectionView(collectionViewLayout: self.collectionViewLayout)
    var collectionViewLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        layout.itemSize = CGSize(width: 300, height: 600)
        return layout
    }()
//    var plansViewModel = PlansViewModel(items: ["free", "tier1", "tier2"])
    var plansViewModel = PlansViewModel(items: ["free"])
    let subscription = UserDefaults.standard.string(forKey: "coupleSubscription")!

    override func viewDidLoad() {

        collectionViewPart.delegate = self

        collectionViewPart.collectionView.register(PlansViewPlanCell.self, forCellWithReuseIdentifier: "PlanCell")
        collectionViewPart.collectionView.showsHorizontalScrollIndicator = false

        let dataSource = RxCollectionViewSectionedReloadDataSource<PlanStruct>(configureCell: {[weak self] (_, collectionView, indexPath, data) -> UICollectionViewCell in

            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlanCell", for: indexPath) as? PlansViewPlanCell else { return UICollectionViewCell() }

            cell.setData(data, self!.subscription)

            return cell
        })

        plansViewModel.data.asObservable().bind(to: collectionViewPart.collectionView.rx.items(dataSource: dataSource)).disposed(by: bag)
        collectionViewPart.collectionView.frame = CGRect(x: 0, y: 0, width: 300, height: 600)

        collectionViewPart.collectionView.backgroundColor = UIColor.white

        let planStack = CardPartStackView()
        planStack.axis = .vertical
        planStack.spacing = 6.0

        planStack.addArrangedSubview(collectionViewPart)

        setupCardParts([planStack])
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //
    }
}

class PlansViewPlanCell: CardPartCollectionViewCardPartsCell {

    let planStackView = CardPartStackView()
    let namePart = CardPartTextView(type: .detail)
    let pricePart = CardPartTextView(type: .normal)
    let statusPart = CardPartTextView(type: .detail)
    let featureStackView = CardPartStackView()

    override init(frame: CGRect) {

        super.init(frame: frame)

        self.backgroundColor = UIColor.pastel
        self.layer.cornerRadius = 6.0

        planStackView.axis = .vertical
        planStackView.alignment = .leading
        planStackView.spacing = 6
        planStackView.margins = UIEdgeInsets(top: 20, left: 50, bottom: 20, right: 50)

        featureStackView.axis = .vertical
        featureStackView.spacing = 12

        namePart.textColor = UIColor.darkGray
        namePart.font = namePart.font.withSize(32.0)

        statusPart.font = statusPart.font.withSize(24.0)

        planStackView.addArrangedSubview(namePart)
        planStackView.addArrangedSubview(pricePart)
        planStackView.addArrangedSubview(CardPartSpacerView(height: 14.0))
        planStackView.addArrangedSubview(featureStackView)
        planStackView.addArrangedSubview(CardPartSpacerView(height: 44.0))
        planStackView.addArrangedSubview(statusPart)

        setupCardParts([planStackView])
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setData(_ plan: String, _ subscription: String) {
        var pricePartText = ""
        var featuresA: [String] = []
        var featuresB: [String] = []
        switch plan {
        case "free":
            namePart.text = "Free"
            pricePartText = "$0/forever"
            featuresA = ["10MB", "20", "Unlimited", "Unlimited", "Unlimited", "", ""]
            featuresB = ["Storage", "Friends", "Guests", "eatwiths", "Records", "Notification Reminders", "Email Support"]
        case "tier1":
            namePart.text = "The Organized"
            pricePartText = "$0.99/forever"
            featuresA = ["50MB", "50", "Unlimited", "Unlimited", "Unlimited", "", ""]
            featuresB = ["Storage", "Friends", "Guests", "eatwiths", "Records", "Notification Reminders", "Email Support"]
        case "tier2":
            namePart.text = "The Meticulous"
            pricePartText = "$1.99/forever"
            featuresA = ["200MB", "100", "Unlimited", "Unlimited", "Unlimited", "", "", "24/7"]
            featuresB = ["Storage", "Friends", "Guests", "eatwiths", "Records", "Notification Reminders", "Email Support", "Phone Support"]
        default:
            namePart.text = "Free"
            pricePartText = "$0/forever"
            featuresA = ["10MB", "20", "Unlimited", "Unlimited", "Unlimited", "", ""]
            featuresB = ["Storage", "Friends", "Guests", "eatwiths", "Records", "Notification Reminders", "Email Support"]
        }
        let priceText = NSMutableAttributedString(string: pricePartText, attributes: [NSAttributedString.Key.font: CardPartTextView(type: .title).font!.withSize(48.0), NSAttributedString.Key.foregroundColor: UIColor.black])
        priceText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray, range: NSRange(location: (pricePartText.count - 8), length: 8))
        priceText.addAttribute(NSAttributedString.Key.font, value: CardPartTextView(type: .detail).font!.withSize(14.0), range: NSRange(location: (pricePartText.count - 8), length: 8))
        pricePart.attributedText = priceText

        var count = 0
        if (featureStackView.arrangedSubviews.count == 0) {
            for fb in featuresB {
                var fa = featuresA[count]
                fa = (fa.count > 0) ? fa + " " : fa
                let featureText = NSMutableAttributedString(string: fa + fb, attributes: [NSAttributedString.Key.font: CardPartTextView(type: .detail).font!.withSize(14.0), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                featureText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location: 0, length: fa.count))
                featureText.addAttribute(NSAttributedString.Key.font, value: CardPartTextView(type: .title).font!.withSize(14.0), range: NSRange(location: 0, length: fa.count))
                let featurePart = CardPartTextView(type: .normal)
                featurePart.attributedText = featureText
                featureStackView.addArrangedSubview(featurePart)

                count = count + 1
            }
        }

        if (plan == subscription) {
            statusPart.textColor = UIColor.pastelGreen
            statusPart.text = "Subscribed"
        } else {
            statusPart.textColor = UIColor.pastelRed
            statusPart.text = "Coming Soon"
        }
    }
}

struct PlanStruct {
    var header: String
    var items: [String]
}

extension PlanStruct: SectionModelType {

    init(original: PlanStruct, items: [String]) {
        self = original
        self.items = items
    }
}

class PlansViewModel {
    typealias ReactiveSection = Variable<[PlanStruct]>
    var data = ReactiveSection([])

    init(items: [String]) {
        data.value = [PlanStruct(header: "", items: items)]
    }
}
