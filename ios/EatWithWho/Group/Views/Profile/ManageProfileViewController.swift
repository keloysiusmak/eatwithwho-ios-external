//
//  NewProfileViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 28/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import Bond

class ManageProfileViewController: CardsViewController {

    var manageProfileCard = ManageProfileViewManageProfileCard()

    var profileViewPresenter: ProfileViewPresenter?

    func initViewPresenter(profileViewPresenter: ProfileViewPresenter) {
        self.profileViewPresenter = profileViewPresenter
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = "Update Profile"

        self.collectionView.backgroundColor = UIColor.white

        let cancelButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissNewProfile))
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(updateProfile))
        navigationItem.leftBarButtonItem = cancelButton
        navigationItem.rightBarButtonItem = saveButton

        manageProfileCard.saveButton.addTarget(self, action: #selector(updateProfile), for: .touchUpInside)

        loadCards(cards: [manageProfileCard])
    }

    @objc func dismissNewProfile() {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }

    @objc func updateProfile() {
        //resets
        self.manageProfileCard.mainErrorText.isHidden = true
        self.manageProfileCard.errorText.isHidden = true
        self.manageProfileCard.profileOldPasswordLabel.textColor = UIColor.gray
        self.manageProfileCard.profileNewPasswordLabel.textColor = UIColor.gray
        self.manageProfileCard.profileNewPassword2Label.textColor = UIColor.gray

        let oldPassword = self.manageProfileCard.profileOldPasswordPart.text!
        let newPassword = self.manageProfileCard.profileNewPasswordPart.text!
        let newPassword2 = self.manageProfileCard.profileNewPassword2Part.text!
        if (oldPassword == "" || newPassword == "" || newPassword2 == "") {
            self.manageProfileCard.errorText.isHidden = false
            self.manageProfileCard.errorText.text = "You have missing fields."
            if (oldPassword == "") {
                self.manageProfileCard.profileOldPasswordLabel.textColor = UIColor.pastelRed
            }
            if (newPassword == "") {
                self.manageProfileCard.profileNewPasswordLabel.textColor = UIColor.pastelRed
            }
            if (newPassword2 == "") {
                self.manageProfileCard.profileNewPassword2Label.textColor = UIColor.pastelRed
            }
        } else {
            var error = false
            if (newPassword != newPassword2) {
                self.manageProfileCard.errorText.isHidden = false
                self.manageProfileCard.errorText.text = "Your new passwords do not match."
                self.manageProfileCard.profileNewPasswordLabel.textColor = UIColor.pastelRed
                self.manageProfileCard.profileNewPassword2Label.textColor = UIColor.pastelRed
                error = true
            } else {
                if (newPassword.count > 30) {
                    self.manageProfileCard.errorText.isHidden = false
                    self.manageProfileCard.errorText.text = "Your passwords must have 30 or less characters."
                    self.manageProfileCard.profileNewPasswordLabel.textColor = UIColor.pastelRed
                    self.manageProfileCard.profileNewPassword2Label.textColor = UIColor.pastelRed
                    error = true
                }
                if (newPassword.count < 6) {
                    self.manageProfileCard.errorText.isHidden = false
                    self.manageProfileCard.errorText.text = "Your passwords must have 6 or more characters."
                    self.manageProfileCard.profileNewPasswordLabel.textColor = UIColor.pastelRed
                    self.manageProfileCard.profileNewPassword2Label.textColor = UIColor.pastelRed
                    error = true
                }
            }
            if (!error) {
                NVeatwithIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
                profileViewPresenter!.changePassword(
                    accountId: UserDefaults.standard.string(forKey: "accountId")!,
                    oldPassword: oldPassword,
                    newPassword: newPassword) { (response) in
                        NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
                        if (response.statusCode == 200) {
                            self.view.endEditing(true)
                            self.dismiss(animated: true, completion: nil)
                        } else if (response.statusCode == 401 ) {
                            self.manageProfileCard.errorText.isHidden = false
                            self.manageProfileCard.errorText.text = "Your old password was incorrect."
                            self.manageProfileCard.profileOldPasswordLabel.textColor = UIColor.pastelRed
                        } else {
                            self.manageProfileCard.mainErrorText.isHidden = false
                            self.manageProfileCard.mainErrorText.text = "Something went wrong. Please try again later."
                        }
                }
            }
        }
    }
}

class ManageProfileViewManageProfileCard: CardPartsViewController, TransparentCardTrait {
    var currency: String?

    let mainErrorText = CardPartTextView(type: .title)
    let errorText = CardPartTextView(type: .title)
    let securitySubtitle = CardPartTextView(type: .title)
    let profileOldPasswordLabel = CardPartLabel()
    let profileOldPasswordPart = CardPartTextField()
    let profileNewPasswordLabel = CardPartLabel()
    let profileNewPasswordPart = CardPartTextField()
    let profileNewPassword2Label = CardPartLabel()
    let profileNewPassword2Part = CardPartTextField()
    let saveButton = CardPartButtonView()

    override func viewDidLoad() {
        profileOldPasswordPart.becomeFirstResponder()
        let stackView = CardPartStackView()
        stackView.axis = .vertical

        mainErrorText.text = ""
        mainErrorText.font = mainErrorText.font.withSize(14.0)
        mainErrorText.textColor = UIColor.pastelRed
        mainErrorText.isHidden = true
        stackView.addArrangedSubview(mainErrorText)

        securitySubtitle.text = "Security Settings"
        securitySubtitle.font = securitySubtitle.font.withSize(24.0)
        securitySubtitle.textColor = UIColor.darkGray
        stackView.addArrangedSubview(securitySubtitle)

        errorText.text = ""
        errorText.font = errorText.font.withSize(14.0)
        errorText.textColor = UIColor.pastelRed
        errorText.isHidden = true
        stackView.addArrangedSubview(errorText)

        let oldPasswordStackView = CardPartStackView()
        oldPasswordStackView.axis = .vertical
        oldPasswordStackView.spacing = 6.0
        profileOldPasswordLabel.text = "OLD PASSWORD"
        profileOldPasswordLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        profileOldPasswordLabel.textColor = UIColor.gray
        profileOldPasswordPart.placeholder = "Old Password"
        profileOldPasswordPart.maxLength = 60
        profileOldPasswordPart.font = Helper.theme.normalTextFont.withSize(24.0)
        profileOldPasswordPart.isSecureTextEntry = true
        oldPasswordStackView.addArrangedSubview(profileOldPasswordLabel)
        oldPasswordStackView.addArrangedSubview(profileOldPasswordPart)
        stackView.addArrangedSubview(oldPasswordStackView)

        let newPasswordStackView = CardPartStackView()
        newPasswordStackView.axis = .vertical
        newPasswordStackView.spacing = 6.0
        profileNewPasswordLabel.text = "NEW PASSWORD"
        profileNewPasswordLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        profileNewPasswordLabel.textColor = UIColor.gray
        profileNewPasswordPart.placeholder = "New Password"
        profileNewPasswordPart.maxLength = 60
        profileNewPasswordPart.font = Helper.theme.normalTextFont.withSize(24.0)
        profileNewPasswordPart.isSecureTextEntry = true
        newPasswordStackView.addArrangedSubview(profileNewPasswordLabel)
        newPasswordStackView.addArrangedSubview(profileNewPasswordPart)
        stackView.addArrangedSubview(newPasswordStackView)

        let newPassword2StackView = CardPartStackView()
        newPassword2StackView.axis = .vertical
        newPassword2StackView.spacing = 6.0
        profileNewPassword2Label.text = "CONFIRM NEW PASSWORD"
        profileNewPassword2Label.font = Helper.theme.detailTextFont.withSize(14.0)
        profileNewPassword2Label.textColor = UIColor.gray
        profileNewPassword2Part.placeholder = "Confirm New Password"
        profileNewPassword2Part.maxLength = 60
        profileNewPassword2Part.font = Helper.theme.normalTextFont.withSize(24.0)
        profileNewPassword2Part.isSecureTextEntry = true
        newPassword2StackView.addArrangedSubview(profileNewPassword2Label)
        newPassword2StackView.addArrangedSubview(profileNewPassword2Part)
        stackView.addArrangedSubview(newPassword2StackView)

        saveButton.backgroundColor = UIColor.pastelRed
        saveButton.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        saveButton.titleLabel?.font = saveButton.titleLabel?.font.withSize(14.0)
        saveButton.layer.cornerRadius = 10.0
        saveButton.contentHorizontalAlignment = .center
        saveButton.setTitleColor(UIColor.white, for: .normal)
        saveButton.setTitle("Save", for: .normal)
        stackView.addArrangedSubview(saveButton)

        stackView.spacing = 24.0

        setupCardParts([stackView])
    }

}
