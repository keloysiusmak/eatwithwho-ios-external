//
//  ManageFriendViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 28/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import Bond
import AWSS3
import AWSCore
import Photos

class ManageFriendViewController: CardsViewController {

    var friend: Friend?

    var friendsViewPresenter: FriendsViewPresenter?
    var friendViewPresenter: FriendViewPresenter?

    var manageFriendCard = ManageFriendViewManageFriendCard()

    func initValues(friend: Friend) {
        self.friend = friend

        manageFriendCard.setFriend(friend: friend)
    }
    func initViewPresenter(friendsViewPresenter: FriendsViewPresenter) {
        self.friendsViewPresenter = friendsViewPresenter
        manageFriendCard.setPresenter(friendsViewPresenter: friendsViewPresenter)
    }
    func initViewPresenter(friendViewPresenter: FriendViewPresenter) {
        self.friendViewPresenter = friendViewPresenter

        manageFriendCard.setPresenter(friendViewPresenter: friendViewPresenter)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.backgroundColor = UIColor.white

        if (friend != nil) {
            let cancelButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissWindow))
            let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(updateFriend))
            navigationItem.leftBarButtonItem = cancelButton
            navigationItem.rightBarButtonItem = saveButton

            self.navigationController?.navigationBar.topItem?.title = "Update Friend"
            manageFriendCard.saveButton.addTarget(self, action: #selector(updateFriend), for: .touchUpInside)
        } else {
            let cancelButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissWindow))
            let saveButton = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(addNewFriend))
            navigationItem.leftBarButtonItem = cancelButton
            navigationItem.rightBarButtonItem = saveButton

            self.navigationController?.navigationBar.topItem?.title = "Add Friend"
            manageFriendCard.saveButton.addTarget(self, action: #selector(addNewFriend), for: .touchUpInside)
        }

        loadCards(cards: [manageFriendCard])
    }

    @objc func dismissWindow() {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }

    @objc func addNewFriend() {
        //resets
        self.manageFriendCard.mainErrorText.isHidden = true
        self.manageFriendCard.errorText.isHidden = true
        self.manageFriendCard.friendFirstNameLabel.textColor = UIColor.gray
        self.manageFriendCard.friendLastNameLabel.textColor = UIColor.gray
        self.manageFriendCard.friendEmailLabel.textColor = UIColor.gray
        self.manageFriendCard.friendNumberLabel.textColor = UIColor.gray
        self.manageFriendCard.friendGenderLabel.textColor = UIColor.gray

        let firstName = self.manageFriendCard.friendFirstNamePart.text!
        let lastName = self.manageFriendCard.friendLastNamePart.text!
        let email = self.manageFriendCard.friendEmailPart.text!
        let number = self.manageFriendCard.friendNumberPart.text!
        let gender = self.manageFriendCard.friendGenderPart.text!
        if (firstName == "" || lastName == "" || email == "" || number == "" || gender == "") {
            self.manageFriendCard.errorText.isHidden = false
            self.manageFriendCard.errorText.text = "You have missing fields."
            if (firstName == "") {
                self.manageFriendCard.friendFirstNameLabel.textColor = UIColor.pastelRed
            }
            if (lastName == "") {
                self.manageFriendCard.friendLastNameLabel.textColor = UIColor.pastelRed
            }
            if (email == "") {
                self.manageFriendCard.friendEmailLabel.textColor = UIColor.pastelRed
            }
            if (number == "") {
                self.manageFriendCard.friendNumberLabel.textColor = UIColor.pastelRed
            }
            if (gender == "") {
                self.manageFriendCard.friendGenderLabel.textColor = UIColor.pastelRed
            }
        } else {
            var error = false
            if (!Helper.isValidEmail(email)) {
                self.manageFriendCard.errorText.isHidden = false
                self.manageFriendCard.errorText.text = "Your friend's email must be valid."
                self.manageFriendCard.friendEmailLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (email.count > 120) {
                self.manageFriendCard.errorText.isHidden = false
                self.manageFriendCard.errorText.text = "Your friend's email must have at most 120 characters."
                self.manageFriendCard.friendEmailLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (number.count > 30) {
                self.manageFriendCard.errorText.isHidden = false
                self.manageFriendCard.errorText.text = "Your friend's number must have at most thirty digits."
                self.manageFriendCard.friendNumberLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (number.count < 6) {
                self.manageFriendCard.errorText.isHidden = false
                self.manageFriendCard.errorText.text = "Your friend's number must have at least six digits."
                self.manageFriendCard.friendNumberLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (lastName.count > 60) {
                self.manageFriendCard.errorText.isHidden = false
                self.manageFriendCard.errorText.text = "Your friend's last name must have at most sixty characters."
                self.manageFriendCard.friendLastNameLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (firstName.count > 60) {
                self.manageFriendCard.errorText.isHidden = false
                self.manageFriendCard.errorText.text = "Your friend's first name must have at most sixty characters."
                self.manageFriendCard.friendFirstNameLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (lastName.count < 2) {
                self.manageFriendCard.errorText.isHidden = false
                self.manageFriendCard.errorText.text = "Your friend's last name must have at least two characters."
                self.manageFriendCard.friendLastNameLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (firstName.count < 2) {
                self.manageFriendCard.errorText.isHidden = false
                self.manageFriendCard.errorText.text = "Your friend's first name must have at least two characters."
                self.manageFriendCard.friendFirstNameLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (!error) {
                NVeatwithIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
                friendsViewPresenter!.addFriend(
                    userId: UserDefaults.standard.string(forKey: "userId")!,
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    number: number,
                    gender: gender.lowercased()) { (response) in
                        if (response.statusCode == 200) {
                            self.view.endEditing(true)
                            self.friendsViewPresenter?.getFriends(userId: UserDefaults.standard.string(forKey: "userId")!) { _ in
                                NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
                            }
                            self.dismiss(animated: true, completion: nil)
                        } else {
                            NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
                            self.manageFriendCard.mainErrorText.isHidden = false
                            self.manageFriendCard.mainErrorText.text = "Something went wrong. Please try again later."
                        }
                }
            }
        }

    }

    @objc func updateFriend() {
        //resets
        self.manageFriendCard.mainErrorText.isHidden = true
        self.manageFriendCard.errorText.isHidden = true
        self.manageFriendCard.friendFirstNameLabel.textColor = UIColor.gray
        self.manageFriendCard.friendLastNameLabel.textColor = UIColor.gray
        self.manageFriendCard.friendEmailLabel.textColor = UIColor.gray
        self.manageFriendCard.friendNumberLabel.textColor = UIColor.gray
        self.manageFriendCard.friendGenderLabel.textColor = UIColor.gray

        let firstName = self.manageFriendCard.friendFirstNamePart.text!
        let lastName = self.manageFriendCard.friendLastNamePart.text!
        let email = self.manageFriendCard.friendEmailPart.text!
        let number = self.manageFriendCard.friendNumberPart.text!
        let gender = self.manageFriendCard.friendGenderPart.text!
        if (firstName == "" || lastName == "" || email == "" || number == "" || gender == "") {
            self.manageFriendCard.errorText.isHidden = false
            self.manageFriendCard.errorText.text = "You have missing fields."
            if (firstName == "") {
                self.manageFriendCard.friendFirstNameLabel.textColor = UIColor.pastelRed
            }
            if (lastName == "") {
                self.manageFriendCard.friendLastNameLabel.textColor = UIColor.pastelRed
            }
            if (email == "") {
                self.manageFriendCard.friendEmailLabel.textColor = UIColor.pastelRed
            }
            if (number == "") {
                self.manageFriendCard.friendNumberLabel.textColor = UIColor.pastelRed
            }
            if (gender == "") {
                self.manageFriendCard.friendGenderLabel.textColor = UIColor.pastelRed
            }
        } else {
            var error = false
            if (!Helper.isValidEmail(email)) {
                self.manageFriendCard.errorText.isHidden = false
                self.manageFriendCard.errorText.text = "Your friend's email must be valid."
                self.manageFriendCard.friendEmailLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (email.count > 120) {
                self.manageFriendCard.errorText.isHidden = false
                self.manageFriendCard.errorText.text = "Your friend's email must have at most 120 characters."
                self.manageFriendCard.friendEmailLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (number.count > 30) {
                self.manageFriendCard.errorText.isHidden = false
                self.manageFriendCard.errorText.text = "Your friend's number must have at most thirty digits."
                self.manageFriendCard.friendNumberLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (number.count < 6) {
                self.manageFriendCard.errorText.isHidden = false
                self.manageFriendCard.errorText.text = "Your friend's number must have at least six digits."
                self.manageFriendCard.friendNumberLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (lastName.count > 60) {
                self.manageFriendCard.errorText.isHidden = false
                self.manageFriendCard.errorText.text = "Your friend's last name must have at most sixty characters."
                self.manageFriendCard.friendLastNameLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (firstName.count > 60) {
                self.manageFriendCard.errorText.isHidden = false
                self.manageFriendCard.errorText.text = "Your friend's first name must have at most sixty characters."
                self.manageFriendCard.friendFirstNameLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (lastName.count < 2) {
                self.manageFriendCard.errorText.isHidden = false
                self.manageFriendCard.errorText.text = "Your friend's last name must have at least two characters."
                self.manageFriendCard.friendLastNameLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (firstName.count < 2) {
                self.manageFriendCard.errorText.isHidden = false
                self.manageFriendCard.errorText.text = "Your friend's first name must have at least two characters."
                self.manageFriendCard.friendFirstNameLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (!error) {
                NVeatwithIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
                friendViewPresenter!.updateFriend(
                    friendId: self.friend!._id,
                    firstName: self.manageFriendCard.friendFirstNamePart.text!,
                    lastName: self.manageFriendCard.friendLastNamePart.text!,
                    email: self.manageFriendCard.friendEmailPart.text!,
                    number: self.manageFriendCard.friendNumberPart.text!,
                    gender: self.manageFriendCard.friendGenderPart.text!.lowercased()) { (response) in
                        if (response.statusCode == 200) {
                            self.view.endEditing(true)
                            self.friendsViewPresenter?.getFriends(userId: UserDefaults.standard.string(forKey: "userId")!) { _ in

                            }
                            self.friendViewPresenter?.getFriend(friendId: self.friend!._id) { _ in
                                NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
                            }
                            self.dismiss(animated: true, completion: nil)
                        } else {
                            NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
                            self.manageFriendCard.mainErrorText.isHidden = false
                            self.manageFriendCard.mainErrorText.text = "Something went wrong. Please try again later."
                        }
                }
            }
        }
    }
}

class ManageFriendViewManageFriendCard: CardPartsViewController, TransparentCardTrait, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var currency: String?
    let mainErrorText = CardPartTextView(type: .title)
    let errorText = CardPartTextView(type: .title)
    let pictureSubtitle = CardPartTextView(type: .title)
    let informationSubtitle = CardPartTextView(type: .title)
    let friendFirstNameLabel = CardPartLabel()
    let friendFirstNamePart = CardPartTextField()
    let friendLastNameLabel = CardPartLabel()
    let friendLastNamePart = CardPartTextField()
    let friendEmailLabel = CardPartLabel()
    let friendEmailPart = CardPartTextField()
    let friendNumberLabel = CardPartLabel()
    let friendNumberPart = CardPartTextField()
    let friendGenderLabel = CardPartLabel()
    let friendGenderPart = CardPartTextField()
    let dateDatePicker = UIDatePicker()
    let friendGenderPicker = UIPickerView()
    let saveButton = CardPartButtonView()
    let imageView = CardPartImageView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
    let imagePicker = UIImagePickerController()

    var friendViewPresenter: FriendViewPresenter?
    var friendsViewPresenter: FriendsViewPresenter?

    let genders = ["male", "female"]
    var friend: Friend?
    var progress: Double = 0.0

    func setPresenter(friendViewPresenter: FriendViewPresenter) {
        self.friendViewPresenter = friendViewPresenter
    }

    func setPresenter(friendsViewPresenter: FriendsViewPresenter) {
        self.friendsViewPresenter = friendsViewPresenter
    }

    func setFriend(friend: Friend) {
        self.friend = friend
        friendFirstNamePart.text = friend.firstName
        friendLastNamePart.text = friend.lastName
        friendEmailPart.text = friend.email
        friendNumberPart.text = friend.number
        friendGenderPart.text = friend.gender.capitalizingFirstLetter()

        imageView.setImage(friendId: self.friend!._id, imageType: self.friend!.imageType, modifiedAt: self.friend!.modifiedAt)
    }

    override func viewDidLoad() {
        friendFirstNamePart.becomeFirstResponder()
        let stackView = CardPartStackView()
        stackView.axis = .vertical

        mainErrorText.text = ""
        mainErrorText.font = mainErrorText.font.withSize(14.0)
        mainErrorText.textColor = UIColor.pastelRed
        mainErrorText.isHidden = true
        stackView.addArrangedSubview(mainErrorText)

        pictureSubtitle.text = "Friend Picture"
        pictureSubtitle.font = informationSubtitle.font.withSize(24.0)
        pictureSubtitle.textColor = UIColor.darkGray

        imageView.layer.cornerRadius = 40.0
        imageView.layer.masksToBounds = true
        imageView.widthAnchor.constraint(equalToConstant: 80.0).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 80.0).isActive = true
        imageView.contentMode = .scaleAspectFill

        let imageStackView = CardPartStackView()
        imageStackView.axis = .horizontal
        imageStackView.spacing = 20.0
        imageStackView.addArrangedSubview(imageView)

        let uploadPicturePart = CardPartButtonView()

        uploadPicturePart.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        uploadPicturePart.titleLabel?.font = uploadPicturePart.titleLabel?.font.withSize(14.0)
        uploadPicturePart.layer.cornerRadius = 10.0
        uploadPicturePart.contentHorizontalAlignment = .center
        uploadPicturePart.backgroundColor = UIColor.pastel
        uploadPicturePart.setTitleColor(UIColor.darkGray, for: .normal)
        uploadPicturePart.setTitle("Upload New Picture", for: .normal)
        uploadPicturePart.heightAnchor.constraint(equalToConstant: 46.0).isActive = true
        uploadPicturePart.margins.top = 24.0
        uploadPicturePart.margins.bottom = 6.0
        uploadPicturePart.addTarget(self, action: #selector(uploadPicture), for: .touchUpInside)

        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self

        let removePicturePart = CardPartTextView(type: .normal)
        removePicturePart.text = "Remove Picture"
        let tap = UITapGestureRecognizer(target: self, action: #selector(deletePicture))
        removePicturePart.isUserInteractionEnabled = true
        removePicturePart.addGestureRecognizer(tap)
        imageStackView.addArrangedSubview(imageView)

        let rightStackView = CardPartStackView()
        rightStackView.axis = .vertical
        rightStackView.spacing = 6.0
        rightStackView.addArrangedSubview(uploadPicturePart)
        rightStackView.addArrangedSubview(removePicturePart)
        imageStackView.addArrangedSubview(imageView)
        imageStackView.addArrangedSubview(rightStackView)
        if (self.friend != nil) {
            stackView.addArrangedSubview(pictureSubtitle)
            stackView.addArrangedSubview(imageStackView)
            stackView.addArrangedSubview(CardPartSeparatorView())
        }

        informationSubtitle.text = "Friend Information"
        informationSubtitle.font = informationSubtitle.font.withSize(24.0)
        informationSubtitle.textColor = UIColor.darkGray
        stackView.addArrangedSubview(informationSubtitle)

        errorText.text = ""
        errorText.font = errorText.font.withSize(14.0)
        errorText.textColor = UIColor.pastelRed
        errorText.isHidden = true
        stackView.addArrangedSubview(errorText)

        let firstNameStackView = CardPartStackView()
        firstNameStackView.axis = .vertical
        firstNameStackView.spacing = 6.0
        friendFirstNameLabel.text = "FIRST NAME"
        friendFirstNameLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        friendFirstNameLabel.textColor = UIColor.gray
        friendFirstNamePart.placeholder = "First Name"
        friendFirstNamePart.maxLength = 60
        friendFirstNamePart.font = Helper.theme.normalTextFont.withSize(24.0)
        firstNameStackView.addArrangedSubview(friendFirstNameLabel)
        firstNameStackView.addArrangedSubview(friendFirstNamePart)
        stackView.addArrangedSubview(firstNameStackView)

        let lastNameStackView = CardPartStackView()
        lastNameStackView.axis = .vertical
        lastNameStackView.spacing = 6.0
        friendLastNameLabel.text = "LAST NAME"
        friendLastNameLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        friendLastNameLabel.textColor = UIColor.gray
        friendLastNamePart.placeholder = "Last Name"
        friendLastNamePart.maxLength = 60
        friendLastNamePart.font = Helper.theme.normalTextFont.withSize(24.0)
        lastNameStackView.addArrangedSubview(friendLastNameLabel)
        lastNameStackView.addArrangedSubview(friendLastNamePart)
        stackView.addArrangedSubview(lastNameStackView)

        let emailStackView = CardPartStackView()
        emailStackView.axis = .vertical
        emailStackView.spacing = 6.0
        friendEmailLabel.text = "EMAIL"
        friendEmailLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        friendEmailLabel.textColor = UIColor.gray
        friendEmailPart.placeholder = "Email"
        friendEmailPart.maxLength = 60
        friendEmailPart.font = Helper.theme.normalTextFont.withSize(24.0)
        emailStackView.addArrangedSubview(friendEmailLabel)
        emailStackView.addArrangedSubview(friendEmailPart)
        stackView.addArrangedSubview(emailStackView)

        let numberStackView = CardPartStackView()
        numberStackView.axis = .vertical
        numberStackView.spacing = 6.0
        friendNumberLabel.text = "NUMBER"
        friendNumberLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        friendNumberLabel.textColor = UIColor.gray
        friendNumberPart.placeholder = "Number"
        friendNumberPart.maxLength = 60
        friendNumberPart.font = Helper.theme.normalTextFont.withSize(24.0)
        numberStackView.addArrangedSubview(friendNumberLabel)
        numberStackView.addArrangedSubview(friendNumberPart)
        stackView.addArrangedSubview(numberStackView)

        let genderStackView = CardPartStackView()
        genderStackView.axis = .vertical
        genderStackView.spacing = 6.0
        friendGenderLabel.text = "GENDER"
        friendGenderLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        friendGenderLabel.textColor = UIColor.gray
        friendGenderPicker.dataSource = self
        friendGenderPicker.delegate = self
        friendGenderPart.placeholder = "Gender"
        friendGenderPart.maxLength = 60
        friendGenderPart.font = Helper.theme.normalTextFont.withSize(24.0)
        friendGenderPart.inputView = friendGenderPicker
        let friendGenderToolbar = UIToolbar()
        friendGenderToolbar.sizeToFit()
        let friendGenderDoneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClicked))
        friendGenderToolbar.setItems([friendGenderDoneButton], animated: true)
        friendGenderPart.inputAccessoryView = friendGenderToolbar

        genderStackView.addArrangedSubview(friendGenderLabel)
        genderStackView.addArrangedSubview(friendGenderPart)
        stackView.addArrangedSubview(genderStackView)

        saveButton.backgroundColor = UIColor.pastelRed
        saveButton.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        saveButton.titleLabel?.font = saveButton.titleLabel?.font.withSize(14.0)
        saveButton.layer.cornerRadius = 10.0
        saveButton.contentHorizontalAlignment = .center
        saveButton.setTitleColor(UIColor.white, for: .normal)
        saveButton.setTitle("Save", for: .normal)
        stackView.addArrangedSubview(saveButton)

        stackView.spacing = 24.0

        setupCardParts([stackView])
    }

    @objc func uploadPicture() {
        self.present(imagePicker, animated: true, completion: nil)
    }

    @objc func deletePicture() {
        self.mainErrorText.isHidden = true
        self.errorText.isHidden = true

        NVeatwithIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
        self.friendViewPresenter!.deletePicture(
            friendId: self.friend!._id) { (response) in
                if (response.statusCode == 200) {
                    self.imageView.imageName = "default"
                    Helper.imageCache.removeObject(forKey: self.friend!._id as AnyObject)
                    self.friendViewPresenter?.getFriend(friendId: self.friend!._id) { _ in
                        NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
                    }
                    self.friendsViewPresenter?.getFriends(userId: UserDefaults.standard.string(forKey: "userId")!) { _ in
                    }
                    let alert = UIAlertController(title: "Successfully deleted picture", message: "", preferredStyle: .alert)

                    self.present(alert, animated: true)

                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        alert.dismiss(animated: true, completion: nil)
                    }
                } else {
                    NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
                    self.mainErrorText.isHidden = false
                    self.mainErrorText.text = "Something went wrong. Please try again later."
                }
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {

        if #available(iOS 11.0, *) {
            NVeatwithIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
            guard let fileUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL else { return }

            let fileName = self.friend!._id + "." + fileUrl.pathExtension
            let remoteName = fileName

            let progressBlock: AWSS3TransferUtilityProgressBlock = {(todo, progress) in
                DispatchQueue.main.async(execute: {
                    self.progress = progress.fractionCompleted
                })
            }
            var image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            image = image.resizeWithWidth(width: 200)!
            let compressData = image.jpegData(compressionQuality: 0.5)
            let compressedImage = UIImage(data: compressData!)
            let fileData = compressedImage!.pngData()

            let completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock = { (todo, error) -> Void in
                DispatchQueue.main.async(execute: {
                    if let error = error {
                        NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
                        self.mainErrorText.isHidden = false
                        self.mainErrorText.text = "Something went wrong. Please try again later."
                    } else if (self.progress != 1.0) {
                        NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
                        self.mainErrorText.isHidden = false
                        self.mainErrorText.text = "Something went wrong. Please try again later."
                    } else {
                        self.friendViewPresenter!.uploadPicture(
                            friendId: self.friend!._id,
                            imageType: fileUrl.pathExtension) { (response) in
                                if (response.statusCode == 200) {
                                    self.imageView.setImage(friendId: self.friend!._id, imageType: fileUrl.pathExtension, modifiedAt: Int(NSDate().timeIntervalSince1970))
                                    self.friendViewPresenter?.getFriend(friendId: self.friend!._id) { _ in

                                    }
                                    self.friendsViewPresenter?.getFriends(userId: UserDefaults.standard.string(forKey: "userId")!) { _ in

                                    }
                                    let alert = UIAlertController(title: "Successfully updated picture", message: "", preferredStyle: .alert)

                                    self.present(alert, animated: true)

                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                        alert.dismiss(animated: true, completion: nil)
                                    }
                                } else {
                                    NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
                                    self.mainErrorText.isHidden = false
                                    self.mainErrorText.text = "Something went wrong. Please try again later."
                                }
                        }
                    }
                })
            }

            let expression = AWSS3TransferUtilityUploadExpression()
            expression.progressBlock = progressBlock

            let transferUtility: (AWSS3TransferUtility?) = AWSS3TransferUtility.s3TransferUtility(forKey: "eatwithwhoPicturesS3Upload")

            transferUtility!.uploadData (fileData! as Data,
                                         bucket: "eatwithwho-friend-pictures-" + appSecrets.env,
                                         key: remoteName,
                                         contentType: "text/plain",
                                         expression: expression,
                                         completionHandler: completionHandler).continueWith { (todo: AWStodo) -> AnyObject? in

                                            if let error = todo.error {
                                                NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
                                                self.mainErrorText.isHidden = false
                                                self.mainErrorText.text = "Something went wrong. Please try again later."
                                            }
                                            if let uploadtodo = todo.result {
                                                //
                                            }

                                            return nil
            }
            self.dismiss(animated: true, completion: nil)
        } else {
            NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
            self.mainErrorText.isHidden = false
            self.mainErrorText.text = "Something went wrong. Please try again later."
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genders.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genders[row].capitalizingFirstLetter()
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        friendGenderPart.text = genders[row].capitalizingFirstLetter()
    }

    @objc func doneClicked() {
        self.view.endEditing(true)
    }
}
