//
//  FriendViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 02/03/20.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import Bond
import Foundation
import MessageUI

class FriendViewController: CardsViewController, FriendView, NVeatwithIndicatorViewable {

    var currency: String?
    var friend: Friend?
    var friendsViewPresenter: FriendsViewPresenter?
    var isCouple: Bool?

    let friendViewPresenter = FriendViewPresenter(friendService: FriendService())
    let friendCard = FriendViewFriendCard()

    func initValues(friend: Friend, isCouple: Bool) {
        self.friend = friend
        self.isCouple = isCouple

        friendCard.setValues(friend: friend, isCouple: isCouple)
        NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
    }
    func initViewPresenter(friendsViewPresenter: FriendsViewPresenter) {
        self.friendsViewPresenter = friendsViewPresenter
        NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.backgroundColor = UIColor.white

        friendViewPresenter.attachView(view: self)

        var buttons: [UIBarButtonItem] = []

        if (!self.isCouple!) {
            let deleteButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteFriend))
            buttons.append(deleteButton)
        }

        navigationItem.rightBarButtonItems = buttons

        friendCard.editButtonPart.addTarget(self, action: #selector(editFriend), for: .touchUpInside)

        loadCards(cards: [friendCard])
    }

    func presentValues(friend: Friend) {
        self.friend = friend

        friendCard.setValues(friend: friend, isCouple: self.isCouple!)
        NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
    }

    func dismiss() {
        self.friendsViewPresenter?.loadFriends(userId: UserDefaults.standard.string(forKey: "userId")!) { _ in

        }
        self.navigationController?.popViewController(animated: true)
    }

    @objc func deleteFriend(sender: UIButton!) {
        NetworkManager.isReachable { _ in
            let refreshAlert = UIAlertController(title: "Are you sure you want to delete this friend?", message: "This action is irreversible.", preferredStyle: UIAlertController.Style.alert)

            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_: UIAlertAction!) in
                NVeatwithIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
                self.friendViewPresenter.deleteFriend(friendId: self.friend!._id) { _ in
                    self.friendsViewPresenter!.getFriends(userId: UserDefaults.standard.string(forKey: "userId")!) { _ in
                        NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
                    }
                }
            }))

            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

            self.present(refreshAlert, animated: true, completion: nil)
        }
        NetworkManager.isUnreachable { _ in
            let alert = UIAlertController(title: "No Internet Connection", message: "", preferredStyle: .alert)

            self.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
    @objc func editFriend(sender: UIButton!) {
        NetworkManager.isReachable { _ in
            let manageFriendVC = ManageFriendViewController()
            manageFriendVC.initValues(friend: self.friend!)

            manageFriendVC.initViewPresenter(friendsViewPresenter: self.friendsViewPresenter!)
            manageFriendVC.initViewPresenter(friendViewPresenter: self.friendViewPresenter)

            let nc = UINavigationController(rootViewController: manageFriendVC)
            self.navigationController?.present(nc, animated: true, completion: { () -> Void   in

            })
        }
        NetworkManager.isUnreachable { _ in
            let alert = UIAlertController(title: "No Internet Connection", message: "", preferredStyle: .alert)

            self.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
}

class FriendViewFriendCard: CardPartsViewController, TransparentCardTrait, NoTopBottomMarginsCardTrait, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate {

    var selectedTab = 0
    var genderPart = CardPartTextView(type: .detail)
    var namePart = CardPartTextView(type: .title)
    var descriptionPart = CardPartTextView(type: .normal)
    var friend: Friend?
    var isCouple: Bool?

    let tableStackView = CardPartStackView()
    let editButtonPart = CardPartButtonView()
    let contactButtonPart = CardPartButtonView()

    let friendPhoneLabel = CardPartTextView(type: .detail)
    let friendPhoneText = CardPartTextView(type: .normal)

    let friendEmailLabel = CardPartTextView(type: .detail)
    let friendEmailText = CardPartTextView(type: .normal)

    let friendAttendanceLabel = CardPartTextView(type: .detail)
    let friendAttendanceText = CardPartTextView(type: .normal)

    let friendNotesLabel = CardPartTextView(type: .detail)
    let friendNotesText = CardPartTextView(type: .normal)

    let imageView = CardPartImageView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @objc func contactPopup() {
        let alert = UIAlertController(title: "Contact " + self.friend!.firstName + " " + self.friend!.lastName, message: nil, preferredStyle: .actionSheet)

        if (self.friend!.number != "") {
            alert.addAction(UIAlertAction(title: "Call", style: .default, handler: { _ in
                guard let number = URL(string: "tel://" + self.friend!.number) else { return }
                UIApplication.shared.open(number)
            }))
            alert.addAction(UIAlertAction(title: "Message", style: .default, handler: { _ in
                if (MFMessageComposeViewController.canSendText()) {
                    let composeVC = MFMessageComposeViewController()
                    composeVC.messageComposeDelegate = self

                    composeVC.recipients = [self.friend!.number]
                    composeVC.body = ""

                    self.present(composeVC, animated: true, completion: nil)
                }
            }))
        }
        if (self.friend!.email != "") {
            alert.addAction(UIAlertAction(title: "Email", style: .default, handler: { _ in
                if (MFMailComposeViewController.canSendMail()) {
                    let mail = MFMailComposeViewController()
                    mail.mailComposeDelegate = self
                    mail.setToRecipients([self.friend!.email])
                    mail.setMessageBody("", isHTML: true)

                    self.present(mail, animated: true)
                }
            }))
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }

    func setValues(friend: Friend, isCouple: Bool) {
        self.friend = friend
        self.isCouple = isCouple

        friendPhoneText.text = friend.number
        friendEmailText.text = friend.email

        genderPart.text = self.friend!.gender.uppercased()
        namePart.text = self.friend!.firstName + " " + self.friend!.lastName

        var additionalGuests = ""
        if (friend.status == "attending" && friend.additionalGuests > 0) {
            additionalGuests = " (" + String(friend.additionalGuests) + " additional guest" + Helper.isPlural(friend.additionalGuests) + ")"
        } else {
            additionalGuests = ""
        }
        friendAttendanceText.text = Helper.formatStatus(friend.status) + additionalGuests
        friendNotesText.text = (friend.note != nil && friend.note!.count > 0) ? friend.note : "No note added."

        imageView.setImage(friendId: friend._id, imageType: friend.imageType, modifiedAt: friend.modifiedAt)

        let friendStackView = CardPartStackView()
        friendStackView.spacing = 6.0
        friendStackView.alignment = .leading
        friendStackView.axis = .vertical
        friendStackView.isLayoutMarginsRelativeArrangement = true

        imageView.layer.cornerRadius = 40.0
        imageView.layer.masksToBounds = true
        imageView.widthAnchor.constraint(equalToConstant: 80.0).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 80.0).isActive = true
        imageView.contentMode = .scaleAspectFill

        genderPart.text = self.friend!.gender.uppercased()
        genderPart.font = genderPart.font.withSize(14.0)

        namePart.text = self.friend!.firstName + " " + self.friend!.lastName
        namePart.font = namePart.font.withSize(24.0)
        namePart.textColor = UIColor.darkGray

        let descriptionText = NSMutableAttributedString(string: "Friend Code: " + self.friend!.reference.uppercased(), attributes: [NSAttributedString.Key.font: CardPartTextView(type: .title).font!.withSize(14.0), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        descriptionText.addAttribute(NSAttributedString.Key.font, value: CardPartTextView(type: .detail).font.withSize(12.0), range: NSRange(location: 0, length: 12))
        descriptionText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray, range: NSRange(location: 0, length: 12))

        descriptionPart.attributedText = descriptionText

        let imageStackView = CardPartStackView()
        imageStackView.axis = .horizontal
        imageStackView.spacing = 20.0
        imageStackView.alignment = .center

        let rightStackView = CardPartStackView()
        rightStackView.axis = .vertical
        rightStackView.spacing = 0.0
        rightStackView.addArrangedSubview(namePart)
        rightStackView.addArrangedSubview(descriptionPart)

        imageStackView.addArrangedSubview(imageView)
        imageStackView.addArrangedSubview(rightStackView)
        friendStackView.addArrangedSubview(imageStackView)
        friendStackView.addArrangedSubview(CardPartSpacerView(height: 20.0))

        let leftStack = CardPartStackView()
        leftStack.axis = .vertical
        leftStack.spacing = 6.0
        let rightStack = CardPartStackView()
        rightStack.axis = .vertical
        rightStack.spacing = 6.0
        let leftRightStack = CardPartStackView()
        leftRightStack.axis = .horizontal
        leftRightStack.distribution = .fillEqually
        leftRightStack.alignment = .top
        leftRightStack.widthAnchor.constraint(equalToConstant: self.view!.bounds.width).isActive = true
        leftRightStack.spacing = 20.0
        leftRightStack.addArrangedSubview(leftStack)
        leftRightStack.addArrangedSubview(rightStack)

        friendPhoneLabel.text = "Phone"
        friendPhoneLabel.font = friendPhoneLabel.font.withSize(14.0)
        friendPhoneLabel.textColor = UIColor.gray
        friendPhoneText.font = friendPhoneText.font.withSize(16.0)
        friendPhoneText.textColor = UIColor.darkGray
        leftStack.addArrangedSubview(friendPhoneLabel)
        leftStack.addArrangedSubview(friendPhoneText)

        friendEmailLabel.text = "Email"
        friendEmailLabel.font = friendEmailLabel.font.withSize(14.0)
        friendEmailLabel.textColor = UIColor.gray
        friendEmailText.font = friendEmailText.font.withSize(16.0)
        friendEmailText.textColor = UIColor.darkGray
        rightStack.addArrangedSubview(friendEmailLabel)
        rightStack.addArrangedSubview(friendEmailText)

        leftRightStack.addArrangedSubview(leftStack)
        leftRightStack.addArrangedSubview(rightStack)
        friendStackView.addArrangedSubview(leftRightStack)
        friendStackView.addArrangedSubview(CardPartSpacerView(height: 20))

        editButtonPart.backgroundColor = UIColor.pastel
        editButtonPart.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 32.0, bottom: 16.0, right: 32.0)
        editButtonPart.titleLabel?.font = editButtonPart.titleLabel?.font.withSize(14.0)
        editButtonPart.layer.cornerRadius = 10.0
        editButtonPart.contentHorizontalAlignment = .center
        editButtonPart.setTitleColor(UIColor.darkGray, for: .normal)
        editButtonPart.setTitle("Edit Friend", for: .normal)

        contactButtonPart.backgroundColor = UIColor.pastelRed
        contactButtonPart.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 32.0, bottom: 16.0, right: 32.0)
        contactButtonPart.titleLabel?.font = contactButtonPart.titleLabel?.font.withSize(14.0)
        contactButtonPart.layer.cornerRadius = 10.0
        contactButtonPart.contentHorizontalAlignment = .center
        contactButtonPart.setTitleColor(UIColor.white, for: .normal)
        contactButtonPart.setTitle("Contact", for: .normal)
        contactButtonPart.addTarget(self, action: #selector(contactPopup), for: .touchUpInside)

        let buttonStack = CardPartStackView()
        buttonStack.axis = .horizontal
        buttonStack.spacing = 6.0
        buttonStack.distribution = .fillEqually
        buttonStack.widthAnchor.constraint(equalToConstant: self.view!.bounds.width).isActive = true

        buttonStack.addArrangedSubview(editButtonPart)
        buttonStack.addArrangedSubview(contactButtonPart)

        friendStackView.addArrangedSubview(buttonStack)

        friendAttendanceLabel.text = "Attendance"
        friendAttendanceLabel.font = friendAttendanceLabel.font.withSize(14.0)
        friendAttendanceLabel.textColor = UIColor.gray
        friendAttendanceText.font = friendAttendanceText.font.withSize(16.0)
        friendAttendanceText.textColor = UIColor.darkGray

        friendNotesLabel.text = "Notes"
        friendNotesLabel.font = friendNotesLabel.font.withSize(14.0)
        friendNotesLabel.textColor = UIColor.gray
        friendNotesText.font = friendNotesText.font.withSize(16.0)
        friendNotesText.textColor = UIColor.darkGray

        friendStackView.addArrangedSubview(CardPartSpacerView(height: 20))
        friendStackView.addArrangedSubview(friendNotesLabel)
        friendStackView.addArrangedSubview(friendNotesText)
        friendStackView.addArrangedSubview(CardPartSpacerView(height: 20))
        friendStackView.addArrangedSubview(friendAttendanceLabel)
        friendStackView.addArrangedSubview(friendAttendanceText)

        setupCardParts([friendStackView, tableStackView])
    }
}
