//
//  FriendsViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 22/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import RxDataSources
import Bond

class FriendsViewController: CardsViewController, FriendsView, NVeatwithIndicatorViewable {

    var friends = [Friend]()
    private let friendsViewPresenter = FriendsViewPresenter(coupleService: CoupleService(), friendService: FriendService())

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: UIControlEvents.valueChanged)

        return refreshControl
    }()

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {

        self.friendsViewPresenter.getFriends(userId: UserDefaults.standard.string(forKey: "userId")!) {_ in
            refreshControl.endRefreshing()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.addSubview(refreshControl)
        NVeatwithIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)

        self.navigationController?.navigationBar.topItem?.title = "Friends"

        friendsViewPresenter.attachView(view: self)
        friendsViewPresenter.loadFriends(userId: UserDefaults.standard.string(forKey: "userId")!) { _ in

        }

        NetworkManager.sharedInstance.reachability.whenReachable = { _ in
            self.friendsViewPresenter.loadFriends(userId: UserDefaults.standard.string(forKey: "userId")!) { _ in

            }
        }

        navigationItem.leftBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .stop, target: self, action: #selector(dismisseatwith))
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(newFriend))

        self.collectionView.backgroundColor = UIColor.white
    }

    @objc func dismisseatwith() {
        self.dismiss(animated: true, completion: nil)
    }

    @objc func newFriend(sender: UIButton!) {
        NetworkManager.isReachable { _ in
            let manageFriendVC = ManageFriendViewController()
            manageFriendVC.initViewPresenter(friendsViewPresenter: self.friendsViewPresenter)

            let nc = UINavigationController(rootViewController: manageFriendVC)
            self.navigationController?.present(nc, animated: true, completion: { () -> Void   in

            })
        }
        NetworkManager.isUnreachable { _ in
            let alert = UIAlertController(title: "No Internet Connection", message: "", preferredStyle: .alert)

            self.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }

    func presentValues(coupleFriends: [Friend], friends: [Friend]) {
        let coupleFriendsId = coupleFriends.map { (friend) -> String in
            return friend._id
        }
        let eatwithFriends = friends.filter({ (friend) -> Bool in
            !friend.isDeleted && !coupleFriendsId.contains(friend._id)
        }).sorted(by: { (friend1, friend2) -> Bool in
            friend1.firstName < friend2.firstName
        })
        self.friends = coupleFriends.sorted(by: { (friend1, friend2) -> Bool in
            friend1.firstName < friend2.firstName
        }) + eatwithFriends

        var count = 0
        var cards: [CardController] = []
        for _ in self.friends {
            if (count < 2) {
                //friendCard.setCouple(true)
                count = count + 1
            }
            //friendCard.setFriendsViewPresenter(presenter: self.friendsViewPresenter)
            //friendCard.setFriend(friend: friend)
        }
        let couple = self.friends.prefix(2).filter { (_) -> Bool in
            return true
        }
        let friends = eatwithFriends.filter { (_) -> Bool in
            return true
        }
        let coupleCard = FriendsViewFriendCollectionCard()
        let subscriptionFriends = Helper.getCoupleSubscription(UserDefaults.standard.string(forKey: "coupleSubscription")!).maxFriends
        coupleCard.setFriends(friends: couple, subscriptionFriends: subscriptionFriends)
        coupleCard.isCouple = true
        coupleCard.setFriendsViewPresenter(presenter: self.friendsViewPresenter)
        let friendCard = FriendsViewFriendCollectionCard()
        friendCard.setFriends(friends: friends, subscriptionFriends: subscriptionFriends)
        friendCard.isCouple = false
        friendCard.numberOfFriends = friends.count
        friendCard.setFriendsViewPresenter(presenter: self.friendsViewPresenter)
        friendCard.addFriendButtonPart.addTarget(self, action: #selector(newFriend), for: .touchUpInside)
        cards.append(coupleCard)
        cards.append(friendCard)
        cards.append(FriendsViewHelpCard())
        loadCards(cards: cards)
        NVeatwithIndicatorPresenter.sharedInstance.stopAnimating()
    }
}

class FriendsViewHelpCard: CardPartsViewController, TransparentCardTrait, NoTopBottomMarginsCardTrait {
    let helpFriendsBox = Box()
    let helpStack = CardPartStackView()
    @objc func loadHelpFriends() {
        let scheduleHelpVC = HelpFriendsViewController()

        let nc = UINavigationController(rootViewController: scheduleHelpVC)
        self.navigationController?.present(nc, animated: true, completion: { () -> Void   in

        })
    }

    override func viewDidLoad() {
        helpStack.axis = .vertical
        helpStack.spacing = 10.0
        helpFriendsBox.setValues(title: "Friends make everything come together.", subtitle: "Add them as a Friend for your eatwith, and they'll be able to keep up to date with the eatwith.", button: "Learn More", image: "help-friends")
        helpStack.addArrangedSubview(helpFriendsBox.view!)
        helpFriendsBox.buttonPart.addTarget(self, action: #selector(loadHelpFriends), for: .touchUpInside)
        setupCardParts([helpStack])
    }

}

struct FriendsStruct {
    var header: String
    var items: [Friend]
}

extension FriendsStruct: SectionModelType {

    init(original: FriendsStruct, items: [Friend]) {
        self = original
        self.items = items
    }
}

class FriendsViewFriendCollectionCard: CardPartsViewController, TransparentCardTrait, NoTopBottomMarginsCardTrait, CardPartCollectionViewDelegte {

    let displayPretitlePart = CardPartTextView(type: .title)
    let displayTitlePart = CardPartTextView(type: .title)
    let noFriendsPart = CardPartTextView(type: .normal)
    lazy var collectionViewPart = CardPartCollectionView(collectionViewLayout: self.collectionViewLayout)
    var collectionViewLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionView.ScrollDirection.vertical
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        layout.itemSize = CGSize(width: 96, height: 118)
        return layout
    }()
    var friendViewModel: FriendViewModel?
    var isCouple: Bool = false
    var numberOfFriends: Int = 0
    var friends: [Friend]?
    var subscriptionFriends: Int = 20
    var presenter: FriendsViewPresenter?
    var bottomPartStack = CardPartStackView()

    let addFriendButtonPart = CardPartButtonView()

    func setFriends(friends: [Friend], subscriptionFriends: Int) {

        collectionViewPart.collectionView.frame = CGRect(x: 0, y: 0, width: 200, height: 128 * ceil(Double(friends.count) / 3))
        self.friends = friends
        self.subscriptionFriends = subscriptionFriends
        bottomPartStack = CardPartStackView()
        bottomPartStack.spacing = 16.0
        bottomPartStack.axis = .vertical

        friendViewModel = FriendViewModel(friends: friends)
        if (friends.count == 0) {
            collectionViewPart.isHidden = true
            bottomPartStack.addArrangedSubview(noFriendsPart)
        } else {
            collectionViewPart.isHidden = false
        }
        bottomPartStack.addArrangedSubview(CardPartSpacerView(height: 0))
        bottomPartStack.addArrangedSubview(addFriendButtonPart)
    }
    public func setFriendsViewPresenter(presenter: FriendsViewPresenter) {
        self.presenter = presenter
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        noFriendsPart.font = noFriendsPart.font.withSize(14.0)
        noFriendsPart.text = "No friends added."

        collectionViewPart.delegate = self
        displayTitlePart.font = displayTitlePart.font.withSize(24.0)
        if (self.isCouple) {
            let removedFriend = friends?.filter({ (friend) -> Bool in
                return friend._id != UserDefaults.standard.string(forKey: "friendId")!
            })
            displayTitlePart.text = "You and " + (removedFriend?.item(at: 0).firstName)!
        } else {
            displayTitlePart.text = "Friends"
        }

        collectionViewPart.collectionView.register(FriendsViewFriendCell.self, forCellWithReuseIdentifier: "FriendCell")
        collectionViewPart.collectionView.backgroundColor = .clear
        collectionViewPart.collectionView.showsHorizontalScrollIndicator = false

        let dataSource = RxCollectionViewSectionedReloadDataSource<FriendsStruct>(configureCell: {(_, collectionView, indexPath, data) -> UICollectionViewCell in

            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FriendCell", for: indexPath) as? FriendsViewFriendCell else { return UICollectionViewCell() }

            cell.setData(data)

            return cell
        })

        friendViewModel!.data.asObservable().bind(to: collectionViewPart.collectionView.rx.items(dataSource: dataSource)).disposed(by: bag)

        if (!self.isCouple) {
            let friendDetailStack = CardPartStackView()
            friendDetailStack.axis = .vertical
            friendDetailStack.spacing = 4.0

            let percentageBar = CardPartBarView()
            let percentage = Double(numberOfFriends) / 20.0
            percentageBar.percent = percentage
            if (percentage < 0.6) {
                percentageBar.barColor = UIColor.pastelGreen
            } else if (percentage < 0.8) {
                percentageBar.barColor = UIColor.pastelOrange
            } else {
                percentageBar.barColor = UIColor.pastelRed
            }
            percentageBar.barHeight = 2.0
            percentageBar.verticalLine.removeFromSuperlayer()
            percentageBar.heightAnchor.constraint(equalToConstant: 6.0).isActive = true

            let friendsText = NSMutableAttributedString(string: String(numberOfFriends) + " / " + String(subscriptionFriends) + " Friends ", attributes: [NSAttributedString.Key.font: CardPartTextView(type: .detail).font.withSize(12.0), NSAttributedString.Key.foregroundColor: UIColor.gray])
            friendsText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range: NSRange(location: 0, length: String(numberOfFriends).count))
            friendsText.addAttribute(NSAttributedString.Key.font, value: CardPartTextView(type: .title).font.withSize(12.0), range: NSRange(location: 0, length: String(numberOfFriends).count))
            let friendsLimit = CardPartTextView(type: .detail)
            friendsLimit.textAlignment = .right
            friendsLimit.textColor = .gray
            friendsLimit.attributedText = friendsText

            addFriendButtonPart.backgroundColor = UIColor.pastelRed
            addFriendButtonPart.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
            addFriendButtonPart.titleLabel?.font = addFriendButtonPart.titleLabel?.font.withSize(14.0)
            addFriendButtonPart.layer.cornerRadius = 10.0
            addFriendButtonPart.contentHorizontalAlignment = .center
            addFriendButtonPart.setTitleColor(UIColor.white, for: .normal)
            addFriendButtonPart.setTitle("Add Friend", for: .normal)
            addFriendButtonPart.margins.top = 24.0
            addFriendButtonPart.margins.bottom = 6.0

            friendDetailStack.addArrangedSubview(percentageBar)
            friendDetailStack.addArrangedSubview(friendsLimit)

            setupCardParts([displayTitlePart, CardPartSpacerView(height: 10), friendDetailStack, CardPartSpacerView(height: 10), collectionViewPart, bottomPartStack])
        } else {

            let displayWriteup = CardPartTextView(type: .normal)
            displayWriteup.font = displayWriteup.font.withSize(14.0)
            displayWriteup.textColor = UIColor.gray
            displayWriteup.text = "Add friends to allocate and assign todos easily."

            setupCardParts([displayWriteup, CardPartSpacerView(height: 16), displayTitlePart, CardPartSpacerView(height: 10), collectionViewPart])
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let friendVC = FriendViewController()
        NVeatwithIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
        friendVC.initValues(friend: self.friends![indexPath[1]], isCouple: self.isCouple)
        friendVC.initViewPresenter(friendsViewPresenter: self.presenter!)
        self.navigationController?.pushViewController(friendVC, animated: true)
    }
}

class FriendViewModel {
    typealias ReactiveSection = Variable<[FriendsStruct]>
    var data = ReactiveSection([])

    init(friends: [Friend]) {

        data.value = [FriendsStruct(header: "", items: friends)]
    }
}

class FriendsViewFriendCell: CardPartCollectionViewCardPartsCell {

    let friendStackView = CardPartStackView()
    let nameCP = CardPartTextView(type: .normal)
    var friend: Friend?
    let imageView = CardPartImageView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))

    override init(frame: CGRect) {

        super.init(frame: frame)

        friendStackView.axis = .vertical
        friendStackView.alignment = .center
        friendStackView.spacing = 10
        friendStackView.margins.left = 0
        friendStackView.margins.right = 0

        imageView.layer.cornerRadius = 40.0
        imageView.layer.masksToBounds = true
        imageView.widthAnchor.constraint(equalToConstant: 80.0).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 80.0).isActive = true
        imageView.contentMode = .scaleAspectFill

        friendStackView.addArrangedSubview(imageView)

        nameCP.textColor = UIColor.darkGray
        nameCP.font = nameCP.font.withSize(14.0)

        friendStackView.addArrangedSubview(nameCP)

        setupCardParts([friendStackView])
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setData(_ friend: Friend) {
        self.friend = friend

        imageView.setImage(friendId: friend._id, imageType: friend.imageType, modifiedAt: friend.modifiedAt)
        nameCP.text = friend.firstName
    }
}
