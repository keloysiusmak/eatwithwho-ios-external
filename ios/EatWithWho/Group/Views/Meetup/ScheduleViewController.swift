//
//  ScheduleViewController.swift
//  EatWithWho
//
//  start by Keloysius Mak on 11/15/16.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import RxDataSources
import Bond

class ScheduleViewController: CardsViewController, ScheduleView, NVMeetupIndicatorViewable {

    @IBOutlet var scheduleView: UIView!

    var eatwiths = [Meetup]()
    var schedule: Schedule?

    private let scheduleViewPresenter = ScheduleViewPresenter(scheduleService: ScheduleService())

    let header = ScheduleViewHeaderCard()

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: UIControlEvents.valueChanged)

        return refreshControl
    }()

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {

        self.scheduleViewPresenter.getSchedule(scheduleId: UserDefaults.standard.string(forKey: "scheduleId")!) {_ in
            refreshControl.endRefreshing()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.addSubview(refreshControl)
        NVMeetupIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)

        self.collectionView.backgroundColor = UIColor.white

        scheduleViewPresenter.attachView(view: self)
        scheduleViewPresenter.loadSchedule(scheduleId: UserDefaults.standard.string(forKey: "scheduleId")!) { _ in

        }

        NetworkManager.sharedInstance.reachability.whenReachable = { _ in
            self.scheduleViewPresenter.loadSchedule(scheduleId: UserDefaults.standard.string(forKey: "scheduleId")!) { _ in

            }
        }

        let imageView = CardPartImageView(frame: CGRect(x: 0, y: 400, width: 24, height: 24))
        imageView.imageName = "home"
        imageView.layer.masksToBounds = true
        imageView.widthAnchor.constraint(equalToConstant: 24.0).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
        imageView.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(loadProfile))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tap)

        navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: imageView)

        navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(newMeetup))
    }

    @objc func loadProfile(sender: UIButton!) {
        NetworkManager.isReachable { _ in
            let profileVC = ProfileViewController()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        NetworkManager.isUnreachable { _ in
            let alert = UIAlertController(title: "No Internet Connection", message: "", preferredStyle: .alert)

            self.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }

    @objc func editSchedule(sender: UIButton!) {
        NetworkManager.isReachable { _ in
            let manageScheduleVC = ManageScheduleViewController()
            manageScheduleVC.initValues(schedule: self.schedule!)
            manageScheduleVC.initViewPresenter(scheduleViewPresenter: self.scheduleViewPresenter)

            let nc = UINavigationController(rootViewController: manageScheduleVC)
            self.navigationController?.present(nc, animated: true, completion: { () -> Void   in

            })
        }
        NetworkManager.isUnreachable { _ in
            let alert = UIAlertController(title: "No Internet Connection", message: "", preferredStyle: .alert)

            self.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }

    @objc func newMeetup(sender: UIButton!) {
        NetworkManager.isReachable { _ in
            let manageMeetupVC = ManageMeetupViewController()
            manageMeetupVC.initValues(schedule: self.schedule!)
            manageMeetupVC.initViewPresenter(scheduleViewPresenter: self.scheduleViewPresenter)

            let nc = UINavigationController(rootViewController: manageMeetupVC)
            self.navigationController?.present(nc, animated: true, completion: { () -> Void   in

            })
        }
        NetworkManager.isUnreachable { _ in
            let alert = UIAlertController(title: "No Internet Connection", message: "", preferredStyle: .alert)

            self.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }

    func seteatwiths(eatwiths: [Meetup]) {
        self.eatwiths = eatwiths.sorted(by: { (meetup1, meetup2) -> Bool in
            return meetup1.startTime < meetup2.startTime
        }).filter({ (meetup) -> Bool in
            return !meetup.isDeleted
        })

        var cards: [CardController] = [header]
        var currDate: String = ""
        var meetupHold: [Meetup] = []
        var categories = Set<String>()
        for meetup in self.eatwiths {
            if (meetup.category != "" && !categories.contains(meetup.category)) {
                categories.insert(meetup.category)
            }
            let newDate = Helper.formatDate(text: String(meetup.startTime), format: "d MMMM")
            if (currDate == "" || newDate != currDate) {
                if (currDate != "") {
                    let eatwithsCard = ScheduleVieweatwithsCard()
                    eatwithsCard.setScheduleViewPresenter(presenter: self.scheduleViewPresenter)
                    eatwithsCard.seteatwiths(eatwiths: meetupHold)
                    eatwithsCard.setController(controller: self)
                    cards.append(eatwithsCard)
                    meetupHold.removeAll()
                }
                let newDateCard = ScheduleViewDateCard()
                newDateCard.setDate(date: newDate, endTime: meetup.endTime)
                cards.append(newDateCard)
                currDate = newDate
            }
            meetupHold.append(meetup)
        }

        if (eatwiths.count > 0) {
            let eatwithsCard = ScheduleVieweatwithsCard()
            eatwithsCard.setScheduleViewPresenter(presenter: self.scheduleViewPresenter)
            eatwithsCard.seteatwiths(eatwiths: meetupHold)
            cards.append(eatwithsCard)
        }

        cards.append(ScheduleViewHelpCard())
        loadCards(cards: cards)
        NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
    }

    func presentValues(schedule: Schedule, eatwiths: [Meetup], name: String) {

        header.setValues(schedule: schedule, name: name, controller: self)

        self.navigationController?.navigationBar.topItem?.title = schedule.name
        self.schedule = schedule
        self.eatwiths = eatwiths.sorted(by: { (meetup1, meetup2) -> Bool in
            return meetup1.startTime < meetup2.startTime
        }).filter({ (meetup) -> Bool in
            return !meetup.isDeleted
        })
        header.preTitleLabel.text = name
        header.editScheduleButton.addTarget(self, action: #selector(editSchedule), for: .touchUpInside)

        var cards: [CardController] = [header]
        var currDate: String = ""
        var meetupHold: [Meetup] = []
        var categories = Set<String>()
        for meetup in self.eatwiths {
            if (meetup.category != "" && !categories.contains(meetup.category)) {
                categories.insert(meetup.category)
            }
            let newDate = Helper.formatDate(text: String(meetup.startTime), format: "d MMMM")
            if (currDate == "" || newDate != currDate) {
                if (currDate != "") {
                    let eatwithsCard = ScheduleVieweatwithsCard()
                    eatwithsCard.setScheduleViewPresenter(presenter: self.scheduleViewPresenter)
                    eatwithsCard.seteatwiths(eatwiths: meetupHold)
                    eatwithsCard.setController(controller: self)
                    cards.append(eatwithsCard)
                    meetupHold.removeAll()
                }
                let newDateCard = ScheduleViewDateCard()
                newDateCard.setDate(date: newDate, endTime: meetup.endTime)
                cards.append(newDateCard)
                currDate = newDate
            }
            meetupHold.append(meetup)
        }
        header.setCategories(categories: Array(categories))

        if (eatwiths.count > 0) {
            let eatwithsCard = ScheduleVieweatwithsCard()
            eatwithsCard.setScheduleViewPresenter(presenter: self.scheduleViewPresenter)
            eatwithsCard.seteatwiths(eatwiths: meetupHold)
            cards.append(eatwithsCard)
        }

        cards.append(ScheduleViewHelpCard())
        loadCards(cards: cards)
        NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
    }

}

class ScheduleViewHelpCard: CardPartsViewController, TransparentCardTrait, NoTopBottomMarginsCardTrait {
    let helpScheduleBox = Box()
    let helpStack = CardPartStackView()
    @objc func loadHelpSchedule() {
        let scheduleHelpVC = HelpScheduleViewController()

        let nc = UINavigationController(rootViewController: scheduleHelpVC)
        self.navigationController?.present(nc, animated: true, completion: { () -> Void   in

        })
    }

    override func viewDidLoad() {
        helpStack.axis = .vertical
        helpStack.spacing = 10.0
        helpScheduleBox.setValues(title: "New to Schedule?", subtitle: "Schedule is a great way to keep tabs of all your eatwiths and todos. Let us show you around to help you get started.", button: "Learn More", image: "help-schedule")
        helpStack.addArrangedSubview(helpScheduleBox.view!)
        helpScheduleBox.buttonPart.addTarget(self, action: #selector(loadHelpSchedule), for: .touchUpInside)
        setupCardParts([helpStack])
    }

}

class ScheduleViewHeaderCard: CardPartsViewController, TransparentCardTrait, NoTopBottomMarginsCardTrait, CardPartCollectionViewDelegte {

    let categoriesViewModel = ScheduleViewCategoriesCardModel()
    let titleLabel = CardPartTextView(type: .normal)
    let preTitleLabel = CardPartTextView(type: .title)
    let editScheduleButton = CardPartButtonView()
    let categoriesStack = CardPartStackView()
    lazy var categoriesCollection = CardPartCollectionView(collectionViewLayout: self.collectionViewLayout)
    var collectionViewLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        layout.itemSize = CGSize(width: 120, height: 36)
        return layout
    }()
    var schedule: Schedule?
    var name: String?
    var controller: ScheduleViewController?
    var categories: [String] = []

    var headerStack = CardPartStackView()

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var category = ""
        if (indexPath.row != 0) {
            category = categories[indexPath.row]
        } else {
            category = ""
        }
        let eatwiths = schedule?.eatwiths.filter({ (meetup) -> Bool in
            if (category != "") {
                return meetup.category == category
            } else {
                return true
            }
        })
        self.controller!.seteatwiths(eatwiths: eatwiths!)
    }
    func setValues(schedule: Schedule, name: String, controller: ScheduleViewController) {
        self.schedule = schedule
        self.name = name
        self.controller = controller
    }

    func setCategories(categories: [String]) {
        var newCategories = categories
        newCategories.insert("All eatwiths", at: 0)
        categoriesViewModel.setValues(categories: newCategories)
        self.categories = newCategories
    }

    override func viewDidLoad() {
        categoriesStack.axis = .vertical
        categoriesStack.spacing = 20.0
        categoriesStack.addArrangedSubview(CardPartSeparatorView())
        categoriesStack.addArrangedSubview(categoriesCollection)

        headerStack.axis = .vertical
        headerStack.spacing = 10.0
        headerStack.addArrangedSubview(titleLabel)
        headerStack.addArrangedSubview(CardPartSpacerView(height: 2))
        headerStack.addArrangedSubview(editScheduleButton)
        headerStack.addArrangedSubview(CardPartSpacerView(height: 0))
        headerStack.addArrangedSubview(categoriesStack)

        categoriesCollection.delegate = self

        categoriesCollection.collectionView.register(ScheduleViewCategoriesCell.self, forCellWithReuseIdentifier: "CategoriesCell")
        categoriesCollection.collectionView.backgroundColor = .clear
        categoriesCollection.collectionView.showsHorizontalScrollIndicator = false
        categoriesCollection.collectionView.frame = CGRect(x: 0, y: 0, width: 120, height: 36)

        let dataSource = RxCollectionViewSectionedReloadDataSource<CategoriesStruct>(configureCell: {[weak self] (_, collectionView, indexPath, _) -> UICollectionViewCell in

            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as? ScheduleViewCategoriesCell else { return UICollectionViewCell() }

            cell.setData(self!.categories[indexPath.row])

            return cell
        })

        categoriesViewModel.data.asObservable().bind(to: categoriesCollection.collectionView.rx.items(dataSource: dataSource)).disposed(by: bag)

        editScheduleButton.backgroundColor = UIColor.pastelRed
        editScheduleButton.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        editScheduleButton.titleLabel?.font = editScheduleButton.titleLabel?.font.withSize(14.0)
        editScheduleButton.layer.cornerRadius = 10.0
        editScheduleButton.contentHorizontalAlignment = .center
        editScheduleButton.setTitleColor(UIColor.white, for: .normal)
        editScheduleButton.setTitle("Edit Schedule", for: .normal)

        preTitleLabel.font = preTitleLabel.font.withSize(36.0)
        titleLabel.font = titleLabel.font.withSize(14.0)

        titleLabel.text = "Add eatwiths to your schedule to help yourself stay organized with the events concering your eatwith."
        setupCardParts([headerStack])
    }
}

class ScheduleViewCategoriesCardModel {
    typealias ReactiveSection = Variable<[CategoriesStruct]>
    var data = ReactiveSection([])

    func setValues(categories: [String]) {
        data.value = [CategoriesStruct(header: "", items: categories)]
    }
}

struct CategoriesStruct {
    var header: String
    var items: [String]
}
extension CategoriesStruct: SectionModelType {

    init(original: CategoriesStruct, items: [String]) {
        self = original
        self.items = items
    }
}

class ScheduleViewCategoriesCell: CardPartCollectionViewCardPartsCell {

    let cellStackView = CardPartStackView()
    let categoryName = CardPartTextView(type: .detail)

    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                self.layer.backgroundColor = UIColor.darkGray.cgColor
                categoryName.textColor = UIColor.white

            } else {
                self.layer.backgroundColor = UIColor.pastel.cgColor
                categoryName.textColor = UIColor.darkGray
            }
        }
    }

    override init(frame: CGRect) {

        super.init(frame: frame)

        self.layer.backgroundColor = UIColor.pastel.cgColor
        self.layer.cornerRadius = 10.0

        categoryName.font = categoryName.font.withSize(14.0)
        categoryName.lineHeightMultiple = 0.8
        categoryName.textAlignment = .center

        cellStackView.axis = .vertical
        cellStackView.spacing = 16.0
        cellStackView.margins = UIEdgeInsets(top: 12, left: 44, bottom: 12, right: 44)
        cellStackView.alignment = .center

        cellStackView.addArrangedSubview(categoryName)

        setupCardParts([cellStackView])
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setData(_ category: String) {
        categoryName.text = (category != "All eatwiths") ? Helper.getMeetupCategory(category) : "All eatwiths"
    }
}

class ScheduleViewDateCard: CardPartsViewController, TransparentCardTrait, NoTopBottomMarginsCardTrait {
    var datePart = CardPartTextView(type: .title)

    public func setDate(date: String, endTime: Int) {
        datePart.text = date
        datePart.font = datePart.font.withSize(24.0)
        datePart.textColor = UIColor.pastelRed
        datePart.textAlignment = .left

        let dateLineView = CardPartSeparatorView()
        dateLineView.backgroundColor = UIColor.pastelRed
        dateLineView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        dateLineView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        let spacerView = UIView()
        spacerView.setContentHuggingPriority(.defaultLow, for: .horizontal)

        let lineStack = CardPartStackView()
        lineStack.axis = .horizontal
        lineStack.spacing = 6.0
        lineStack.addArrangedSubview(dateLineView)
        lineStack.addArrangedSubview(spacerView)
        setupCardParts([datePart, lineStack])

        if (endTime < Int(NSDate().timeIntervalSince1970)) {
            datePart.textColor = UIColor.lighterGray
            dateLineView.backgroundColor = UIColor.lighterGray
        }
    }
}

class ScheduleVieweatwithsCard: CardPartsViewController, NoTopBottomMarginsCardTrait, CardPartTableViewDelegte, TransparentCardTrait {

    var eatwithsTableView = CardPartTableView()
    var eatwiths = MutableObservableArray<Meetup>([])
    var presenter: ScheduleViewPresenter?
    var controller: ScheduleViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        eatwithsTableView.tableView.register(ScheduleMeetupCell.self, forCellReuseIdentifier: "MeetupCell")

        eatwithsTableView.tableView.estimatedRowHeight = 156
        eatwithsTableView.rowHeight = UITableView.automaticDimension

        eatwithsTableView.delegate = self

        setupCardParts([eatwithsTableView])

        self.eatwiths.bind(to: eatwithsTableView.tableView) { data, indexPath, tableView in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MeetupCell", for: indexPath) as? ScheduleMeetupCell else { return UITableViewCell() }

            cell.setData(data[indexPath.row])

            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NVMeetupIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
        let meetupVC = MeetupViewController()
        meetupVC.initValues(meetup: self.eatwiths.array[indexPath.row])
        meetupVC.initViewPresenter(scheduleViewPresenter: self.presenter!)

        let nc = UINavigationController(rootViewController: meetupVC)
        self.navigationController?.present(nc, animated: true, completion: { () -> Void   in
            NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
        })

    }

    public func setScheduleViewPresenter(presenter: ScheduleViewPresenter) {
        self.presenter = presenter
    }

    public func seteatwiths(eatwiths: [Meetup]) {
        self.eatwiths.replace(with: eatwiths)
    }
    public func setController(controller: ScheduleViewController) {
        self.controller = controller
    }
}

class ScheduleMeetupCell: CardPartTableViewCardPartsCell {

    let meetupStartTime = CardPartTextView(type: .title)
    let meetupTitle = CardPartTextView(type: .normal)
    let meetupDesc = CardPartTextView(type: .normal)
    let meetupTime = CardPartTextView(type: .detail)
    let meetupStatus = CardPartTextView(type: .detail)
    let meetupFile = CardPartTextView(type: .detail)
    let meetupAssigned = CardPartTextView(type: .detail)

    var locationPart = CardPartTextView(type: .detail)
    var locationIconPart = CardPartImageView()
    var locationStack = CardPartStackView()

    var statusIconPart = CardPartImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
    var timeIconPart = CardPartImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
    var fileIconPart = CardPartImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
    var assignedIconPart = CardPartImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))

    let meetupDetailStack = CardPartStackView()
    let assignedStack = CardPartStackView()
    let timeStack = CardPartStackView()
    let statusStack = CardPartStackView()
    let fileStack = CardPartStackView()

    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        selectionStyle = .none

        let meetupStack = CardPartStackView()
        meetupStack.axis = .vertical
        meetupStack.margins = UIEdgeInsets(top: 16, left: 28, bottom: 16, right: 28)
        meetupStack.spacing = 6.0

        meetupDetailStack.axis = .vertical
        meetupDetailStack.margins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        meetupDetailStack.spacing = 6.0
        meetupDetailStack.clipsToBounds = true
        meetupDetailStack.alignment = .leading

        meetupStartTime.font = meetupStartTime.font.withSize(16.0)
        meetupStartTime.textColor = UIColor.darkGray

        meetupTitle.font = meetupTitle.font.withSize(18.0)
        meetupTitle.textColor = UIColor.darkGray

        meetupDesc.font = meetupDesc.font.withSize(14.0)
        meetupDesc.textColor = UIColor.gray

        timeIconPart.imageName = "time"
        timeIconPart.image = timeIconPart.image!.withRenderingMode(.alwaysTemplate)
        timeIconPart.tintColor = UIColor.gray
        timeIconPart.heightAnchor.constraint(equalToConstant: 24).isActive = true
        timeIconPart.widthAnchor.constraint(equalToConstant: 24).isActive = true
        timeIconPart.contentMode = .scaleAspectFit
        meetupTime.font = meetupTime.font.withSize(14.0)
        meetupTime.textColor = UIColor.gray
        timeStack.axis = .horizontal
        timeStack.addArrangedSubview(timeIconPart)
        timeStack.addArrangedSubview(meetupTime)
        timeStack.spacing = 6.0

        assignedIconPart.imageName = "friends"
        assignedIconPart.image = assignedIconPart.image!.withRenderingMode(.alwaysTemplate)
        assignedIconPart.tintColor = UIColor.gray
        assignedIconPart.heightAnchor.constraint(equalToConstant: 24).isActive = true
        assignedIconPart.widthAnchor.constraint(equalToConstant: 24).isActive = true
        assignedIconPart.contentMode = .scaleAspectFit
        meetupAssigned.font = meetupAssigned.font.withSize(14.0)
        meetupAssigned.textColor = UIColor.gray
        assignedStack.axis = .horizontal
        assignedStack.addArrangedSubview(assignedIconPart)
        assignedStack.addArrangedSubview(meetupAssigned)
        assignedStack.spacing = 6.0

        statusIconPart.imageName = "status"
        statusIconPart.image = statusIconPart.image!.withRenderingMode(.alwaysTemplate)
        statusIconPart.heightAnchor.constraint(equalToConstant: 24).isActive = true
        statusIconPart.widthAnchor.constraint(equalToConstant: 24).isActive = true
        statusIconPart.contentMode = .scaleAspectFit
        meetupStatus.font = meetupStatus.font.withSize(14.0)
        meetupStatus.textColor = UIColor.lightGray
        statusStack.axis = .horizontal
        statusStack.addArrangedSubview(statusIconPart)
        statusStack.addArrangedSubview(meetupStatus)
        statusStack.spacing = 6.0

        fileIconPart.imageName = "attachment"
        fileIconPart.image = fileIconPart.image!.withRenderingMode(.alwaysTemplate)
        fileIconPart.heightAnchor.constraint(equalToConstant: 24).isActive = true
        fileIconPart.widthAnchor.constraint(equalToConstant: 24).isActive = true
        fileIconPart.tintColor = UIColor.gray
        fileIconPart.contentMode = .scaleAspectFit
        meetupFile.font = meetupFile.font.withSize(14.0)
        meetupFile.textColor = UIColor.gray
        fileStack.axis = .horizontal
        fileStack.addArrangedSubview(fileIconPart)
        fileStack.addArrangedSubview(meetupFile)
        fileStack.spacing = 6.0

        locationPart.textColor = UIColor.lightGray
        locationPart.font = locationPart.font.withSize(14.0)
        locationIconPart.imageName = "location"
        locationIconPart.image = locationIconPart.image!.withRenderingMode(.alwaysTemplate)
        locationIconPart.heightAnchor.constraint(equalToConstant: 24).isActive = true
        locationIconPart.widthAnchor.constraint(equalToConstant: 24).isActive = true
        locationIconPart.tintColor = UIColor.lightGray
        locationIconPart.contentMode = .scaleAspectFit
        locationPart.font = locationPart.font.withSize(14.0)
        locationPart.textColor = UIColor.gray
        locationStack.axis = .horizontal
        locationStack.addArrangedSubview(locationIconPart)
        locationStack.addArrangedSubview(locationPart)
        locationStack.spacing = 6.0

        meetupStack.addArrangedSubview(meetupStartTime)
        meetupStack.addArrangedSubview(meetupTitle)
        meetupStack.addArrangedSubview(meetupDesc)
        meetupStack.addArrangedSubview(CardPartSpacerView(height: 10))
        meetupDetailStack.addArrangedSubview(timeStack)
        meetupStack.addArrangedSubview(locationStack)
        meetupStack.addArrangedSubview(meetupDetailStack)

        setupCardParts([meetupStack])
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setData(_ meetup: Meetup) {
        if (meetup.location.count > 0) {
            locationStack.isHidden = false
            locationPart.text = meetup.location
        } else {
            locationStack.isHidden = true
        }
        locationPart.textColor = (meetup.endTime >= Int(NSDate().timeIntervalSince1970)) ? UIColor.gray : UIColor.lighterGray
        locationIconPart.tintColor = (meetup.endTime >= Int(NSDate().timeIntervalSince1970)) ? UIColor.gray : UIColor.lighterGray

        var daysDifferential = ""
        if (meetup.endTime - 86400 > meetup.startTime) {
            let timeDifference = Helper.timeDifferenceFull(time1: meetup.endTime, time2: meetup.startTime, allowedUnits: [.day])
            daysDifferential = " (+" + timeDifference + ")"
        }

        meetupStartTime.text = Helper.formatDate(text: String(meetup.startTime), format: "hh:mm a")
        meetupStartTime.textColor = UIColor.darkGray
        meetupTitle.text = meetup.name
        meetupDesc.text = meetup.desc
        let endTime = Helper.formatDate(text: String(meetup.endTime), format: "hh:mm a")
        let endTimeText = NSMutableAttributedString(string: "Ends " + endTime + daysDifferential, attributes: [NSAttributedString.Key.font: CardPartTextView(type: .detail).font.withSize(14.0), NSAttributedString.Key.foregroundColor: (meetup.endTime < Int(NSDate().timeIntervalSince1970)) ? UIColor.lighterGray : UIColor.gray])
        if (meetup.endTime >= Int(NSDate().timeIntervalSince1970)) {
            endTimeText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray, range: NSRange(location: 5 + endTime.count, length: daysDifferential.count))
        }
        endTimeText.addAttribute(NSAttributedString.Key.font, value: CardPartTextView(type: .detail).font.withSize(12.0), range: NSRange(location: 5 + endTime.count, length: daysDifferential.count))
        meetupTime.attributedText = endTimeText

        if (meetup.files!.count > 0) {
            meetupDetailStack.addArrangedSubview(fileStack)
            meetupFile.text = String(meetup.files!.count) + " file" + Helper.isPlural(meetup.files!.count) + " attached."
        }

        if (meetup.endTime < Int(NSDate().timeIntervalSince1970)) {
            meetupStartTime.textColor = UIColor.lighterGray
            meetupTitle.textColor = UIColor.lighterGray
            meetupDesc.textColor = UIColor.lighterGray
            meetupTime.textColor = UIColor.lighterGray
            timeIconPart.tintColor = UIColor.lighterGray
            meetupStatus.textColor = UIColor.lighterGray
            meetupFile.textColor = UIColor.lighterGray
            fileIconPart.tintColor = UIColor.lighterGray
            statusIconPart.tintColor = UIColor.lighterGray
            assignedIconPart.tintColor = UIColor.lighterGray
            meetupAssigned.textColor = UIColor.lighterGray
        }
    }
}
