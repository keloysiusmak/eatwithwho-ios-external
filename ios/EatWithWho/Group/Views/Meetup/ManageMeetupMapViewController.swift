//
//  MeetupMapViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 25/3/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import Foundation
import CardParts
import MapKit
import Contacts

class ManageMeetupMapViewController: CardsViewController {

    let overlayCard = ManageMeetupMapViewOverlayCard()

    var mapView: MKMapView?
    var controller: ManageMeetupViewManageMeetupCard?
    let regionRadius: CLLocationDistance = 500
    var long: String = ""
    var lat: String = ""
    var locationName: String = "Location"

    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView!.setRegion(coordinateRegion, animated: true)
    }

    func setLocation(lat: Double, long: Double, name: String, locationName: String, isAnnotation: Bool) {
        let window = UIWindow(frame: UIScreen.main.bounds)
        mapView = MKMapView(frame: CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height - 300))

        overlayCard.setTop(CGFloat(window.frame.height - 300 - self.navigationController!.navigationBar.frame.size.height - UIApplication.shared.statusBarFrame.height))

        centerMapOnLocation(location: CLLocation(latitude: lat, longitude: long))

        self.locationName = locationName

        if (isAnnotation) {
            self.overlayCard.latPart.text = String(lat)
            self.overlayCard.longPart.text = String(long)
            let location = Location(title: name,
                                   locationName: locationName,
                                   coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long))
            mapView!.addAnnotation(location)
        } else {
            self.overlayCard.latPart.text = "Not Set"
            self.overlayCard.longPart.text = "Not Set"
        }
    }

    @objc func addAnnotation(gestureRecognizer: UIGestureRecognizer) {
        if gestureRecognizer.state == UIGestureRecognizer.State.began {
            let touchPoint = gestureRecognizer.location(in: mapView!)
            let newCoordinates = mapView!.convert(touchPoint, toCoordinateFrom: mapView!)
            let annotation = MKPointAnnotation()
            annotation.coordinate = newCoordinates
            self.overlayCard.latPart.text = String(newCoordinates.latitude)
            self.overlayCard.longPart.text = String(newCoordinates.longitude)
            self.lat = String(newCoordinates.latitude)
            self.long = String(newCoordinates.longitude)

            CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: newCoordinates.latitude, longitude: newCoordinates.longitude), completionHandler: {(_, error) -> Void in
                if error != nil {
                    return
                }

                annotation.title = self.locationName

                self.mapView!.removeAnnotations(self.mapView!.annotations)
                self.mapView!.addAnnotation(annotation)
            })
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let uilgr = UILongPressGestureRecognizer(target: self, action: #selector(addAnnotation))
        uilgr.minimumPressDuration = 0.5

        mapView!.addGestureRecognizer(uilgr)

        mapView!.delegate = self

        let cancelButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissWindow))

        navigationItem.leftBarButtonItem = cancelButton

        self.collectionView.backgroundColor = UIColor.white

        self.view.addSubview(mapView!)

        overlayCard.removeMapButton.addTarget(self, action: #selector(removeMap), for: .touchUpInside)
        overlayCard.saveMapButton.addTarget(self, action: #selector(saveMap), for: .touchUpInside)

        loadCards(cards: [overlayCard])
    }
    @objc func dismissWindow() {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func removeMap() {
        self.controller!.updateGeolocation("")
        self.dismiss(animated: true, completion: nil)
    }
    @objc func saveMap() {
        self.controller!.updateGeolocation(self.lat + "," + self.long)
        self.dismiss(animated: true, completion: nil)
    }
    func setViewController(_ controller: ManageMeetupViewManageMeetupCard) {
        self.controller = controller
    }

    override func willMove(toParent: UIViewController?) {
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.black
        ]
        self.navigationController?.navigationBar.setValue(false, forKey: "hidesShadow")
    }
}

extension ManageMeetupMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? Location else { return nil }
        let identifier = "marker"
        if #available(iOS 11.0, *) {
            var view: MKMarkerAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                as? MKMarkerAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
            }
            return view
        } else {
            return nil
        }
    }
}
class ManageMeetupMapViewOverlayCard: CardPartsViewController, TransparentCardTrait, NoTopBottomMarginsCardTrait {

    let overlayStack = CardPartStackView()
    let saveMapButton = CardPartButtonView()
    let removeMapButton = CardPartButtonView()
    let latPart = CardPartTextView(type: .title)
    let longPart = CardPartTextView(type: .title)

    var top: CGFloat?
    func setTop(_ top: CGFloat) {
        self.top = top
    }

    override func viewDidLoad() {
        overlayStack.margins.top = CGFloat(self.top!)
        overlayStack.axis = .vertical
        overlayStack.layer.backgroundColor = UIColor.white.cgColor
        overlayStack.spacing = 6.0

        let leftStack = CardPartStackView()
        leftStack.axis = .vertical
        leftStack.spacing = 6.0
        let latLabel = CardPartTextView(type: .detail)
        latLabel.font = latLabel.font.withSize(14.0)
        latLabel.text = "LATITUDE"
        latLabel.textColor = UIColor.gray

        leftStack.addArrangedSubview(latLabel)
        leftStack.addArrangedSubview(latPart)

        let rightStack = CardPartStackView()
        rightStack.axis = .vertical
        rightStack.spacing = 6.0
        let longLabel = CardPartTextView(type: .detail)
        longLabel.font = longLabel.font.withSize(14.0)
        longLabel.text = "LONGITUDE"
        longLabel.textColor = UIColor.gray

        rightStack.addArrangedSubview(longLabel)
        rightStack.addArrangedSubview(longPart)

        let leftRightStack = CardPartStackView()
        leftRightStack.axis = .horizontal
        leftRightStack.distribution = .fillEqually
        leftRightStack.alignment = .top
        leftRightStack.spacing = 20.0
        leftRightStack.addArrangedSubview(leftStack)
        leftRightStack.addArrangedSubview(rightStack)

        overlayStack.addArrangedSubview(leftRightStack)

        removeMapButton.backgroundColor = UIColor.pastelRed
        removeMapButton.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        removeMapButton.titleLabel?.font = removeMapButton.titleLabel?.font.withSize(14.0)
        removeMapButton.layer.cornerRadius = 10.0
        removeMapButton.contentHorizontalAlignment = .center
        removeMapButton.setTitleColor(UIColor.white, for: .normal)
        removeMapButton.setTitle("Remove Map", for: .normal)

        saveMapButton.backgroundColor = UIColor.pastel
        saveMapButton.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        saveMapButton.titleLabel?.font = saveMapButton.titleLabel?.font.withSize(14.0)
        saveMapButton.layer.cornerRadius = 10.0
        saveMapButton.contentHorizontalAlignment = .center
        saveMapButton.setTitleColor(UIColor.darkGray, for: .normal)
        saveMapButton.setTitle("Save", for: .normal)

        overlayStack.addArrangedSubview(CardPartSpacerView(height: 20))
        overlayStack.addArrangedSubview(saveMapButton)
        overlayStack.addArrangedSubview(removeMapButton)

        setupCardParts([overlayStack])
    }
}
