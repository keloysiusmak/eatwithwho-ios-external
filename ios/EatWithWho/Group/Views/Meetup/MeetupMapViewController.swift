//
//  MeetupMapViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 25/3/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import Foundation
import CardParts
import MapKit
import Contacts

class MeetupMapViewController: CardsViewController {

    var mapView: MKMapView?
    let regionRadius: CLLocationDistance = 500

    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView!.setRegion(coordinateRegion, animated: true)
    }

    func setLocation(lat: Double, long: Double, name: String, locationName: String) {
        let window = UIWindow(frame: UIScreen.main.bounds)
        mapView = MKMapView(frame: CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height))

        centerMapOnLocation(location: CLLocation(latitude: lat, longitude: long))

        let artwork = Location(title: name,
                               locationName: locationName,
                               coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long))
        mapView!.addAnnotation(artwork)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        mapView?.delegate = self

        let cancelButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissWindow))

        navigationItem.leftBarButtonItem = cancelButton

        self.collectionView.backgroundColor = UIColor.white

        self.view.addSubview(self.mapView!)
    }
    @objc func dismissWindow() {
        self.dismiss(animated: true, completion: nil)
    }

    override func willMove(toParent: UIViewController?) {
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.black
        ]
        self.navigationController?.navigationBar.setValue(false, forKey: "hidesShadow")
    }
}
extension MeetupMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                 calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! Location
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? Location else { return nil }
        let identifier = "marker"
        if #available(iOS 11.0, *) {
            var view: MKMarkerAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                as? MKMarkerAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            }
            return view
        } else {
            return nil
        }
    }
}

class Location: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let coordinate: CLLocationCoordinate2D

    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate

        super.init()
    }

    var subtitle: String? {
        return locationName
    }

    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
}
