//
//  ManageMeetupViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 28/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import RxDataSources
import Bond

class ManageMeetupViewController: CardsViewController {

    var meetup: Meetup?
    var schedule: Schedule?

    var scheduleViewPresenter: ScheduleViewPresenter?
    var meetupViewPresenter: MeetupViewPresenter?

    var manageMeetupCard = ManageMeetupViewManageMeetupCard()

    func initValues(meetup: Meetup) {
        self.meetup = meetup

        manageMeetupCard.setMeetup(meetup: meetup)
    }
    func initValues(schedule: Schedule) {
        self.schedule = schedule

        manageMeetupCard.setSchedule(schedule: schedule)
    }
    func initViewPresenter(scheduleViewPresenter: ScheduleViewPresenter) {
        self.scheduleViewPresenter = scheduleViewPresenter
    }
    func initViewPresenter(meetupViewPresenter: MeetupViewPresenter) {
        self.meetupViewPresenter = meetupViewPresenter
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.backgroundColor = UIColor.white

        if (meetup != nil) {
            self.navigationController?.navigationBar.topItem?.title = "Update Meetup"
            let cancelButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissWindow))
            let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(updateMeetup))
            self.manageMeetupCard.saveButton.setTitle("Save", for: .normal)
            self.manageMeetupCard.saveButton.addTarget(self, action: #selector(updateMeetup), for: .touchUpInside)
            navigationItem.leftBarButtonItem = cancelButton
            navigationItem.rightBarButtonItem = saveButton
        } else {
            self.navigationController?.navigationBar.topItem?.title = "Add Meetup"
            let cancelButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissWindow))
            let saveButton = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(addNewMeetup))
            self.manageMeetupCard.saveButton.setTitle("Add Meetup", for: .normal)
            self.manageMeetupCard.saveButton.addTarget(self, action: #selector(addNewMeetup), for: .touchUpInside)
            navigationItem.leftBarButtonItem = cancelButton
            navigationItem.rightBarButtonItem = saveButton
        }

        loadCards(cards: [manageMeetupCard])
    }

    @objc func dismissWindow() {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }

    @objc func addNewMeetup() {
        //resets
        self.manageMeetupCard.mainErrorText.isHidden = true
        self.manageMeetupCard.errorText.isHidden = true
        self.manageMeetupCard.errorText2.isHidden = true
        self.manageMeetupCard.meetupNameLabel.textColor = UIColor.gray
        self.manageMeetupCard.meetupDescLabel.textColor = UIColor.gray
        self.manageMeetupCard.meetupCategoryLabel.textColor = UIColor.gray
        self.manageMeetupCard.meetupStartTimeLabel.textColor = UIColor.gray
        self.manageMeetupCard.meetupEndTimeLabel.textColor = UIColor.gray

        let name = self.manageMeetupCard.meetupNamePart.text!
        let desc = self.manageMeetupCard.meetupDescPart.text!
        let location = self.manageMeetupCard.meetupLocationPart.text!
        let geolocation = self.manageMeetupCard.geolocation
        let startTime = self.manageMeetupCard.startTimeDatePicker.date
        let endTime = self.manageMeetupCard.endTimeDatePicker.date
        let categoryIndex = self.manageMeetupCard.currentCategory

        if (name == "" || desc == "" || categoryIndex == -1) {
            self.manageMeetupCard.errorText.isHidden = false
            self.manageMeetupCard.errorText.text = "You have missing fields."
            if (name == "") {
                self.manageMeetupCard.meetupNameLabel.textColor = UIColor.pastelRed
            }
            if (desc == "") {
                self.manageMeetupCard.meetupDescLabel.textColor = UIColor.pastelRed
            }
            if (categoryIndex == -1) {
                self.manageMeetupCard.meetupCategoryLabel.textColor = UIColor.pastelRed
            }
        } else if (geolocation != "" && location == "") {
            self.manageMeetupCard.errorText.isHidden = false
            self.manageMeetupCard.errorText.text = "You have missing fields."
            self.manageMeetupCard.meetupLocationLabel.textColor = UIColor.pastelRed
        } else if (startTime == nil || endTime == nil) {
            self.manageMeetupCard.errorText2.isHidden = false
            self.manageMeetupCard.errorText2.text = "You have missing fields."
            if (startTime == nil) {
                self.manageMeetupCard.meetupStartTimeLabel.textColor = UIColor.pastelRed
            }
            if (endTime == nil) {
                self.manageMeetupCard.meetupEndTimeLabel.textColor = UIColor.pastelRed
            }
        } else {
            var error = false
            if (!Helper.isValidGeolocation(geolocation)) {
                self.manageMeetupCard.errorText.isHidden = false
                self.manageMeetupCard.errorText.text = "Your geolocation must be valid."
                self.manageMeetupCard.meetupGeolocationLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (location.count > 0 && !Helper.isValidName(location)) {
                self.manageMeetupCard.errorText.isHidden = false
                self.manageMeetupCard.errorText.text = "Your location name must have between 2 to 60 characters."
                self.manageMeetupCard.meetupLocationLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (!Helper.isValidShortDesc(desc)) {
                self.manageMeetupCard.errorText.isHidden = false
                self.manageMeetupCard.errorText.text = "Your meetup description must have 280 or less characters."
                self.manageMeetupCard.meetupDescLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (!Helper.isValidName(name)) {
                self.manageMeetupCard.errorText.isHidden = false
                self.manageMeetupCard.errorText.text = "Your meetup name must have between 2 to 60 characters."
                self.manageMeetupCard.meetupNameLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (startTime >= endTime) {
                self.manageMeetupCard.errorText2.isHidden = false
                self.manageMeetupCard.errorText2.text = "Your meetup must end after it has started."
                self.manageMeetupCard.meetupStartTimeLabel.textColor = UIColor.pastelRed
                self.manageMeetupCard.meetupEndTimeLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (!error) {
                NVMeetupIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
                let category = self.manageMeetupCard.categories[categoryIndex]
                scheduleViewPresenter!.addMeetup(
                    name: name,
                    desc: desc,
                    location: location,
                    geolocation: geolocation,
                    category: category,
                    startTime: startTime,
                    endTime: endTime) { (response) in
                        if (response.statusCode == 200) {
                            self.view.endEditing(true)
                            self.scheduleViewPresenter?.getSchedule(scheduleId: UserDefaults.standard.string(forKey: "scheduleId")!) { _ in
                                NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
                                self.dismiss(animated: true, completion: nil)
                            }
                        } else if (response.statusCode == 409) {
                            NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
                            self.manageMeetupCard.errorText2.isHidden = false
                            self.manageMeetupCard.errorText2.text = "There was an error in the start and end times."
                            self.manageMeetupCard.meetupStartTimeLabel.textColor = UIColor.pastelRed
                            self.manageMeetupCard.meetupEndTimeLabel.textColor = UIColor.pastelRed
                        } else {
                            NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
                            self.manageMeetupCard.mainErrorText.isHidden = false
                            self.manageMeetupCard.mainErrorText.text = "Something went wrong. Please try again later."
                        }

                }
            }
        }
    }

    @objc func updateMeetup() {
        //resets
        self.manageMeetupCard.mainErrorText.isHidden = true
        self.manageMeetupCard.errorText.isHidden = true
        self.manageMeetupCard.errorText2.isHidden = true
        self.manageMeetupCard.meetupNameLabel.textColor = UIColor.gray
        self.manageMeetupCard.meetupDescLabel.textColor = UIColor.gray
        self.manageMeetupCard.meetupCategoryLabel.textColor = UIColor.gray
        self.manageMeetupCard.meetupStartTimeLabel.textColor = UIColor.gray
        self.manageMeetupCard.meetupEndTimeLabel.textColor = UIColor.gray

        let name = self.manageMeetupCard.meetupNamePart.text!
        let desc = self.manageMeetupCard.meetupDescPart.text!
        let location = self.manageMeetupCard.meetupLocationPart.text!
        let geolocation = self.manageMeetupCard.geolocation
        let startTime = self.manageMeetupCard.startTimeDatePicker.date
        let endTime = self.manageMeetupCard.endTimeDatePicker.date
        let categoryIndex = self.manageMeetupCard.currentCategory

        if (name == "" || desc == "" || categoryIndex == -1) {
            self.manageMeetupCard.errorText.isHidden = false
            self.manageMeetupCard.errorText.text = "You have missing fields."
            if (name == "") {
                self.manageMeetupCard.meetupNameLabel.textColor = UIColor.pastelRed
            }
            if (desc == "") {
                self.manageMeetupCard.meetupDescLabel.textColor = UIColor.pastelRed
            }
            if (categoryIndex == -1) {
                self.manageMeetupCard.meetupCategoryLabel.textColor = UIColor.pastelRed
            }
        } else if (geolocation != "" && location == "") {
            self.manageMeetupCard.errorText.isHidden = false
            self.manageMeetupCard.errorText.text = "You have missing fields."
            self.manageMeetupCard.meetupLocationLabel.textColor = UIColor.pastelRed
        } else if (startTime == nil || endTime == nil) {
            self.manageMeetupCard.errorText2.isHidden = false
            self.manageMeetupCard.errorText2.text = "You have missing fields."
            if (startTime == nil) {
                self.manageMeetupCard.meetupStartTimeLabel.textColor = UIColor.pastelRed
            }
            if (endTime == nil) {
                self.manageMeetupCard.meetupEndTimeLabel.textColor = UIColor.pastelRed
            }
        } else {
            var error = false
            if (!Helper.isValidGeolocation(geolocation)) {
                self.manageMeetupCard.errorText.isHidden = false
                self.manageMeetupCard.errorText.text = "Your geolocation must be valid."
                self.manageMeetupCard.meetupGeolocationLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (location.count > 0 && !Helper.isValidName(location)) {
                self.manageMeetupCard.errorText.isHidden = false
                self.manageMeetupCard.errorText.text = "Your location name must have between 2 to 60 characters."
                self.manageMeetupCard.meetupLocationLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (!Helper.isValidShortDesc(desc)) {
                self.manageMeetupCard.errorText.isHidden = false
                self.manageMeetupCard.errorText.text = "Your meetup description must have 280 or less characters."
                self.manageMeetupCard.meetupDescLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (!Helper.isValidName(name)) {
                self.manageMeetupCard.errorText.isHidden = false
                self.manageMeetupCard.errorText.text = "Your meetup name must have between 2 to 60 characters."
                self.manageMeetupCard.meetupNameLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (startTime >= endTime) {
                self.manageMeetupCard.errorText2.isHidden = false
                self.manageMeetupCard.errorText2.text = "Your meetup must end after it has started."
                self.manageMeetupCard.meetupStartTimeLabel.textColor = UIColor.pastelRed
                self.manageMeetupCard.meetupEndTimeLabel.textColor = UIColor.pastelRed
                error = true
            }

            if (!error) {
                NVMeetupIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
                let category = self.manageMeetupCard.categories[categoryIndex]
                meetupViewPresenter!.updateMeetup(
                    meetupId: self.meetup!._id,
                    name: self.manageMeetupCard.meetupNamePart.text!,
                    desc: self.manageMeetupCard.meetupDescPart.text!,
                    location: location,
                    geolocation: geolocation,
                    category: category,
                    startTime: self.manageMeetupCard.startTimeDatePicker.date,
                    endTime: self.manageMeetupCard.endTimeDatePicker.date) { (response) in
                        if (response.statusCode == 200) {
                            self.view.endEditing(true)
                            self.scheduleViewPresenter?.getSchedule(scheduleId: UserDefaults.standard.string(forKey: "scheduleId")!) { _ in

                            }
                            self.meetupViewPresenter?.getMeetup(meetupId: self.meetup!._id) { _ in
                                NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
                                self.dismiss(animated: true, completion: nil)
                            }
                        } else if (response.statusCode == 409) {
                            NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
                            self.manageMeetupCard.errorText2.isHidden = false
                            self.manageMeetupCard.errorText2.text = "There was an error in the start and end times."
                            self.manageMeetupCard.meetupStartTimeLabel.textColor = UIColor.pastelRed
                            self.manageMeetupCard.meetupEndTimeLabel.textColor = UIColor.pastelRed
                        } else {
                            NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
                            self.manageMeetupCard.mainErrorText.isHidden = false
                            self.manageMeetupCard.mainErrorText.text = "Something went wrong. Please try again later."
                        }
                }
            }
        }
    }
}

class ManageMeetupViewManageMeetupCard: CardPartsViewController, TransparentCardTrait, CardPartCollectionViewDelegte {

    let mainErrorText = CardPartTextView(type: .title)
    let errorText = CardPartTextView(type: .title)
    let errorText2 = CardPartTextView(type: .title)
    let informationSubtitle = CardPartTextView(type: .title)
    let dateSubtitle = CardPartTextView(type: .title)
    let meetupNameLabel = CardPartLabel()
    let meetupNamePart = CardPartTextField()
    let meetupDescLabel = CardPartLabel()
    let meetupDescPart = CardPartTextField()
    let meetupLocationLabel = CardPartLabel()
    let meetupLocationPart = CardPartTextField()
    let meetupGeolocationLabel = CardPartLabel()
    let meetupGeolocationPart = CardPartTextView(type: .normal)
    let meetupGeolocationButton = CardPartTextView(type: .detail)
    let meetupCategoryLabel = CardPartLabel()
    lazy var meetupCategoryPart = CardPartCollectionView(collectionViewLayout: self.collectionViewLayout)
    var collectionViewLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        layout.itemSize = CGSize(width: 180, height: 46)
        return layout
    }()
    let meetupStartTimeLabel = CardPartLabel()
    let meetupStartTimePart = CardPartTextField()
    let meetupEndTimeLabel = CardPartLabel()
    let meetupEndTimePart = CardPartTextField()
    let startTimeDatePicker = UIDatePicker()
    let endTimeDatePicker = UIDatePicker()
    let saveButton = CardPartButtonView()
    var currentCategory: Int = -1
    var currentCell: ManageMeetupViewCategoryCell?
    var categoryIndex: Int?
    var categories = ["teaceremony", "travel", "makeup", "banquet", "rest", "photoshoot", "preparation", "others"]
    var tableViewPart = CardPartTableView()

    var meetup: Meetup?
    var geolocation = ""

    func setSchedule(schedule: Schedule) {
        let largerDate = (schedule.date > Int(NSDate().timeIntervalSince1970)) ? schedule.date : Int(NSDate().timeIntervalSince1970)
        startTimeDatePicker.date = Helper.getDate(text: String(largerDate))
        endTimeDatePicker.date = Helper.getDate(text: String(largerDate + 3600))

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMMM YYYY, h:mma"

        meetupStartTimePart.text = dateFormatter.string(from: startTimeDatePicker.date)
        meetupEndTimePart.text = dateFormatter.string(from: endTimeDatePicker.date)
        meetupGeolocationPart.text = "No map available."
        meetupGeolocationButton.text = "Add a Map"
    }
    func setMeetup(meetup: Meetup) {
        self.meetup = meetup
        meetupNamePart.text = meetup.name
        meetupDescPart.text = String(meetup.desc)
        meetupStartTimePart.text = Helper.formatDate(text: String(meetup.startTime), format: "d MMMM YYYY, h:mma")
        meetupEndTimePart.text = Helper.formatDate(text: String(meetup.endTime), format: "d MMMM YYYY, h:mma")
        startTimeDatePicker.date = Helper.getDate(text: String(meetup.startTime))
        endTimeDatePicker.date = Helper.getDate(text: String(meetup.endTime))
        meetupLocationPart.text = meetup.location
        categoryIndex = categories.firstIndex(of: meetup.category)
        currentCategory = categoryIndex!
        geolocation = meetup.geolocation
        meetupGeolocationPart.text = (meetup.geolocation.count > 0) ? "A map has been added." : "No map available."
        meetupGeolocationButton.text = (meetup.geolocation.count > 0) ? "Edit / Remove your Map" : "Add a Map"
    }

    func updateGeolocation(_ geolocation: String) {
        self.geolocation = geolocation
        meetupGeolocationPart.text = (geolocation.count > 0) ? "A map has been added." : "No map available."
        meetupGeolocationButton.text = "Add a Map to your Meetup"
        meetupGeolocationButton.text = (geolocation.count > 0) ? "Edit / Remove your Map" : "Add a Map"
    }

    @objc func loadMap() {
        let mapViewVC = ManageMeetupMapViewController()
        mapViewVC.setViewController(self)

        let nc = UINavigationController(rootViewController: mapViewVC)

        let name = (meetupNamePart.text == "") ? "New Meetup" : meetupNamePart.text!
        let locationName = (meetupLocationPart.text == "") ? "Location" : meetupLocationPart.text!

        if (meetup != nil && self.geolocation != "") {
            let geolocation = self.geolocation.split(separator: ",")

            mapViewVC.setLocation(lat: Double(geolocation[0])!, long: Double(geolocation[1])!, name: name, locationName: locationName, isAnnotation: true)
        } else {
            mapViewVC.setLocation(lat: Helper.Geolocation["Singapore"]!["lat"]!, long: Helper.Geolocation["Singapore"]!["long"]!, name: name, locationName: locationName, isAnnotation: false)
        }

        self.navigationController?.present(nc, animated: true, completion: { () -> Void   in

        })
    }

    override func viewDidLoad() {

        let categoryViewModel = MeetupCategoryViewModel(categories: self.categories)

        let dataSource = RxCollectionViewSectionedReloadDataSource<MeetupCategoryStruct>(configureCell: {[weak self] (_, collectionView, indexPath, data) -> UICollectionViewCell in

            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as? ManageMeetupViewCategoryCell else { return UICollectionViewCell() }

            cell.setData(data)

            if (self!.categories.firstIndex(of: data) == self!.currentCategory) {
                self!.meetupCategoryPart.collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .centeredHorizontally)
                cell.categoryName.textColor = UIColor.white
                cell.layer.backgroundColor = UIColor.pastelRed.cgColor
                cell.layer.borderColor = UIColor.pastelRed.cgColor
            }

            return cell
        })

        categoryViewModel.data.asObservable().bind(to: meetupCategoryPart.collectionView.rx.items(dataSource: dataSource)).disposed(by: bag)

        meetupCategoryPart.delegate = self

        meetupNamePart.becomeFirstResponder()
        let stackView = CardPartStackView()
        stackView.axis = .vertical

        mainErrorText.text = ""
        mainErrorText.font = mainErrorText.font.withSize(14.0)
        mainErrorText.textColor = UIColor.pastelRed
        mainErrorText.isHidden = true
        stackView.addArrangedSubview(mainErrorText)

        informationSubtitle.text = "Meetup Information"
        informationSubtitle.font = informationSubtitle.font.withSize(24.0)
        informationSubtitle.textColor = UIColor.darkGray
        stackView.addArrangedSubview(informationSubtitle)

        errorText.text = ""
        errorText.font = errorText.font.withSize(14.0)
        errorText.textColor = UIColor.pastelRed
        errorText.isHidden = true
        stackView.addArrangedSubview(errorText)

        let nameStackView = CardPartStackView()
        nameStackView.axis = .vertical
        nameStackView.spacing = 6.0
        meetupNameLabel.text = "NAME"
        meetupNameLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        meetupNameLabel.textColor = UIColor.gray
        meetupNamePart.placeholder = "Name"
        meetupNamePart.maxLength = 60
        meetupNamePart.font = Helper.theme.normalTextFont.withSize(24.0)
        meetupNamePart.autocapitalizationType = .sentences

        nameStackView.addArrangedSubview(meetupNameLabel)
        nameStackView.addArrangedSubview(meetupNamePart)
        stackView.addArrangedSubview(nameStackView)

        let valueStackView = CardPartStackView()
        valueStackView.axis = .vertical
        valueStackView.spacing = 6.0
        meetupDescLabel.text = "DESCRIPTION"
        meetupDescLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        meetupDescLabel.textColor = UIColor.gray
        meetupDescPart.placeholder = "Description"
        meetupDescPart.maxLength = 120
        meetupDescPart.autocapitalizationType = .sentences
        meetupDescPart.font = Helper.theme.normalTextFont.withSize(24.0)
        valueStackView.addArrangedSubview(meetupDescLabel)
        valueStackView.addArrangedSubview(meetupDescPart)
        stackView.addArrangedSubview(valueStackView)

        let locationStackView = CardPartStackView()
        locationStackView.axis = .vertical
        locationStackView.spacing = 6.0
        meetupLocationLabel.text = "LOCATION"
        meetupLocationLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        meetupLocationLabel.textColor = UIColor.gray
        meetupLocationPart.placeholder = "Location"
        meetupLocationPart.maxLength = 120
        meetupLocationPart.autocapitalizationType = .sentences
        meetupLocationPart.font = Helper.theme.normalTextFont.withSize(24.0)
        locationStackView.addArrangedSubview(meetupLocationLabel)
        locationStackView.addArrangedSubview(meetupLocationPart)
        stackView.addArrangedSubview(locationStackView)

        let geolocationStackView = CardPartStackView()
        geolocationStackView.axis = .vertical
        geolocationStackView.spacing = 6.0
        meetupGeolocationLabel.text = "MAP"
        meetupGeolocationLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        meetupGeolocationLabel.textColor = UIColor.gray
        geolocationStackView.addArrangedSubview(meetupGeolocationLabel)
        meetupGeolocationPart.textColor = UIColor.darkGray
        meetupGeolocationPart.font = meetupGeolocationPart.font.withSize(18.0)
        geolocationStackView.addArrangedSubview(meetupGeolocationPart)
        let tap = UITapGestureRecognizer(target: self, action: #selector(loadMap))
        meetupGeolocationButton.isUserInteractionEnabled = true
        meetupGeolocationButton.addGestureRecognizer(tap)
        meetupGeolocationButton.textColor = UIColor.pastelRed
        meetupGeolocationButton.font = meetupGeolocationButton.font.withSize(14.0)
        geolocationStackView.addArrangedSubview(meetupGeolocationButton)
        stackView.addArrangedSubview(geolocationStackView)

        let categoryStackView = CardPartStackView()
        categoryStackView.axis = .vertical
        categoryStackView.spacing = 12.0
        meetupCategoryLabel.text = "TYPE"
        meetupCategoryLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        meetupCategoryLabel.textColor = UIColor.gray

        meetupCategoryPart.collectionView.register(ManageMeetupViewCategoryCell.self, forCellWithReuseIdentifier: "CategoryCell")
        meetupCategoryPart.collectionView.backgroundColor = .clear
        meetupCategoryPart.collectionView.showsHorizontalScrollIndicator = false
        meetupCategoryPart.collectionView.frame = CGRect(x: 0, y: 0, width: 180, height: 46)

        categoryStackView.addArrangedSubview(meetupCategoryLabel)
        categoryStackView.addArrangedSubview(meetupCategoryPart)
        stackView.addArrangedSubview(categoryStackView)

        stackView.addArrangedSubview(CardPartSeparatorView())

        dateSubtitle.text = "Meetup Date"
        dateSubtitle.font = dateSubtitle.font.withSize(24.0)
        dateSubtitle.textColor = UIColor.darkGray
        stackView.addArrangedSubview(dateSubtitle)

        errorText2.text = ""
        errorText2.font = errorText2.font.withSize(14.0)
        errorText2.textColor = UIColor.pastelRed
        errorText2.isHidden = true
        stackView.addArrangedSubview(errorText2)

        let startTimeStackView = CardPartStackView()
        startTimeStackView.axis = .vertical
        startTimeStackView.spacing = 6.0
        meetupStartTimeLabel.text = "START TIME"
        meetupStartTimeLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        meetupStartTimeLabel.textColor = UIColor.gray
        startTimeDatePicker.datePickerMode = .dateAndTime
        meetupStartTimePart.inputView = startTimeDatePicker
        let startTimeToolbar = UIToolbar()
        startTimeToolbar.sizeToFit()
        let startTimeDoneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClicked))
        startTimeToolbar.setItems([startTimeDoneButton], animated: true)
        meetupStartTimePart.inputAccessoryView = startTimeToolbar
        meetupStartTimePart.placeholder = "Date"
        meetupStartTimePart.font = Helper.theme.normalTextFont.withSize(24.0)
        startTimeStackView.addArrangedSubview(meetupStartTimeLabel)
        startTimeStackView.addArrangedSubview(meetupStartTimePart)
        stackView.addArrangedSubview(startTimeStackView)

        let endTimeStackView = CardPartStackView()
        endTimeStackView.axis = .vertical
        endTimeStackView.spacing = 6.0
        meetupEndTimeLabel.text = "END TIME"
        meetupEndTimeLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        meetupEndTimeLabel.textColor = UIColor.gray
        endTimeDatePicker.datePickerMode = .dateAndTime
        meetupEndTimePart.inputView = endTimeDatePicker
        let endTimeToolbar = UIToolbar()
        endTimeToolbar.sizeToFit()
        let endTimeDoneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClicked))
        endTimeToolbar.setItems([endTimeDoneButton], animated: true)
        meetupEndTimePart.inputAccessoryView = endTimeToolbar
        meetupEndTimePart.placeholder = "Date"
        meetupEndTimePart.font = Helper.theme.normalTextFont.withSize(24.0)
        endTimeStackView.addArrangedSubview(meetupEndTimeLabel)
        endTimeStackView.addArrangedSubview(meetupEndTimePart)
        stackView.addArrangedSubview(endTimeStackView)

        saveButton.backgroundColor = UIColor.pastelRed
        saveButton.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        saveButton.titleLabel?.font = saveButton.titleLabel?.font.withSize(14.0)
        saveButton.layer.cornerRadius = 10.0
        saveButton.contentHorizontalAlignment = .center
        saveButton.setTitleColor(UIColor.white, for: .normal)
        stackView.addArrangedSubview(saveButton)

        stackView.spacing = 24.0

        setupCardParts([stackView])
    }

    @objc func doneClicked() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMMM YYYY, h:mma"
        self.meetupStartTimePart.text = dateFormatter.string(from: startTimeDatePicker.date)
        self.meetupEndTimePart.text = dateFormatter.string(from: endTimeDatePicker.date)
        self.view.endEditing(true)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.cellForItem(at: indexPath)?.isSelected = true
        currentCategory = indexPath[1]
    }
}
struct MeetupCategoryStruct {
    var header: String
    var items: [String]
}

extension MeetupCategoryStruct: SectionModelType {

    init(original: MeetupCategoryStruct, items: [String]) {
        self = original
        self.items = items
    }
}

class MeetupCategoryViewModel {
    typealias ReactiveSection = Variable<[MeetupCategoryStruct]>
    var data = ReactiveSection([])

    init(categories: [String]) {
        data.value = [MeetupCategoryStruct(header: "", items: categories)]
    }
}

class ManageMeetupViewCategoryCell: CardPartCollectionViewCardPartsCell {

    let categoryStackView = CardPartStackView()
    let categoryName = CardPartTextView(type: .normal)
    var category: String?

    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                self.categoryName.textColor = UIColor.white
                self.layer.backgroundColor = UIColor.pastelRed.cgColor
            } else {
                self.categoryName.textColor = UIColor.darkGray
                self.layer.backgroundColor = UIColor.pastel.cgColor
            }
        }
    }

    override init(frame: CGRect) {

        super.init(frame: frame)

        self.layer.cornerRadius = 5

        self.layer.backgroundColor = UIColor.pastel.cgColor
        categoryName.textColor = UIColor.darkGray
        categoryName.font = categoryName.font.withSize(16.0)
        categoryName.clipsToBounds = true
        categoryName.textAlignment = .center
        categoryName.margins.top = 2
        categoryName.lineHeightMultiple = 0.8
        categoryName.widthAnchor.constraint(equalToConstant: self.layer.bounds.width).isActive = true
        categoryName.heightAnchor.constraint(equalToConstant: 46).isActive = true

        setupCardParts([categoryName])
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setData(_ category: String) {
        categoryName.text = Helper.getMeetupCategory(category)
        self.category = category
    }
}
