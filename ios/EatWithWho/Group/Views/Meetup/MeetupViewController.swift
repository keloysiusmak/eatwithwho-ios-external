//
//  MeetupViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 02/03/20.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import RxDataSources
import Bond
import BottomPopup

class MeetupViewController: CardsViewController, MeetupView, NVMeetupIndicatorViewable {

    var meetup: Meetup?
    var scheduleViewPresenter: ScheduleViewPresenter?

    let meetupViewPresenter = MeetupViewPresenter(meetupService: MeetupService(), coupleService: CoupleService(), friendService: FriendService(), todoService: todoService())
    let meetupCard = MeetupViewMeetupCard()

    func initValues(meetup: Meetup) {
        self.meetup = meetup

        meetupCard.setMeetup(meetup: meetup)
    }
    func initViewPresenter(scheduleViewPresenter: ScheduleViewPresenter) {
        self.scheduleViewPresenter = scheduleViewPresenter
    }

    @objc func dismissMeetup() {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.backgroundColor = UIColor.white
        self.collectionView.contentInset.top = 0

        meetupViewPresenter.attachView(view: self)
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissMeetup))

        navigationItem.leftBarButtonItem = cancelButton

        let deleteButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteMeetup))

        navigationItem.rightBarButtonItem = deleteButton

        meetupCard.setController(controller: self)
        meetupCard.editButtonPart.addTarget(self, action: #selector(editMeetup), for: .touchUpInside)

        loadCards(cards: [meetupCard])
    }

    func marktodo(todoId: String, meetupId: String, status: String) {
        meetupViewPresenter.marktodo(todoId: todoId, meetupId: meetupId, status: status) { _ in
            self.scheduleViewPresenter?.getSchedule(scheduleId: UserDefaults.standard.string(forKey: "scheduleId")!) { _ in

            }
        }
    }

    func loadFile(file: File) {
        NetworkManager.isReachable { _ in
            let destination: URL = URL(string: "https://s3-ap-southeast-1.amazonaws.com/eatwithwho-uploads-" + appSecrets.env +  "/" + file.fileStorageName)!

            let controller = WebView()
            controller.setURL(url: destination)

            self.navigationController?.pushViewController(controller, animated: true)
        }
        NetworkManager.isUnreachable { _ in
            let alert = UIAlertController(title: "No Internet Connection", message: "", preferredStyle: .alert)

            self.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }

    func presentValues(meetup: Meetup, showPanel: Int) {
        self.meetup = meetup

        meetupCard.setMeetup(meetup: meetup)
    }

    func dismiss() {
        self.scheduleViewPresenter?.loadSchedule(scheduleId: UserDefaults.standard.string(forKey: "scheduleId")!) { _ in

        }
        self.navigationController?.popViewController(animated: true)
    }

    @objc func deleteMeetup(sender: UIButton!) {
        NetworkManager.isReachable { _ in
            let refreshAlert = UIAlertController(title: "Are you sure you want to delete this meetup?", message: "This action is irreversible.", preferredStyle: UIAlertController.Style.alert)

            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_: UIAlertAction!) in
                NVMeetupIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
                self.meetupViewPresenter.deleteMeetup(meetupId: self.meetup!._id) { _ in
                    self.scheduleViewPresenter!.getSchedule(scheduleId: UserDefaults.standard.string(forKey: "scheduleId")!) { _ in
                        self.dismiss(animated: true, completion: nil)
                        NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
                    }
                }
            }))

            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

            self.present(refreshAlert, animated: true, completion: nil)
        }
        NetworkManager.isUnreachable { _ in
            let alert = UIAlertController(title: "No Internet Connection", message: "", preferredStyle: .alert)

            self.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }

    @objc func editMeetup(sender: UIButton!) {
        NetworkManager.isReachable { _ in
            let manageMeetupVC = ManageMeetupViewController()

            manageMeetupVC.initValues(meetup: self.meetup!)
            manageMeetupVC.initViewPresenter(scheduleViewPresenter: self.scheduleViewPresenter!)
            manageMeetupVC.initViewPresenter(meetupViewPresenter: self.meetupViewPresenter)

            let nc = UINavigationController(rootViewController: manageMeetupVC)

            self.navigationController?.present(nc, animated: true, completion: { () -> Void   in

            })
        }
        NetworkManager.isUnreachable { _ in
            let alert = UIAlertController(title: "No Internet Connection", message: "", preferredStyle: .alert)

            self.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }

    func manageFiles() {
        NetworkManager.isReachable { _ in
            let manageFilesVC = ManageFilesViewController()

            manageFilesVC.initViewPresenter(scheduleViewPresenter: self.scheduleViewPresenter!)
            manageFilesVC.initViewPresenter(meetupViewPresenter: self.meetupViewPresenter)
            manageFilesVC.initValues(files: (self.meetup?.files)!, meetup: self.meetup!)

            let nc = UINavigationController(rootViewController: manageFilesVC)

            self.navigationController?.present(nc, animated: true, completion: { () -> Void   in

            })
        }
        NetworkManager.isUnreachable { _ in
            let alert = UIAlertController(title: "No Internet Connection", message: "", preferredStyle: .alert)

            self.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
}

class MeetupViewMeetupCard: CardPartsViewController, TransparentCardTrait, NoTopBottomMarginsCardTrait, CardPartTableViewDelegte, CardPartCollectionViewDelegte {

    var titlePart = CardPartTextView(type: .title)
    var datePart = CardPartTextView(type: .detail)
    var locationPart = CardPartTextView(type: .detail)
    var locationMapPart = CardPartTextView(type: .detail)
    var locationIconPart = CardPartImageView()
    var locationStack = CardPartStackView()
    var categoryPart = CardPartTextView(type: .title)
    var startTimePart = CardPartTextView(type: .title)
    var endTimePart = CardPartTextView(type: .title)
    var durationPart = CardPartTextView(type: .title)
    var descriptionPart = CardPartTextView(type: .normal)
    let filesButtonPart = CardPartButtonView()
    let editButtonPart = CardPartButtonView()
    var controller: MeetupViewController?
    let friendTitlePart = CardPartButtonView()
    let friendLineView = CardPartSeparatorView()
    let todoTitlePart = CardPartButtonView()
    let todoLineView = CardPartSeparatorView()
    let filesTitlePart = CardPartButtonView()
    let filesLineView = CardPartSeparatorView()
    let filesBottomStack = CardPartStackView()
    let toggleStack = CardPartStackView()
    let friendSpacer = CardPartSpacerView(height: 20)

    let filesEmptyTitle = CardPartTextView(type: .detail)

    var scheduleViewPresenter: ScheduleViewPresenter?

    let filesTable = CardPartTableView()

    var files = MutableObservableArray<File>([])

    var meetup: Meetup?

    @objc func loadMap() {
        let mapViewVC = MeetupMapViewController()

        let nc = UINavigationController(rootViewController: mapViewVC)

        let geolocation = meetup!.geolocation.split(separator: ",")

        mapViewVC.setLocation(lat: Double(geolocation[0])!, long: Double(geolocation[1])!, name: meetup!.name, locationName: meetup!.location)

        self.navigationController?.present(nc, animated: true, completion: { () -> Void   in

        })
    }

    override func viewDidLoad() {

        filesTable.tableView.register(MeetupFileCell.self, forCellReuseIdentifier: "FileCell")
        filesTable.tableView.estimatedRowHeight = 156
        filesTable.rowHeight = UITableView.automaticDimension
        filesTable.delegate = self

        datePart.textColor = UIColor.pastelRed
        datePart.font = datePart.font.withSize(16.0)

        locationPart.textColor = UIColor.lightGray
        locationPart.font = locationPart.font.withSize(14.0)
        locationIconPart.imageName = "location"
        locationIconPart.image = locationIconPart.image!.withRenderingMode(.alwaysTemplate)
        locationIconPart.heightAnchor.constraint(equalToConstant: 24).isActive = true
        locationIconPart.widthAnchor.constraint(equalToConstant: 24).isActive = true
        locationIconPart.tintColor = UIColor.lightGray
        locationIconPart.contentMode = .scaleAspectFit
        locationMapPart.font = locationMapPart.font.withSize(14.0)
        locationMapPart.textColor = UIColor.pastelRed
        locationMapPart.text = "Map Available"
        let tap = UITapGestureRecognizer(target: self, action: #selector(loadMap))
        locationMapPart.isUserInteractionEnabled = true
        locationMapPart.addGestureRecognizer(tap)
        locationPart.font = locationPart.font.withSize(14.0)
        locationPart.textColor = UIColor.gray
        locationStack.axis = .horizontal
        locationStack.addArrangedSubview(locationIconPart)
        locationStack.addArrangedSubview(locationPart)
        locationStack.addArrangedSubview(locationMapPart)
        let spacerView = UIView()
        spacerView.setContentHuggingPriority(.defaultLow, for: .horizontal)
        locationStack.addArrangedSubview(spacerView)
        locationStack.spacing = 6.0

        startTimePart.textColor = UIColor.darkGray
        endTimePart.textColor = UIColor.darkGray
        titlePart.font = titlePart.font.withSize(32.0)
        titlePart.textColor = UIColor.darkGray
        descriptionPart.font = descriptionPart.font.withSize(16.0)

        editButtonPart.backgroundColor = UIColor.pastel
        editButtonPart.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        editButtonPart.titleLabel?.font = editButtonPart.titleLabel?.font.withSize(14.0)
        editButtonPart.layer.cornerRadius = 10.0
        editButtonPart.contentHorizontalAlignment = .center
        editButtonPart.setTitleColor(UIColor.darkGray, for: .normal)
        editButtonPart.setTitle("Edit Meetup", for: .normal)

        filesButtonPart.backgroundColor = UIColor.pastelRed
        filesButtonPart.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        filesButtonPart.titleLabel?.font = filesButtonPart.titleLabel?.font.withSize(14.0)
        filesButtonPart.layer.cornerRadius = 10.0
        filesButtonPart.contentHorizontalAlignment = .center
        filesButtonPart.setTitleColor(UIColor.white, for: .normal)
        filesButtonPart.setTitle("Manage Files", for: .normal)
        filesButtonPart.addTarget(self, action: #selector(manageFiles), for: .touchUpInside)

        let stack = CardPartStackView()
        stack.axis = .vertical

        let stack2 = CardPartStackView()
        stack2.axis = .vertical
        stack2.spacing = 6.0
        stack2.isLayoutMarginsRelativeArrangement = true
        stack2.addArrangedSubview(datePart)
        stack2.addArrangedSubview(titlePart)
        stack2.addArrangedSubview(locationStack)
        stack2.addArrangedSubview(descriptionPart)
        stack2.addArrangedSubview(CardPartSpacerView(height: 4))

        let leftStack = CardPartStackView()
        leftStack.axis = .vertical
        leftStack.spacing = 6.0
        let startTimeLabel = CardPartTextView(type: .detail)
        startTimeLabel.font = startTimeLabel.font.withSize(14.0)
        startTimeLabel.text = "Starts"
        startTimeLabel.textColor = UIColor.gray
        leftStack.addArrangedSubview(startTimeLabel)
        leftStack.addArrangedSubview(startTimePart)
        leftStack.addArrangedSubview(CardPartSpacerView(height: 4))

        let rightStack = CardPartStackView()
        rightStack.axis = .vertical
        rightStack.spacing = 6.0
        let endTimeLabel = CardPartTextView(type: .detail)
        endTimeLabel.font = endTimeLabel.font.withSize(14.0)
        endTimeLabel.text = "Ends"
        endTimeLabel.textColor = UIColor.gray
        rightStack.addArrangedSubview(endTimeLabel)
        rightStack.addArrangedSubview(endTimePart)
        rightStack.addArrangedSubview(CardPartSpacerView(height: 4))

        let leftRightStack = CardPartStackView()
        leftRightStack.axis = .horizontal
        leftRightStack.distribution = .fillEqually
        leftRightStack.alignment = .top
        leftRightStack.spacing = 20.0
        leftRightStack.addArrangedSubview(leftStack)
        leftRightStack.addArrangedSubview(rightStack)

        stack2.addArrangedSubview(leftRightStack)

        stack.backgroundColor = UIColor.white
        stack.addArrangedSubview(stack2)

        stack2.addArrangedSubview(CardPartSpacerView(height: 10))
        stack2.addArrangedSubview(editButtonPart)
        stack2.addArrangedSubview(CardPartSpacerView(height: 20))
        stack2.addArrangedSubview(CardPartSeparatorView())

        let bottomStack = CardPartStackView()
        bottomStack.axis = .vertical
        bottomStack.alignment = .leading

        let filesText = NSMutableAttributedString(string: "FILES", attributes: [:])
        filesText.addAttribute(NSAttributedString.Key.kern, value: 1.6, range: NSRange(location: 0, length: 5))
        filesText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.pastelRed, range: NSRange(location: 0, length: 5))
        filesText.addAttribute(NSAttributedString.Key.font, value: CardPartTextView(type: .title).font!.withSize(14.0), range: NSRange(location: 0, length: 5))

        filesTitlePart.setAttributedTitle(filesText, for: .normal)
        filesTitlePart.heightAnchor.constraint(equalToConstant: 18).isActive = true
        filesTitlePart.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        filesTitlePart.backgroundColor = nil
        filesTitlePart.contentHorizontalAlignment = .center
        filesTitlePart.heightAnchor.constraint(equalToConstant: 18).isActive = true

        let filesStack = CardPartStackView()
        filesStack.axis = .vertical
        filesStack.spacing = 6.0
        filesStack.clipsToBounds = true
        filesStack.alignment = .center

        filesStack.addArrangedSubview(filesTitlePart)
        filesLineView.backgroundColor = UIColor.pastelRed
        filesLineView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        filesLineView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        filesStack.addArrangedSubview(filesLineView)

        let panelSelectStackView = CardPartStackView()
        panelSelectStackView.axis = .horizontal
        panelSelectStackView.spacing = 10.0
        panelSelectStackView.distribution = .equalCentering

        panelSelectStackView.addArrangedSubview(filesStack)

        bottomStack.addArrangedSubview(panelSelectStackView)

        filesBottomStack.axis = .vertical

        filesEmptyTitle.font = filesEmptyTitle.font.withSize(14.0)
        filesEmptyTitle.text = "No Files"
        filesEmptyTitle.lineHeightMultiple = 2.0
        filesEmptyTitle.textAlignment = .center

        filesBottomStack.addArrangedSubview(filesEmptyTitle)
        filesBottomStack.addArrangedSubview(filesTable)
        filesBottomStack.addArrangedSubview(CardPartSpacerView(height: 20))
        filesBottomStack.addArrangedSubview(filesButtonPart)

        setupCardParts([stack, bottomStack, filesBottomStack])

        self.files.bind(to: filesTable.tableView) { data, indexPath, tableView in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FileCell", for: indexPath) as? MeetupFileCell else { return UITableViewCell() }

            cell.setData(data[indexPath.row], self.meetup!, controller: self.controller!)

            return cell
        }
    }

    func setController(controller: MeetupViewController) {
        self.controller = controller
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView == filesTable.tableView) {
            self.controller!.loadFile(file: files.array[indexPath.row])
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }

    func setMeetup(meetup: Meetup) {
        self.meetup = meetup
        if (meetup.geolocation != "") {
            locationPart.text = meetup.location + " · "
            locationMapPart.isHidden = false
        } else {
            locationPart.text = meetup.location
            locationMapPart.isHidden = true
        }
        if (meetup.files!.count == 0) {
            filesBottomStack.isHidden = true
            filesEmptyTitle.isHidden = false
        } else {
            filesBottomStack.isHidden = false
            filesEmptyTitle.isHidden = true
        }

        if (meetup.location.count > 0) {
            locationStack.isHidden = false
        } else {
            locationStack.isHidden = true
        }

        datePart.text = Helper.formatDate(text: String(meetup.startTime), format: "EEEE, d MMM YYYY")

        categoryPart.text = Helper.getMeetupCategory(meetup.category)

        startTimePart.text = Helper.formatTime(text: String(meetup.startTime))

        let endTimeText = Helper.formatTime(text: String(meetup.endTime))
        var daysDifferential = ""
        if (meetup.endTime - 86400 > meetup.startTime) {
            let timeDifference = Helper.timeDifferenceFull(time1: meetup.endTime, time2: meetup.startTime, allowedUnits: [.day])
            daysDifferential = " (+" + timeDifference + ")"
        }

        let endTime = NSMutableAttributedString(string: endTimeText + daysDifferential, attributes: [NSAttributedString.Key.font: CardPartTextView(type: .title).font.withSize(16.0), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        endTime.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray, range: NSRange(location: endTimeText.count, length: daysDifferential.count))
        endTime.addAttribute(NSAttributedString.Key.font, value: CardPartTextView(type: .detail).font.withSize(14.0), range: NSRange(location: endTimeText.count, length: daysDifferential.count))

        endTimePart.attributedText = endTime

        durationPart.text = Helper.timeDifferenceFull(time1: meetup.startTime, time2: meetup.endTime, allowedUnits: [.hour, .minute])

        titlePart.text = meetup.name

        descriptionPart.text = meetup.desc

        self.files.replace(with: meetup.files!)
    }

    @objc func manageFiles() {
        self.controller?.manageFiles()
    }
}

class MeetupFileCell: CardPartTableViewCardPartsCell {

    let fileTitle = CardPartTextView(type: .title)
    let fileSize = CardPartTextView(type: .detail)
    var fileStatusValue: String?
    var file: File?
    var meetup: Meetup?

    var controller: MeetupViewController?

    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        selectionStyle = .none

        let fileStack = CardPartStackView()
        fileStack.axis = .vertical
        fileStack.margins = UIEdgeInsets(top: 6, left: 28, bottom: 6, right: 28)
        fileStack.spacing = 6.0

        let fileDetailStack = CardPartStackView()
        fileDetailStack.axis = .horizontal
        fileDetailStack.margins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        fileDetailStack.spacing = 20.0

        let spacerView = UIView()
        spacerView.setContentHuggingPriority(.defaultLow, for: .horizontal)

        fileTitle.font = fileTitle.font.withSize(21.0)
        fileTitle.textColor = UIColor.darkGray

        let fileLabel = CardPartTextView(type: .detail)
        fileLabel.font = fileLabel.font.withSize(14.0)
        fileLabel.textColor = UIColor.gray
        fileLabel.text = "File Size: "

        fileSize.font = fileSize.font.withSize(16.0)
        fileSize.textColor = UIColor.darkGray
        let createdStack = CardPartStackView()
        createdStack.axis = .horizontal
        createdStack.addArrangedSubview(fileLabel)
        createdStack.addArrangedSubview(fileSize)
        createdStack.spacing = 6.0

        fileStack.addArrangedSubview(fileTitle)
        fileDetailStack.addArrangedSubview(createdStack)
        fileDetailStack.addArrangedSubview(spacerView)
        fileStack.addArrangedSubview(fileDetailStack)
        fileStack.addArrangedSubview(CardPartSpacerView(height: 0))

        setupCardParts([fileStack])
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setData(_ file: File, _ meetup: Meetup, controller: MeetupViewController) {
        self.controller = controller
        fileTitle.text = file.fileName
        self.file = file
        self.meetup = meetup
        fileSize.text = ByteCountFormatter.string(fromByteCount: Int64(file.fileSize), countStyle: .file)
    }
}
