//
//  ManageFilesViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 28/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import Bond
import MobileCoreServices
import AWSS3
import AWSCore

class ManageFilesViewController: CardsViewController {

    var files: [File] = []
    var meetup: Meetup?

    var scheduleViewPresenter: ScheduleViewPresenter?
    var meetupViewPresenter: MeetupViewPresenter?
    var manageFilesViewPresenter = ManageFilesViewPresenter(fileService: FileService())

    var manageFileCard = ManageFilesViewManageFilesCard()

    func initValues(files: [File], meetup: Meetup) {
        self.files = files
        self.meetup = meetup

        manageFileCard.setFiles(files: files)
        manageFileCard.setController(controller: self)
    }
    func initViewPresenter(scheduleViewPresenter: ScheduleViewPresenter) {
        self.scheduleViewPresenter = scheduleViewPresenter
    }
    func initViewPresenter(meetupViewPresenter: MeetupViewPresenter) {
        self.meetupViewPresenter = meetupViewPresenter
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = "Files"

        self.collectionView.backgroundColor = UIColor.white
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissWindow))
        navigationItem.leftBarButtonItem = cancelButton

        self.meetupViewPresenter!.loadUsage(userId: UserDefaults.standard.string(forKey: "userId")!) { result in
            self.manageFileCard.setValues(filesSize: result)
        }
        NetworkManager.sharedInstance.reachability.whenReachable = { _ in
            self.meetupViewPresenter!.loadUsage(userId: UserDefaults.standard.string(forKey: "userId")!) { result in
                self.manageFileCard.setValues(filesSize: result)
            }
        }
        loadCards(cards: [manageFileCard])

    }
    func deleteFile(_ file: File) {
        NetworkManager.isReachable { _ in
            let refreshAlert = UIAlertController(title: "Are you sure you want to delete this file?", message: "This action is irreversible.", preferredStyle: UIAlertController.Style.alert)

            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_: UIAlertAction!) in
                NVMeetupIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
                self.manageFilesViewPresenter.deleteFile(fileId: file._id) { _ in
                    let newFiles = self.files.filter({ (f) -> Bool in
                        return f != file
                    })
                    self.files = newFiles
                    UserDefaults.standard.set(NSDate().timeIntervalSince1970, forKey: "lastUpdatedUsage")
                    self.manageFileCard.setFiles(files: newFiles)
                    self.manageFileCard.setValues(filesSize: self.manageFileCard.filesSize - file.fileSize)
                    self.meetupViewPresenter!.getMeetup(meetupId: self.meetup!._id) { _ in
                        NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
                    }
                    self.scheduleViewPresenter!.getSchedule(scheduleId: UserDefaults.standard.string(forKey: "scheduleId")!) { _ in
                    }
                }
            }))

            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

            self.present(refreshAlert, animated: true, completion: nil)
        }
        NetworkManager.isUnreachable { _ in
            let alert = UIAlertController(title: "No Internet Connection", message: "", preferredStyle: .alert)

            self.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }

    func uploadFile(fileName: String, fileSize: Int) {
        NetworkManager.isReachable { _ in
            NVMeetupIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
            self.manageFilesViewPresenter.uploadFile(meetupId: self.meetup!._id, fileName: fileName, fileSize: fileSize) { response in
                if (response.data!.file != nil) {
                    self.files.append(response.data!.file!)
                    self.meetupViewPresenter!.getMeetup(meetupId: self.meetup!._id) { _ in
                        NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
                    }
                    self.scheduleViewPresenter!.getSchedule(scheduleId: UserDefaults.standard.string(forKey: "scheduleId")!) { _ in

                    }
                    UserDefaults.standard.set(NSDate().timeIntervalSince1970, forKey: "lastUpdatedUsage")
                    self.manageFileCard.setValues(filesSize: self.manageFileCard.filesSize + response.data!.file!.fileSize)
                }
                self.manageFileCard.setFiles(files: self.files)
            }
        }
        NetworkManager.isUnreachable { _ in
            let alert = UIAlertController(title: "No Internet Connection", message: "", preferredStyle: .alert)

            self.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }

    @objc func dismissWindow() {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
}

class ManageFilesViewManageFilesCard: CardPartsViewController, TransparentCardTrait, CardPartTableViewDelegte, UIDocumentMenuDelegate, UIDocumentPickerDelegate, UINavigationControllerDelegate {

    let filesNameLabel = CardPartLabel()
    let filesNamePart = CardPartTextField()
    let mainErrorText = CardPartTextView(type: .title)
    let errorText = CardPartTextView(type: .title)
    let informationTitle = CardPartTextView(type: .title)
    let informationSubtitle = CardPartTextView(type: .detail)
    let filesSubtitle = CardPartTextView(type: .detail)
    let filesHelp = CardPartTextView(type: .detail)
    let saveButton = CardPartButtonView()
    let storagePercentageBar = CardPartBarView()
    let storageAvailablePart = CardPartTextView(type: .title)
    let storageQuotaPart = CardPartTextView(type: .title)
    let filesTable = CardPartTableView()
    let noFilesPart = CardPartTextView(type: .detail)

    var controller: ManageFilesViewController?
    var filesArray = MutableObservableArray<File>([])

    var filesSize: Int = 0

    var files: [File] = []

    var progress: Double = 0.0

    func setFiles(files: [File]) {
        self.files = files
        self.filesArray.replace(with: files)
        if (files.count == 0) {
            self.filesTable.isHidden = true
            self.noFilesPart.isHidden = false
        } else {
            self.filesTable.isHidden = false
            self.noFilesPart.isHidden = true
        }
    }
    func setController(controller: ManageFilesViewController) {
        self.controller = controller
    }

    override func viewDidLoad() {
        filesNamePart.becomeFirstResponder()
        let stackView = CardPartStackView()
        stackView.axis = .vertical

        mainErrorText.text = ""
        mainErrorText.font = mainErrorText.font.withSize(14.0)
        mainErrorText.textColor = UIColor.pastelRed
        mainErrorText.isHidden = true
        stackView.addArrangedSubview(mainErrorText)

        informationTitle.text = "Storage"
        informationTitle.font = informationTitle.font.withSize(24.0)
        informationTitle.textColor = UIColor.black

        let titleStackView = CardPartStackView()
        titleStackView.axis = .vertical
        titleStackView.spacing = 6.0

        informationSubtitle.text = "Available Space"
        informationSubtitle.font = informationSubtitle.font.withSize(14.0)
        informationSubtitle.textColor = UIColor.gray
        titleStackView.addArrangedSubview(informationTitle)
        titleStackView.addArrangedSubview(informationSubtitle)
        stackView.addArrangedSubview(titleStackView)

        let storageBottomStack = CardPartStackView()
        storageBottomStack.axis = .horizontal
        storageBottomStack.distribution = .fillEqually
        storageBottomStack.alignment = .top

        storageAvailablePart.font = storageAvailablePart.font.withSize(14.0)
        storageAvailablePart.textColor = UIColor.black

        storageQuotaPart.font = storageQuotaPart.font.withSize(14.0)
        storageQuotaPart.textColor = UIColor.black
        storageQuotaPart.textAlignment = .right

        storagePercentageBar.barColor = UIColor.pastelGreen
        storagePercentageBar.barHeight = 2.0
        storagePercentageBar.verticalLine.removeFromSuperlayer()
        storagePercentageBar.heightAnchor.constraint(equalToConstant: 2.0).isActive = true

        storageBottomStack.addArrangedSubview(storageAvailablePart)
        storageBottomStack.addArrangedSubview(storageQuotaPart)

        let barStack = CardPartStackView()
        barStack.axis = .vertical
        barStack.spacing = 6.0

        barStack.addArrangedSubview(storagePercentageBar)
        barStack.addArrangedSubview(storageBottomStack)
        stackView.addArrangedSubview(barStack)
        stackView.addArrangedSubview(CardPartSeparatorView())

        let filesTitleStackView = CardPartStackView()
        filesTitleStackView.axis = .vertical
        filesTitleStackView.spacing = 6.0

        filesSubtitle.text = "Showing all files attached to this meetup."
        filesSubtitle.font = filesSubtitle.font.withSize(14.0)
        filesSubtitle.textColor = UIColor.gray

        filesHelp.text = "(click on a file to delete)"
        filesHelp.font = filesSubtitle.font.withSize(14.0)
        filesHelp.textColor = UIColor.gray
        filesTitleStackView.addArrangedSubview(filesSubtitle)
        filesTitleStackView.addArrangedSubview(filesHelp)
        stackView.addArrangedSubview(filesTitleStackView)

        filesTable.tableView.register(ManageMeetupFileCell.self, forCellReuseIdentifier: "ManageMeetupFileCell")

        filesTable.tableView.estimatedRowHeight = 156
        filesTable.rowHeight = UITableView.automaticDimension

        filesTable.delegate = self

        stackView.addArrangedSubview(filesTable)

        noFilesPart.font = noFilesPart.font.withSize(14.0)
        noFilesPart.text = "No Files"
        noFilesPart.lineHeightMultiple = 2.0
        noFilesPart.textAlignment = .center

        stackView.addArrangedSubview(noFilesPart)

        errorText.text = ""
        errorText.font = errorText.font.withSize(14.0)
        errorText.textColor = UIColor.pastelRed
        errorText.isHidden = true
        stackView.addArrangedSubview(errorText)

        saveButton.setTitle("Add Files", for: .normal)
        saveButton.backgroundColor = UIColor.pastelRed
        saveButton.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        saveButton.titleLabel?.font = saveButton.titleLabel?.font.withSize(14.0)
        saveButton.layer.cornerRadius = 10.0
        saveButton.contentHorizontalAlignment = .center
        saveButton.setTitleColor(UIColor.white, for: .normal)
        saveButton.addTarget(self, action: #selector(addFile), for: .touchUpInside)
        stackView.addArrangedSubview(saveButton)

        stackView.spacing = 24.0

        setupCardParts([stackView])

        self.filesArray.bind(to: filesTable.tableView) { data, indexPath, tableView in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ManageMeetupFileCell", for: indexPath) as? ManageMeetupFileCell else { return UITableViewCell() }

            cell.setData(data[indexPath.row])
            cell.selectionStyle = .none

            return cell
        }
    }

    @objc func addFile() {
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeJPEG), String(kUTTypePNG)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }

    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        NVMeetupIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
        let fileName = String(url.absoluteString.split(separator: "/").last!)
        let remoteName = (self.controller?.meetup!._id)! + "/" + fileName

        let progressBlock: AWSS3TransferUtilityProgressBlock = {(todo, progress) in
            DispatchQueue.main.async(execute: {
                self.progress = progress.fractionCompleted
            })
        }
        let fileData = NSData(contentsOfFile: url.relativePath)

        let completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock = { (todo, error) -> Void in
            DispatchQueue.main.async(execute: {
                if let error = error {
                    NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
                    self.mainErrorText.isHidden = false
                    self.mainErrorText.text = "Something went wrong. Please try again later."
                } else if (self.progress != 1.0) {
                    NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
                    self.mainErrorText.isHidden = false
                    self.mainErrorText.text = "Something went wrong. Please try again later."
                } else {
                    self.controller?.uploadFile(fileName: fileName, fileSize: fileData!.count)
                }
            })
        }

        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock = progressBlock

        let transferUtility: (AWSS3TransferUtility?) = AWSS3TransferUtility.s3TransferUtility(forKey: "eatwithwhoS3Upload")

        transferUtility!.uploadData (fileData! as Data,
                                    bucket: "eatwithwho-uploads-" + appSecrets.env,
                                    key: remoteName,
                                    contentType: "text/plain",
                                    expression: expression,
                                    completionHandler: completionHandler).continueWith { (todo: AWStodo) -> AnyObject? in

                if let error = todo.error {
                    NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
                    self.mainErrorText.isHidden = false
                    self.mainErrorText.text = "Something went wrong. Please try again later."
                }
                if let uploadtodo = todo.result {
                    //
                }

                return nil
        }
    }

    func mimeTypeForPath(path: String) -> String {
        let url = NSURL(fileURLWithPath: path)
        let pathExtension = url.pathExtension

        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }

    func fileSize(_ url: Any) -> Int {
        var fileURL: URL?
        var fileSize: Int = 0
        if (url is URL) || (url is String) {
            if (url is URL) {
                fileURL = url as? URL
            } else {
                fileURL = URL(fileURLWithPath: url as! String)
            }
            var fileSizeValue = 0
            try? fileSizeValue = (fileURL?.resourceValues(forKeys: [URLResourceKey.fileSizeKey]).allValues.first?.value as! Int?)!
            if fileSizeValue > 0 {
                fileSize = (Int(fileSizeValue) / (1000 * 1000))
            }
        }
        return fileSize
    }

    public func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }

    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.controller!.deleteFile(self.files[indexPath.row])
    }

    func setValues(filesSize: Int) {
        self.filesSize = filesSize
        let storageQuotaSize = Helper.getCoupleSubscription(UserDefaults.standard.string(forKey: "coupleSubscription")!).maxFilesSize

        let storageAvailableText = ByteCountFormatter.string(fromByteCount: Int64(storageQuotaSize - filesSize), countStyle: .file)
        let storageAvailableAttrText = NSMutableAttributedString(string: "Available: " + storageAvailableText, attributes: [NSAttributedString.Key.font: CardPartTextView(type: .title).font.withSize(16.0)])
        storageAvailableAttrText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray, range: NSRange(location: 0, length: 10))
        storageAvailableAttrText.addAttribute(NSAttributedString.Key.font, value: Helper.theme.detailTextFont, range: NSRange(location: 0, length: 10))
        storageAvailablePart.attributedText = storageAvailableAttrText

        let storageQuotaText = ByteCountFormatter.string(fromByteCount: Int64(storageQuotaSize), countStyle: .file)
        let storageQuotaAttrText = NSMutableAttributedString(string: "Quota: " + storageQuotaText, attributes: [NSAttributedString.Key.font: CardPartTextView(type: .title).font.withSize(16.0)])
        storageQuotaAttrText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray, range: NSRange(location: 0, length: 6))
        storageQuotaAttrText.addAttribute(NSAttributedString.Key.font, value: Helper.theme.detailTextFont, range: NSRange(location: 0, length: 6))
        storageQuotaPart.attributedText = storageQuotaAttrText

        let storagePercentage = Double(filesSize) / Double(storageQuotaSize)
        storagePercentageBar.percent = storagePercentage
    }
}

class ManageMeetupFileCell: CardPartTableViewCardPartsCell {

    let fileStackView = CardPartStackView()
    let namePart = CardPartTextView(type: .title)
    let sizePart = CardPartTextView(type: .detail)
    let addedPart = CardPartTextView(type: .detail)

    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        fileStackView.axis = .vertical
        fileStackView.alignment = .leading
        fileStackView.spacing = 6
        fileStackView.margins = UIEdgeInsets(top: 10, left: 28, bottom: 10, right: 28)

        namePart.textColor = UIColor.black
        namePart.font = namePart.font.withSize(18.0)
        namePart.clipsToBounds = true
        namePart.textAlignment = .left

        fileStackView.addArrangedSubview(namePart)

        let bottomStack = CardPartStackView()
        bottomStack.axis = .horizontal
        bottomStack.spacing = 20
        bottomStack.addArrangedSubview(sizePart)
        bottomStack.addArrangedSubview(addedPart)
        fileStackView.addArrangedSubview(bottomStack)

        setupCardParts([fileStackView])
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setData(_ file: File) {
        namePart.text = file.fileName

        let sizeText = ByteCountFormatter.string(fromByteCount: Int64(file.fileSize), countStyle: .file)
        let sizeAttrText = NSMutableAttributedString(string: "File Size: " + sizeText, attributes: [NSAttributedString.Key.font: CardPartTextView(type: .title).font.withSize(14.0)])
        sizeAttrText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray, range: NSRange(location: 0, length: 10))
        sizeAttrText.addAttribute(NSAttributedString.Key.font, value: Helper.theme.detailTextFont, range: NSRange(location: 0, length: 10))
        sizePart.attributedText = sizeAttrText

        let dateAddedText = Helper.formatDate(text: String(file.createdAt), format: "d MMMM YYYY")
        let dateAddedAttrText = NSMutableAttributedString(string: "Added: " + dateAddedText, attributes: [NSAttributedString.Key.font: CardPartTextView(type: .title).font.withSize(14.0)])
        dateAddedAttrText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray, range: NSRange(location: 0, length: 6))
        dateAddedAttrText.addAttribute(NSAttributedString.Key.font, value: Helper.theme.detailTextFont, range: NSRange(location: 0, length: 6))
        addedPart.attributedText = dateAddedAttrText
    }
}
