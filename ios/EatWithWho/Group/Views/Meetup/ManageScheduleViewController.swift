//
//  ManageScheduleViewController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 28/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import Bond

class ManageScheduleViewController: CardsViewController {

    var schedule: Schedule?

    var scheduleViewPresenter: ScheduleViewPresenter?

    var manageScheduleCard = ManageScheduleViewManageScheduleCard()

    func initValues(schedule: Schedule) {
        self.schedule = schedule

        manageScheduleCard.setSchedule(schedule: schedule)
    }
    func initViewPresenter(scheduleViewPresenter: ScheduleViewPresenter) {
        self.scheduleViewPresenter = scheduleViewPresenter
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = "Update Schedule"

        self.collectionView.backgroundColor = UIColor.white

        let cancelButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissWindow))
        let saveButton = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(updateSchedule))
        navigationItem.leftBarButtonItem = cancelButton
        navigationItem.rightBarButtonItem = saveButton

        manageScheduleCard.saveButton.addTarget(self, action: #selector(updateSchedule), for: .touchUpInside)

        loadCards(cards: [manageScheduleCard])
    }

    @objc func dismissWindow() {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }

    @objc func updateSchedule() {
        //resets
        self.manageScheduleCard.mainErrorText.isHidden = true
        self.manageScheduleCard.errorText.isHidden = true
        self.manageScheduleCard.scheduleNameLabel.textColor = UIColor.gray
        self.manageScheduleCard.scheduleDateLabel.textColor = UIColor.gray

        let name = self.manageScheduleCard.scheduleNamePart.text!
        let date = self.manageScheduleCard.scheduleDatePart.text!

        if (name == "" || date == "") {
            self.manageScheduleCard.errorText.isHidden = false
            self.manageScheduleCard.errorText.text = "You have missing fields."
            if (name == "") {
                self.manageScheduleCard.scheduleNameLabel.textColor = UIColor.pastelRed
            }
            if (date == "") {
                self.manageScheduleCard.scheduleDateLabel.textColor = UIColor.pastelRed
            }
        } else {
            var error = false
            if (name.count > 60) {
                self.manageScheduleCard.errorText.isHidden = false
                self.manageScheduleCard.errorText.text = "Your schedule's name must have at most sixty characters."
                self.manageScheduleCard.scheduleNameLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (name.count < 2) {
                self.manageScheduleCard.errorText.isHidden = false
                self.manageScheduleCard.errorText.text = "Your schedule's name must have at least two characters."
                self.manageScheduleCard.scheduleNameLabel.textColor = UIColor.pastelRed
                error = true
            }
            if (!error) {
                NVMeetupIndicatorPresenter.sharedInstance.startAnimating(Helper.loader)
                scheduleViewPresenter!.updateSchedule(
                    scheduleId: UserDefaults.standard.string(forKey: "scheduleId")!,
                    name: self.manageScheduleCard.scheduleNamePart.text!,
                    date: self.manageScheduleCard.scheduleDatePicker.date) { (response) in
                        if (response.statusCode == 200) {
                            self.view.endEditing(true)
                            self.scheduleViewPresenter?.getSchedule(scheduleId: UserDefaults.standard.string(forKey: "scheduleId")!) { _ in
                                self.dismiss(animated: true, completion: nil)
                                NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
                            }
                        } else {
                            NVMeetupIndicatorPresenter.sharedInstance.stopAnimating()
                            self.manageScheduleCard.mainErrorText.isHidden = false
                            self.manageScheduleCard.mainErrorText.text = "Something went wrong. Please try again later."
                        }
                }
            }
        }
    }
}

class ManageScheduleViewManageScheduleCard: CardPartsViewController, TransparentCardTrait {

    let mainErrorText = CardPartTextView(type: .title)
    let errorText = CardPartTextView(type: .title)
    let detailsSubtitle = CardPartTextView(type: .title)
    let saveButton = CardPartButtonView()

    let scheduleNameLabel = CardPartLabel()
    let scheduleNamePart = CardPartTextField()
    let scheduleDateLabel = CardPartLabel()
    let scheduleDatePart = CardPartTextField()
    let scheduleDatePicker = UIDatePicker()

    func setSchedule(schedule: Schedule) {
        scheduleNamePart.text = schedule.name
        scheduleDatePart.text = Helper.formatDate(text: String(schedule.date), format: "EEEE, d MMM YYYY")
        scheduleDatePicker.date = Helper.getDate(text: String(schedule.date))
    }

    override func viewDidLoad() {

        scheduleNamePart.becomeFirstResponder()
        let stackView = CardPartStackView()
        stackView.axis = .vertical

        mainErrorText.text = ""
        mainErrorText.font = mainErrorText.font.withSize(14.0)
        mainErrorText.textColor = UIColor.pastelRed
        mainErrorText.isHidden = true
        stackView.addArrangedSubview(mainErrorText)

        detailsSubtitle.text = "Schedule Details"
        detailsSubtitle.font = detailsSubtitle.font.withSize(24.0)
        detailsSubtitle.textColor = UIColor.darkGray
        stackView.addArrangedSubview(detailsSubtitle)

        errorText.text = ""
        errorText.font = errorText.font.withSize(14.0)
        errorText.textColor = UIColor.pastelRed
        errorText.isHidden = true
        stackView.addArrangedSubview(errorText)

        let nameStackView = CardPartStackView()
        nameStackView.axis = .vertical
        nameStackView.spacing = 6.0
        scheduleNameLabel.text = "NAME"
        scheduleNameLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        scheduleNameLabel.textColor = UIColor.gray
        scheduleNamePart.placeholder = "Name"
        scheduleNamePart.maxLength = 60
        scheduleNamePart.font = Helper.theme.normalTextFont.withSize(24.0)
        nameStackView.addArrangedSubview(scheduleNameLabel)
        nameStackView.addArrangedSubview(scheduleNamePart)
        stackView.addArrangedSubview(nameStackView)

        let scheduleDateStackView = CardPartStackView()
        scheduleDateStackView.axis = .vertical
        scheduleDateStackView.spacing = 6.0
        scheduleDateLabel.text = "eatwith DATE"
        scheduleDateLabel.font = Helper.theme.detailTextFont.withSize(14.0)
        scheduleDateLabel.textColor = UIColor.gray
        scheduleDatePicker.datePickerMode = .date
        scheduleDatePart.inputView = scheduleDatePicker
        let scheduleDateToolbar = UIToolbar()
        scheduleDateToolbar.sizeToFit()
        let scheduleDateDoneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClicked))
        scheduleDateToolbar.setItems([scheduleDateDoneButton], animated: true)
        scheduleDatePart.inputAccessoryView = scheduleDateToolbar
        scheduleDatePart.placeholder = "Date"
        scheduleDatePart.font = Helper.theme.normalTextFont.withSize(24.0)
        scheduleDateStackView.addArrangedSubview(scheduleDateLabel)
        scheduleDateStackView.addArrangedSubview(scheduleDatePart)
        stackView.addArrangedSubview(scheduleDateStackView)

        saveButton.backgroundColor = UIColor.pastelRed
        saveButton.contentEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        saveButton.titleLabel?.font = saveButton.titleLabel?.font.withSize(14.0)
        saveButton.layer.cornerRadius = 10.0
        saveButton.contentHorizontalAlignment = .center
        saveButton.setTitleColor(UIColor.white, for: .normal)
        saveButton.setTitle("Save", for: .normal)
        stackView.addArrangedSubview(saveButton)

        stackView.spacing = 24.0

        setupCardParts([stackView])
    }

    @objc func doneClicked() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, d MMM YYYY"
        self.scheduleDatePart.text = dateFormatter.string(from: scheduleDatePicker.date)
        self.view.endEditing(true)
    }
}
