//
//  EatWithWhoTheme.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 12/3/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//
import CardParts
import Foundation

public class EatWithWhoTheme: CardPartsTheme {

    public var cardsViewContentInsetTop: CGFloat = 0.0
    public var cardsLineSpacing: CGFloat = 12

    public var cardShadow: Bool = true
    public var cardCellMargins: UIEdgeInsets = UIEdgeInsets(top: 9.0, left: 12.0, bottom: 12.0, right: 12.0)
    public var cardPartMargins: UIEdgeInsets = UIEdgeInsets(top: 5.0, left: 15.0, bottom: 5.0, right: 15.0)

    // CardPartSeparatorView
    public var separatorColor: UIColor = UIColor.lighterGray
    public var horizontalSeparatorMargins: UIEdgeInsets = UIEdgeInsets(top: 5.0, left: 15.0, bottom: 5.0, right: 15.0)

    // CardPartTextView
    public var smallTextFont: UIFont = UIFont(name: "TitilliumWeb-Regular", size: 10.0)!
    public var smallTextColor: UIColor = UIColor.gray
    public var normalTextFont: UIFont = UIFont(name: "TitilliumWeb-Regular", size: 14.0)!
    public var normalTextColor: UIColor = UIColor.gray
    public var titleTextFont: UIFont = UIFont(name: "TitilliumWeb-Bold", size: 16.0)!
    public var titleTextColor: UIColor = UIColor.black
    public var headerTextFont: UIFont = UIFont(name: "TitilliumWeb-Regular", size: 16.0)!
    public var headerTextColor: UIColor = UIColor.black
    public var detailTextFont: UIFont = UIFont(name: "TitilliumWeb-Regular", size: 12.0)!
    public var detailTextColor: UIColor = UIColor.gray

    // CardPartTitleView
    public var titleFont: UIFont = UIFont(name: "TitilliumWeb-Regular", size: 16.0)!
    public var titleColor: UIColor = UIColor.black
    public var titleViewMargins: UIEdgeInsets = UIEdgeInsets(top: 5.0, left: 15.0, bottom: 10.0, right: 15.0)

    // CardPartButtonView
    public var buttonTitleFont: UIFont = UIFont(name: "TitilliumWeb-Regular", size: 17.0)!
    public var buttonTitleColor: UIColor = UIColor.darkGray
    public var buttonCornerRadius: CGFloat = CGFloat(0.0)

    // CardPartBarView
    public var barBackgroundColor: UIColor = UIColor.lighterGray
    public var barColor: UIColor = UIColor.black
    public var todayLineColor: UIColor = UIColor.lighterGray
    public var barHeight: CGFloat = 13.5
    public var roundedCorners: Bool = false
    public var showTodayLine: Bool = true

    // CardPartTableView
    public var tableViewMargins: UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 14.0, bottom: 0.0, right: 14.0)

    // CardPartTableViewCell and CardPartTitleDescriptionView
    public var leftTitleFont: UIFont = UIFont(name: "HelveticaNeue", size: CGFloat(17))!
    public var leftDescriptionFont: UIFont = UIFont(name: "HelveticaNeue", size: CGFloat(12))!
    public var rightTitleFont: UIFont = UIFont(name: "HelveticaNeue", size: CGFloat(17))!
    public var rightDescriptionFont: UIFont = UIFont(name: "HelveticaNeue", size: CGFloat(12))!
    public var leftTitleColor: UIColor = UIColor.init(red: 17, green: 17, blue: 17, alpha: 1.0)
    public var leftDescriptionColor: UIColor = UIColor.init(red: 169, green: 169, blue: 169, alpha: 1.0)
    public var rightTitleColor: UIColor = UIColor.init(red: 17, green: 17, blue: 17, alpha: 1.0)
    public var rightDescriptionColor: UIColor = UIColor.init(red: 169, green: 169, blue: 169, alpha: 1.0)
    public var secondaryTitlePosition: CardPartSecondaryTitleDescPosition = .right

    public init() {

    }
}
