//
//  Account.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 17/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation
import os.log

struct AccountPropertyKeys {
    static let _id = "account-_id"
    static let username = "account-username"
    static let validRefreshToken = "account-validRefreshToken"
    static let type = "account-type"
    static let friend = "account-friend"
    static let modifiedAt = "account-modifiedAt"
    static let createdAt = "account-createdAt"
}

class Account: NSObject, NSCoding, Decodable {
    let _id: String
    let username: String
    let validRefreshToken: Int
    let type: String
    let friend: Friend
    let modifiedAt: Int
    let createdAt: Int

    init(_id: String, username: String, validRefreshToken: Int, type: String, friend: Friend, createdAt: Int, modifiedAt: Int) {
        self._id = _id
        self.username = username
        self.validRefreshToken = validRefreshToken
        self.friend = friend
        self.type = type
        self.modifiedAt = modifiedAt
        self.createdAt = createdAt
    }

    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("account")

    func encode(with aCoder: NSCoder) {
        aCoder.encode(_id, forKey: AccountPropertyKeys._id)
        aCoder.encode(username, forKey: AccountPropertyKeys.username)
        aCoder.encode(validRefreshToken, forKey: AccountPropertyKeys.validRefreshToken)
        aCoder.encode(type, forKey: AccountPropertyKeys.type)
        aCoder.encode(friend, forKey: AccountPropertyKeys.friend)
        aCoder.encode(modifiedAt, forKey: AccountPropertyKeys.modifiedAt)
        aCoder.encode(createdAt, forKey: AccountPropertyKeys.createdAt)
    }

    required convenience init?(coder aDecoder: NSCoder) {

        guard let _id = aDecoder.decodeObject(forKey: AccountPropertyKeys._id) as? String else {
            return nil
        }

        guard let username = aDecoder.decodeObject(forKey: AccountPropertyKeys.username) as? String else {
            return nil
        }
        let validRefreshToken = aDecoder.decodeInteger(forKey: AccountPropertyKeys.validRefreshToken)

        guard let type = aDecoder.decodeObject(forKey: AccountPropertyKeys.type) as? String else {
            return nil
        }

        guard let friend = aDecoder.decodeObject(forKey: AccountPropertyKeys.friend) as? Friend else {
            return nil
        }

        let createdAt = aDecoder.decodeInteger(forKey: AccountPropertyKeys.createdAt)
        let modifiedAt = aDecoder.decodeInteger(forKey: AccountPropertyKeys.modifiedAt)

        self.init(_id: _id, username: username, validRefreshToken: validRefreshToken, type: type, friend: friend, createdAt: createdAt, modifiedAt: modifiedAt)
    }
}
