//
//  Notification.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 17/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

struct NotificationPropertyKeys {
    static let _id = "notification-_id"
    static let type = "notification-type"
    static let subtype = "notification-subtype"
    static let object = "notification-object"
    static let param1 = "notification-param1"
    static let param2 = "notification-param2"
    static let recipients = "notification-recepients"
    static let modifiedAt = "notification-modifiedAt"
    static let createdAt = "notification-createdAt"
}

class Notification: NSObject, NSCoding, Decodable {
    let _id: String
    let type: Int
    let subtype: Int
    let object: String?
    let param1: Friend
    let param2: todo
    let recipients: [String]?
    let modifiedAt: Int
    let createdAt: Int

    init(_id: String, type: Int, subtype: Int, object: String, param1: Friend, param2: todo, recipients: [String], createdAt: Int, modifiedAt: Int) {
        self._id = _id
        self.type = type
        self.subtype = subtype
        self.object = object
        self.param1 = param1
        self.param2 = param2
        self.recipients = recipients
        self.modifiedAt = modifiedAt
        self.createdAt = createdAt
    }

    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("notification")

    func encode(with aCoder: NSCoder) {
        aCoder.encode(_id, forKey: NotificationPropertyKeys._id)
        aCoder.encode(type, forKey: NotificationPropertyKeys.type)
        aCoder.encode(subtype, forKey: NotificationPropertyKeys.subtype)
        aCoder.encode(object, forKey: NotificationPropertyKeys.object)
        aCoder.encode(param1, forKey: NotificationPropertyKeys.param1)
        aCoder.encode(param2, forKey: NotificationPropertyKeys.param2)
        aCoder.encode(recipients, forKey: NotificationPropertyKeys.recipients)
        aCoder.encode(modifiedAt, forKey: NotificationPropertyKeys.modifiedAt)
        aCoder.encode(createdAt, forKey: NotificationPropertyKeys.createdAt)
    }

    required convenience init?(coder aDecoder: NSCoder) {

        guard let _id = aDecoder.decodeObject(forKey: NotificationPropertyKeys._id) as? String else {
            return nil
        }
        let type = aDecoder.decodeInteger(forKey: NotificationPropertyKeys.type)
        let subtype = aDecoder.decodeInteger(forKey: NotificationPropertyKeys.subtype)
        guard let object = aDecoder.decodeObject(forKey: NotificationPropertyKeys.object) as? String else {
            return nil
        }
        guard let param1 = aDecoder.decodeObject(forKey: NotificationPropertyKeys.param1) as? Friend else {
            return nil
        }
        guard let param2 = aDecoder.decodeObject(forKey: NotificationPropertyKeys.param2) as? todo else {
            return nil
        }
        guard let recipients = aDecoder.decodeObject(forKey: NotificationPropertyKeys.recipients) as? [String] else {
            return nil
        }
        let createdAt = aDecoder.decodeInteger(forKey: NotificationPropertyKeys.createdAt)
        let modifiedAt = aDecoder.decodeInteger(forKey: NotificationPropertyKeys.modifiedAt)

        self.init(_id: _id, type: type, subtype: subtype, object: object, param1: param1, param2: param2, recipients: recipients, createdAt: createdAt, modifiedAt: modifiedAt)
    }
}
