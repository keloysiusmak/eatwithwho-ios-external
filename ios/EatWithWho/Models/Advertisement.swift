//
//  Advertisement.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 17/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation
import os.log

struct AdvertisementPropertyKeys {
    static let _id = "advertisement-_id"
    static let name = "advertisement-name"
    static let desc = "advertisement-desc"
    static let imageUrl = "advertisement-imageUrl"
    static let targetUrl = "advertisement-targetUrl"
    static let views = "advertisement-views"
    static let clicks = "advertisement-clicks"
    static let modifiedAt = "advertisement-modifiedAt"
    static let createdAt = "advertisement-createdAt"
}

class Advertisement: NSObject, NSCoding, Decodable {
    let _id: String
    let name: String
    let desc: String
    let imageUrl: String
    let targetUrl: String
    let views: Int
    let clicks: Int
    let modifiedAt: Int
    let createdAt: Int

    init(_id: String, name: String, desc: String, imageUrl: String, targetUrl: String, views: Int, clicks: Int, createdAt: Int, modifiedAt: Int) {
        self._id = _id
        self.name = name
        self.desc = desc
        self.imageUrl = imageUrl
        self.targetUrl = targetUrl
        self.views = views
        self.clicks = clicks
        self.modifiedAt = modifiedAt
        self.createdAt = createdAt
    }

    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("advertisement")

    func encode(with aCoder: NSCoder) {
        aCoder.encode(_id, forKey: AdvertisementPropertyKeys._id)
        aCoder.encode(name, forKey: AdvertisementPropertyKeys.name)
        aCoder.encode(desc, forKey: AdvertisementPropertyKeys.desc)
        aCoder.encode(imageUrl, forKey: AdvertisementPropertyKeys.imageUrl)
        aCoder.encode(targetUrl, forKey: AdvertisementPropertyKeys.targetUrl)
        aCoder.encode(views, forKey: AdvertisementPropertyKeys.views)
        aCoder.encode(clicks, forKey: AdvertisementPropertyKeys.clicks)
        aCoder.encode(modifiedAt, forKey: AdvertisementPropertyKeys.modifiedAt)
        aCoder.encode(createdAt, forKey: AdvertisementPropertyKeys.createdAt)
    }

    required convenience init?(coder aDecoder: NSCoder) {

        guard let _id = aDecoder.decodeObject(forKey: AdvertisementPropertyKeys._id) as? String else {
            return nil
        }

        guard let name = aDecoder.decodeObject(forKey: AdvertisementPropertyKeys.name) as? String else {
            return nil
        }

        guard let desc = aDecoder.decodeObject(forKey: AdvertisementPropertyKeys.desc) as? String else {
            return nil
        }

        guard let imageUrl = aDecoder.decodeObject(forKey: AdvertisementPropertyKeys.imageUrl) as? String else {
            return nil
        }

        guard let targetUrl = aDecoder.decodeObject(forKey: AdvertisementPropertyKeys.targetUrl) as? String else {
            return nil
        }
        let views = aDecoder.decodeInteger(forKey: AdvertisementPropertyKeys.views)
        let clicks = aDecoder.decodeInteger(forKey: AdvertisementPropertyKeys.clicks)
        let createdAt = aDecoder.decodeInteger(forKey: AdvertisementPropertyKeys.createdAt)
        let modifiedAt = aDecoder.decodeInteger(forKey: AdvertisementPropertyKeys.modifiedAt)

        self.init(_id: _id, name: name, desc: desc, imageUrl: imageUrl, targetUrl: targetUrl, views: views, clicks: clicks, createdAt: createdAt, modifiedAt: modifiedAt)
    }
}
