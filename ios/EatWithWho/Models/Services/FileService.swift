//
//  FileService.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 22/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

class FileService {
    func uploadFile(eatwithId: String, fileName: String, fileSize: Int, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = fileApiConfig.scheme
        urlComponents.host = fileApiConfig.host
        urlComponents.port = fileApiConfig.port
        let stage = (fileApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/eatwith/" + eatwithId + "/file"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "POST"

        let param = ["fileName": fileName, "fileSize": fileSize] as [String: Any]
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }
    func deleteFile(fileId: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = fileApiConfig.scheme
        urlComponents.host = fileApiConfig.host
        urlComponents.port = fileApiConfig.port
        let stage = (fileApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/file/" + fileId

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "DELETE"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }
}
