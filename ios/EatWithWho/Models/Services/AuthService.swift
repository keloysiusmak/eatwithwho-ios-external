//
//  AuthService.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 16/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

class AuthService {
    func getApiToken(completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = authApiConfig.scheme
        urlComponents.host = authApiConfig.host
        urlComponents.port = authApiConfig.port
        let stage = (authApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/auth/token"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(appSecrets.apiKey, forHTTPHeaderField: "api-key")
        request.httpMethod = "GET"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }
    func checkApiToken(completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = authApiConfig.scheme
        urlComponents.host = authApiConfig.host
        urlComponents.port = authApiConfig.port
        let stage = (authApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/auth/api/check"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "apiToken"), forHTTPHeaderField: "api-token")
        request.httpMethod = "GET"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }
    func refreshAccessToken(completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = authApiConfig.scheme
        urlComponents.host = authApiConfig.host
        urlComponents.port = authApiConfig.port
        let stage = (authApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/auth/refresh"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "refreshToken") ?? "", forHTTPHeaderField: "refresh-token")
        request.httpMethod = "GET"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }
    func getFriendAccessToken(friendId: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = authApiConfig.scheme
        urlComponents.host = authApiConfig.host
        urlComponents.port = authApiConfig.port
        let stage = (authApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/auth/friend/" + friendId + "/token"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "apiToken"), forHTTPHeaderField: "api-token")
        request.httpMethod = "GET"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }
}
