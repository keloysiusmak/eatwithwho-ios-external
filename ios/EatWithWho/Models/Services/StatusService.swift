//
//  GetStatus.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 16/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

class StatusService {
    func getStatus(completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = statusApiConfig.scheme
        urlComponents.host = statusApiConfig.host
        urlComponents.port = statusApiConfig.port
        let stage = (statusApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/status/"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "apiToken"), forHTTPHeaderField: "api-token")
        request.httpMethod = "GET"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }

    func loadAdvertisement(advertisementId: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = statusApiConfig.scheme
        urlComponents.host = statusApiConfig.host
        urlComponents.port = statusApiConfig.port
        let stage = (statusApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/advertisement/" + advertisementId

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "apiToken"), forHTTPHeaderField: "api-token")
        request.httpMethod = "GET"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }

    func getAdvertisement(completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = statusApiConfig.scheme
        urlComponents.host = statusApiConfig.host
        urlComponents.port = statusApiConfig.port
        let stage = (statusApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/advertisement/"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "apiToken"), forHTTPHeaderField: "api-token")
        request.httpMethod = "GET"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }
}
