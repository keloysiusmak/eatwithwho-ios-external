//
//  AccountService.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 17/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

class AccountService {
    func loginAccount(username: String, password: String, token: String, notificationArn: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = accountApiConfig.scheme
        urlComponents.host = accountApiConfig.host
        urlComponents.port = accountApiConfig.port
        let stage = (accountApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/account/login"

        let param = ["username": username, "password": password, "token": token, "notificationArn": notificationArn]

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "apiToken"), forHTTPHeaderField: "api-token")
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }

    func checkAccount(username1: String, username2: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = accountApiConfig.scheme
        urlComponents.host = accountApiConfig.host
        urlComponents.port = accountApiConfig.port
        let stage = (accountApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/account/check"

        let param = ["username1": username1, "username2": username2]

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "apiToken"), forHTTPHeaderField: "api-token")
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }

    func registerAccount(username1: String, password1: String, firstName1: String, lastName1: String, gender1: String, email1: String, number1: String, username2: String, password2: String, firstName2: String, lastName2: String, gender2: String, email2: String, number2: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = accountApiConfig.scheme
        urlComponents.host = accountApiConfig.host
        urlComponents.port = accountApiConfig.port
        let stage = (accountApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/account/register"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        let param = ["friend1": ["username": username1, "password": password1, "firstName": firstName1, "lastName": lastName1, "gender": gender1, "email": email1, "number": number1], "friend2": ["username": username2, "password": password2, "firstName": firstName2, "lastName": lastName2, "gender": gender2, "email": email2, "number": number2]]

        var request = URLRequest(url: url)
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)
        request.setValue(UserDefaults.standard.string(forKey: "apiToken"), forHTTPHeaderField: "api-token")
        request.httpMethod = "POST"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }

    func logoutAccount(accountId: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = accountApiConfig.scheme
        urlComponents.host = accountApiConfig.host
        urlComponents.port = accountApiConfig.port
        let stage = (accountApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/account/" + accountId + "/logout"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "GET"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }

    func getAccount(accountId: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = accountApiConfig.scheme
        urlComponents.host = accountApiConfig.host
        urlComponents.port = accountApiConfig.port
        let stage = (accountApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/account/" + accountId

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "GET"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }

    func getAccountType(accountId: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = accountApiConfig.scheme
        urlComponents.host = accountApiConfig.host
        urlComponents.port = accountApiConfig.port
        let stage = (accountApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/account/" + accountId + "/type/"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "GET"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }

    func getInitializers(accountId: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = accountApiConfig.scheme
        urlComponents.host = accountApiConfig.host
        urlComponents.port = accountApiConfig.port
        let stage = (accountApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/account/" + accountId + "/initializers/"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "GET"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }

    func changePassword(accountId: String, oldPassword: String, newPassword: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = accountApiConfig.scheme
        urlComponents.host = accountApiConfig.host
        urlComponents.port = accountApiConfig.port
        let stage = (accountApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/account/" + accountId + "/password"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "PUT"

        let param = ["oldPassword": oldPassword, "newPassword": newPassword] as [String: Any]
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }
}
