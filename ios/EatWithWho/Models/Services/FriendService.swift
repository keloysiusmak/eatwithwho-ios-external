//
//  FriendnService.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 22/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

class FriendService {
    func getFriend(friendId: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = friendApiConfig.scheme
        urlComponents.host = friendApiConfig.host
        urlComponents.port = friendApiConfig.port
        let stage = (friendApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/friend/" + friendId

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "GET"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }
    func logoutFriend(friendId: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = friendApiConfig.scheme
        urlComponents.host = friendApiConfig.host
        urlComponents.port = friendApiConfig.port
        let stage = (friendApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/friend/" + friendId + "/logout"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "GET"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }
    func getFriendId(reference: String, token: String, notificationArn: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = friendApiConfig.scheme
        urlComponents.host = friendApiConfig.host
        urlComponents.port = friendApiConfig.port
        let stage = (friendApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/friend/reference/" + reference

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        let param = ["token": token, "notificationArn": notificationArn]

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "apiToken"), forHTTPHeaderField: "api-token")
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }

    func addFriend(userId: String, firstName: String, lastName: String, email: String, number: String, gender: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = friendApiConfig.scheme
        urlComponents.host = friendApiConfig.host
        urlComponents.port = friendApiConfig.port
        let stage = (friendApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/couple/" + userId + "/friend"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "POST"

        let param = ["firstName": firstName, "lastName": lastName, "gender": gender, "email": email, "number": number] as [String: Any]
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }

    func getAvailableFriends(userId: String, startTime: Date, endTime: Date, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = friendApiConfig.scheme
        urlComponents.host = friendApiConfig.host
        urlComponents.port = friendApiConfig.port
        let stage = (friendApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/couple/" + userId + "/available"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "POST"

        let param = ["startTime": Int(startTime.timeIntervalSince1970), "endTime": Int(endTime.timeIntervalSince1970)] as [String: Any]
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }

    func updateFriend(friendId: String, firstName: String, lastName: String, email: String, number: String, gender: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = friendApiConfig.scheme
        urlComponents.host = friendApiConfig.host
        urlComponents.port = friendApiConfig.port
        let stage = (friendApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/friend/" + friendId

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "PUT"

        let param = ["firstName": firstName, "lastName": lastName, "email": email, "number": number, "gender": gender] as [String: Any]
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }

    func deleteFriend(friendId: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = friendApiConfig.scheme
        urlComponents.host = friendApiConfig.host
        urlComponents.port = friendApiConfig.port
        let stage = (friendApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/friend/" + friendId

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "DELETE"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }
    func uploadPicture(friendId: String, imageType: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = friendApiConfig.scheme
        urlComponents.host = friendApiConfig.host
        urlComponents.port = friendApiConfig.port
        let stage = (friendApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/friend/" + friendId + "/picture"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "POST"

        let param = ["imageType": imageType]
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }

    func deletePicture(friendId: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = friendApiConfig.scheme
        urlComponents.host = friendApiConfig.host
        urlComponents.port = friendApiConfig.port
        let stage = (friendApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/friend/" + friendId + "/picture"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "DELETE"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }

    func getFriends(userId: String, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = friendApiConfig.scheme
        urlComponents.host = friendApiConfig.host
        urlComponents.port = friendApiConfig.port
        let stage = (friendApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/couple/" + userId + "/friends"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "GET"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }
}
