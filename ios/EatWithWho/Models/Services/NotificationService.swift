//
//  NotifcationService.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 22/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

class NotificationService {
    func getNotifications(friendId: String, lastUpdated: Int, completion: ((ResponseState<Response>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = notificationApiConfig.scheme
        urlComponents.host = notificationApiConfig.host
        urlComponents.port = notificationApiConfig.port
        let stage = (notificationApiConfig.stage == "prod") ? "/prod" : ""
        urlComponents.path = stage + "/friend/" + friendId + "/notifications"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.setValue(UserDefaults.standard.string(forKey: "accessToken"), forHTTPHeaderField: "access-token")
        request.httpMethod = "POST"

        let param = ["lastUpdated": String(lastUpdated)]
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let todo = session.datatodo(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()

                    do {
                        let response = try decoder.decode(Response.self, from: jsonData)
                        completion?(.success(response))
                    } catch {
                        completion?(.failure(error))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                }
            }
        }

        todo.resume()
    }
}
