//
//  File.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 11/10/16.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import UIKit
import os.log

struct FilePropertyKeys {
    static let _id = "file-_id"
    static let fileSize = "file-fileSize"
    static let fileName = "file-fileName"
    static let fileStorageName = "file-fileStorageName"
    static let modifiedAt = "file-modifiedAt"
    static let createdAt = "file-createdAt"
}

class File: NSObject, NSCoding, Decodable {
    let _id: String
    let fileSize: Int
    let fileName: String
    let fileStorageName: String
    let modifiedAt: Int
    let createdAt: Int

    init(_id: String, fileSize: Int, fileName: String, fileStorageName: String, createdAt: Int, modifiedAt: Int) {
        self._id = _id
        self.fileSize = fileSize
        self.fileName = fileName
        self.fileStorageName = fileStorageName
        self.modifiedAt = modifiedAt
        self.createdAt = createdAt
    }

    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("file")

    func encode(with aCoder: NSCoder) {
        aCoder.encode(_id, forKey: FilePropertyKeys._id)
        aCoder.encode(fileSize, forKey: FilePropertyKeys.fileSize)
        aCoder.encode(fileName, forKey: FilePropertyKeys.fileName)
        aCoder.encode(fileStorageName, forKey: FilePropertyKeys.fileStorageName)
        aCoder.encode(modifiedAt, forKey: FilePropertyKeys.modifiedAt)
        aCoder.encode(createdAt, forKey: FilePropertyKeys.createdAt)
    }

    required convenience init?(coder aDecoder: NSCoder) {

        guard let _id = aDecoder.decodeObject(forKey: FilePropertyKeys._id) as? String else {
            return nil
        }
        let fileSize = aDecoder.decodeInteger(forKey: FilePropertyKeys.fileSize)
        guard let fileName = aDecoder.decodeObject(forKey: FilePropertyKeys.fileName) as? String else {
            return nil
        }
        guard let fileStorageName = aDecoder.decodeObject(forKey: FilePropertyKeys.fileStorageName) as? String else {
            return nil
        }
        let createdAt = aDecoder.decodeInteger(forKey: FilePropertyKeys.createdAt)
        let modifiedAt = aDecoder.decodeInteger(forKey: FilePropertyKeys.modifiedAt)

        self.init(_id: _id, fileSize: fileSize, fileName: fileName, fileStorageName: fileStorageName, createdAt: createdAt, modifiedAt: modifiedAt)
    }
}
