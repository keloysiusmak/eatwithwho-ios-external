//
//  Response.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 16/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

struct Response: Decodable {
    let statusCode: Int
    let error: String
    let message: String
    let data: ResponseData?
}
struct ResponseError {
    let response = Response(statusCode: 503, error: "Offline", message: "Offline", data: nil)
}

struct Timestamps: Decodable {
    let account: Int?
    let budget: Int?
    let schedule: Int?
    let couple: Int?
    let friend: Int?
    let friends: Int?
    let guests: Int?
    let todos: Int?
}

struct isEmpty: Decodable {
    let schedule: Bool
    let budget: Bool
}

struct Defaults: Decodable {
    let currency: String
}

struct ResponseData: Decodable {
    let apiToken: String?
    let schedule: Schedule?
    let accessToken: String?
    let refreshToken: String?
    let account: Account?
    let couple: Couple?
    let budget: Budget?
    let record: Record?
    let records: [Record]?
    let eatwith: eatwith?
    let eatwiths: [eatwith]?
    let friend: Friend?
    let friends: [Friend]?
    let todos: [todo]?
    let friendId: String?
    let timestamps: Timestamps?
    let lastUpdated: Int?
    let coupleFriends: [Friend]?
    let filesSize: Int?
    let friendCount: Int?
    let file: File?
    let isEmpty: isEmpty?
    let defaults: Defaults?
    let payment: Payment?
    let status: String?
    let ios_versions: [String]?
    let validUsernames: Bool?
    let advertisement: Advertisement?
}
