//
//  ResponseState.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 16/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

enum ResponseState<Value> {
    case success(Value)
    case failure(Error)
}
