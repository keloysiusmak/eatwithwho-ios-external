//
//  Schedule.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 16/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

struct SchedulePropertyKeys {
    static let _id = "schedule-_id"
    static let name = "schedule-name"
    static let eatwiths = "schedule-eatwiths"
    static let date = "schedule-date"
    static let modifiedAt = "schedule-modifiedAt"
    static let createdAt = "schedule-createdAt"
}

class Schedule: NSObject, NSCoding, Decodable {
    let _id: String
    let name: String
    let eatwiths: [eatwith]
    let date: Int
    let modifiedAt: Int
    let createdAt: Int

    init(_id: String, name: String, eatwiths: [eatwith], date: Int, createdAt: Int, modifiedAt: Int) {
        self._id = _id
        self.name = name
        self.eatwiths = eatwiths
        self.date = date
        self.modifiedAt = modifiedAt
        self.createdAt = createdAt
    }

    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("schedule")

    func encode(with aCoder: NSCoder) {
        aCoder.encode(_id, forKey: SchedulePropertyKeys._id)
        aCoder.encode(name, forKey: SchedulePropertyKeys.name)
        aCoder.encode(eatwiths, forKey: SchedulePropertyKeys.eatwiths)
        aCoder.encode(date, forKey: SchedulePropertyKeys.date)
        aCoder.encode(modifiedAt, forKey: SchedulePropertyKeys.modifiedAt)
        aCoder.encode(createdAt, forKey: SchedulePropertyKeys.createdAt)
    }

    required convenience init?(coder aDecoder: NSCoder) {

        guard let _id = aDecoder.decodeObject(forKey: SchedulePropertyKeys._id) as? String else {
            return nil
        }
        guard let name = aDecoder.decodeObject(forKey: SchedulePropertyKeys.name) as? String else {
            return nil
        }
        guard let eatwiths = aDecoder.decodeObject(forKey: SchedulePropertyKeys.eatwiths) as? [eatwith] else {
            return nil
        }
        let date = aDecoder.decodeInteger(forKey: SchedulePropertyKeys.date)
        let createdAt = aDecoder.decodeInteger(forKey: SchedulePropertyKeys.createdAt)
        let modifiedAt = aDecoder.decodeInteger(forKey: SchedulePropertyKeys.modifiedAt)

        self.init(_id: _id, name: name, eatwiths: eatwiths, date: date, createdAt: createdAt, modifiedAt: modifiedAt)
    }
}
