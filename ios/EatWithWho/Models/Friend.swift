//
//  Friend.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 17/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import Foundation

struct FriendPropertyKeys {
    static let _id = "friend-_id"
    static let isDeleted = "friend-isDeleted"
    static let firstName = "friend-firstName"
    static let lastName = "friend-lastName"
    static let email = "friend-email"
    static let reference = "friend-reference"
    static let number = "friend-number"
    static let gender = "friend-gender"
    static let imageType = "friend-imageType"
    static let note = "friend-note"
    static let status = "friend-status"
    static let additionalGuests = "friend-additionalGuests"
    static let deletedAt = "friend-deletedAt"
    static let modifiedAt = "friend-modifiedAt"
    static let createdAt = "friend-createdAt"
}

class Friend: NSObject, NSCoding, Decodable {
    let _id: String
    let isDeleted: Bool
    let firstName: String
    let lastName: String
    let reference: String
    let email: String
    let number: String
    let gender: String
    let imageType: String
    let note: String?
    let status: String
    let additionalGuests: Int
    let deletedAt: Int
    let modifiedAt: Int
    let createdAt: Int

    init(_id: String, isDeleted: Bool, firstName: String, lastName: String, reference: String, email: String, number: String, gender: String, imageType: String, note: String?, status: String, additionalGuests: Int, deletedAt: Int, createdAt: Int, modifiedAt: Int) {
        self._id = _id
        self.isDeleted = isDeleted
        self.firstName = firstName
        self.lastName = lastName
        self.reference = reference
        self.email = email
        self.number = number
        self.gender = gender
        self.imageType = imageType
        self.note = note
        self.status = status
        self.additionalGuests = additionalGuests
        self.deletedAt = deletedAt
        self.modifiedAt = modifiedAt
        self.createdAt = createdAt
    }

    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("friend")

    func encode(with aCoder: NSCoder) {
        aCoder.encode(_id, forKey: FriendPropertyKeys._id)
        aCoder.encode(isDeleted, forKey: FriendPropertyKeys.isDeleted)
        aCoder.encode(firstName, forKey: FriendPropertyKeys.firstName)
        aCoder.encode(lastName, forKey: FriendPropertyKeys.lastName)
        aCoder.encode(reference, forKey: FriendPropertyKeys.reference)
        aCoder.encode(email, forKey: FriendPropertyKeys.email)
        aCoder.encode(number, forKey: FriendPropertyKeys.number)
        aCoder.encode(gender, forKey: FriendPropertyKeys.gender)
        aCoder.encode(imageType, forKey: FriendPropertyKeys.imageType)
        aCoder.encode(note, forKey: FriendPropertyKeys.note)
        aCoder.encode(status, forKey: FriendPropertyKeys.status)
        aCoder.encode(additionalGuests, forKey: FriendPropertyKeys.additionalGuests)
        aCoder.encode(deletedAt, forKey: FriendPropertyKeys.deletedAt)
        aCoder.encode(modifiedAt, forKey: FriendPropertyKeys.modifiedAt)
        aCoder.encode(createdAt, forKey: FriendPropertyKeys.createdAt)
    }

    required convenience init?(coder aDecoder: NSCoder) {

        guard let _id = aDecoder.decodeObject(forKey: FriendPropertyKeys._id) as? String else {
            return nil
        }
        let isDeleted = aDecoder.decodeBool(forKey: FriendPropertyKeys.isDeleted)
        guard let firstName = aDecoder.decodeObject(forKey: FriendPropertyKeys.firstName) as? String else {
            return nil
        }
        guard let lastName = aDecoder.decodeObject(forKey: FriendPropertyKeys.lastName) as? String else {
            return nil
        }
        guard let reference = aDecoder.decodeObject(forKey: FriendPropertyKeys.reference) as? String else {
            return nil
        }
        guard let email = aDecoder.decodeObject(forKey: FriendPropertyKeys.email) as? String else {
            return nil
        }
        guard let number = aDecoder.decodeObject(forKey: FriendPropertyKeys.number) as? String else {
            return nil
        }
        guard let gender = aDecoder.decodeObject(forKey: FriendPropertyKeys.gender) as? String else {
            return nil
        }
        guard let imageType = aDecoder.decodeObject(forKey: FriendPropertyKeys.imageType) as? String else {
            return nil
        }
        guard let note = aDecoder.decodeObject(forKey: FriendPropertyKeys.note) as? String else {
            return nil
        }
        guard let status = aDecoder.decodeObject(forKey: FriendPropertyKeys.status) as? String else {
            return nil
        }
        let additionalGuests = aDecoder.decodeInteger(forKey: FriendPropertyKeys.additionalGuests)
        let deletedAt = aDecoder.decodeInteger(forKey: FriendPropertyKeys.deletedAt)
        let createdAt = aDecoder.decodeInteger(forKey: FriendPropertyKeys.createdAt)
        let modifiedAt = aDecoder.decodeInteger(forKey: FriendPropertyKeys.modifiedAt)

        self.init(_id: _id, isDeleted: isDeleted, firstName: firstName, lastName: lastName, reference: reference, email: email, number: number, gender: gender, imageType: imageType, note: note, status: status, additionalGuests: additionalGuests, deletedAt: deletedAt, createdAt: createdAt, modifiedAt: modifiedAt)
    }
}
