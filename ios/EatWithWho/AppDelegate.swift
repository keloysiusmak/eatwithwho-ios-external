//
//  AppDelegate.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 02/03/20.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import AWSSNS
import AWSS3
import UIKit
import CardParts
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    private let authService: AuthService = AuthService()
    private let accountService: AccountService = AccountService()
    private let statusService: StatusService = StatusService()
    let SNSPlatformApplicationArn = "arn:aws:sns:ap-southeast-1:839282166387:app/APNS_SANDBOX/EatWithWho-iOS"
    var window: UIWindow?

    func registerForPushNotifications(application: UIApplication) {
        /// The notifications settings
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, _) in
                if (granted) {
                    UIApplication.shared.registerForRemoteNotifications()
                } else {
                    //Do stuff if unsuccessful…
                }
            })
        } else {
            let settings = UIUserNotificationSettings(types: [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
    }

    // Called when a notification is delivered to a foreground app.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("User Info = ", notification.request.content.userInfo)
        completionHandler([.alert, .badge, .sound])
    }
    // Called to let your app know which action was selected by the user for a given notification.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("User Info = ", response.notification.request.content.userInfo)
        completionHandler()
    }

    func checkApiToken(callback: @escaping () -> Void) {
        authService.checkApiToken { (result) in
            switch result {
            case .success(let response):
                if (response.statusCode != 200) {
                    Helper.deleteUserDefaults()

                    let appDelegateTemp = UIApplication.shared.delegate as? AppDelegate
                    appDelegateTemp?.window?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
                    UserDefaults.standard.removeObject(forKey: "apiToken")
                }

            case .failure:
                Helper.deleteUserDefaults()

                let appDelegateTemp = UIApplication.shared.delegate as? AppDelegate
                appDelegateTemp?.window?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()

                UserDefaults.standard.removeObject(forKey: "apiToken")
                callback()
            }
        }
    }

    func refreshToken(callback: @escaping () -> Void) {
        if (UserDefaults.standard.object(forKey: "loggedInAs") != nil) {
            authService.refreshAccessToken { (result) in
                switch result {
                case .success(let response):
                    if (response.statusCode == 200) {
                        UserDefaults.standard.set(NSDate().timeIntervalSince1970, forKey: "accessTokenExpiry")
                        UserDefaults.standard.set(response.data?.accessToken, forKey: "accessToken")
                    } else {
                        Helper.deleteUserDefaults()
                        let appDelegateTemp = UIApplication.shared.delegate as? AppDelegate
                        appDelegateTemp?.window?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
                    }
                    callback()
                case .failure(let error):
                    fatalError("error: \(error.localizedDescription)")
                }
            }
        }
    }

    @objc func checkRefreshToken() {

        if (UserDefaults.standard.string(forKey: "loggedInAs") == "couple") {
            let currentTimestamp = NSDate().timeIntervalSince1970
            let accessTokenExpiry = UserDefaults.standard.integer(forKey: "accessTokenExpiry")

            //Refresh Every 30 Minutes
            if (accessTokenExpiry == 0 || Int(currentTimestamp) > accessTokenExpiry + 1800) {
                refreshToken {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 300) {
                        self.checkRefreshToken()
                    }
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 300) {
                    self.checkRefreshToken()
                }
            }
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 300) {
                self.checkRefreshToken()
            }
        }

    }
    @objc func checkStatus() {
        statusService.getStatus { (result) in
            switch result {
            case .success(let response):
                if(response.data!.status == "offline") {
                    let alert = UIAlertController(title: "We are currently undergoing server maintainence.", message: "We're working hard to get things back up again.", preferredStyle: .alert)

                    self.window?.rootViewController?.present(alert, animated: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                        alert.dismiss(animated: true, completion: nil)
                    }
                    Helper.deleteUserDefaults()

                    let appDelegateTemp = UIApplication.shared.delegate as? AppDelegate
                    appDelegateTemp?.window?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
                } else {
                    let buildNumber = Bundle.main.infoDictionary!["CFBundleVersion"] as? String
                    if (!response.data!.ios_versions!.contains(buildNumber!) || buildNumber != response.data!.ios_versions![response.data!.ios_versions!.count - 1]) {
                        let alert = UIAlertController(title: "A new app version is available.", message: "Please update your app.", preferredStyle: .alert)

                        self.window?.rootViewController?.present(alert, animated: true)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                            alert.dismiss(animated: true, completion: nil)
                        }
                        if (!response.data!.ios_versions!.contains(buildNumber!) && UserDefaults.standard.object(forKey: "loggedInAs") != nil) {
                            Helper.deleteUserDefaults()

                            let appDelegateTemp = UIApplication.shared.delegate as? AppDelegate
                            appDelegateTemp?.window?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
                        }
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1800) {
                        self.checkStatus()
                    }
                }
            case .failure:
                let alert = UIAlertController(title: "It looks like something's wrong with our servers.", message: "Rest assured we're working hard to get it back up.", preferredStyle: .alert)

                self.window?.rootViewController?.present(alert, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    alert.dismiss(animated: true, completion: nil)
                }
                Helper.deleteUserDefaults()

                let appDelegateTemp = UIApplication.shared.delegate as? AppDelegate
                appDelegateTemp?.window?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
            }
        }
    }

    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        AWSS3TransferUtility.interceptApplication(application,
                                                  handleEventsForBackgroundURLSession: identifier,
                                                  completionHandler: completionHandler)
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        checkRefreshToken()
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType: .APSoutheast1,
                                                                identityPoolId: "ap-southeast-1:58b44d83-285b-4828-b0ca-901baeb2a855")

        let configuration = AWSServiceConfiguration(region: .APSoutheast1, credentialsProvider: credentialsProvider)

        AWSServiceManager.default().defaultServiceConfiguration = configuration

        let tuConf = AWSS3TransferUtilityConfiguration()

        AWSS3TransferUtility.register(with: configuration!, transferUtilityConfiguration: tuConf, forKey: "eatwithwhoS3Upload")
        AWSS3TransferUtility.register(with: configuration!, transferUtilityConfiguration: tuConf, forKey: "eatwithwhoPicturesS3Upload")

        let theme = EatWithWhoTheme()
        theme.cardsViewContentInsetTop = 12.0
        theme.apply()

        UserDefaults.standard.set(0, forKey: "notificationsLastUpdated")
        var apiToken = UserDefaults.standard.string(forKey: "apiToken")
        let apiTokenLastSigned = UserDefaults.standard.double(forKey: "apiTokenLastSigned")
        let timestamp = NSDate().timeIntervalSince1970

        checkApiToken {
            apiToken = UserDefaults.standard.string(forKey: "apiToken")
        }

        if (apiToken == nil || apiTokenLastSigned == 0 || (apiTokenLastSigned + 3600*24*180) < timestamp) {
            authService.getApiToken { (result) in
                switch result {
                case .success(let response):
                    UserDefaults.standard.set(response.data?.apiToken, forKey: "apiToken")
                    UserDefaults.standard.set(timestamp, forKey: "apiTokenLastSigned")
                case .failure(let error):
                    fatalError("error: \(error.localizedDescription)")
                }
            }
        }

        let loggedInAs = UserDefaults.standard.string(forKey: "loggedInAs")
        let tokenLastSigned = UserDefaults.standard.double(forKey: "tokenLastSigned")

        if (loggedInAs == "friend") {
            if (tokenLastSigned == 0 || (tokenLastSigned + 3600) < timestamp) {
                Helper.deleteUserDefaults()
            } else {
                let rootController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Friend")
                self.window?.rootViewController = rootController
            }
        } else {
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            let refreshToken = UserDefaults.standard.string(forKey: "refreshToken")
            let scheduleId = UserDefaults.standard.string(forKey: "scheduleId")
            let budgetId = UserDefaults.standard.string(forKey: "budgetId")
            let userId = UserDefaults.standard.string(forKey: "userId")
            let friendId = UserDefaults.standard.string(forKey: "friendId")
            let accountId = UserDefaults.standard.string(forKey: "accountId")
            let accountType = UserDefaults.standard.string(forKey: "accountType")

            if (tokenLastSigned == 0 || (tokenLastSigned + 3600) < timestamp || accessToken == nil || refreshToken == nil || scheduleId == nil || budgetId == nil || userId == nil || accountId == nil || friendId == nil || accountType == nil) {
                Helper.deleteUserDefaults()
            } else {
                accountService.getInitializers(accountId: UserDefaults.standard.string(forKey: "accountId")!) { (result) in
                    switch result {
                    case .success(let response):
                        if(response.statusCode == 200) {
                            UserDefaults.standard.set(response.data?.timestamps?.account, forKey: "lastUpdatedAccount")
                            UserDefaults.standard.set(response.data?.timestamps?.budget, forKey: "lastUpdatedBudget")
                            UserDefaults.standard.set(response.data?.timestamps?.couple, forKey: "lastUpdatedCouple")
                            UserDefaults.standard.set(response.data?.timestamps?.friend, forKey: "lastUpdatedFriend")
                            UserDefaults.standard.set(response.data?.timestamps?.schedule, forKey: "lastUpdatedSchedule")
                            UserDefaults.standard.set(response.data?.timestamps?.friends, forKey: "lastUpdatedFriends")
                            UserDefaults.standard.set(response.data?.timestamps?.guests, forKey: "lastUpdatedGuests")
                        }
                    default:
                        Helper.deleteUserDefaults()
                    }

                }

                let rootController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Main")
                self.window?.rootViewController = rootController
            }
        }

        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().tintColor = UIColor.black
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont(name: "TitilliumWeb-Bold", size: 16.0)!
        ]
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().isTranslucent = false

        UITabBar.appearance().layer.borderWidth = 0.0
        UITabBar.appearance().clipsToBounds = true

        registerForPushNotifications(application: application)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing todos, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        refreshToken { }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        refreshToken { }
        checkStatus()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        /// Attach the device token to the user defaults
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        UserDefaults.standard.set(token, forKey: "deviceTokenForSNS")
        let sns = AWSSNS.default()
        let request = AWSSNSCreatePlatformEndpointInput()
        request?.token = token
        request?.platformApplicationArn = SNSPlatformApplicationArn
        sns.createPlatformEndpoint(request!).continueWith(executor: AWSExecutor.mainThread(), block: { (todo: AWStodo!) -> AnyObject? in
            if todo.error != nil {
                print("Error: \(String(describing: todo.error))")
            } else {
                let createEndpointResponse = todo.result! as AWSSNSCreateEndpointResponse
                if let endpointArnForSNS = createEndpointResponse.endpointArn {
                    UserDefaults.standard.set(endpointArnForSNS, forKey: "endpointArnForSNS")
                }
            }
            return nil
        })
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error.localizedDescription)
    }

}
