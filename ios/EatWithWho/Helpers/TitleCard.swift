//
//  TitleCardController.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 11/15/16.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import Bond

class TitleCard: CardPartsViewController, TransparentCardTrait, NoTopBottomMarginsCardTrait {

    let titlePart = CardPartTitleView(type: .titleOnly)
    let titleCardViewModel = TitleCardViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        titlePart.margins.top = titleCardViewModel.topMargin.value
        titlePart.margins.bottom = 0.0
        titlePart.titleFont = titlePart.titleFont.withSize(32.0)
        titlePart.title = titleCardViewModel.titleText.value

        setupCardParts([titlePart])
    }

    func disableTopMargin() {
        titleCardViewModel.topMargin.accept(12.0)
    }

    func setTitle(title: String) {
        titleCardViewModel.titleText.accept(title)
    }

}

class TitleCardViewModel {

    var titleText = BehaviorRelay<String>(value: "")
    var topMargin = BehaviorRelay<CGFloat>(value: 36.0)

}
