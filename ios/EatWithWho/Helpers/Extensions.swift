//
//  Extensions.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 29/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Foundation
import CardParts

extension UIImage {
    func resizeWithWidth(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}
extension CardPartImageView {
    func setImage(friendId: String, imageType: String, modifiedAt: Int, withPlaceholder placeholder: UIImage? = nil) {
        self.imageName = "default"
        if (imageType != "none") {
            if let imageFromCache = Helper.imageCache.object(forKey: friendId + String(modifiedAt) as AnyObject) as? UIImage {
                self.image = imageFromCache
            } else {
                let url = URL(string: "https://s3-ap-southeast-1.amazonaws.com/eatwithwho-friend-pictures-" + appSecrets.env + "/" + friendId + "." + imageType)!

                URLSession.shared.datatodo(with: url) { (data, _, _) in
                    if let data = data {
                        let image = UIImage(data: data)
                        if (image != nil) {
                            DispatchQueue.main.async {
                                self.image = image
                                Helper.imageCache.setObject(image!, forKey: friendId + String(modifiedAt) as AnyObject)
                            }
                        }
                    }
                }.resume()
            }
        }
    }
    func load(urlString: String) {
        self.imageName = "placeholder"
        if let imageFromCache = Helper.imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
        } else {
            let url = URL(string: urlString)!

            URLSession.shared.datatodo(with: url) { (data, _, _) in
                if let data = data {
                    let image = UIImage(data: data)
                    if (image != nil) {
                        DispatchQueue.main.async {
                            self.image = image
                            Helper.imageCache.setObject(image!, forKey: urlString as AnyObject)
                        }
                    }
                }
            }.resume()
        }
    }
}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer =     UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIColor {
    static let aqua = UIColor(red: 0.1882, green: 0.8275, blue: 0.7647, alpha: 1.0)
    static let pastelGreen = UIColor(red: 52/255, green: 173/255, blue: 62/255, alpha: 1.0)
    static let pastelRed = UIColor(red: 188/255, green: 56/255, blue: 56/255, alpha: 1.0)
    static let pastelYellow = UIColor(red: 243/255, green: 202/255, blue: 54/255, alpha: 1.0)
    static let pastelOrange = UIColor(red: 211/255, green: 133/255, blue: 84/255, alpha: 1.0)
    static let lighterGray = UIColor(white: 0.8, alpha: 1)
    static let lightestGray = UIColor(red: 221/255, green: 221/255, blue: 221/255, alpha: 1.0)
    static let pastel = UIColor(red: 0.967, green: 0.967, blue: 0.967, alpha: 1)
    static let pastelLightBlue = UIColor(red: 0.949, green: 0.973, blue: 0.988, alpha: 1)
}

public extension UIView {
    func pin(to view: CardPartStackView) {
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: view.leadingAnchor),
            trailingAnchor.constraint(equalTo: view.trailingAnchor),
            topAnchor.constraint(equalTo: view.topAnchor),
            bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
