//
//  SpacerCard.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 11/15/16.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import CardParts
import RxSwift
import RxCocoa
import Bond

class SpacerCard: CardPartsViewController, TransparentCardTrait, NoTopBottomMarginsCardTrait {

    var space: CGFloat = 12.0

    override func viewDidLoad() {
        super.viewDidLoad()

        setupCardParts([CardPartSpacerView(height: space)])
    }

    func setSpace(space: CGFloat) {
        self.space = space
    }

}
