//
//  Helpers.swift
//  EatWithWho
//
//  Created by Keloysius Mak on 29/10/18.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import UIKit
import CardParts

struct Helper {
    static let theme = EatWithWhoTheme()
    static let loader = eatwithData(type: NVeatwithIndicatorType.ballScaleMultiple)

    static func getCurrencyPrefix(_ currency: String, _ value: Double) -> String {
        let val = String(format: "%.2f", value)
        switch currency {
        case "SGD":
            return "S$" + val
        case "SEK":
            return val + "SEK"
        case "USD":
            return "US$" + val
        case "MYR":
            return "RM" + val
        case "THB":
            return "฿" + val
        case "IDR":
            return "Rp" + val
        default:
            return "$" + val
        }
    }
    static func formatStatus(_ status: String) -> String {
        switch status {
        case "pending":
            return "Pending"
        case "notattending":
            return "Not Attending"
        case "attending":
            return "Attending"
        default:
            return "Pending"
        }
    }
    static func formattodoStatus(_ status: String) -> String {
        switch status {
        case "notstarted":
            return "Not Started"
        case "completed":
            return "Completed"
        case "inprogress":
            return "In Progress"
        default:
            return "In Progress"
        }
    }
    static func getStatusImage(_ status: String) -> String {
        switch status {
        case "pending":
            return "pending"
        case "notattending":
            return "cross"
        case "attending":
            return "check"
        default:
            return "pending"
        }
    }
    static func gettodoStatusImage(_ status: String) -> String {
        switch status {
        case "inprogress":
            return "pending"
        case "notstarted":
            return "cross"
        case "completed":
            return "check"
        default:
            return "pending"
        }
    }
    static func getStatusColor(_ status: String) -> UIColor {
        switch status {
        case "pending":
            return UIColor.lightGray
        case "notattending":
            return UIColor.pastelRed
        case "attending":
            return UIColor.pastelGreen
        default:
            return UIColor.lightGray
        }
    }
    static func gettodoStatusColor(_ status: String) -> UIColor {
        switch status {
        case "inprogress":
            return UIColor.gray
        case "notstarted":
            return UIColor.pastelRed
        case "completed":
            return UIColor.pastelGreen
        default:
            return UIColor.gray
        }
    }
    static func getCategory(_ category: String) -> String {
        switch category {
        case "transport":
            return "Transport"
        case "photography":
            return "Photography"
        case "videography":
            return "Videography"
        case "banquet":
            return "Banquet"
        case "flowers":
            return "Flowers"
        case "clothing":
            return "Clothing"
        case "makeup":
            return "Make-Up"
        case "gifts":
            return "Gifts"
        case "decoration":
            return "Decoration"
        default:
            return "Others"
        }
    }
    static func geteatwithCategory(_ type: String) -> String {
        switch type {
        case "teaceremony":
            return "Tea Ceremony"
        case "travel":
            return "Travel"
        case "makeup":
            return "Make-Up"
        case "banquet":
            return "Banquet"
        case "rest":
            return "Rest"
        case "photoshoot":
            return "Photoshoot"
        case "preparation":
            return "Preparation"
        default:
            return "Others"
        }
    }

    static func isPlural(_ num: Int) -> String {
        return (num != 1) ? "s" : ""
    }

    static func formatCoupleSubscription(_ subscription: String) -> String {
        switch subscription {
        case "free":
            return "Free"
        default:
            return "Free"
        }
    }

    static func formatCoupleSubscriptionWriteup(_ subscription: String) -> String {
        switch subscription {
        case "free":
            return "Our free tier will always remain free. Harness all the functionality of what EatWithWho has to offer, and only upgrade when your eatwith demands it."
        default:
            return "Our free tier will always remain free. Harness all the functionality of what EatWithWho has to offer, and only upgrade when your eatwith demands it."
        }
    }

    struct SubscriptionDetails {
        let maxFilesSize: Int
        let maxFriends: Int
    }

    static func getCoupleSubscription(_ subscription: String) -> SubscriptionDetails {
        switch subscription {
        case "free":
            return SubscriptionDetails(maxFilesSize: 10 * 1000 * 1000, maxFriends: 20)
        default:
            return SubscriptionDetails(maxFilesSize: 10 * 1000 * 1000, maxFriends: 20)
        }
    }

    static func expenditureOrIncome(boolean: Bool) -> UIColor {
        return boolean ? UIColor.pastelRed : UIColor.pastelGreen
    }

    static func getColor(record: Record) -> UIColor {
        if (record.type == "expenditure") {
            return UIColor.pastelRed
        } else {
            return UIColor.pastelGreen
        }
    }

    static func getPlusMinus(record: Record) -> String {
        if (record.type == "expenditure") {
            return "-"
        } else {
            return "+"
        }
    }

    static func formatDate(text: String, format: String) -> String {
        let date = Date.init(timeIntervalSince1970: Double(Int(text) ?? 0))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date as Date)
    }

    static func getDate(text: String) -> Date {
        let date = Date.init(timeIntervalSince1970: Double(Int(text) ?? 0))
        return date
    }

    static let imageCache = NSCache<AnyObject, AnyObject> ()

    static func formatTime(text: String) -> String {
        let date = Date.init(timeIntervalSince1970: Double(Int(text) ?? 0))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mma"
        return dateFormatter.string(from: date as Date)
    }

    static func timeDifference(time1: Int, time2: Int, allowedUnits: NSCalendar.Unit) -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = allowedUnits
        formatter.unitsStyle = .abbreviated
        formatter.maximumUnitCount = 1

        return formatter.string(from: Double(abs(time1 - time2)))!
    }

    static func timeDifferenceFull(time1: Int, time2: Int, allowedUnits: NSCalendar.Unit) -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = allowedUnits
        formatter.unitsStyle = .full
        formatter.maximumUnitCount = 1

        return formatter.string(from: Double(abs(time1 - time2)))!
    }
    static func deleteUserDefaults() {
        UserDefaults.standard.removeObject(forKey: "loggedInAs")
        UserDefaults.standard.removeObject(forKey: "accessToken")
        UserDefaults.standard.removeObject(forKey: "tokenLastSigned")
        UserDefaults.standard.removeObject(forKey: "refreshToken")
        UserDefaults.standard.removeObject(forKey: "userId")
        UserDefaults.standard.removeObject(forKey: "coupleFriendCount")
        UserDefaults.standard.removeObject(forKey: "coupleFilesSize")
        UserDefaults.standard.removeObject(forKey: "scheduleId")
        UserDefaults.standard.removeObject(forKey: "friendId")
        UserDefaults.standard.removeObject(forKey: "budgetId")
        UserDefaults.standard.removeObject(forKey: "accountId")
        UserDefaults.standard.removeObject(forKey: "currency")

        UserDefaults.standard.removeObject(forKey: "lastSyncBudget")
        UserDefaults.standard.removeObject(forKey: "lastSyncFriend")
        UserDefaults.standard.removeObject(forKey: "lastSyncCouple")
        UserDefaults.standard.removeObject(forKey: "lastSyncNotification")
        UserDefaults.standard.removeObject(forKey: "lastSyncSchedule")
        UserDefaults.standard.removeObject(forKey: "lastSyncAccount")
        UserDefaults.standard.removeObject(forKey: "lastSyncFriends")
        UserDefaults.standard.removeObject(forKey: "lastSynctodos")

        UserDefaults.standard.removeObject(forKey: "lastUpdatedAccount")
        UserDefaults.standard.removeObject(forKey: "lastUpdatedBudget")
        UserDefaults.standard.removeObject(forKey: "lastUpdatedCouple")
        UserDefaults.standard.removeObject(forKey: "lastUpdatedFriend")
        UserDefaults.standard.removeObject(forKey: "lastUpdatedNotification")
        UserDefaults.standard.removeObject(forKey: "lastUpdatedSchedule")
        UserDefaults.standard.removeObject(forKey: "lastUpdatedFriends")
        UserDefaults.standard.removeObject(forKey: "lastUpdatedtodos")

        UserDefaults.standard.removeObject(forKey: "isEmptyBudget")
        UserDefaults.standard.removeObject(forKey: "isEmptySchedule")
        UserDefaults.standard.removeObject(forKey: "deviceTokenForSNS")
        UserDefaults.standard.removeObject(forKey: "endpointArnForSNS")

        UserDefaults.standard.removeObject(forKey: "friendFirstName")
        UserDefaults.standard.removeObject(forKey: "friendLastName")

        UserDefaults.standard.removeObject(forKey: "accessTokenExpiry")
        UserDefaults.standard.removeObject(forKey: "accountType")
        UserDefaults.standard.removeObject(forKey: "friendAccessToken")
        UserDefaults.standard.removeObject(forKey: "friendImageType")
        UserDefaults.standard.removeObject(forKey: "friendModifiedAt")
        UserDefaults.standard.removeObject(forKey: "friendFirstName")
        UserDefaults.standard.removeObject(forKey: "friendLastName")
    }

    static func isValidUsername(_ username: String) -> Bool {
        return username.count >= 6  && username.count <= 30
    }

    static func isValidNumber(_ number: String) -> Bool {
        return number.count >= 6  && number.count <= 30
    }

    static func isValidName(_ testStr: String) -> Bool {
        let decimalCharacters = CharacterSet.decimalDigits
        let decimalRange = testStr.rangeOfCharacter(from: decimalCharacters)
        return !(decimalRange != nil) && (testStr.count >= 2) && (testStr.count <= 60)
    }

    static func isValidPassword(_ testStr: String) -> Bool {
        return (testStr.count >= 6) && (testStr.count <= 30)
    }

    static func isValidDesc(_ testStr: String) -> Bool {
        return (testStr.count <= 1400)
    }
    static func isValidShortDesc(_ testStr: String) -> Bool {
        return (testStr.count <= 280)
    }
    static func isValidEmail(_ testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr) && testStr.count <= 120
    }
    static func isValidGender(_ gender: String) -> Bool {
        return (gender.lowercased() == "male" || gender.lowercased() == "female")
    }

    static func isValidGeolocation(_ testStr: String) -> Bool {
        if (testStr == "") {
            return true
        }
        let strs = testStr.split(separator: ",")
        if (strs.count != 2) {
            return false
        } else {
            let bools = strs.map({ (str) -> Bool in
                if Double(str) != nil {
                    return true
                } else {
                    return false
                }
            })
            return bools.allSatisfy({$0 == true})
        }
    }

    static let Geolocation = ["Singapore": ["lat": 1.293735, "long": 103.850286]]
}
