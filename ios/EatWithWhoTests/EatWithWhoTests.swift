//
//  EatWithWhoTests.swift
//  EatWithWhoTests
//
//  Created by Jane Appleseed on 02/03/20.
//  Copyright © 2016 Apple Inc. All rights reserved.
//

import XCTest
@testable import EatWithWho

class EatWithWhoTests: XCTestCase {
    
    //MARK: eatwith Class Tests
    
    // Confirm that the eatwith initializer returns a eatwith object when passed valid parameters.
    func testeatwithInitializationSucceeds() {
        
        // Zero rating
        let zeroRatingeatwith = eatwith.init(name: "Zero", photo: nil, rating: 0)
        XCTAssertNotNil(zeroRatingeatwith)

        // Positive rating
        let positiveRatingeatwith = eatwith.init(name: "Positive", photo: nil, rating: 5)
        XCTAssertNotNil(positiveRatingeatwith)

    }
    
    // Confirm that the eatwith initialier returns nil when passed a negative rating or an empty name.
    func testeatwithInitializationFails() {
        
        // Negative rating
        let negativeRatingeatwith = eatwith.init(name: "Negative", photo: nil, rating: -1)
        XCTAssertNil(negativeRatingeatwith)
        
        // Rating exceeds maximum
        let largeRatingeatwith = eatwith.init(name: "Large", photo: nil, rating: 6)
        XCTAssertNil(largeRatingeatwith)

        // Empty String
        let emptyStringeatwith = eatwith.init(name: "", photo: nil, rating: 0)
        XCTAssertNil(emptyStringeatwith)
        
    }
}
